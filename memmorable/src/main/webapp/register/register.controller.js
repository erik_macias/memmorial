﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$location', '$rootScope', 'AppSettingsService', 'FlashService'];
    function RegisterController(UserService, $location, $rootScope, AppSettingsService, FlashService) {
        var vm = this;

        vm.register = register;
        loadReCaptchaClientKey();

        function loadReCaptchaClientKey() {
    		AppSettingsService.getReCaptchaClientKey()
    	   		.then(function (response) {
	                if (response.data) {
	                	$rootScope.model = {
	                            key: response.data.app_setting_value
	                        };
	                }
    	   		});
        }

        function register() {
            vm.dataLoading = true;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response.data.success) {
                        FlashService.Success('Registration successful. Please check your email to activate your account.', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.data.message);
                        vm.dataLoading = false;
                    }
                });
        }
    }

})();
