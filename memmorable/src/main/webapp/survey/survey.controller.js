﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('SurveyListCtrl', SurveyListCtrl);

    SurveyListCtrl.$inject = ['$scope', '$rootScope', '$location', 'SurveyService', 'FlashService'];
    function SurveyListCtrl($scope, $rootScope, $location, SurveyService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = '-createdDate';
        $scope.reverseSort = false;
        
        loadSurveys();
    	
        $scope.viewSurvey = function (surveyId) {
            $location.path('/survey/' + surveyId);
        };
        
		$scope.refreshSurveyList = function() {
			loadSurveys();
		};

		function loadSurveys() {
			SurveyService.getSurveyByFromUser($rootScope.globals.currentUser.username).then(function(response) {
				if (response.data.success) {
					$rootScope.surveyList = response.data.entityList;
				} else {
					$rootScope.surveyList = null;
				}
			});
			
			SurveyService.getSurveyByToUser($rootScope.globals.currentUser.username).then(function(response) {
				if (response.data.success) {
					$rootScope.surveyCompletedList = response.data.entityList;
				} else {
					$rootScope.surveyCompletedList = null;
				}
			});
		}
    }
    
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('SurveyDetailCtrl', SurveyDetailCtrl);

    SurveyDetailCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'SurveyService', '$location', 'FlashService'];
    function SurveyDetailCtrl($scope, $rootScope, $routeParams, SurveyService, $location, FlashService) {
    	var vm = this;
        vm.comments = '';
        vm.status = '';
        vm.surveyRating = 0;
        $scope.txtPattern = /^[a-zA-Z _\.-]+$/;
        
    	loadSurvey();
    	
    	$scope.viewUserProfile = function (userId) {
            $location.path('/profile/' + userId);
        };
        
        $scope.viewArt = function (artId) {
            $location.path('/artDetails/' + artId);
        };
        
        $scope.cancel = function () {
            $location.path('/surveys');
        };
        
        $scope.submitSurvey = function () {
            var survey = {
            		surveyId: $routeParams.param,
            		comments: vm.comments,
            		status: vm.status,
    				rate: vm.surveyRating,
    				username: $rootScope.globals.currentUser.username
    		}
            
            SurveyService.sendSurvey(survey)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Your survey has been sent successfully.');
                	$location.path('/surveys');
                }
            });
        };
        
        function loadSurvey() {
        	var surveyId = $routeParams.param;
        	
        	SurveyService.getSurveyById(surveyId)
            .then(function (response) {
                if (response.data.success) {
                	$scope.survey = response.data.entity;
                }
            });
        }

    }
})();
