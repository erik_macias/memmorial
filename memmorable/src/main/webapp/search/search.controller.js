﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['SearchService', '$rootScope', '$routeParams' ,'$location', '$scope', 'FlashService'] ;
    function SearchController(SearchService, $rootScope, $routeParams, $location, $scope, FlashService) {
        var vm = this;
        vm.keywords = $routeParams.urlParam;
        $rootScope.keywords = '';
        $scope.searchCount = 0;
        $scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.query = { };
        $scope.locations = [];
        
        searchArt();
        
        function searchArt() {
        	
        	var searchOpts;
        	if ($rootScope.globals.currentUser) {
        		var searchOpts = {
            			text: $routeParams.urlParam,
            			maxresults: 0,
            			firstresult: 0,
            			currentUser: $rootScope.globals.currentUser.username
                    }
        	} else {
        		searchOpts = {
        				text: $routeParams.urlParam,
            			maxresults: 0,
            			firstresult: 0
                    }
        	}
        	
        	SearchService.FindFreeText(searchOpts)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.searchCount = response.data.searchResultCount;
                    	$scope.artList = response.data.artEntityList;
                    	
                    	for (var i=0; i<$scope.artList.length; i++) {
                    		$scope.locations.push($scope.artList[i].location[0]);
                    	}
                    	
                    	$scope.orderProp = '-lastUpdatedDate';
                    	if (response.data.surveysPending) {
                    		FlashService.Warning("Please complete your pending surveys and continue to buy, ask and post comments to the users and arts.", true);
                    		$scope.surveysPending = response.data.surveysPending;
                    	}
                    } else {
                    	$scope.searchCount = 0;
                    	$scope.artList = [];
                    }
                });
        }
        
        $scope.filterByStars = function(key){
        	if ($scope.query) {
        		$scope.query.rating = key;
        	}
        }
        
        $scope.filterByCountry = function(key){
        	if ($scope.query) {
        		$scope.query.location = {
        				country : key
        		};
        	}
        }
        
        $scope.filterByState = function(country, state){
        	if ($scope.query) {
        		$scope.query.location = {
        				country : country, 
        				state : state
        		};
        	}
        }
        
        $scope.filterByCity = function(country, state, city){
        	if ($scope.query) {
        		$scope.query.location = {
        				country : country, 
        				state : state,
        				city : city
        		};
        	}
        }
        
        $scope.isBuyEnabled = function(art) {
        	var isBuyBtnEnabled = false;
        	
        	if (!$scope.surveysPending && art.status == 'ACTIVE') {
        		isBuyBtnEnabled = true;
        	} else if (!$scope.surveysPending && art.status == 'SOLD' && art.artType == 'Literature') {
        		isBuyBtnEnabled = true;
        	} else if (art.status == 'IN_PROCESS') {
        		isBuyBtnEnabled = false;
        	} else {
        		isBuyBtnEnabled = false;
        	}
        	
        	return isBuyBtnEnabled;
        }

    }

})();