﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('VerifyController', VerifyController);

    VerifyController.$inject = ['UserService', '$routeParams' ,'$location', '$scope', 'FlashService'] ;
    
    function VerifyController(UserService, $routeParams, $location, $scope, FlashService) {
        var vm = this;
        
        Verify();
            
        function Verify() {
            vm.dataLoading = true;
            
            UserService.verifyUserEmail($routeParams.urlParam)
                .then(function (response) {
                    if (response.data.success) {
                        FlashService.Success('Email Verification Successfull', false);
                    } else {
                        FlashService.Error(response.data.message);
                        vm.dataLoading = false;
                    }
                });
        }
    }
})();