﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['UserService', 'ArtService', 'InboxService', 'SurveyService', 'PaymentsService', '$rootScope', '$routeParams' ,'$location', '$scope', 'FlashService'] ;
    function ProfileController(UserService, ArtService, InboxService, SurveyService, PaymentsService, $rootScope, $routeParams, $location, $scope, FlashService) {
        var vm = this;
        vm.user =  null;
        vm.image = null;
        vm.artState = null;
        vm.resume=null;
        vm.name=null;
        vm.editable=false;
        vm.isOwner=false;
        vm.isUserCertified=false;
        vm.message = '';
        vm.comments = '';
        $scope.currentPage = 1;
        $scope.pageSize = '10';
        $rootScope.Data =null;
        $scope.txtPattern = /^[a-zA-Z _\.-]+$/;
      
        initController();
        GetProfile();
        getUserComments()
        
        function  initController() {
        	vm.comments = '';
            vm.message = '';
            
            vm.user = $routeParams.param;
            if ($rootScope.globals.currentUser != null ){
            	vm.name = $rootScope.globals.currentUser.name + ' ' + $rootScope.globals.currentUser.lastname; 
	            if ($rootScope.globals.currentUser.username == vm.user){
	            	  vm.isOwner=true;
	            }
            }
            
            if ($rootScope.globals.currentUser) {
	            SurveyService.isSurveysPending($rootScope.globals.currentUser.username)
	            .then(function (response) {
	                if (response.data.success) {
	                	if (response.data.entity) {
	                		FlashService.Warning("Please complete your pending surveys and continue to buy, ask and post comments to the users and arts.", true);
	                		$scope.surveysPending = response.data.entity;
	                	}
	                }
	            });
	            
	            var userBought = {
        				buyerUser: $rootScope.globals.currentUser.username,
        				sellerUser: vm.user
                    }
        		PaymentsService.hasUserBoughtToSeller(userBought)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.hasBoughtUser = true;
                    }
                });
        	} else {
        		$scope.surveysPending = false;
        		$scope.hasBoughtUser = false;
        	}
            
            var currentUser = {
        			username: vm.user
                }
            ArtService.getArtByUser(currentUser)
            .then(function (response) {
                if (response.data.success) {
                	$scope.artList = response.data.artEntityList;
                }
            });
            ArtService.getPurchasedArtByUser(currentUser)
            .then(function (response) {
            	if (response.data.success) {
            		$scope.artPurchasedList = response.data.artEntityList;
            	}
            });
        }
        
        $scope.edit = function  edit() {
            var profileEdit = {
            			profile_resume : vm.resume,
            			profile_art_statement : vm.artState,
            			profile_picture : vm.image,
            			user : $rootScope.globals.currentUser
            			};
            UserService.editProfile(profileEdit)
            .then(function (response) {
                    if (response.data.success) {
                    	 vm.editable=false;
                    	 FlashService.Success('Update successful', false);
                    	}
                    });
        }
       
        $scope.editPicture = function  editPicture() {
    	   $location.path('/profilePicture');
        }
        
        function GetProfile() {
        	var profileReq; 
        	if ($rootScope.globals.currentUser) {
        		profileReq = {
            			user_name: vm.user,
            			currrentUser: $rootScope.globals.currentUser.username
                    }
        	} else {
        		profileReq = {
            			user_name: vm.user
                    }
        	}
        	
            UserService.getProfile(profileReq)
                .then(function (response) {
                    if (response.data.success) {
                    	vm.userFullName = response.data.personal_name +' '+ response.data.personal_l_name;
                        vm.image = response.data.profile_picture;
                        vm.artState = response.data.profile_art_statement;
                        vm.resume= response.data.profile_resume;
                        vm.location = response.data.location;
                        vm.phone = response.data.phone;
                        vm.mobile = response.data.mobile;
                        vm.membership = response.data.membership;
                        vm.isUserCertified = response.data.userCertified;
                        vm.visitsCounter = response.data.visitsCounter;
                        vm.email = response.data.email;
                    }else{
                    	$location.path('/');
                    }
                    	  
                });
        }
        
        function getUserComments() {
    		UserService.getUserComments($routeParams.param)
            .then(function (response) {
                if (response.data) {
                	$scope.comments = response.data;
                }
            });
    	}
        
        $scope.addUserComments = function() {
    		var userComment = {
    				toUsername: vm.user,
        			createdBy: $rootScope.globals.currentUser.username,
        	    	comment: vm.comments
                }
        	UserService.addUserComments(userComment)
            .then(function (response) {
                if (response.data) {
                	FlashService.Success('Your comments has been added successfully');
                	getUserComments();
                	clearForm();
                }
            });
        };
        
        $scope.deleteUserComment = function (userCommentId) {
        	UserService.deleteUserComments(userCommentId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Comment has been deleted successfully');
                	getUserComments();
                }
            });
        };
        
        $scope.sendPrivateInbox = function () {
        	var inboxMsg = {
        			from: $rootScope.globals.currentUser.username,
        			to: vm.user,
        			subject: $scope.subject,
        			message: vm.message
                }
        	
        	InboxService.sendNewMessageToUser(inboxMsg)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('New message has been sent to ' + vm.user);
                }
            });
        };
        
        function clearForm() {
        	vm.comments = '';
        }
    }
    
	angular.module('app')
		.controller('TabController', function() {
			this.tab = 1;

			this.setTab = function(tabId) {
				this.tab = tabId;
			};

			this.isSet = function(tabId) {
				return this.tab === tabId;
			};
		})
    
    angular
    .module('app')
    .directive("contenteditable", function() {
  	  return {
  	    require: "ngModel",
  	    link: function(scope, element, attrs, ngModel) {

  	      function read() {
  	        ngModel.$setViewValue(element.html());
  	      }

  	      ngModel.$render = function() {
  	        element.html(ngModel.$viewValue || "");
  	      };

  	      element.bind("blur keyup change", function() {
  	        scope.$apply(read);
  	      });
  	    }
  	  };
  })

})();