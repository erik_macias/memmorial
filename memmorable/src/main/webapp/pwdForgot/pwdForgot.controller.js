﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PwdForgotController', PwdForgotController);

    PwdForgotController.$inject = ['UserService', '$routeParams' ,'$location', '$scope', 'FlashService'];
    function PwdForgotController(UserService, $routeParams, $location, $scope, FlashService) {
        var vm = this;
        
        vm.pwdForgotGenerateLink = pwdForgotGenerateLink;
        
        function pwdForgotGenerateLink() {
        	vm.dataLoading = true;
            UserService.pwdResetForm(vm.user.email)
                .then(function (response) {
                    if (response.data.success) {
                        FlashService.Success('An email has been sent to reset your password. Please, follow the email instructions.', true);
                        $location.path('/login');
                    } else {
                        FlashService.Error(response.data.message);
                        vm.dataLoading = false;
                    }
                });
        }
        
    }
})();