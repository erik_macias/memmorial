﻿﻿
(function () {
    'use strict';
  
    angular
        .module('app', [
                        'ngRoute', 'ngCookies','ngAnimate','ngImgCrop','vcRecaptcha','ngFileUpload',
                        'angular.filter','searchFilters','angularUtils.directives.dirPagination','resultAnimations',
                        'angular-loading-bar','angular-input-stars', 'mm.acl', 'ngGentle', 'chart.js', 'ui.bootstrap.datetimepicker'])
        .config(config)
        .run(run);

    
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
    	$routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })
            .when('/about', {
                controller: 'AboutController',
                templateUrl: 'about/about.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                        return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/policies', {
                controller: 'PoliciesController',
                templateUrl: 'policies/policies.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                        return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/pricing', {
                controller: 'PricingController',
                templateUrl: 'pricing/pricing.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/pricing/details/:param', {
                controller: 'PricingDetController',
                templateUrl: 'pricing/pricing-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/contact', {
                controller: 'ContactController',
                templateUrl: 'contact/contact.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })
            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'register/register.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/reports', {
                controller: 'ReportsController',
                templateUrl: 'reports/reports.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/settings', {
                controller: 'SettingsController',
                templateUrl: 'settings/settings.view.html',
                controllerAs: 'vm',
                	resolve : {
                        'acl' : ['$q', 'AclService', function($q, AclService){
                        	return resolveAbility($q, AclService, 'member_content');
                        }]
                      }
            })
            .when('/profile/:param', {
                controller: 'ProfileController',
                templateUrl: 'profile/profile.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/profilePicture', {
                controller: 'PictureController',
                templateUrl: 'profilePicture/profilePic.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/media', {
                controller: 'MediaController',
                templateUrl: 'media/media.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/mediaEdit/:urlParam', {
                controller: 'MediaEditController',
                templateUrl: 'mediaEdit/mediaEdit.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPreview', {
                controller: 'MediaPreviewController',
                templateUrl: 'media-preview/mediapreview.view.html',
                controllerAs: 'vm'
            })
            .when('/artFeePayment/:urlParam', {
                controller: 'MediaPayController',
                templateUrl: 'media-pay/mediapay.view.html',
                controllerAs: 'vm',
                resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artFeePay/exec/:pi/:urlParam', {
                controller: 'MediaPayExecController',
                templateUrl: 'media-pay/mediapay-exec.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artFeePay/confirmation', {
                controller: 'MediaPayConfirmController',
                templateUrl: 'media-pay/mediapay-confirm.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artFeePay/cancel', {
                controller: 'MediaPayCancelController',
                templateUrl: 'media-pay/mediapay-cancel.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPayment/:urlParam', {
                controller: 'ArtPayController',
                templateUrl: 'art-pay/artpay.view.html',
                controllerAs: 'vm',
                resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/exec/paypal/:urlParam', {
                controller: 'ArtPayExecPaypalController',
                templateUrl: 'art-pay/artpay-exec-paypal.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/exec/bank/deposit/:urlParam', {
                controller: 'ArtPayExecBankDepositController',
                templateUrl: 'art-pay/artpay-exec-bank.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/exec/bank/transfer/:urlParam', {
                controller: 'ArtPayExecBankTransferController',
                templateUrl: 'art-pay/artpay-exec-bank.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/exec/own/:urlParam', {
                controller: 'ArtPayExecOwnController',
                templateUrl: 'art-pay/artpay-exec-own.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/confirmation', {
                controller: 'ArtPayConfirmController',
                templateUrl: 'art-pay/artpay-confirm.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artPay/cancel', {
                controller: 'ArtPayCancelController',
                templateUrl: 'art-pay/artpay-cancel.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/artDetails/:urlParam', {
                controller: 'ArtDetailsController',
                templateUrl: 'artDetails/artdetails.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/verify/:urlParam', {
                controller: 'VerifyController',
                templateUrl: 'verify/verify.view.html',
                controllerAs: 'vm'
            })
            .when('/pwdForgot', {
                controller: 'PwdForgotController',
                templateUrl: 'pwdForgot/pwdForgot.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/pwdReset/:urlParam', {
                controller: 'PwdResetController',
                templateUrl: 'pwdReset/pwdReset.view.html',
                controllerAs: 'vm'
            })
            .when('/search/:urlParam', {
                controller: 'SearchController',
                templateUrl: 'search/search.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .when('/inbox', {
                controller: 'InboxListCtrl',
                templateUrl: 'inbox/inbox-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/inbox/message/:param', {
                controller: 'InboxDetailCtrl',
                templateUrl: 'inbox/inbox-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/surveys', {
                controller: 'SurveyListCtrl',
                templateUrl: 'survey/survey-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/survey/:param', {
                controller: 'SurveyDetailCtrl',
                templateUrl: 'survey/survey-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/payments/:param', {
                controller: 'PaymentController',
                templateUrl: 'payment/payment.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/payment/confirmation', {
                controller: 'PaymentConfController',
                templateUrl: 'payment/payment-confirm.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/payment/cancel', {
                controller: 'PaymentCancelController',
                templateUrl: 'payment/payment-cancel.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/admin/users', {
                controller: 'UserListCtrl',
                templateUrl: 'admin/user/user-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/user/:param', {
                controller: 'UserDetailCtrl',
                templateUrl: 'admin/user/user-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/users/create', {
                controller: 'UserCreationCtrl',
                templateUrl: 'admin/user/user-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arts', {
                controller: 'ArtListCtrl',
                templateUrl: 'admin/catalogs/arts/art-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttypes', {
                controller: 'ArtTypeCatListCtrl',
                templateUrl: 'admin/catalogs/art-type/art-type-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttype/:param', {
                controller: 'ArtTypeCatDetailCtrl',
                templateUrl: 'admin/catalogs/art-type/art-type-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttypes/create', {
                controller: 'ArtTypeCatCreationCtrl',
                templateUrl: 'admin/catalogs/art-type/art-type-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/artsubtypes', {
                controller: 'ArtSubTypeCatListCtrl',
                templateUrl: 'admin/catalogs/art-subtype/art-subtype-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/artsubtype/:param', {
                controller: 'ArtSubTypeCatDetailCtrl',
                templateUrl: 'admin/catalogs/art-subtype/art-subtype-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/artsubtypes/create', {
                controller: 'ArtSubTypeCatCreationCtrl',
                templateUrl: 'admin/catalogs/art-subtype/art-subtype-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttypefees', {
                controller: 'ArtTypeFeeCatListCtrl',
                templateUrl: 'admin/catalogs/art-type-fee/art-type-fee-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttypefee/:param', {
                controller: 'ArtTypeFeeCatDetailCtrl',
                templateUrl: 'admin/catalogs/art-type-fee/art-type-fee-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/arttypefees/create', {
                controller: 'ArtTypeFeeCatCreationCtrl',
                templateUrl: 'admin/catalogs/art-type-fee/art-type-fee-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/addresstypes', {
                controller: 'AddressTypeCatListCtrl',
                templateUrl: 'admin/catalogs/address-type/address-type-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/addresstype/:param', {
                controller: 'AddressTypeCatDetailCtrl',
                templateUrl: 'admin/catalogs/address-type/address-type-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/addresstypes/create', {
                controller: 'AddressTypeCatCreationCtrl',
                templateUrl: 'admin/catalogs/address-type/address-type-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/countries', {
                controller: 'CountryCatListCtrl',
                templateUrl: 'admin/catalogs/countries/countries-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/country/:param', {
                controller: 'CountryCatDetailCtrl',
                templateUrl: 'admin/catalogs/countries/countries-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/countries/create', {
                controller: 'CountryCatCreationCtrl',
                templateUrl: 'admin/catalogs/countries/countries-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/currencies', {
                controller: 'CurrencyCatListCtrl',
                templateUrl: 'admin/catalogs/currencies/currencies-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/currency/:param', {
                controller: 'CurrencyCatDetailCtrl',
                templateUrl: 'admin/catalogs/currencies/currencies-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/currencies/create', {
                controller: 'CurrencyCatCreationCtrl',
                templateUrl: 'admin/catalogs/currencies/currencies-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/roles', {
                controller: 'RolesCatListCtrl',
                templateUrl: 'admin/catalogs/roles/roles-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/catalogs/role/:param', {
                controller: 'RoleCatDetailCtrl',
                templateUrl: 'admin/catalogs/roles/roles-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/settings', {
                controller: 'SettingsListCtrl',
                templateUrl: 'admin/settings/settings-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/settings/edit/:param', {
                controller: 'SettingsDetailCtrl',
                templateUrl: 'admin/settings/settings-detail.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/settings/create', {
                controller: 'SettingsCreationCtrl',
                templateUrl: 'admin/settings/settings-creation.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/payments', {
                controller: 'PaymentsListCtrl',
                templateUrl: 'admin/payments/payments-list.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'manage_content');
                    }]
                  }
            })
            .when('/admin/reports', {
                controller: 'ReportsAdminController',
                templateUrl: 'admin/reports/admin.reports.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'member_content');
                    }]
                  }
            })
            .when('/unauthorized', {
                controller: 'UnauthorizedCtrl',
                templateUrl: 'unauthorized/unauthorized.view.html',
                controllerAs: 'vm',
            	resolve : {
                    'acl' : ['$q', 'AclService', function($q, AclService){
                    	return resolveAbility($q, AclService, 'free_content');
                    }]
                  }
            })
            .otherwise({ redirectTo: '/' });
    	
    	function resolveAbility($q, AclService, ability) {
    		if(AclService.can(ability)){
    			return true;
    		} else {
    			return $q.reject('Unauthorized');
    		}
    	}
    	
    	//cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
    }

    function isloggedin($rootScope, $location, $cookieStore, $http) {
    	return x;
    }
    
    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', 'AuthenticationService', 'InboxService', 'AclService'];
    function run($rootScope, $location, $cookieStore, $http, AuthenticationService, InboxService, AclService) {
    	initRolesAndAbilities();
    	
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
        
        $http.defaults.headers.common['Cache-Control'] = 'no-cache';
        $http.defaults.cache = false;

        if (!$http.defaults.headers.get) {
        	$http.defaults.headers.get = {};
        }
        $http.defaults.headers.get['If-Modified-Since'] = '0';
        
        //Load Unread Inbox messages
        loadInbox();
        
        // If the route change failed due to our "Unauthorized" error, redirect them
        $rootScope.$on('$routeChangeError', function(event, current, previous, rejection) {
          if(rejection === 'Unauthorized') {
            $location.path('/unauthorized');
          }
        })
        
       $rootScope.isNotLoggedIn = function() {
        	var loggedIn = $rootScope.globals.currentUser;
            if (!loggedIn) {
            	return true;
            }else{
            	$rootScope.user= $rootScope.globals.currentUser.username;
            	$rootScope.membername= $rootScope.globals.currentUser.name + ' ' 
            		+ $rootScope.globals.currentUser.lastname;
            	return false;
            }
        };
        
        $rootScope.isUserOwner = function(username) {
        	var loggedIn = $rootScope.globals.currentUser;
            if (loggedIn && $rootScope.globals.currentUser.username == username) {
            	return true;
            } else {
            	return false;
            }
        };
        
        $rootScope.hasAdminPrivileges = function() {
        	if(AclService.can('manage_content')){
    			return true;
    		} else {
    			return false;
    		}
        };
        
        $rootScope.searchArt = function() {
        	$location.path('/search/'+$rootScope.keywords);
        };
        
        function loadInbox() {
        	if ($rootScope.globals.currentUser) {
        		var user = {
            			username: $rootScope.globals.currentUser.username
                    }
                InboxService.getAllUnreadByStatusAndUser(user)
                .then(function (response) {
                    if (response.data.success) {
                    	$rootScope.inboxList = response.data.msgsInbox;
                    }
                });
        	}
        }
        
        function initRolesAndAbilities() {
        	if (!AclService.resume()) {
	        	var aclData = {
	        		    guest: ['free_content'],
	        		    member: ['free_content', 'member_content'],
	        		    admin: ['free_content', 'member_content', 'manage_content']
	        		  }
	        	AclService.setAbilities(aclData);
	        	AclService.attachRole('guest');
        	}
        }
        
        $rootScope.logout = function() {
        	// reset login status
            AuthenticationService.ClearCredentials();
            initRolesAndAbilities()
            $location.path('/');
        };
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
    
})();