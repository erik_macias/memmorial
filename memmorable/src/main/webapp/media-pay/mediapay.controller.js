﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('MediaPayController', MediaPayController);

    MediaPayController.$inject = ['UserService',  'SearchService', 'PaymentsService', '$rootScope', '$routeParams', '$scope', 'FlashService', '$location'];
    function MediaPayController(UserService,  SearchService, PaymentsService, $rootScope, $routeParams, $scope, FlashService, $location) {
    	 var vm = this;
    	 vm.user = $rootScope.globals.currentUser.username;
    	 $scope.artFeeCurrency = 0;
    	 $scope.showPanel = false;
         
    	initController();
    	
    	function initController() {
    		var art = {
           		 art_Id: $routeParams.urlParam
                }
            PaymentsService.validateArtFeePayment(art)
            .then(function (response) {
            	 if (!response.data.success) {
            		 $scope.creditAvailable = response.data.creditAvailable;
            		 
            		//load art details
            		 var searchOpt = {
            				 artId: $routeParams.urlParam, 
            				 currentUser: $rootScope.globals.currentUser.username
                         }
                     SearchService.FindByArtId(searchOpt)
                     .then(function (response) {
                         if (response.data.success) {
                         	$scope.art = response.data.artDetailsDTO;
                         	
                         	var artTypeUsr = {
                              		 artType: $scope.art.artType,
                              		 username: $rootScope.globals.currentUser.username
                                   }
                         	
                         	PaymentsService.getArtTypeFeeDetUsr(artTypeUsr).then(function(response) {
                         		if (response.data.success) {
                         			$scope.artFee = response.data.entity.fee_amount;
                         			$scope.artFeeCurrency = ($scope.art.price * $scope.artFee)/100;
                         			$scope.artFeeCurrencyEnd = $scope.artFeeCurrency;
                         			
                         			// 20%off
                    				if (!$scope.art.feePaymentPending) {
                    					$scope.artFeeCurrency = $scope.artFeeCurrency * 0.8;
                    				}
                         			
                         			if ($scope.artFeeCurrency > 250) {
                         				$scope.artFeeCurrency = 250;
                         			}
                         			
                         			if ($scope.artFeeCurrencyEnd > 250) {
                         				$scope.artFeeCurrencyEnd = 250;
                         			}
                         			
                         			if ($scope.creditAvailable > 0) {
                         				$scope.artFeeCurrencyFinal = $scope.artFeeCurrency - $scope.creditAvailable;
                    					if ($scope.artFeeCurrencyFinal <= 0.00) {
                    						$scope.artFeeCurrencyFinalPP = 0.01;
                    						$scope.artFeeCurrencyFinal = 0.00;
                    					} else {
                    						$scope.artFeeCurrencyFinalPP = $scope.artFeeCurrencyFinal;
                    					}
                    					
                    					$scope.artFeeCurrencyEndFinal = $scope.artFeeCurrencyEnd - $scope.creditAvailable;
                    					if ($scope.artFeeCurrencyEndFinal <= 0.00) {
                    						$scope.artFeeCurrencyEndFinal = 0.00;
                    					}
                    				}
                                 	$scope.showPanel = true;
                         		}
                    		});
                         } else {
                        	 FlashService.Error('The Art ID: ' + $routeParams.urlParam + ' is not active. Please contact our admin team.');
                         }
                     });
            	 } else{
            		 FlashService.Error(response.data.message);
         		}
            });
    	 };
    	 
    	 $scope.submitPayPalPayment = function() {
    		 $location.path('/artFeePay/exec/paypal/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitDepositPayment = function() {
    		 $location.path('/artFeePay/exec/deposit/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitTransferPayment = function() {
    		 $location.path('/artFeePay/exec/transfer/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitPayLaterPayment = function() {
    		 $location.path('/artFeePay/exec/later/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitPaymentWithCredit = function() {
    		 $location.path('/artFeePay/exec/credit/'+$routeParams.urlParam);
    	 }
    	 
     }   
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('MediaPayExecController', MediaPayExecController);

    MediaPayExecController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function MediaPayExecController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 	$scope.isBankDetailsEnabled = false;
   	 	$scope.isLaterEnabled = false;;
   	 
    	initialize();
    	
    	function initialize() {
    		var pi = $routeParams.pi;
    		if (pi === 'paypal') {
    			execArtPaypalPayment();
    		} else if (pi === 'deposit') {
    			execArtDepositPayment()
    		} else if (pi === 'transfer') {
    			execArtTransferPayment()
    		} else if (pi === 'later') {
    			execArtLaterPayment()
    		} else if (pi === 'credit') {
    			execArtCreditPayment()
    		} else {
    			FlashService.Error('The Art ID: ' + $routeParams.urlParam + ' could not activated. Please contact our admin team.');
    		}
    	}
    	
    	$scope.cancel = function () {
    		$location.path('/artDetails/'+$routeParams.urlParam);
        };
        
        $scope.activateArtWithNoFee = function () {
        	var art = {
    				art_Id: $routeParams.urlParam,
    				username: vm.user
            	}
            	PaymentsService.activateArtWithNoFee(art)
                .then(function (response) {
                	if (response.data.success) {
                		FlashService.Success('Your Art has been activated. Please proceed with your fee payment once your art is sold.', true);
                		$location.path('/profile/'+vm.user);
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
        }
    	
    	function execArtPaypalPayment() {
    		var art = {
    				art_Id: $routeParams.urlParam,
    				username: vm.user
            	}
            	PaymentsService.execArtFeePayment(art)
                .then(function (response) {
                	if (response.data.success) {
                    	$scope.approval_url = response.data.entity;
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
    	};
    	
    	function execArtCreditPayment() {
    		var art = {
    				art_Id: $routeParams.urlParam,
    				username: vm.user
            	}
            	PaymentsService.execArtFeeCreditPayment(art)
                .then(function (response) {
                	if (response.data.success) {
                		FlashService.Success('Credit successfully applied.', true);
                		$location.path('/profile/'+vm.user);
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
    	};
    	
    	function execArtDepositPayment() {
    		$scope.isBankDetailsEnabled = true;
    	}
    	
    	function execArtTransferPayment() {
    		$scope.isBankDetailsEnabled = true;
    	}
    	
    	function execArtLaterPayment() {
    		$scope.isLaterEnabled = true;
    	}
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('MediaPayConfirmController', MediaPayConfirmController);

    MediaPayConfirmController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function MediaPayConfirmController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execPaypalAgrmnt();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/profile/'+vm.user);
        };
    	
    	function execPaypalAgrmnt() {
    		$scope.queryStrs = $location.search();
    		$scope.isConfirmed = true;
    		$scope.showConfirmed = false;
    		
    		var payment = {
    				paymentId: $scope.queryStrs.paymentId,
    				PayerID: $scope.queryStrs.PayerID,
    				username: vm.user
            	}
            	PaymentsService.confirmArtFeePayment(payment)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.paymentId = response.data.entity;
                    	$scope.isConfirmed = response.data.success;
                    	$scope.showConfirmed = true;
                    } else {
                    	$scope.isConfirmed = false;
                    	$scope.showConfirmed = false;
                    	FlashService.Error(response.data.message);
                    }
                });
    	};
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('MediaPayCancelController', MediaPayCancelController);

    MediaPayCancelController.$inject = ['$scope', '$rootScope', '$routeParams', '$location'];
    function MediaPayCancelController($scope, $rootScope, $routeParams, $location) {
    	
    }

})();
