﻿(function() {
	'use strict';

	angular.module('app').controller('MediaController', MediaController);

	MediaController.$inject = ['$scope', '$rootScope', '$location', 'Upload', 'ArtService', 'FlashService', 'PreviewService' ];
	function MediaController($scope, $rootScope, $location, Upload, ArtService, FlashService, PreviewService) {
		var vm = this;
		vm.user = $rootScope.globals.currentUser.username;

		vm.types = '';
		vm.currency = '';
		vm.type = '';
		vm.primary = '';
		vm.subtypes = '';
		vm.services = '';
		vm.genres = ''; 
		vm.formats = '';
		vm.medialist = [];
		vm.videolist = [];
		vm.audiolist = [];
		vm.pdflist = [];
		vm.pdflistDownload = [];
		vm.something = '';
		vm.art = '';
		vm.artEntityId;
		vm.primaryFile = '';
		vm.pdfGetter = '';
		vm.pdfRule=0;
		vm.audioRule=0;
		vm.imageRule=0;
		vm.videoRule=0;
		vm.lit="Literature";
		vm.allowedToPublish = true;

		ArtService.isAbleToPublish(vm.user).then(function(response) {
			vm.allowedToPublish = response.data.success;
			if (!vm.allowedToPublish) {
				if (response.data.message === 'PROFILE_PENDING') {
					FlashService.Warning('Please complete your Default Address Details in order to publish new arts.', true);
					$location.path('/settings');
				} else {
					FlashService.Error(response.data.message);
				}
			}
		});
		
		checkData();
		
		$scope.setFile = function(file) {
			$scope.f = file;
	    };
	    
		ArtService.getTypes().then(function(response) {
			vm.types = response.data;
			
			if(vm.art.art_subType) {
				$rootScope.fillSub(vm.art.art_type);
			}
		});

		ArtService.getCurrency().then(function(response) {
			vm.currency = response.data;
		});
		
		function checkData() {
			if ($rootScope.Data != null) {
				vm.medialist = $rootScope.Data.medialist;
				vm.pdflist = $rootScope.Data.pdflist;
				vm.pdflistDownload = $rootScope.Data.pdflistDownload;
				vm.audiolist = $rootScope.Data.audiolist;
				vm.videolist = $rootScope.Data.videolist;
				vm.art = $rootScope.Data.art;
				vm.primaryFile = $rootScope.Data.primary;
			} else {
				$rootScope.Data = '';
			}
		};

		$rootScope.setPrimary = function(file) {
			vm.primaryFile = file;
		};

		$rootScope.submit = function() {
			if (form.file.$valid && $rootScope.file) {
				$rootScope.upload($rootScope.file);
			}
		};

		vm.pdfGetter = function(file) {
			return file;
		};

		// upload on file select or drop
		$rootScope.upload = function(file, type, primary) {
			vm.type = type;
			vm.primary = primary;
			Upload.upload({
				url : 'rest/art/uploadfile',
				data : {
					file : file,
					'username' : $rootScope.username
				}
			}).then(
					function(resp) {
						ArtService.saveMedia({
							art_id : vm.artEntityId,
							media_url : resp.data.filePath,
							media_type : resp.data.fileType,
							media_primary : vm.primary
						}).then(
								function(response) {
									if (response.data.success) {
										FlashService.Success(
												'Updload successful', false);
										$location.path('/');
									}
								});

					},
					function(resp) {
						
					},
					function(evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded
								/ evt.total);
					});
		};
		
		$rootScope.fillSub = function(artType) {
			ArtService.getSubTypes({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.subtypes = response.data;
			});
			
			ArtService.getServices({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.services = response.data;
			});
			ArtService.getGenres({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.genres = response.data;
			});
			ArtService.getFormats({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.formats = response.data;
			});
			
			$rootScope.getRules(artType);
		};
		
		$rootScope.getRules = function(artType) {
			ArtService.getMediaRules({
				artType : artType.art_type_name,
				user :  $rootScope.globals.currentUser
			}).then(function(response) {
				vm.pdfRule=response.data.media_pdf_qty;
				vm.audioRule=response.data.media_audio_qty;
				vm.imageRule=response.data.media_image_qty;
				vm.videoRule=response.data.media_video_qty;
				
			});
		};

		$rootScope.addPicture = function(file) {
			vm.medialist = vm.medialist.concat(file);
			if (vm.primaryFile === '') {
				$rootScope.setPrimary(file);
			}
			vm.picfile = '';
			$scope.f = '';
		};
		$rootScope.addVideo = function(file) {
			vm.videolist = vm.videolist.concat(file);
			if (vm.primaryFile === '') {
				$rootScope.setPrimary(file);
			}
			vm.vidfile = '';
			$scope.f = '';
		};
		$rootScope.addAudio = function(file) {
			vm.audiolist = vm.audiolist.concat(file);
			if (vm.primaryFile === '') {
				$rootScope.setPrimary(file);
			}
			vm.audiofile = '';
			$scope.f = '';
		};
		$rootScope.addPdf = function(file) {
			vm.pdflist = vm.pdflist.concat(file);
			if (vm.primaryFile === '') {
				$rootScope.setPrimary(file);
			}
			vm.pdffile = '';
			$scope.f = '';
		};
		$rootScope.addPdfDownload = function(file) {
			vm.pdflistDownload = vm.pdflistDownload.concat(file);
			vm.pdffileDownload = '';
			$scope.f = '';
		};

		$rootScope.removePicture = function(file) {
			for (var i = 0; i < vm.medialist.length; i++) {
				if (vm.medialist[i] === file) {
					vm.medialist.splice(i, 1);
					if (vm.primaryFile === file) {
						if (vm.medialist.length > 0) {
							vm.primaryFile = vm.medialist[0];
						} else {
							vm.primaryFile = '';
							vm.medialist = [];
						}
					}
				}
			}
		};
		
		$rootScope.removeAudio = function(file) {
			for (var i = 0; i < vm.audiolist.length; i++) {
				if (vm.audiolist[i] === file) {
					vm.audiolist.splice(i, 1);
					
						if (vm.audiolist == null || vm.audiolist.length == 0) {
							vm.audiolist = [];
						}
					}
			}			
		};
		
		$rootScope.removeVideo = function(file) {
			for (var i = 0; i < vm.videolist.length; i++) {
				if (vm.videolist[i] === file) {
					vm.videolist.splice(i, 1);
					
						if (vm.videolist == null || vm.videolist.length == 0) {
							vm.videolist = [];
						}
					}
			}			
		};
		
		$rootScope.removePdf = function(file) {
			for (var i = 0; i < vm.pdflist.length; i++) {
				if (vm.pdflist[i] === file) {
					vm.pdflist.splice(i, 1);
					if (vm.pdflist == null || vm.pdflist.length == 0) {
						vm.pdflist = [];
					}
				}
			}			
		};
		
		$rootScope.removePdfDownload = function(file) {
			for (var i = 0; i < vm.pdflistDownload.length; i++) {
				if (vm.pdflistDownload[i] === file) {
					vm.pdflistDownload.splice(i, 1);
					if (vm.pdflistDownload == null || vm.pdflistDownload.length == 0) {
						vm.pdflistDownload = [];
					}
				}
			}			
		};
		
		$rootScope.saveArt = function() {
			vm.dataLoading = true;
			vm.art.username = vm.user;
			ArtService.saveArt(vm.art).then(function(response) {
				if (response.data.success) {
					//FlashService.Success('Registration successful', false);
					vm.artEntityId = response.data.Entityid;
					for (var i = 0; i < vm.medialist.length; i++) {
						$rootScope.upload(vm.medialist[i], "image", false);
					}
					for (var i = 0; i < vm.videolist.length; i++) {
						$rootScope.upload(vm.videolist[i], "video", false);
					}
					for (var i = 0; i < vm.audiolist.length; i++) {
						$rootScope.upload(vm.audiolist[i], "audio", false);
					}
					for (var i = 0; i < vm.pdflist.length; i++) {
						$rootScope.upload(vm.pdflist[i], "pdf", false);
					}
					for (var i = 0; i < vm.pdflistDownload.length; i++) {
						$rootScope.uploadDownload(vm.pdflistDownload[i], "pdf", false);
					}
					vm.dataLoading = false;
				} else {
					FlashService.Error(response.data.message);
					vm.dataLoading = false;
				}
			});
		};
		
		$rootScope.validateFormToPreview = function() {
			if ((vm.pdflist && vm.pdflist.length > 0) || 
					(vm.audiolist && vm.audiolist.length > 0) || 
					(vm.videolist && vm.videolist.length > 0) || 
					(vm.medialist && vm.medialist.length > 0)) {
				return true;
			} else {
				return false;
			}
		}
		
		$rootScope.showPreview = function() {
			$rootScope.Data = {
				pdflist : vm.pdflist,
				pdflistDownload : vm.pdflistDownload,
				audiolist : vm.audiolist,
				videolist : vm.videolist,
				medialist : vm.medialist,
				art : vm.art,
				primary : vm.primaryFile

			};
			PreviewService.setArtData($rootScope.Data);
			$location.path('/artPreview');
		};
	}

})();