﻿(function() {
	'use strict';

	angular.module('app').controller('MediaEditController', MediaEditController);

	MediaEditController.$inject = ['$scope', '$rootScope', '$location', 'Upload','ArtService', 'FlashService', 'PreviewService','$routeParams' ,'SearchService'];
	function MediaEditController($scope, $rootScope, $location, Upload, ArtService,FlashService, PreviewService,$routeParams,SearchService) {
		var vm = this;
		vm.user = $rootScope.globals.currentUser.username;
		vm.sArt ='';
		vm.artId='';
		vm.types = '';
		vm.currency = '';
		vm.type = '';
		vm.primary = '';
		vm.subtypes = '';
		vm.services = '';
		vm.genres = ''; 
		vm.formats = '';
		vm.sMediaList = [];
		vm.medialist = [];
		vm.videolist = [];
		vm.audiolist = [];
		vm.pdflist = [];
		vm.something = '';
		vm.art = {};
		vm.artEntityId;
		vm.primaryFile = '';
		vm.pdfGetter = '';
		vm.pdfRule=0;
		vm.audioRule=0;
		vm.imageRule=0;
		vm.videoRule=0;
		vm.imagePosted=0;
		vm.audioPosted=0;
		vm.videoPosted=0;
		vm.pdfPosted=0;
		vm.lit="Literature";
		vm.responsedata;
		
		
		$scope.setFile = function(file) {
			$scope.f = file;
	    };

		ArtService.getTypes().then(function(response) {
			vm.types = response.data;
		});
		
		
		
		ArtService.getCurrency().then(function(response) {
			vm.currency = response.data;
		});

		checkData();
		
		
		function checkData() {
			 //load art details
	        var artOpt = {
	    			artId: $routeParams.urlParam, 
	    			currentUser: $rootScope.globals.currentUser.username
	            }
	        SearchService.FindEditByArtId(artOpt)
	        .then(function (response) {
	            if (response.data.success) {
	            	vm.sArt = response.data.artDetailsDTO;
	            	vm.sMediaList = vm.sArt.media;
	            	vm.artId = vm.sArt.artId;
	            	vm.art.art_description= vm.sArt.artDescription;
	        		vm.art.art_name=vm.sArt.artName;
	        		$rootScope.getRules(vm.sArt.artType);
	        		
	        		angular.forEach(vm.sMediaList, function(media, key) {
	            		if(media.artMediaType === 'image' ){
	            			vm.imagePosted++;
	            			if(media.primary === true ){
	            				vm.primaryFile = media;
	            			}
	            		}
	            		if(media.artMediaType === 'video' ){
	            			vm.videoPosted++;
	            		}
	            		if(media.artMediaType === 'audio' ){
	            			vm.audioPosted++;
	            		}
	            		if(media.artMediaType === 'pdf' ){
	            			vm.pdfPosted++;
	            		}
	            	});
	        		
	            } else {
	            	$location.path('/');
	            }
	        });
		};

	
		$rootScope.setPrimary = function(file) {
			vm.primaryFile = file;

		};
		
		$rootScope.setMediaPry = function(file) {
			vm.primaryFile = file;
			ArtService.removePrimaryAll(vm.artId).then(function(response) {
				if (response.data.success) {
			ArtService.setPrimaryMedia(file.artMediaId);
			}
			});
		};
		
		$rootScope.setImagePry = function(file) {
			vm.primaryFile = file;
			ArtService.removePrimaryAll(vm.artId);
		};

		$rootScope.submit = function() {
			if (form.file.$valid && $rootScope.file) {
				$rootScope.upload($rootScope.file);
			}
		};

		vm.pdfGetter = function(file) {
			return file;
		};

		// upload on file select or drop
		$rootScope.upload = function(file, type, primary) {
			vm.type = type;
			vm.primary = primary;
			Upload.upload({
				url : 'rest/art/uploadfile',
				data : {
					file : file,
					'username' : $rootScope.username
				}
			}).then(
					function(resp) {
						ArtService.saveMedia({
							art_id : vm.artEntityId,
							media_url : resp.data.filePath,
							media_type : resp.data.fileType,
							media_primary : vm.primary
						}).then(
								function(response) {
									if (response.data.success) {
										FlashService.Success(
												'Updload successful', false);
										$location.path('/');
									}
								});

					},
					function(resp) {
						
					},
					function(evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded
								/ evt.total);
					});
		};

		$rootScope.fillSub = function(artType) {
			ArtService.getSubTypes({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.subtypes = response.data;
			});
			
			ArtService.getServices({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.services = response.data;
			});
			ArtService.getGenres({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.genres = response.data;
			});
			ArtService.getFormats({
				Type : artType.art_type_name,
				id : artType.art_type_id
			}).then(function(response) {
				vm.formats = response.data;
			});
			ArtService.FindByArtId(searchOpt)
	        .then(function (response) {})
			
	        $rootScope.getRules(artType);
		};
		
		$rootScope.getRules = function(artType) {
			ArtService.getMediaRules({
				artType : artType,
				user :  $rootScope.globals.currentUser
			}).then(function(response) {
				vm.pdfRule=response.data.media_pdf_qty;
				vm.audioRule=response.data.media_audio_qty;
				vm.imageRule=response.data.media_image_qty;
				vm.videoRule=response.data.media_video_qty;
				
			});
		};

		$rootScope.addPicture = function(file) {
			vm.medialist = vm.medialist.concat(file);
			if (vm.primaryFile === '') {
				$rootScope.setPrimary(file);
			}
			vm.picfile = '';
			$scope.f = '';
		};
		$rootScope.addVideo = function(file) {
			vm.videolist = vm.videolist.concat(file);
			vm.vidfile = '';
			$scope.f = '';
		};
		$rootScope.addAudio = function(file) {
			vm.audiolist = vm.audiolist.concat(file);
			vm.audiofile = '';
			$scope.f = '';
		};
		$rootScope.addPdf = function(file) {
			vm.pdflist = vm.pdflist.concat(file);
			vm.pdffile = '';
			$scope.f = '';
		};
		
		$rootScope.validateFormToSubmit = function() {
			if ($rootScope.validateImageList() ||
					$rootScope.validatePdfList() || 
					$rootScope.validateAudioList() || 
					$rootScope.validateVideoList()) {
				return true;
			} else {
				return false;
			}
		}
		
		$rootScope.validateImageList = function() {
			if ((vm.sMediaList && vm.sMediaList.length > 0) ||
					(vm.medialist && vm.medialist.length > 0)) {
				return true;
			} else {
				return false;
			}
		}
		
		$rootScope.validatePdfList = function() {
			if ((vm.sMediaList && vm.sMediaList.length > 0) ||
					(vm.pdflist && vm.pdflist.length > 0)) {
				return true;
			} else {
				return false;
			}
		}
		
		$rootScope.validateAudioList = function() {
			if ((vm.sMediaList && vm.sMediaList.length > 0) ||
					(vm.audiolist && vm.audiolist.length > 0)) {
				return true;
			} else {
				return false;
			}
		}
		
		$rootScope.validateVideoList = function() {
			if ((vm.sMediaList && vm.sMediaList.length > 0) ||
					(vm.videolist && vm.videolist.length > 0)) {
				return true;
			} else {
				return false;
			}
		}

		$rootScope.removePicture = function(file) {
			for (var i = 0; i < vm.medialist.length; i++) {
				if (vm.medialist[i] === file) {
					vm.medialist.splice(i, 1);
					if (vm.primaryFile === file) {
						if (vm.medialist.length > 0) {
							vm.primaryFile = vm.medialist[0];
						} else {
							vm.primaryFile = '';
							vm.medialist = [];
						}
					}
				}
			}
		};
		
		
		
		$rootScope.removeMedia = function(file) {
			for (var i = 0; i < vm.sMediaList.length; i++) {
				if (vm.sMediaList[i] === file) {
					vm.sMediaList.splice(i, 1);
					if (vm.primaryFile === file) {
						if (vm.sMediaList.length > 0) {
							vm.primaryFile = vm.sMediaList[0];
						} else {
							vm.primaryFile = '';
							vm.sMediaList = [];
						}
					}
				}
			}
			if(file.artMediaType =='image'){
				vm.imagePosted--;
			}
			ArtService.deleteMedia(file.artMediaId).then(function(response) {
				vm.responsedata = response.data;
			});
		};
		
		$rootScope.removeAudio = function(file) {
			for (var i = 0; i < vm.audiolist.length; i++) {
				if (vm.audiolist[i] === file) {
					vm.audiolist.splice(i, 1);
					
						if (vm.audiolist == null || vm.audiolist.length == 0) {
							vm.audiolist = [];
						}
					}
				
			}			
		};
		
		$rootScope.removeVideo = function(file) {
			for (var i = 0; i < vm.videolist.length; i++) {
				if (vm.videolist[i] === file) {
					vm.videolist.splice(i, 1);
					
						if (vm.videolist == null || vm.videolist.length == 0) {
							vm.videolist = [];
						}
					}
				}
			
		};
		
		$rootScope.removePdf = function(file) {
			for (var i = 0; i < vm.pdflist.length; i++) {
				if (vm.pdflist[i] === file) {
					vm.pdflist.splice(i, 1);
					
						if (vm.pdflist == null || vm.pdflist.length == 0) {
							vm.pdflist = [];
						}
					}
			}			
		};
		
		$rootScope.saveArt = function() {
			vm.dataLoading = true;
			vm.art.username = vm.user;
			vm.art.art_id = $routeParams.urlParam;
			ArtService.updateArt(vm.art).then(function(response) {
				if (response.data.success) {
					FlashService.Success('Art Updated successfully', true);
					vm.artEntityId = response.data.Entityid;
					for (var i = 0; i < vm.medialist.length; i++) {
						$rootScope.upload(vm.medialist[i], "image", vm.medialist[i] === vm.primaryFile);
					}
					for (var i = 0; i < vm.videolist.length; i++) {
						$rootScope.upload(vm.videolist[i], "video", false);
					}
					for (var i = 0; i < vm.audiolist.length; i++) {
						$rootScope.upload(vm.audiolist[i], "audio", false);
					}
					for (var i = 0; i < vm.pdflist.length; i++) {
						$rootScope.upload(vm.pdflist[i], "pdf", false);
					}
					vm.dataLoading = false;
					
					$location.path('/profile/'+vm.user);
					
				} else {
					FlashService.Error(response.data.message);
					vm.dataLoading = false;
				}
			});
		};
	
	}

})();