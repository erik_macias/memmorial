﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ReportsController', ReportsController);

    ReportsController.$inject = ['ReportsService', '$location', '$scope', '$rootScope', 'FlashService'];
    function ReportsController(ReportsService, $location, $scope, $rootScope, FlashService) {
    	$scope.isSalesCurrentActive = false;
    	$scope.isSalesLastActive = false;
    	$scope.isVisitsActive = false;
        
    	initController();
    	
    	function  initController() {
    		$scope.isVisitsActive = true;
    		$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
            
            loadVisitsReport();
    	}
    	
    	$scope.showVisits = function () {
			$scope.isVisitsActive = true;
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		
    		loadVisitsReport()
		}
    	
    	$scope.showSalesCurrent = function () {
    		$scope.isVisitsActive = false;
    		$scope.isSalesCurrentActive = true;
    		$scope.isSalesLastActive = false;
            
            loadSalesCurrentReport();
    	}
    	
		$scope.showSalesLast = function () {
			$scope.isVisitsActive = false;
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = true;
    		
    		loadSalesLastReport();
		}
		
		function loadVisitsReport() {
			ReportsService.getUserVisits($rootScope.globals.currentUser.username).then(function(response) {
				if (response.data.success) {
					//Pie Chart
			        $scope.labels3 = response.data.labelsList;
			        var profileVisits = response.data.dataListA;
			        var artVisits = response.data.dataListB;
			        $scope.data3 = [profileVisits[0], artVisits[0]];
			        
					$scope.optionsPie = {
					        legend: {
					            display: true
					        },
					};

				}
			});
		}
		
		function loadSalesCurrentReport() {
			ReportsService.getCurrentYearSales($rootScope.globals.currentUser.username).then(function(response) {
				if (response.data.success) {
					$scope.labels = response.data.labelsList;
		            $scope.series = response.data.seriesList;
		            $scope.data = [response.data.dataListA];
		            $scope.onClick = function (points, evt) {
		              console.log(points, evt);
		            };
		            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
		            $scope.options = {
		              scales: {
		                yAxes: [
		                  {
		                    id: 'y-axis-1',
		                    type: 'linear',
		                    display: true,
		                    position: 'left'
		                  },
		                  {
		                    id: 'y-axis-2',
		                    type: 'linear',
		                    display: true,
		                    position: 'right'
		                  }
		                ]
		              }
		            };
				}
			});
		}
		
		function loadSalesLastReport() {
			ReportsService.getLastYearSales($rootScope.globals.currentUser.username).then(function(response) {
				if (response.data.success) {
					$scope.labels2 = response.data.labelsList;
			        $scope.series2 = response.data.seriesList;
			        $scope.data2 = [response.data.dataListA];
				}
			});
		}
		
    }

})();
