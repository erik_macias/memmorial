﻿	(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayController', ArtPayController);

    ArtPayController.$inject = ['ArtService', 'UserService',  'SearchService', 'PaymentsService', '$rootScope', '$routeParams', '$scope', 'FlashService', '$location'];
    function ArtPayController(ArtService, UserService,  SearchService, PaymentsService, $rootScope, $routeParams, $scope, FlashService, $location) {
    	var vm = this;
    	vm.user = $rootScope.globals.currentUser.username;
    	$scope.showPanel = false;
         
    	initController();
    	
    	function initController() {
    		
    		ArtService.isAbleToPublish(vm.user).then(function(response) {
    			vm.allowedToPublish = response.data.success;
    			if (!vm.allowedToPublish) {
    				if (response.data.message === 'PROFILE_PENDING') {
    					FlashService.Warning('Please complete your Default Address Details in order to publish new arts.', true);
    					$location.path('/settings');
    				} else {
    					FlashService.Error(response.data.message);
    				}
    			}
    		});
    		
    		var art = {
              		 art_Id: $routeParams.urlParam
                   }
               PaymentsService.validateArtGoodsPayment(art)
               .then(function (response) {
               	 if (!response.data.success) {
               		 //load art details
                     var searchOpt = {
                 			artId: $routeParams.urlParam, 
        	    			currentUser: $rootScope.globals.currentUser.username
                         }
                     SearchService.FindByArtId(searchOpt)
                     .then(function (response) {
                         if (response.data.success) {
                         	$scope.art = response.data.artDetailsDTO;
                         	$scope.showPanel = true;
                         } else{
                        	 FlashService.Error('The Art ID: ' + $routeParams.urlParam + ' is not active. Please contact our admin team.');
                         }
                     });
               	 } else {
               		 FlashService.Error(response.data.message);
            		}
               });
    	 };
    	 
    	 $scope.submitPaypalPayment = function() {
    		 $location.path('/artPay/exec/paypal/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitBankDepositPayment = function() {
    		 $location.path('/artPay/exec/bank/deposit/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitBankTransferPayment = function() {
    		 $location.path('/artPay/exec/bank/transfer/'+$routeParams.urlParam);
    	 }
    	 
    	 $scope.submitOwnArrangementPayment = function() {
    		 $location.path('/artPay/exec/own/'+$routeParams.urlParam);
    	 }
    	 
     }   
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayExecPaypalController', ArtPayExecPaypalController);

    ArtPayExecPaypalController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function ArtPayExecPaypalController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execArtPaypalPayment();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/artDetails/'+$routeParams.urlParam);
        };
    	
    	function execArtPaypalPayment() {
    		var art = {
				art_Id: $routeParams.urlParam
        	}
        	PaymentsService.execArtPayment(art)
            .then(function (response) {
            	if (response.data.success) {
                	$scope.approval_url = response.data.entity;
                } else {
                	FlashService.Error(response.data.message);
                }
            });
    	};
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayExecBankDepositController', ArtPayExecBankDepositController);

    ArtPayExecBankDepositController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function ArtPayExecBankDepositController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execArtBankDepositPayment();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/profile/'+vm.user);
        };
    	
    	function execArtBankDepositPayment() {
    		var artReq = {
    				art_Id: $routeParams.urlParam,
    				username: vm.user
    			}
            	PaymentsService.execArtBankDepositPayment(artReq)
                .then(function (response) {
                	if (response.data.success) {
                    	FlashService.Success('Bank Deposit payment method for Art ID: ' + $routeParams.urlParam + ' has been registered');
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
    	}
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayExecBankTransferController', ArtPayExecBankTransferController);

    ArtPayExecBankTransferController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function ArtPayExecBankTransferController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execArtBankTransferPayment();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/profile/'+vm.user);
        };
    	
    	function execArtBankTransferPayment() {
    		var artReq = {
    				art_Id: $routeParams.urlParam,
    				username: vm.user
    			}
            	PaymentsService.execArtBankTransferPayment(artReq)
                .then(function (response) {
                	if (response.data.success) {
                    	FlashService.Success('Bank Transfer payment method for Art ID: ' + $routeParams.urlParam + ' has been registered');
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
    	}
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayExecOwnController', ArtPayExecOwnController);

    ArtPayExecOwnController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function ArtPayExecOwnController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execArtPaypalPayment();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/profile/'+vm.user);
        };
    	
    	function execArtPaypalPayment() {
    		
    	};
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayConfirmController', ArtPayConfirmController);

    ArtPayConfirmController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function ArtPayConfirmController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	var vm = this;
   	 	vm.user = $rootScope.globals.currentUser.username;
   	 
    	initialize();
    	
    	function initialize() {
    		execPaypalAgrmnt();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/profile/'+vm.user);
        };
    	
    	function execPaypalAgrmnt() {
    		$scope.queryStrs = $location.search();
    		$scope.isConfirmed = true;
    		$scope.showConfirmed = false;
    		
    		var payment = {
    				paymentId: $scope.queryStrs.paymentId,
    				PayerID: $scope.queryStrs.PayerID,
    				username: vm.user
            	}
            	PaymentsService.confirmArtPayment(payment)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.paymentId = response.data.entity;
                    	$scope.isConfirmed = response.data.success;
                    	$scope.showConfirmed = true;
                    } else {
                    	$scope.isConfirmed = false;
                    	$scope.showConfirmed = false;
                    	FlashService.Error(response.data.message);
                    }
                });
    	};
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtPayCancelController', ArtPayCancelController);

    ArtPayCancelController.$inject = ['$scope', '$rootScope', '$routeParams', '$location'];
    function ArtPayCancelController($scope, $rootScope, $routeParams, $location) {
    	
    }

})();
