﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtDetailsController', ArtDetailsController);

    ArtDetailsController.$inject = ['UserService', 'SearchService', 'ArtService', '$rootScope', '$routeParams', '$scope', 'InboxService', '$location', 'FlashService'];
    function ArtDetailsController(UserService, SearchService, ArtService, $rootScope, $routeParams, $scope, InboxService, $location, FlashService) {
    	 var vm = this;
         vm.comments = '';
         vm.message = '';
         $scope.pageSize = '10';
         vm.viewerMedia ='';
         $scope.txtPattern = /^[a-zA-Z _\.-]+$/;
         
    	initController();
    	getArtComments();
    	getArtRatingByUser();
    	
    	
    	function  initController() {
            vm.comments = '';
            vm.message = '';
            
            //load art details
            var searchOpt; 
        	if ($rootScope.globals.currentUser) {
        		searchOpt = {
        				artId: $routeParams.urlParam, 
    	    			currentUser: $rootScope.globals.currentUser.username
                    }
        	} else {
        		searchOpt = {
        				artId: $routeParams.urlParam
                    }
        	}
        	SearchService.FindByArtId(searchOpt)
            .then(function (response) {
                if (response.data.success) {
                	$scope.art = response.data.artDetailsDTO;
                	$scope.subject = 'Regarding: ' + $scope.art.artName;
                	$scope.isArtActive = true;
                	if (response.data.surveysPending) {
                		FlashService.Warning("Please complete your pending surveys and continue to buy, ask and post comments to the users and arts.", true);
                		$scope.surveysPending = response.data.surveysPending;
                	}
            		angular.forEach($scope.art.media, function(media, key) {
	            		if (media.primary) {
	            			 $scope.setMedia2Viewer(media);
	            		}
            		});
                } else {
                	FlashService.Error('The Art ID: ' + $routeParams.urlParam + ' is not active. Please contact our admin team.');
                	$scope.isArtActive = false;
                }
            });
        }
    	
    	function getArtComments() {
    		ArtService.getArtComments($routeParams.urlParam)
            .then(function (response) {
                if (response.data) {
                	$scope.comments = response.data;
                }
            });
    	}
    	
    	function getArtRatingByUser() {
    		if ($rootScope.globals.currentUser) {
    			var artRateUser = {
        				artId: $routeParams.urlParam,
        				createdBy: $rootScope.globals.currentUser.username
        		}
        		ArtService.getArtRatingByUser(artRateUser)
                .then(function (response) {
                    if (response.data.success) {
                    	vm.artRating = response.data.rating;
                    }
                });
    		}
    	}
    	
    	$scope.rateArt = function() {
    		var artRate = {
    				artId: $routeParams.urlParam,
        			createdBy: $rootScope.globals.currentUser.username,
        	    	rating: vm.artRating,
                }
        	ArtService.rateArtByUser(artRate)
            .then(function (response) {
                if (response.data) {
                	initController()
                	getArtRatingByUser();
                }
            });
        };
        
        $scope.setMedia2Viewer = function(media) {
    		vm.viewerMedia=media;
        };
    	
      
    	$scope.addComments = function() {
    		var artComment = {
    				artId: $routeParams.urlParam,
        			createdBy: $rootScope.globals.currentUser.username,
        	    	comment: vm.comments
                }
        	ArtService.addArtComments(artComment)
            .then(function (response) {
                if (response.data) {
                	FlashService.Success('Your comments has been added successfully');
                	getArtComments();
                	clearForm()
                }
            });
        };
        
        $scope.deleteArtComment = function (artCommentId) {
        	ArtService.deleteArtComments(artCommentId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Comment has been deleted successfully');
                	getArtComments();
                }
            });
        };
        
        $scope.deleteArt = function (artId) {
        	ArtService.deleteArt(artId)
            .then(function (response) {
                if (response.status) {
                	FlashService.Success('Art has been deleted successfully');
                	$location.path('/home');
                }
            });
        };

        $scope.sendPrivateInbox = function () {
        	var inboxMsg = {
        			from: $rootScope.globals.currentUser.username,
        			to: $scope.art.createdBy,
        			subject: $scope.subject,
        			message: vm.message
                }
        	
        	InboxService.sendNewMessageToUser(inboxMsg)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('New message has been sent to ' + $scope.art.createdBy);
                }
            });
        };
        
        $scope.isBuyEnabled = function(art) {
        	var isBuyBtnEnabled = false;
        	
        	if (!$scope.surveysPending && art.status == 'ACTIVE') {
        		isBuyBtnEnabled = true;
        	} else if (!$scope.surveysPending && art.status == 'SOLD' && art.artType == 'Literature') {
        		isBuyBtnEnabled = true;
        	} else if (art.status == 'IN_PROCESS') {
        		isBuyBtnEnabled = false;
        	} else {
        		isBuyBtnEnabled = false;
        	}
        	
        	return isBuyBtnEnabled;
        }
        
        //TODO: Add logic here
        $scope.boughtByCurrent = function () {
        	return false;
        }
        
        function clearForm() {
        	vm.comments = '';
        }
    	
    }

})();