﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PaymentController', PaymentController);

    PaymentController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function PaymentController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {
    	
    	initialize();
    	
    	function initialize() {
    		getPaypalPlanId();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/pricing');
        };
    	
    	function getPaypalPlanId() {
    		$scope.membershipPlan = $routeParams.param;
    		var role = {
    				role_name: $scope.membershipPlan
    		}
        	PaymentsService.getPaymentPlanId(role)
            .then(function (response) {
                if (response.data.success) {
                	var planId = response.data.message;
                	if (planId) {
                			var plan = {
                        		id: planId
                        	}
                        	PaymentsService.createPaypalBillingPlanAgrmnt(plan)
                            .then(function (response) {
                                if (response.data.success) {
                                	$scope.agreement = response.data.entity;
                                	angular.forEach($scope.agreement.links, function(value, key) {
                                		if (value.rel === 'approval_url') {
                                			$scope.approval_url = value.href;
                                		}
                                	});
                                } else {
                                	FlashService.Error(response.data.message);
                                }
                            });
                	}
                } else {
                	FlashService.Error(response.data.message);
                }
            });
        };
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('PaymentConfController', PaymentConfController);

    PaymentConfController.$inject = ['$scope', '$rootScope', '$routeParams', '$location', 'PaymentsService', 'FlashService'];
    function PaymentConfController($scope, $rootScope, $routeParams, $location, PaymentsService, FlashService) {

    	initialize();
    	
    	function initialize() {
    		execPaypalAgrmnt();
    	}
    	
    	$scope.cancel = function () {
        	$location.path('/pricing');
        };
    	
    	function execPaypalAgrmnt() {
    		$scope.queryStrs = $location.search();
    		var agreement = {
            		token: $scope.queryStrs.token
            	}
            	PaymentsService.execPaymentPlanAgrmnt(agreement)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.agreementId = response.data.entity;
                    	if ($scope.agreementId) {
                    		var membership = {
                           			username: $rootScope.globals.currentUser.username,
                           			agreementId: $scope.agreementId
                                   }
                    		PaymentsService.updateUserMembership(membership)
                            .then(function (response) {
                                if (response.data.success) {
                                	$scope.msgResp = response.data.message;
                                	$scope.membershipResp = response.data.entity;
                                } else {
                                    FlashService.Error(response.data.message);
                                }
                            });
                    	}
                    } else {
                    	FlashService.Error(response.data.message);
                    }
                });
    	};
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('PaymentCancelController', PaymentCancelController);

    PaymentCancelController.$inject = ['$scope', '$rootScope', '$routeParams', '$location'];
    function PaymentCancelController($scope, $rootScope, $routeParams, $location) {
    	
    }

})();
