﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('MediaPreviewController', MediaPreviewController);

    MediaPreviewController.$inject = ['UserService',  'ArtService', '$rootScope', '$routeParams', '$scope', 'InboxService', 'FlashService', 'PreviewService', '$location', 'Upload'];
    function MediaPreviewController(UserService,  ArtService, $rootScope, $routeParams, $scope, InboxService, FlashService, PreviewService, $location, Upload) {
    	 var vm = this;
    	 vm.user = $rootScope.globals.currentUser.username;
         vm.comments = '';
         vm.message = '';
         $scope.pageSize = '10';
         vm.viewerMedia = '';
         vm.data = '';
         vm.type = '';
         vm.subtype = '';
         vm.medialist = '';
         vm.pdflist = '';
         vm.pdflistDownload = '';
         vm.audiolist = '';
         vm.videolist = '';
         vm.art = '';
         $scope.currentDate = new Date();
         
    	initController();
    	
    	function  initController() {
	    	 if ($rootScope.Data === undefined ) {
	    		 $location.path('/media');
	    	 } else {
		         vm.data = $rootScope.Data;
		         vm.type =  vm.data.art.art_type.art_type_name;
		         vm.subtype = vm.data.art.art_subType.art_subtype_name;
		         vm.medialist = vm.data.medialist;
		         vm.audiolist = vm.data.audiolist;
		         vm.videolist = vm.data.videolist;
		         vm.pdflist = vm.data.pdflist;
		         vm.pdflistDownload = vm.data.pdflistDownload;
		         vm.viewerMedia = {artMediaType:'image', artMediaFilename:vm.data.primary };
		         vm.art = vm.data.art;
	    	 }
    	 };
        
        $scope.setMedia2Viewer = function(media , type) {
        	vm.viewerMedia={
        			artMediaType:type,
        			artMediaFilename:media
        			};
        };
        
        $scope.return = function() {
        	$location.path('/media');
        };
        
        // upload on file select or drop
		$rootScope.upload = function(file, type, primary, dowload) {
			vm.type=type;
			
			Upload.upload({
				url : 'rest/art/uploadfile',
				data : {
					file : file,
					'username' : $rootScope.username
				}
			}).then(
					function(resp) {
						ArtService.saveMedia({art_id:vm.artEntityId, media_url:resp.data.filePath, media_type:resp.data.fileType, media_primary:primary, downloadable:dowload }).then(function (response) {
			                  if (response.data.success) {
			                 	 FlashService.Success('Updload successful', false);	
			                 	 $location.path('/artFeePayment/'+vm.artEntityId);
			                 	 }
			                 }); 
								
					},
					function(resp) {
						
					},
					function(evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					});
		};
		
		$rootScope.saveArt = function() {
			vm.dataLoading = true;
			vm.art.username = vm.user;
			ArtService.saveArt(vm.art).then(function(response) {
				if (response.data.success) {
					//FlashService.Success('Registration successful', false);
					vm.artEntityId = response.data.Entityid;
					for (var i = 0; i < vm.medialist.length; i++) {
						$rootScope.upload(vm.medialist[i], "image", vm.medialist[i] === vm.data.primary, false);
					}
					for (var i = 0; i < vm.videolist.length; i++){
						$rootScope.upload(vm.videolist[i], "video", vm.videolist[i] === vm.data.primary, false);
					}
					for (var i = 0; i < vm.audiolist.length; i++){
						$rootScope.upload(vm.audiolist[i], "audio", vm.audiolist[i] === vm.data.primary, false);
					}
					for (var i = 0; i < vm.pdflist.length; i++){
						$rootScope.upload(vm.pdflist[i], "pdf", vm.pdflist[i] === vm.data.primary, false);
					}
					for (var i = 0; i < vm.pdflistDownload.length; i++) {
						$rootScope.upload(vm.pdflistDownload[i], "pdf", false, true);
					}
					vm.dataLoading = false;   
                } else {
                	FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
			});
		};
    	
    }   
})();