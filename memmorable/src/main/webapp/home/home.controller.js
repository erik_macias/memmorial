﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'SearchService'];
    function HomeController($scope, SearchService) {

    	initController();
    	
    	function  initController() {
    		
            SearchService.findCaruselArt()
	            .then(function (response) {
	                if (response.data.success) {
	                	$scope.artList = response.data.artSummary;
	                	$("#myCarousel2").carousel();
	                }
	            });
        }
    	
    }

})();