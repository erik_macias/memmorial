﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PwdResetController', PwdResetController);

    PwdResetController.$inject = ['UserService', '$routeParams' ,'$location', '$scope', 'FlashService', '$timeout'];
    function PwdResetController(UserService, $routeParams, $location, $scope, FlashService, $timeout) {
        var vm = this;
        vm.username = null;
        
        GetUserEmail();
        
        vm.pwdResetByEmailLink = changePassword;
        
        function changePassword() {
        	vm.dataLoading = true;
        	
        	var currentUser = {
                username: vm.username,
                authdata: null
            }
        	
            UserService.changePassword({currentUser, newPassword : vm.user.password, password : vm.user.rePassword})
            	.then(function (response) {
                    if (response.data.success) {
                    	 vm.checkedPass=false;
                    	 FlashService.Success('Your Password has been changed', true);
                    	 $location.path('/login');
                    } else {
                        FlashService.Error(response.data.message);
                        vm.dataLoading = false;
                    }
                });
        }
        
        function GetUserEmail() {
            UserService.getUserFromUrlParamEncoded($routeParams.urlParam)
                .then(function (response) {
                    if (response.data.success) {
                        vm.username = response.data.username;
                    } else {
                    	FlashService.Error(response.data.message);
                    	$timeout(function() {
                    		$location.path('/pwdForgot');              	      
                    		}, 3000);
                    }
                });
        }
    }
})();