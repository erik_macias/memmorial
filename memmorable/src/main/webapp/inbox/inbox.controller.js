﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('InboxListCtrl', InboxListCtrl);

    InboxListCtrl.$inject = ['$scope', '$rootScope', '$location', 'InboxService', 'FlashService'];
    function InboxListCtrl($scope, $rootScope, $location, InboxService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = '-created_date';
        $scope.reverseSort = false;
        $scope.txtPattern = /^[a-zA-Z _\.-]+$/;
        
    	loadInboxMessages();
    	
        $scope.viewMessage = function (msgId) {
            $location.path('/inbox/message/' + msgId);
        };
        
        $scope.deleteMessage = function (msgId) {
            InboxService.Delete(msgId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Message has been deleted successfully');
                	loadInboxMessages();
                }
            });
        };
        
       $scope.isDeleteAllowed = function(userId) {
    	   if ($rootScope.globals.currentUser != null) { 
	    	   if ($rootScope.globals.currentUser.username == userId) {
		          return true;
		       }
    	   } else {
    		   return false;
    	   }
       };
       
       $scope.createNewInboxMessage = function () {
    	   $location.path('/inbox/message/create');
       };
       
       $scope.refreshInboxMessages = function () {
    	   loadInboxMessages();
       };

       function loadInboxMessages() {
    	   var user = {
       			username: $rootScope.globals.currentUser.username
               }
           InboxService.getAllByStatusAndUser(user)
           .then(function (response) {
               if (response.data.success) {
            	   $rootScope.inboxList = response.data.msgsInbox;
            	   $rootScope.sentList = response.data.msgsSent;
               }
           });
        }
    }
    
    angular.module('app')
	.controller('TabController', function() {
		this.tab = 1;

		this.setTab = function(tabId) {
			this.tab = tabId;
		};

		this.isSet = function(tabId) {
			return this.tab === tabId;
		};
	})
    
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('InboxDetailCtrl', InboxDetailCtrl);

    InboxDetailCtrl.$inject = ['$scope', '$rootScope', '$routeParams', 'InboxService', '$location', 'FlashService'];
    function InboxDetailCtrl($scope, $rootScope, $routeParams, InboxService, $location, FlashService) {
    	var vm = this;
    	vm.message = '';
        
    	loadInboxMessage();
    	
    	$scope.viewUserProfile = function (userId) {
            $location.path('/profile/' + userId);
        };
    	
        $scope.cancel = function () {
            $location.path('/inbox');
        };
        
        function loadInboxMessage() {
        	var msgId = $routeParams.param;
        	
            InboxService.getByInboxMessageId(msgId)
            .then(function (response) {
                if (response.data.success) {
                	$scope.message = response.data.entity;
                	$scope.subject = 'RE: ' + response.data.entity.subject;
                }
            });
        }
        
        $scope.deleteMsg = function (msgId) {
        	InboxService.deleteMsg(msgId)
            .then(function (response) {
                if (response.data.success) {
                	$location.path('/inbox');
                	FlashService.Success('Message has been deleted successfully');
                }
            });
        };
        
        $scope.sendReplyToInbox = function () {
        	var inboxMsg = {
        			from: $rootScope.globals.currentUser.username,
        			to: $scope.message.fromUser.user_name,
        			subject: $scope.subject,
        			message: vm.message,
                }
        	
        	InboxService.sendNewMessageToUser(inboxMsg)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('RE message has been sent');
                	$location.path('/inbox');
                }
            });
        };

    }
})();
