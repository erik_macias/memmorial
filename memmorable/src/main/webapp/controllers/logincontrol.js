var myApp = angular.module('loginApp',[]);

myApp.controller('LoginController', function ($scope, $rootScope, AUTH_EVENTS, AuthService) {
		  $scope.credentials = {
	   		username: '',
	 	   	password: ''
	  	};
	  	$scope.login = function (credentials) {
	    	AuthService.login(credentials).then(function (user) {
	      	$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
	      	$scope.setCurrentUser(user);
	    }, function () {
	      $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
	    });
	  };
	})
myApp.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})

  $scope.setCurrentUser = function(user) {
    $scope.username = user.username;
  }
myApp.factory('AuthService', function ($http, Session) {
  	var authService = {};
 
  	authService.login = function (credentials) {
    	return $http
      	.post('./rest/Login', credentials)
      	.then(function (res) {
        Session.create(res.data.user_id, res.data.user_name,
                       res.data.user_role);
        return res.data.user;
      });
  };
 
 	authService.isAuthenticated = function () {
   	return !!Session.userId;
  };
 
  	authService.isAuthorized = function (authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (authService.isAuthenticated() &&
      authorizedRoles.indexOf(Session.userRole) !== -1);
  };
 
  	return authService;
})

myApp.service('Session', function () {
  this.create = function (sessionId, userId, userRole) {
    this.user_id = sessionId;
    this.user_name = userId;
    this.user_role = userRole;
  };
  this.destroy = function () {
    this.user_id = null;
    this.user_name = null;
    this.user_role = null;
  };
})
