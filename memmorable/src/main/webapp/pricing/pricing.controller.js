﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PricingController', PricingController);

    PricingController.$inject = ['$location', '$rootScope', '$routeParams', '$scope', 'CatalogsService', 'UserService', 'FlashService'];
    function PricingController($location, $rootScope, $routeParams, $scope, CatalogsService, UserService, FlashService) {
    	
    	initController();
    	
    	function initController() {
        	CatalogsService.getRoles()
            .then(function (response) {
                if (response.data) {
                	$scope.roleList = response.data;
                	getProfile();
                }
            });
        }
    	
    	function getProfile() {
        	if ($rootScope.globals.currentUser) {
        		var profileReq = {
            			user_name: $rootScope.globals.currentUser.username
                    }
        		UserService.getMembershipInfo(profileReq)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.membershipInfo = response.data.membership;
                    }
                });
        	}
        }
    	
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('PricingDetController', PricingDetController)
        .directive('modal', function () {
        return {
          template: '<div class="modal fade">' + 
              '<div class="modal-dialog">' + 
                '<div class="modal-content">' + 
                  '<div class="modal-header">' + 
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                    '<h4 class="modal-title">{{ title }}</h4>' + 
                  '</div>' + 
                  '<div class="modal-body" ng-transclude></div>' + 
                '</div>' + 
              '</div>' + 
            '</div>',
          restrict: 'E',
          transclude: true,
          replace:true,
          scope:true,
          link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function(value){
              if(value == true)
                $(element).modal('show');
              else
                $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function(){
              scope.$apply(function(){
                scope.$parent[attrs.visible] = true;
              });
            });

            $(element).on('hidden.bs.modal', function(){
              scope.$apply(function(){
                scope.$parent[attrs.visible] = false;
              });
            });
          }
        };
      });

    PricingDetController.$inject = ['$location', '$rootScope', '$routeParams', '$scope', 'AuthenticationService', 'CatalogsService', 'UserService', 'FlashService'];
    function PricingDetController($location, $rootScope, $routeParams, $scope, AuthenticationService, CatalogsService, UserService, FlashService) {
    	var vm = this;
    	$scope.showError = false;
    	
    	initController();
    	
    	$scope.toggleModal = function(){
            $scope.showModal = !$scope.showModal;
        };
    	
        $scope.register = function() {
            $scope.showModal = false
            $location.path('/register');
        };
        
        $scope.pwdForgot = function() {
            $scope.showModal = false;
            $location.path('/pwdForgot');
        };
        
        function initController() {
        	//load membership details
        	var role = {
        			role_name: $routeParams.param
                }
        	
        	CatalogsService.getRoleDetails(role)
            .then(function (response) {
                if (response.data) {
                	$scope.role = response.data;
                	getProfile();
                }
            });
        }
        
        $scope.login = function() {
        	vm.dataLoading = true;
            
        	AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(response);
                    $scope.showModal = false;
                } else {
                    $scope.error = response.message;
                    $scope.showError = true;
                    vm.dataLoading = false;
                    $scope.showModal = true;
                }
            });
        };
        
        function getProfile() {
        	if ($rootScope.globals.currentUser) {
        		var profileReq = {
            			user_name: $rootScope.globals.currentUser.username
                    }
        		UserService.getMembershipInfo(profileReq)
                .then(function (response) {
                    if (response.data.success) {
                    	$scope.membershipInfo = response.data.membership;
                    }
                });
        	}
        }
        
    }
    
    

})();

