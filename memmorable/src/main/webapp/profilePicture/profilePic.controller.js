﻿(function () {
    'use strict';

    angular.
    module('app')
    .controller('PictureController', PictureController);
    
    
   PictureController.$inject = ['$rootScope', '$routeParams', '$location','Upload','UserService','FlashService'];
    function PictureController($rootScope,  $routeParams, $location,Upload, UserService, FlashService) {
    var vm = this;
    vm.user =  $rootScope.globals.currentUser.username;
    vm.myImage='';
    vm.imagen='';
    vm.filePath='';
    
    vm.handleFileSelect= handleFileSelect;
    
    $rootScope.uploadFile = uploadFile;
    $rootScope.editPicture = editPicture;
    $rootScope.base64ToBlob=base64ToBlob;
    
      

      function handleFileSelect(evt){
    	  
    	var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        
        reader.onload = function (evt) {
          $rootScope.$apply(function($rootScope){
          vm.myImage=evt.target.result;
          });
        };
        reader.readAsDataURL(file);
      };
      angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
      
      function uploadFile(file){
    	  var file2 = base64ToBlob(file.replace('data:image/png;base64,',''), 'image/jpeg');
    	  Upload.upload({  
              url: 'rest/pro/uploadfile',
              data: {file: file2, 'username': $rootScope.username}
          }).then(function (resp) {
              UserService.editProfilePic({fileName:resp.data.filePath , user:$rootScope.globals.currentUser}).then(function (response) {
                  if (response.data.success) {
                 	 vm.editable=false;
                 	 FlashService.Success('Update successful', false);
                 	 $location.path('/profile/'+$rootScope.globals.currentUser.username); 
                 	}
                 }); 
             
          }, function (resp) {
        	  
          }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          });
      };
      
      function editPicture(imagen){
    	  uploadFile(imagen);
      };

      function base64ToBlob(base64Data, contentType) {
          contentType = contentType || '';
          var sliceSize = 1024;
          var byteCharacters = atob(base64Data);
          var bytesLength = byteCharacters.length;
          var slicesCount = Math.ceil(bytesLength / sliceSize);
          var byteArrays = new Array(slicesCount);

          for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
              var begin = sliceIndex * sliceSize;
              var end = Math.min(begin + sliceSize, bytesLength);

              var bytes = new Array(end - begin);
              for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                  bytes[i] = byteCharacters[offset].charCodeAt(0);
              }
              byteArrays[sliceIndex] = new Uint8Array(bytes);
          }
          return new Blob(byteArrays, { type: contentType });
      };
      };
    
})();