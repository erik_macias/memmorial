﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserListCtrl', UserListCtrl);

    UserListCtrl.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'FlashService'];
    function UserListCtrl($scope, $rootScope, $location, UserService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'username';
        $scope.reverseSort = false;
        $scope.transactionId = '';
        $scope.paymentMethod = '';
        
        loadUsers();
        
        $scope.setUser = function(user) {
	    		$scope.userSelected = user;
	    }
    	
        $scope.editUser = function (userId) {
            $location.path('/admin/user/' + userId);
        };
        
        $scope.viewUserProfile = function (userId) {
            $location.path('/profile/' + userId);
        };

        $scope.deleteUser = function (userId) {
            UserService.Delete(userId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('User ' + userId + ' has been deleted successfully');
                	loadUsers();
                }
            });
        };
        
        $scope.certifyUser = function (userId) {
            UserService.certifyUser(userId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('User ' + userId + ' has been Certified successfully');
                	loadUsers();
                }
            });
        };
        
        $scope.removeCertifyUser = function (userId) {
            UserService.removeCertifyUser(userId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Certification for User ' + userId + ' has been removed successfully');
                	loadUsers();
                }
            });
        };
        
       $scope.isDeleteAllowed = function(userId) {
    	   if ($rootScope.globals.currentUser != null) { 
	    	   if ($rootScope.globals.currentUser.username == userId) {
		          return true;
		       }
    	   } else {
    		   return false;
    	   }
       };
       
       $scope.resetUserPwdLink = function (email) {
    	   UserService.pwdResetForm(email)
	           .then(function (response) {
	               if (response.data.success) {
	                   FlashService.Success('An email has been sent to "' + email +'" with instructions to reset password.', false);
	                   loadUsers();
	               } else {
	                   FlashService.Error(response.data.message);
	               }
	           });
       };
       
       $scope.createNewUser = function () {
    	   	$location.path('/admin/users/create');
       };
       
       $scope.refreshUsers = function () {
    	   	loadUsers();
       };
       
       $scope.registerUserCertPayment = function(usr) {
    	   	var certRequest = {
    			   	username: usr.username,
           		paymentId: $scope.transactionId,
           		paymentMethod: $scope.paymentMethod
           	}
    	   
    	   	UserService.registerCertPayment(certRequest)
            .then(function (response) {
                if (response.status) {
                		loadUsers();
                		$scope.transactionId = '';
                		$scope.paymentMethod = '';
                		FlashService.Success('User Certificate Payment has been registered successfully');
                } else {
                		FlashService.Error('Error while registering User Certificate for user: ' + username);
                }
            });
       	};

       function loadUsers() {
    	   UserService.GetAll()
    	   		.then(function (response) {
	                if (response.data.success) {
	                	$scope.users = response.data.users;
	                }
    	   		});
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserDetailCtrl', UserDetailCtrl);

    UserDetailCtrl.$inject = ['$scope', '$routeParams', 'UserService', 'CatalogsService', '$location', 'FlashService'];
    function UserDetailCtrl($scope, $routeParams, UserService, CatalogsService, $location, FlashService) {
    	var vm = this;
        vm.roles = null;
        vm.userStatus = null;
        
    	//load roles
    	CatalogsService.getRoles().then(function(response) {
			vm.roles = response.data;
		});
    	
    	//load user status
    	CatalogsService.getUserStatus().then(function(response) {
			vm.userStatus = response.data;
		});
    	
    	loadUser();
    	
        $scope.updateUser = function () {
            UserService.Update($scope.user)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('User "' + $scope.user.username + '" has been updated successfully');
                	$location.path('/admin/users');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/users');
        };

        function loadUser() {
        	var user = {
        			username: $routeParams.param
                }
        	
            UserService.GetByUsername(user)
            .then(function (response) {
                if (response.data.success) {
                	$scope.user = response.data.user;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserCreationCtrl', UserCreationCtrl);

    UserCreationCtrl.$inject = ['$scope', '$routeParams', 'UserService', 'CatalogsService', '$location', 'FlashService'];
    function UserCreationCtrl($scope, $routeParams, UserService, CatalogsService, $location, FlashService) {
    	var vm = this;
        vm.roles = null;
        vm.userStatus = null;
        
    	//load roles
    	CatalogsService.getRoles().then(function(response) {
			vm.roles = response.data;
		});
    	
    	//load user status
    	CatalogsService.getUserStatus().then(function(response) {
			vm.userStatus = response.data;
		});
    	
    	 // callback for ng-click 'createNewUser':
        $scope.createNewUser = function () {
        	UserService.Create($scope.newUser)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New user created successful', false);
                    $location.path('/admin/users');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/users');
        };
     
    }

})();