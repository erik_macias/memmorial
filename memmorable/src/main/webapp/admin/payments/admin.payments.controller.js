﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PaymentsListCtrl', PaymentsListCtrl);

    PaymentsListCtrl.$inject = ['$scope', '$rootScope', '$location', 'PaymentsService', 'CatalogsService', 'FlashService'];
    function PaymentsListCtrl($scope, $rootScope, $location, PaymentsService, CatalogsService, FlashService) {
    	var vm = this;
    	vm.roles = null;
    	vm.plan = null;
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'plan_id';
        $scope.reverseSort = false;
        $scope.showModal = false;
        $scope.showField = false;
        
        loadPaypalPlansList();
        
        $scope.toggleModal = function() {
            $scope.showModal = !$scope.showModal;
            $scope.showField = true;
            if($scope.showModal) {
            	loadRoles();
            }
        };
    	
        $scope.loadCreatePlanTemplate = function () {
        	PaymentsService.getPaymentCreatePlanTemplate()
            .then(function (response) {
                if (response.data.success) {
                	vm.plan = response.data.entity;
                }
            });
        };
         
        $scope.deletePaypalPlan = function (planId) {
        	PaymentsService.deletePaymentPlan(planId)
             .then(function (response) {
                 if (response.data.success) {
                 	FlashService.Success('Plan ' + planId + ' has been deleted successfully');
                 	loadPaypalPlansList();
                 } else {
                	 FlashService.Error(response.data.message);
                 }
             });
        };
        
        $scope.createNewPaypalPlan = function () {
        	var billingPlan = {
        		plan: vm.plan,
        		role: {
        			role_name: vm.role
        		}
        	}
        	PaymentsService.createNewPaypalBillingPlan(billingPlan)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('New Billing Plan has been created successfully');
                	loadPaypalPlansList();
                	$scope.showField = false;
                	$scope.showModal = false;
                } else {
                	FlashService.Error(response.data.message);
                	$scope.showField = false;
                	$scope.showModal = false;
                }
            });
        };
        
        $scope.viewPaypalPlan = function (planId, role) {
        	var plan = {
        		id: planId
        	}
        	PaymentsService.getPaymentPlanDetail(plan)
            .then(function (response) {
                if (response.data.success) {
                	vm.plan = response.data.entity;
                	vm.role = role;
                	loadRoles();
                	$scope.showField = false;
                	$scope.showModal = true;
                }
            });
        };
        
        $scope.activatePaypalPlan = function (planId) {
        	var plan = {
        		id: planId
        	}
        	PaymentsService.activatePaymentPlan(plan)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Plan ' + planId + ' has been activated successfully');
                 	loadPaypalPlansList();
                } else {
                	FlashService.Error(response.data.message);
                }
            });
        };
        
        $scope.cancel = function () {
        	$scope.showField = false;
        	$scope.showModal = false;
        };
        
        $scope.refreshPaypalPlans = function () {
        	loadPaypalPlansList();
        };
    	
        function loadPaypalPlansList() {
        	PaymentsService.getPaymentPlans()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.plans = response.data.entityList;
	                }
    	   		});
        }
        
        function loadRoles() {
        	CatalogsService.getRoles().then(function(response) {
    			vm.roles = response.data;
    		});
        }
	}
})();
