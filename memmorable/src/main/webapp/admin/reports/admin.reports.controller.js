﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ReportsAdminController', ReportsAdminController);

    ReportsAdminController.$inject = ['ReportsService', '$location', '$scope', '$rootScope', '$filter', 'FlashService'];
    function ReportsAdminController(ReportsService, $location, $scope, $rootScope, $filter, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'username';
        $scope.reverseSort = false;
    	$scope.isSalesCurrentActive = false;
    	$scope.isSalesLastActive = false;
    	$scope.isSalesCustomActive = false;
    	$scope.isCurrentFeesPaid = false;
    	$scope.isCustomFeesPaid = false;
    	$scope.isFeesPaidPending = false;
    	$scope.isUsersCreated = false;
        
    	initController();
    	
    	function  initController() {
    		$scope.isSalesCurrentActive = true;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
            
            loadAdmnSalesCurrentReport();
    	}
    	
    	$scope.showSalesCurrent = function () {
    		$scope.isSalesCurrentActive = true;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
            
    		loadAdmnSalesCurrentReport();
    	}
    	
		$scope.showSalesLast = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = true;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
    		
    		loadAdmSalesLastReport();
		}
		
		$scope.showSalesCustom = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = true;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
    		$scope.dateRangeStart2 = '';
    		$scope.dateRangeEnd2 = '';
    		$scope.customReport = false;
    		$scope.series7 = null;
    		$rootScope.flash = null;
		}
		
		$scope.showCurrentFeesPaid = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = true;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
    		
    		loadAdmCurrentFeesPaid();
		}
		
		$scope.showCustomFeesPaid = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = true;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = false;
    		$scope.dateRangeStart3 = '';
    		$scope.dateRangeEnd3 = '';
    		$scope.customFeePaidReport = false;
    		$scope.series7 = null;
    		$rootScope.flash = null;
		}
		
		$scope.showFeesPaidPending = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = true;
    		$scope.isUsersCreated = false;
    		
    		loadAdmFeesPending();
		}
		
		$scope.showUsersCreated = function () {
			$scope.isSalesCurrentActive = false;
    		$scope.isSalesLastActive = false;
    		$scope.isSalesCustomActive = false;
    		$scope.isCurrentFeesPaid = false;
    		$scope.isCustomFeesPaid = false;
    		$scope.isFeesPaidPending = false;
    		$scope.isUsersCreated = true;
			
			loadUsersCreated();
		}
		
		$scope.viewUserProfile = function (userId) {
            $location.path('/profile/' + userId);
        };
		
		function loadAdmnSalesCurrentReport() {
			ReportsService.getAdmnCurrentYearSales().then(function(response) {
				if (response.data.success) {
					$scope.labels = response.data.labelsList;
		            $scope.series = response.data.seriesList;
		            $scope.data = [response.data.dataListA];
				}
			});
		}
		
		function loadAdmSalesLastReport() {
			ReportsService.getAdmnLastYearSales().then(function(response) {
				if (response.data.success) {
					$scope.labels2 = response.data.labelsList;
			        $scope.series2 = response.data.seriesList;
			        $scope.data2 = [response.data.dataListA];
				}
			});
		}
		
		function loadAdmCurrentFeesPaid() {
			ReportsService.getAdmnCurrentYearFee().then(function(response) {
				if (response.data.success) {
					$scope.labels4 = response.data.labelsList;
			        $scope.series4 = response.data.seriesList;
			        $scope.data4 = [response.data.dataListA];
			        
			        $scope.options = {
					        legend: {
					            display: true
					        },
					};
				}
			});
		}
		
		function loadAdmFeesPending() {
			ReportsService.getAdmnFeePending().then(function(response) {
				if (response.data.success) {
					$scope.users = response.data.users;
				}
			});
		}
		
		function loadUsersCreated() {
			ReportsService.getCreatedUsers().then(function(response) {
				if (response.data.success) {
					$scope.labels5 = response.data.labelsList;
			        $scope.series5 = response.data.seriesList;
			        $scope.data5 = [response.data.dataListA];
			        
			        $scope.options = {
					        legend: {
					            display: true
					        },
					};
				}
			});
		}
		
		$scope.startDateOnSetTime = function (newDate, oldDate) {
			$scope.dateRangeStart2 = $filter('date')(newDate, 'yyyy-MM-dd');
			$scope.$broadcast('start-date-changed');
		};
		
		$scope.startDateOnSetTimeFees = function (newDate, oldDate) {
			$scope.dateRangeStart3 = $filter('date')(newDate, 'yyyy-MM-dd');
			$scope.$broadcast('start-date-fees-changed');
		};

		$scope.endDateOnSetTime = function (newDate, oldDate) {
			$scope.dateRangeEnd2 = $filter('date')(newDate, 'yyyy-MM-dd');
			$scope.$broadcast('end-date-fees-changed');
		};
		
		$scope.endDateOnSetTimeFees = function (newDate, oldDate) {
			$scope.dateRangeEnd3 = $filter('date')(newDate, 'yyyy-MM-dd');
			$scope.$broadcast('end-date-changed');
		};

		$scope.startDateBeforeRender = function ($dates) {
			if ($scope.dateRangeEnd2) {
				var activeDate = moment($scope.dateRangeEnd2);

				$dates.filter(function(date) {
					return date.localDateValue() >= activeDate.valueOf()
				}).forEach(function(date) {
					date.selectable = false;
				})
			}
		};
		
		$scope.startDateBeforeRenderFees = function ($dates) {
			if ($scope.dateRangeEnd3) {
				var activeDate = moment($scope.dateRangeEnd3);

				$dates.filter(function(date) {
					return date.localDateValue() >= activeDate.valueOf()
				}).forEach(function(date) {
					date.selectable = false;
				})
			}
		};

		$scope.endDateBeforeRender = function ($view, $dates) {
			if ($scope.dateRangeStart2) {
				var activeDate = moment($scope.dateRangeStart2).subtract(1, $view).add(1, 'minute');

				$dates.filter(function(date) {
					return date.localDateValue() <= activeDate.valueOf()
				}).forEach(function(date) {
					date.selectable = false;
				})
			}
		};
		
		$scope.endDateBeforeRenderFees = function ($view, $dates) {
			if ($scope.dateRangeStart3) {
				var activeDate = moment($scope.dateRangeStart2).subtract(1, $view).add(1, 'minute');

				$dates.filter(function(date) {
					return date.localDateValue() <= activeDate.valueOf()
				}).forEach(function(date) {
					date.selectable = false;
				})
			}
		};
		
		$scope.loadAdmSalesCustomReport = function () {
			var rangesObj = {
					startDate: $scope.dateRangeStart2,
					endDate: $scope.dateRangeEnd2
			};
			
			ReportsService.getAdmnRangeSales(rangesObj).then(function(response) {
				if (response.data.success) {
			        $scope.series6 = response.data.seriesList;
			        $scope.customReport = true;
			        $rootScope.flash = null;
				} else {
					FlashService.Error(response.data.message, false);
					$scope.series6 = '';
					$scope.customReport = false;
				}
			});
		};
		
		$scope.loadAdmCustomFeesPaid = function () {
			var rangesObj = {
					startDate: $scope.dateRangeStart3,
					endDate: $scope.dateRangeEnd3
			};
			ReportsService.getAdmnCustomFees(rangesObj).then(function(response) {
				if (response.data.success) {
			        $scope.series7 = response.data.seriesList;
			        $scope.customFeePaidReport = true;
			        $rootScope.flash = null;
				} else {
					FlashService.Error(response.data.message, false);
					$scope.series7 = '';
			        $scope.customFeePaidReport = false;
				}
			});
		};
		
    }

})();
