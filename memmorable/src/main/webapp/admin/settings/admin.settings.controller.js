﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('SettingsListCtrl', SettingsListCtrl);

    SettingsListCtrl.$inject = ['$scope', '$rootScope', '$location', 'AppSettingsService', 'FlashService'];
    function SettingsListCtrl($scope, $rootScope, $location, AppSettingsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'app_setting_name';
        $scope.reverseSort = false;
        
        loadKeyValuesList();
    	
        $scope.editKeyValue = function(key) {
        	$location.path('/admin/settings/edit/' + key);
        };
         
        $scope.deleteKeyValue = function(key) {
        	AppSettingsService.deleteKeyValue(key)
             .then(function (response) {
                 if (response.data.success) {
                 	FlashService.Success('Key ' + key + ' has been deleted successfully');
                 	loadKeyValuesList();
                 }
             });
        };
         
        $scope.createNewKeyValue = function() {
        	 $location.path('/admin/settings/create');
        };
        
        $scope.refreshKeyValues = function() {
        	 loadKeyValuesList();
        };
    	
        function loadKeyValuesList() {
    		AppSettingsService.getAllKeyValues()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.keyValueList = response.data;
	                }
    	   		});
        }
        
        $scope.reBuildLuceneIndex = function() {
        	AppSettingsService.reBuildLuceneIndex()
	   		.then(function (response) {
                if (response.data) {
                	FlashService.Success('Lucene Index has been rebuilt successfully', false);
                }
	   		});
        }
	}
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('SettingsDetailCtrl', SettingsDetailCtrl);

    SettingsDetailCtrl.$inject = ['$scope', '$routeParams', 'AppSettingsService', '$location', 'FlashService'];
    function SettingsDetailCtrl($scope, $routeParams, AppSettingsService, $location, FlashService) {
    	var vm = this;
        
    	loadKeyValue();
    	
        $scope.updateKeyValue = function () {
        	AppSettingsService.updateKeyValue($scope.appSetting)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Key "' + $scope.appSetting.app_setting_name + '" has been updated successfully', true);
                	$location.path('/admin/settings');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/settings');
        };

        function loadKeyValue() {
        	var appSettings = {
        			app_setting_name: $routeParams.param
                }
        	
        	AppSettingsService.getKeyValue(appSettings)
            .then(function (response) {
                if (response.data) {
                	$scope.appSetting = response.data;
                }
            });
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('SettingsCreationCtrl', SettingsCreationCtrl);

    SettingsCreationCtrl.$inject = ['$scope', '$routeParams', 'AppSettingsService', '$location', 'FlashService'];
    function SettingsCreationCtrl($scope, $routeParams, AppSettingsService, $location, FlashService) {
    	
        $scope.createNewKeyValue = function () {
        	AppSettingsService.createKeyValue($scope.appSetting)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New App Setting has benn created successful', true);
                    $location.path('/admin/settings');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/settings');
        };
    }
})();