﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddressTypeCatListCtrl', AddressTypeCatListCtrl);

    AddressTypeCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function AddressTypeCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'address_type_name';
        $scope.reverseSort = false;
        
    	loadAddressTypes();
    	
        $scope.editAddressType = function (addressTypeId) {
            $location.path('/admin/catalogs/addresstype/' + addressTypeId);
        };
        
        $scope.createNewAddressType = function () {
        	$location.path('/admin/catalogs/addresstypes/create');
        };
        
        $scope.refreshAddressTypes = function () {
        	loadAddressTypes();
        };

        function loadAddressTypes() {
        	CatalogsService.getAddressTypes()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.addressTypes = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddressTypeCatDetailCtrl', AddressTypeCatDetailCtrl);

    AddressTypeCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function AddressTypeCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadAddressType();
    	
        $scope.updateAddressType = function () {
        	var addressTypeUpd = {
        			address_type_id: $scope.addressType.address_type_id,
        			address_type_name: $scope.addressType.address_type_name
                   } 
        	
        	CatalogsService.updateAddressType(addressTypeUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Address Type has been updated successfully', true);
                	$location.path('/admin/catalogs/addresstypes');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/addresstypes');
        };

        function loadAddressType() {
        	CatalogsService.getAddressTypeDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.addressType = response.data.entity;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddressTypeCatCreationCtrl', AddressTypeCatCreationCtrl);

    AddressTypeCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function AddressTypeCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
        $scope.createAddressType = function () {
        	var addressTypeNew = {
        			address_type_name: $scope.addressType.address_type_name
                   } 
        	
        	CatalogsService.createAddressType(addressTypeNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Address Type created successful', true);
                    $location.path('/admin/catalogs/addresstypes');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/addresstypes');
        };
     
    }

})();