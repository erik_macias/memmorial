﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeFeeCatListCtrl', ArtTypeFeeCatListCtrl);

    ArtTypeFeeCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function ArtTypeFeeCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'art_type_name';
        $scope.reverseSort = false;
        
    	loadArtTypeFees();
    	
        $scope.editArtTypeFee = function (artTypeFeeId) {
            $location.path('/admin/catalogs/arttypefee/' + artTypeFeeId);
        };
        
        $scope.createNewArtTypeFee = function () {
        	$location.path('/admin/catalogs/arttypefees/create');
        };
        
        $scope.refreshArtTypeFees = function () {
        	loadArtTypeFees();
        };

        function loadArtTypeFees() {
        	CatalogsService.getArtTypeFees()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.artTypeFees = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeFeeCatDetailCtrl', ArtTypeFeeCatDetailCtrl);

    ArtTypeFeeCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtTypeFeeCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
    	vm.roles = null;
        
    	//load roles
    	CatalogsService.getRoles().then(function(response) {
			vm.roles = response.data;
		});
    	
    	loadArtTypeFee();
    	
        function loadArtTypeFee() {
        	CatalogsService.getArtTypeFeeDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.artTypeFee = response.data.entity;
                }
            });
        }
        
        $scope.updateArtTypeFee = function () {
        	var artTypeFeeUpd = {
        			artTypeFeeId: $scope.artTypeFee.art_type_fee_id,
        			artType: $scope.artTypeFee.art_type.art_type_name,
        			role: $scope.artTypeFee.role.role_name,
        			fee: $scope.artTypeFee.fee_amount
                   } 
        	
        	CatalogsService.updateArtTypeFee(artTypeFeeUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Art Type Fee has been updated successfully', true);
                	$location.path('/admin/catalogs/arttypefees');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/arttypefees');
        };
        
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeFeeCatCreationCtrl', ArtTypeFeeCatCreationCtrl);

    ArtTypeFeeCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtTypeFeeCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
    	vm.roles = null;
    	
    	//load art types
        CatalogsService.getArtTypes().then(function (response) {
        	$scope.artTypes = response.data;
    	});
    	
    	//load roles
    	CatalogsService.getRoles().then(function(response) {
			vm.roles = response.data;
		});
        
        $scope.createArtTypeFee = function () {
        	var artTypeFeeNew = {
        			artType: $scope.artTypeFee.art_type.art_type_name,
        			role: $scope.artTypeFee.role.role_name,
        			fee: $scope.artTypeFee.fee_amount
                   } 
        	
        	CatalogsService.createArtTypeFee(artTypeFeeNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Art Type Fee created successful', true);
                    $location.path('/admin/catalogs/arttypefees');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/arttypefees');
        };
     
    }

})();