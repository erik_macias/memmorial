﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtSubTypeCatListCtrl', ArtSubTypeCatListCtrl);

    ArtSubTypeCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function ArtSubTypeCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'art_subtype_name';
        $scope.reverseSort = false;
        
    	loadArtSubTypes();
    	
        $scope.editArtSubType = function (artSubTypeId) {
            $location.path('/admin/catalogs/artsubtype/' + artSubTypeId);
        };
        
        $scope.createNewArtSubType = function () {
        	$location.path('/admin/catalogs/artsubtypes/create');
        };

        $scope.refreshArtSubTypes = function () {
        	loadArtSubTypes();
        };

        function loadArtSubTypes() {
        	CatalogsService.getArtSubTypes()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.artSubTypes = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtSubTypeCatDetailCtrl', ArtSubTypeCatDetailCtrl);

    ArtSubTypeCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtSubTypeCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadArtSubType();
    	loadArtTypes();
    	
        $scope.updateArtSubType = function () {
        	var artSubTypeUpd = {
        			art_subtype_id: $scope.artSubType.art_subtype_id,
        			art_type: $scope.artSubType.art_type,
        			art_subtype_name: $scope.artSubType.art_subtype_name
                   } 
        	
        	CatalogsService.updateArtSubType(artSubTypeUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Art SubType has been updated successfully', true);
                	$location.path('/admin/catalogs/artsubtypes');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/artsubtypes');
        };

        function loadArtTypes() {
        	CatalogsService.getArtTypes()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.artTypes = response.data;
	                }
    	   		});
        }
        
        function loadArtSubType() {
        	CatalogsService.getArtSubTypeDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.artSubType = response.data.entity;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtSubTypeCatCreationCtrl', ArtSubTypeCatCreationCtrl);

    ArtSubTypeCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtSubTypeCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadArtTypes();
    	
        $scope.createArtSubType = function () {
        	var artSubTypeNew = {
        			art_type: $scope.artSubType.art_type,
        			art_subtype_name: $scope.artSubType.art_subtype_name
                   } 
        	
        	CatalogsService.createArtSubType(artSubTypeNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Art SubType created successful', true);
                    $location.path('/admin/catalogs/artsubtypes');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        function loadArtTypes() {
        	CatalogsService.getArtTypes()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.artTypes = response.data;
	                }
    	   		});
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/artsubtypes');
        };
     
    }

})();