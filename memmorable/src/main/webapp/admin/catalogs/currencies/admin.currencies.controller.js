﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('CurrencyCatListCtrl', CurrencyCatListCtrl);

    CurrencyCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function CurrencyCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'currency_type';
        $scope.reverseSort = false;
        
    	loadCurrencies();
    	
        $scope.editCurrency = function (currencyId) {
            $location.path('/admin/catalogs/currency/' + currencyId);
        };
        
        $scope.createNewCurrency = function () {
        	$location.path('/admin/catalogs/currencies/create');
        };
        
        $scope.refreshCurrencies = function () {
        	loadCurrencies();
        };

        function loadCurrencies() {
        	CatalogsService.getCurrencies()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.currencies = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('CurrencyCatDetailCtrl', CurrencyCatDetailCtrl);

    CurrencyCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function CurrencyCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadCurrency();
    	
        $scope.updateCurrency = function () {
        	var currencyUpd = {
        			currency_id: $scope.currency.currency_id,
        			currency_type: $scope.currency.currency_type
                   } 
        	
        	CatalogsService.updateCurrency(currencyUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Currency has been updated successfully', true);
                	$location.path('/admin/catalogs/currencies');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/currencies');
        };

        function loadCurrency() {
        	CatalogsService.getCurrencyDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.currency = response.data.entity;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('CurrencyCatCreationCtrl', CurrencyCatCreationCtrl);

    CurrencyCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function CurrencyCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
        $scope.createCurrency = function () {
        	var currencyNew = {
        			currency_type: $scope.currency.currency_type
                   } 
        	
        	CatalogsService.createCurrency(currencyNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Currency created successful', true);
                    $location.path('/admin/catalogs/currencies');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/currencies');
        };
     
    }

})();