﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RolesCatListCtrl', RolesCatListCtrl);

    RolesCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function RolesCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'role_id';
        $scope.reverseSort = false;
        
        loadRolesList();
    	
        $scope.editRole = function (role) {
        	$location.path('/admin/catalogs/role/' + role);
        };
         
        $scope.refreshRoles = function () {
        	loadRolesList();
        };
        
        function loadRolesList() {
    		CatalogsService.getRoles()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.rolesList = response.data;
	                }
    	   		});
        }
	}
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('RoleCatDetailCtrl', RoleCatDetailCtrl);

    RoleCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function RoleCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadRole();
    	
        $scope.updateRole = function () {
        	CatalogsService.updateRole($scope.role)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Role "' + $scope.role.role_name + '" has been updated successfully', true);
                	$location.path('/admin/catalogs/roles');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/roles');
        };

        function loadRole() {
        	var role = {
        			role_name: $routeParams.param
                }
        	
        	CatalogsService.getRoleDetails(role)
            .then(function (response) {
                if (response.data) {
                	$scope.role = response.data;
                }
            });
        }
    }
})();
