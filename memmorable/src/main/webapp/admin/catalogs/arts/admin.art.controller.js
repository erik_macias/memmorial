﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtListCtrl', ArtListCtrl);

    ArtListCtrl.$inject = ['$scope', '$rootScope', '$location', 'ArtService', 'FlashService'];
    function ArtListCtrl($scope, $rootScope, $location, ArtService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.reverseSort = false;
        $scope.transactionId = '';
        $scope.paymentMethod = '';
        
        loadArtList();
    	
        $scope.setArt = function(art) {
        		$scope.art = art;
        }
        
        $scope.activateArt = function(artId) {
        	var artRequest = {
        		artId: artId,
        		paymentId: $scope.transactionId,
        		paymentMethod: $scope.paymentMethod
        	}
        	ArtService.activateArt(artRequest)
            .then(function (response) {
                if (response.status) {
                	loadArtList();
                	 $scope.transactionId = '';
                     $scope.paymentMethod = '';
                	FlashService.Success('Art has been activated successfully');
                } else {
                	FlashService.Error('Error while activating art id: ' + artId);
                }
            });
        };
        
        $scope.registerArtPayment = function(artId) {
        	var artRequest = {
            		artId: artId,
            		paymentId: $scope.transactionId,
            		paymentMethod: $scope.paymentMethod
            	}
            	ArtService.registerArtPayment(artRequest)
                .then(function (response) {
                    if (response.status) {
                    	loadArtList();
                    	 $scope.transactionId = '';
                         $scope.paymentMethod = '';
                    	FlashService.Success('Art Payment has been registered successfully');
                    } else {
                    	FlashService.Error('Error while registering Art Payment for art id: ' + artId);
                    }
                });
        };

        
        
        $scope.viewUserProfile = function (userId) {
            $location.path('/profile/' + userId);
        };

        $scope.deleteArt = function (artId) {
        	ArtService.deleteArt(artId)
            .then(function (response) {
                if (response.status) {
                	loadArtList();
                	FlashService.Success('Art has been deleted successfully');
                }
            });
        };
       
       $scope.refreshArtLis = function () {
    	   loadArtList();
       };

       function loadArtList() {
       		ArtService.getAllArtList()
               .then(function (response) {
                   if (response.data.success) {
                   	$scope.searchCount = response.data.searchResultCount;
                   	$scope.artList = response.data.artEntityList;
                   	$scope.orderProp = '-lastUpdatedDate';
                   }
               });
       }
    }

})();
