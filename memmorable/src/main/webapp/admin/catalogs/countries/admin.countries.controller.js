﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('CountryCatListCtrl', CountryCatListCtrl);

    CountryCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function CountryCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'country_code';
        $scope.reverseSort = false;
        
    	loadCountries();
    	
        $scope.editCountry = function (countryId) {
            $location.path('/admin/catalogs/country/' + countryId);
        };
        
        $scope.createNewCountry = function () {
        	$location.path('/admin/catalogs/countries/create');
        };

        $scope.refreshCountries = function () {
        	loadCountries();
        };

        function loadCountries() {
        	CatalogsService.getCountries()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.countries = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('CountryCatDetailCtrl', CountryCatDetailCtrl);

    CountryCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function CountryCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadCountry();
    	
        $scope.updateCountry = function () {
        	var countryUpd = {
        			country_id: $scope.country.country_id,
        			country_name: $scope.country.country_name,
        			country_code: $scope.country.country_code
                   } 
        	
        	CatalogsService.updateCountry(countryUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Country has been updated successfully', true);
                	$location.path('/admin/catalogs/countries');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/countries');
        };

        function loadCountry() {
        	CatalogsService.getCountryDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.country = response.data.entity;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('CountryCatCreationCtrl', CountryCatCreationCtrl);

    CountryCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function CountryCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
        $scope.createCountry = function () {
        	var countryNew = {
        			country_name: $scope.country.country_name,
        			country_code: $scope.country.country_code
                   } 
        	
        	CatalogsService.createCountry(countryNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Country created successful', true);
                    $location.path('/admin/catalogs/countries');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/countries');
        };
     
    }

})();