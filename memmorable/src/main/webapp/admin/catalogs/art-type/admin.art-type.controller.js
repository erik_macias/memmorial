﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeCatListCtrl', ArtTypeCatListCtrl);

    ArtTypeCatListCtrl.$inject = ['$scope', '$rootScope', '$location', 'CatalogsService', 'FlashService'];
    function ArtTypeCatListCtrl($scope, $rootScope, $location, CatalogsService, FlashService) {
    	$scope.currentPage = 1;
        $scope.pageSize = '10';
        $scope.orderProp = 'art_type_name';
        $scope.reverseSort = false;
        
    	loadArtTypes();
    	
        $scope.editArtType = function (artTypeId) {
            $location.path('/admin/catalogs/arttype/' + artTypeId);
        };
        
        $scope.createNewArtType = function () {
        	$location.path('/admin/catalogs/arttypes/create');
        };
        
        $scope.refreshArtTypes = function () {
        	loadArtTypes();
        };

        function loadArtTypes() {
        	CatalogsService.getArtTypes()
    	   		.then(function (response) {
	                if (response.data) {
	                	$scope.artTypes = response.data;
	                }
    	   		});
        }
	}

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeCatDetailCtrl', ArtTypeCatDetailCtrl);

    ArtTypeCatDetailCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtTypeCatDetailCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
    	loadArtType();
    	
        $scope.updateArtType = function () {
        	var artTypeUpd = {
        			art_type_id: $scope.artType.art_type_id,
        			art_type_name: $scope.artType.art_type_name
                   } 
        	
        	CatalogsService.updateArtType(artTypeUpd)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Art Type has been updated successfully', true);
                	$location.path('/admin/catalogs/arttypes');
                }
            });
        };

        $scope.cancel = function () {
            $location.path('/admin/catalogs/arttypes');
        };

        function loadArtType() {
        	CatalogsService.getArtTypeDetails($routeParams.param)
            .then(function (response) {
                if (response.data.success) {
                	$scope.artType = response.data.entity;
                }
            });
        }
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('ArtTypeCatCreationCtrl', ArtTypeCatCreationCtrl);

    ArtTypeCatCreationCtrl.$inject = ['$scope', '$routeParams', 'CatalogsService', '$location', 'FlashService'];
    function ArtTypeCatCreationCtrl($scope, $routeParams, CatalogsService, $location, FlashService) {
    	var vm = this;
        
        $scope.createArtType = function () {
        	var artTypeNew = {
        			art_type_name: $scope.artType.art_type_name
                   } 
        	
        	CatalogsService.createArtType(artTypeNew)
            .then(function (response) {
                if (response.data.success) {
                    FlashService.Success('New Art Type created successful', true);
                    $location.path('/admin/catalogs/arttypes');
                } else {
                    FlashService.Error(response.data.message);
                    vm.dataLoading = false;
                }
            });
        }
        
        $scope.cancel = function () {
            $location.path('/admin/catalogs/arttypes');
        };
     
    }

})();