﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['$scope', '$location', 'AuthenticationService', 'UserService', 'CatalogsService', '$rootScope', 'FlashService'];
    function SettingsController($scope, $location, AuthenticationService, UserService, CatalogsService, $rootScope, FlashService) {
        var vm = this;
        vm.user = null;
        vm.personal= null;
        vm.editPersonal = editPersonal;
        vm.checked = false;
        vm.checkedPass = false;
        vm.checkedAddrs = false;
        vm.changePassword = changePassword;
        vm.countries = '';
        vm.addressTypes = '';
        vm.addressAction = 'Add';
        vm.addAddress = addAddress;
        vm.updateAddress = updateAddress;
        vm.isNewAddress = true;
        vm.states = '';
        vm.cities = '';
        $scope.selectedCountry = '';
        $scope.selectedState = '';
        $scope.selectedCity = '';
       
        CatalogsService.getCountries().then(function(response) {
			vm.countries = response.data;
		});
        
        CatalogsService.getAddressTypes().then(function(response) {
			vm.addressTypes = response.data;
		});
        
        initController();
        GetPersonal();
        loadUserAddresses();
        
        $rootScope.fillStates = function(countryObj) {
        	var country = {
				countryId : countryObj.country_id
			}
        	CatalogsService.getStates(country).then(function(response) {
				vm.states = response.data;
			});
		};
		
		$rootScope.fillCities = function(stateObj) {
        	var state = {
				stateId : stateObj.stateId
			}
        	CatalogsService.getCities(state).then(function(response) {
				vm.cities = response.data;
			});
		};
        
        function editPersonal() {
            UserService.UpdatePersonal({user : $rootScope.globals.currentUser, personal: vm.personal}).then(function (response) {
                    if (response.data.success) {
                    	 vm.checked=false;
                    	 FlashService.Success('Update successful', false);
                    } 
                });
        }
        
        function changePassword() {
            UserService.changePassword({currentUser : $rootScope.globals.currentUser, newPassword: vm.newPassword, password : vm.oldPassword}).then(function (response) {
                    if (response.data.success) {
                    	 vm.checkedPass=false;
                    	 AuthenticationService.ClearCredentials();
                         FlashService.Success('Password was changed. Please login back again.', false);
                         $location.path('/login');
                    } 
                });
        }
        
        function initController() {
        	 vm.user = $rootScope.globals.currentUser.username;
        }

        function GetPersonal() {
            UserService.GetPersonalByUser($rootScope.globals.currentUser)
                .then(function (response) {
                    if (response.data.success) {
                        vm.personal = response.data.entity;
                    } 
                });
        }
        
        function loadUserAddresses() {
        	var user = {
        			username: $rootScope.globals.currentUser.username
                }
            UserService.getUserAddresses(user)
            .then(function (response) {
                if (response.data.success) {
                	$scope.addresses = response.data.addresses;
                }
            });
        }
        
        $scope.editAddress = function (address) {
        	vm.addressAction = 'Update';
        	vm.isNewAddress = false;
        	$scope.address = address;
        	$scope.address.countryId = {
        			country_id: $scope.address.countryId
        	};
        	$rootScope.fillStates($scope.address.countryId)
        	$scope.address.addressStateId = {
        			stateId: $scope.address.addressStateId
        	};
        	$rootScope.fillCities($scope.address.addressStateId)
        	$scope.address.addressCityId = {
        			cityId: $scope.address.addressCityId
        	};
        };
        
        function addAddress(address) {
        	address.user = vm.user;
        	
        	address.countryId = address.countryId.country_id;
        	address.country = address.addressCityId.countryState.country.country_name;
        	address.addressStateId = address.addressStateId.stateId;
        	address.addressState = address.addressCityId.countryState.stateName;
        	address.addressCity = address.addressCityId.cityName;
        	address.addressCityId = address.addressCityId.cityId;
        	
        	UserService.addNewUserAddress(address)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('New address added successfully.', false);
                	loadUserAddresses();
                }
            });
        	vm.isNewAddress = true;
        }
        
        function updateAddress(address) {
        	vm.isNewAddress = true;
        	
        	address.countryId = address.countryId.country_id;
        	address.country = address.addressCityId.countryState.country.country_name;
        	address.addressStateId = address.addressStateId.stateId;
        	address.addressState = address.addressCityId.countryState.stateName;
        	address.addressCity = address.addressCityId.cityName;
        	address.addressCityId = address.addressCityId.cityId;
        	
        	UserService.updateAddress(address)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Address updated successfully.', false);
                	loadUserAddresses();
                }
            });
        }
        
        $scope.deleteAddress = function (addressId) {
        	UserService.deleteAddress(addressId)
            .then(function (response) {
                if (response.data.success) {
                	FlashService.Success('Address deleted successfully');
                	loadUserAddresses();
                }
            });
        };
     
    }

})();