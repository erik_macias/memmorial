﻿(function() {
	'use strict';

	angular.module('app').controller('ContactController', ContactController);

	ContactController.$inject = [ 'UserService', '$scope', '$rootScope', '$location', 'FlashService'];
	function ContactController(UserService, $scope, $rootScope, $location, FlashService) {
		$scope.formData;
		
		$scope.submit = function() {
			UserService.sendFormUserComments($scope.formData)
            .then(function (response) {
                if (response.data.success) {
                	$location.path('/');
                	FlashService.Success('Thanks! We have received your message', true);
                } else {
                	FlashService.Error(response.data.message);
                }
            });
		}
	}

})();