﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AppSettingsService', AppSettingsService);

    AppSettingsService.$inject = ['$http'];
    function AppSettingsService($http) {
        var service = {};

        service.getAllKeyValues = getAllKeyValues;
        service.getKeyValue = getKeyValue;
        service.createKeyValue = createKeyValue;
        service.updateKeyValue = updateKeyValue;
        service.deleteKeyValue = deleteKeyValue;
        service.getReCaptchaClientKey = getReCaptchaClientKey;
        service.reBuildLuceneIndex = reBuildLuceneIndex;
        
        return service;

        function getAllKeyValues() {
            return $http.post('rest/admin/settings').then(handleSuccess, handleError('Error getting key values settings'));
        }
        
        function getKeyValue(key) {
            return $http.post('rest/admin/settings/key', key).then(handleSuccess, handleError('Error getting key value settings'));
        }
        
        function createKeyValue(appSetting) {
            return $http.post('rest/admin/settings/new', appSetting).then(handleSuccess, handleError('Error creating new key value'));
        }

        function updateKeyValue(appSetting) {
            return $http.put('rest/admin/settings', appSetting).then(handleSuccess, handleError('Error updating key value'));
        }
        
        function deleteKeyValue(key) {
            return $http.delete('rest/admin/settings/' + key).then(handleSuccess, handleError('Error deleting key and value'));
        }
        
        function getReCaptchaClientKey() {
            return $http.post('rest/admin/settings/recaptcha/client/key').then(handleSuccess, handleError('Error getting reCaptcha client key'));
        }
        
        function reBuildLuceneIndex() {
            return $http.post('rest/admin/settings/index/rebuild').then(handleSuccess, handleError('Error rebuilding Lucene Index'));
        }
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
