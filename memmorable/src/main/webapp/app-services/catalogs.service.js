﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('CatalogsService', CatalogsService);

    CatalogsService.$inject = ['$http'];
    function CatalogsService($http) {
        var service = {};

        service.getCountries = getCountries;
        service.getStates = getStates;
        service.getCities = getCities;
        service.getCountryDetails = getCountryDetails;
        service.createCountry = createCountry;
        service.updateCountry = updateCountry;
        service.getRoles = getRoles;
        service.getRoleDetails = getRoleDetails;
        service.updateRole = updateRole;
        service.getUserStatus = getUserStatus;
        service.getAddressTypes = getAddressTypes;
        service.getAddressTypeDetails = getAddressTypeDetails;
        service.createAddressType = createAddressType;
        service.updateAddressType = updateAddressType;
        service.getArtTypes = getArtTypes;
        service.getArtTypeDetails = getArtTypeDetails;
        service.createArtType = createArtType;
        service.updateArtType = updateArtType;
        service.getArtSubTypes = getArtSubTypes;
        service.getArtSubTypeDetails = getArtSubTypeDetails;
        service.createArtSubType = createArtSubType;
        service.updateArtSubType = updateArtSubType;
        service.getCurrencies = getCurrencies;
        service.getCurrencyDetails = getCurrencyDetails;
        service.createCurrency = createCurrency;
        service.updateCurrency = updateCurrency;
        
        service.getArtTypeFees = getArtTypeFees;
        service.getArtTypeFeeDetails = getArtTypeFeeDetails;
        service.createArtTypeFee = createArtTypeFee;
        service.updateArtTypeFee = updateArtTypeFee;
        service.deleteArtTypeFee = deleteArtTypeFee;
        
        return service;

        function getCountries() {
            return $http.post('rest/catalog/countries').then(handleSuccess, handleError('Error getting countries catalog'));
        }
        
        function getStates(country) {
            return $http.post('rest/catalog/states/', country).then(handleSuccess, handleError('Error getting states catalog'));
        }
        
        function getCities(state) {
            return $http.post('rest/catalog/cities/', state).then(handleSuccess, handleError('Error getting cities catalog'));
        }
        
        function getCountryDetails(countryId) {
            return $http.post('rest/catalog/country/', countryId).then(handleSuccess, handleError('Error getting country details'));
        }
        
        function createCountry(country) {
            return $http.post('rest/catalog/countries/create', country).then(handleSuccess, handleError('Error creating country in catalog'));
        }

        function updateCountry(country) {
            return $http.put('rest/catalog/countries', country).then(handleSuccess, handleError('Error updating country in catalog'));
        }
        
        function getRoles() {
            return $http.post('rest/catalog/roles').then(handleSuccess, handleError('Error getting roles catalog'));
        }
        
        function getRoleDetails(role) {
            return $http.post('rest/catalog/role/', role).then(handleSuccess, handleError('Error getting role details'));
        }
        
        function updateRole(role) {
            return $http.put('rest/catalog/roles', role).then(handleSuccess, handleError('Error updating role in catalog'));
        }
        
        function getUserStatus() {
            return $http.post('rest/catalog/userstatus').then(handleSuccess, handleError('Error getting user status catalog'));
        }
        
        function getAddressTypes() {
            return $http.post('rest/catalog/addresstypes').then(handleSuccess, handleError('Error getting address types catalog'));
        }
        
        function getAddressTypeDetails(addressTypeId) {
            return $http.post('rest/catalog/addresstype/', addressTypeId).then(handleSuccess, handleError('Error getting address type details'));
        }
        
        function createAddressType(addressType) {
            return $http.post('rest/catalog/addresstypes/create', addressType).then(handleSuccess, handleError('Error creating address type in catalog'));
        }

        function updateAddressType(addressType) {
            return $http.put('rest/catalog/addresstypes', addressType).then(handleSuccess, handleError('Error updating address type in catalog'));
        }
        
        function getArtTypes() {
            return $http.post('rest/catalog/arttypes').then(handleSuccess, handleError('Error getting art types catalog'));
        }
        
        function getArtTypeDetails(artTypeId) {
            return $http.post('rest/catalog/arttype/', artTypeId).then(handleSuccess, handleError('Error getting art types catalog'));
        }
        
        function createArtType(artType) {
            return $http.post('rest/catalog/arttypes/create', artType).then(handleSuccess, handleError('Error creating new art type in catalog'));
        }

        function updateArtType(artType) {
            return $http.put('rest/catalog/arttypes', artType).then(handleSuccess, handleError('Error updating art type in catalog'));
        }
        
        function getArtSubTypes() {
            return $http.post('rest/catalog/artsubtypes').then(handleSuccess, handleError('Error getting art subtypes catalog'));
        }
        
        function getArtSubTypeDetails(artSubtTypeId) {
            return $http.post('rest/catalog/artsubtype/', artSubtTypeId).then(handleSuccess, handleError('Error getting Art subtype details'));
        }
        
        function createArtSubType(artSubType) {
            return $http.post('rest/catalog/artsubtypes/create', artSubType).then(handleSuccess, handleError('Error creating new art subtype in catalog'));
        }

        function updateArtSubType(artSubType) {
            return $http.put('rest/catalog/artsubtypes', artSubType).then(handleSuccess, handleError('Error updating art subtypes in catalog'));
        }
        
        function getCurrencies() {
			return $http.post('rest/catalog/currencies/').then(handleSuccess, handleError('Error getting currency catalog'));
		}
        
        function getCurrencyDetails(currencyId) {
            return $http.post('rest/catalog/currency/', currencyId).then(handleSuccess, handleError('Error getting currency details'));
        }
        
        function createCurrency(currency) {
            return $http.post('rest/catalog/currencies/create', currency).then(handleSuccess, handleError('Error creating new currency in catalog'));
        }

        function updateCurrency(currency) {
            return $http.put('rest/catalog/currencies', currency).then(handleSuccess, handleError('Error updating currency in catalog'));
        }
        
        function getArtTypeFees() {
            return $http.post('rest/catalog/arttypefees').then(handleSuccess, handleError('Error getting art type fees catalog'));
        }
        
        function getArtTypeFeeDetails(artTypeFeeId) {
            return $http.post('rest/catalog/arttypefee/', artTypeFeeId).then(handleSuccess, handleError('Error getting art types fee catalog'));
        }
        
        function createArtTypeFee(artTypeFee) {
            return $http.post('rest/catalog/arttypefees/create', artTypeFee).then(handleSuccess, handleError('Error creating new art type fee in catalog'));
        }

        function updateArtTypeFee(artTypeFee) {
            return $http.put('rest/catalog/arttypefees', artTypeFee).then(handleSuccess, handleError('Error updating art type fee in catalog'));
        }
        
        function deleteArtTypeFee(artTypeFee) {
        	return $http.delete('rest/catalog/arttypefees', artTypeFee).then(handleSuccess, handleError('Error deleting art type fee in catalog'));
        }
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
