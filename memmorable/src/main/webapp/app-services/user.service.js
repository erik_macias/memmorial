﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetPersonalByUser = GetPersonalByUser;
        service.UpdatePersonal = UpdatePersonal;
        service.changePassword = changePassword;
        service.getProfile = getProfile;
        service.getMembershipInfo = getMembershipInfo;
        service.editProfile = editProfile;
        service.editProfilePic = editProfilePic;
        service.verifyUserEmail = verifyUserEmail;
        service.pwdResetForm = pwdResetForm;
        service.pwdResetByLink = pwdResetByLink;
        service.getUserFromUrlParamEncoded = getUserFromUrlParamEncoded;
        service.getUserAddresses = getUserAddresses;
        service.addNewUserAddress = addNewUserAddress;
        service.updateAddress = updateAddress;
        service.deleteAddress = deleteAddress;
        service.getUserComments = getUserComments;
		service.addUserComments = addUserComments;
		service.sendFormUserComments = sendFormUserComments;
		service.deleteUserComments = deleteUserComments;
		service.certifyUser = certifyUser;
		service.removeCertifyUser = removeCertifyUser;
		service.registerCertPayment = registerCertPayment;
        
        return service;

        function GetAll() {
            return $http.get('rest/api/users').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.get('rest/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }

        function GetByUsername(currentUser) {
            return $http.post('rest/api/user/', currentUser).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Create(user) {
            return $http.post('rest/api/create', user).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.put('rest/api/user/', user).then(handleSuccess, handleError('Error updating user'));
        }
        
        function Delete(username) {
            return $http.delete('rest/api/user/' + username).then(handleSuccess, handleError('Error deleting user'));
        }
        
        function verifyUserEmail(urlParam) {
            return $http.get('rest/api/verify/' + urlParam).then(handleSuccess, handleError('Error activating user'));
        }
        
        function pwdResetForm(email) {
            return $http.post('rest/api/pwdForgot/', email).then(handleSuccess, handleError('Error reseting user password'));
        }
        
        function pwdResetByLink(user) {
            return $http.post('rest/api/pwdReset/', user).then(handleSuccess, handleError('Error reseting user password from Email link'));
        }
        
        function getUserFromUrlParamEncoded(urlParam) {
            return $http.get('rest/api/pwdReset/' + urlParam).then(handleSuccess, handleError('Error getting user from url encoded link'));
        }

        function GetPersonalByUser(currentUser) {
            return $http.post('rest/api/personalByName',currentUser).then(handleSuccess, handleError('Error creating user'));
        }
        
        function UpdatePersonal(personal) {
        	return $http.post('rest/api/updatePersonal',personal).then(handleSuccess, handleError('Error updating user'));
        }
        function changePassword(passwordChange) {
        	return $http.post('rest/api/changePassword',passwordChange).then(handleSuccess, handleError('Error updating password'));
        }
        function getProfile(user) {
        	return $http.post('rest/pro/getProfile',user).then(handleSuccess, handleError('Error getting profile info'));
        }
        function getMembershipInfo(user) {
        	return $http.post('rest/api/user/membership',user).then(handleSuccess, handleError('Error while getting membership information'));
        }
        function editProfile(editProfile) {
        	return $http.post('rest/pro/updateProfile',editProfile).then(handleSuccess, handleError('Error updating password'));
        }
        function editProfilePic(editProfilePic) {
        	return $http.post('rest/pro/updateProfilePicture',editProfilePic).then(handleSuccess, handleError('Error updating password'));
        }
        function getUserAddresses(currentUser) {
            return $http.post('rest/api/addresses', currentUser).then(handleSuccess, handleError('Error retrieven user registered addresses'));
        }
        function addNewUserAddress(userAddress) {
            return $http.post('rest/api/user/address/', userAddress).then(handleSuccess, handleError('Error adding user address'));
        }
        function updateAddress(userAddress) {
            return $http.put('rest/api/user/address/', userAddress).then(handleSuccess, handleError('Error updating user address'));
        }
        function deleteAddress(addressId) {
            return $http.delete('rest/api/user/address/' + addressId).then(handleSuccess, handleError('Error deleting user address'));
        }
        function getUserComments(username) {
			return $http.post('rest/api/user/comments/', username).then(handleSuccess, handleError('Error getting user comments.'));
		}
		function addUserComments(userComments) {
			return $http.put('rest/api/user/comments/', userComments).then(handleSuccess, handleError('Error adding user comments.'));
		}
		function deleteUserComments(userCommentId) {
            return $http.delete('rest/api/user/comments/' + userCommentId).then(handleSuccess, handleError('Error deleting user comment'));
        }
		function sendFormUserComments(formContact) {
			return $http.post('rest/api/user/form/contact', formContact).then(handleSuccess, handleError('Error sending user contact form comment'));
		}
		function certifyUser(username) {
			return $http.post('rest/api/user/certificate', username).then(handleSuccess, handleError('Error while trying to certify user'));
		}
		function removeCertifyUser(username) {
			return $http.delete('rest/api/user/certificate/' + username).then(handleSuccess, handleError('Error while trying to remove certification for user'));
		}
		function registerCertPayment(username) {
			return $http.post('rest/api/user/payment/register/', username).then(handleSuccess, handleError('Error while registering user certificate payment'));
		}

        // private functions

        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
