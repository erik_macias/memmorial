﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('SearchService', SearchService);

    SearchService.$inject = ['$http'];
    function SearchService($http) {
        var service = {};

        service.FindFreeText = FindFreeText;
        service.FindByArtId = FindByArtId;
        service.findCaruselArt = findCaruselArt;
        service.FindEditByArtId = FindEditByArtId;
        
        return service;
        
        function FindFreeText(urlParams) {
            return $http.get('rest/api/search/' + urlParams.text +'/'+ urlParams.maxresults +'/'+ urlParams.firstresult+'/'+ urlParams.currentUser)
            	.then(handleSuccess, handleError('Error while searching for art'));
        }
        
        function FindByArtId(urlParam) {
            return $http.post('rest/api/searchdetails/', urlParam).then(handleSuccess, handleError('Error while searching for art details'));
        }
        
        function FindEditByArtId(urlParam) {
            return $http.post('rest/api/edit/details/', urlParam).then(handleSuccess, handleError('Error while searching for art details'));
        }
        
        function findCaruselArt() {
        	return $http.post('rest/api/searchcarusel/').then(handleSuccess, handleError('Error while searching for carusel details'));
        }

        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
