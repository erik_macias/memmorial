﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('ReportsService', ReportsService);

    ReportsService.$inject = ['$http'];
    function ReportsService($http) {
        var service = {};

        service.getUserVisits = getUserVisits;
        service.getCurrentYearSales = getCurrentYearSales;
        service.getLastYearSales = getLastYearSales;
        service.getAdmnCurrentYearSales = getAdmnCurrentYearSales;
        service.getAdmnLastYearSales = getAdmnLastYearSales;
        service.getAdmnRangeSales = getAdmnRangeSales;
        service.getAdmnCurrentYearFee = getAdmnCurrentYearFee;
        service.getAdmnCustomFees = getAdmnCustomFees;
        service.getAdmnFeePending = getAdmnFeePending;
        service.getCreatedUsers = getCreatedUsers;
        
        return service;
        
        function getUserVisits(user) {
            return $http.get('rest/api/reports/user/visits/' + user)
            	.then(handleSuccess, handleError('Error while getting user visits'));
        }
        
        function getCurrentYearSales(user) {
            return $http.get('rest/api/reports/user/sales/current/' + user)
            	.then(handleSuccess, handleError('Error while getting user current year sales'));
        }
        
        function getLastYearSales(user) {
            return $http.get('rest/api/reports/user/sales/last/' + user)
            	.then(handleSuccess, handleError('Error while getting user current vs last year sales'));
        }
        
        function getAdmnCurrentYearSales() {
            return $http.get('rest/api/reports/admin/sales/current/')
            	.then(handleSuccess, handleError('Error while getting user current year sales'));
        }
        
        function getAdmnLastYearSales() {
            return $http.get('rest/api/reports/admin/sales/last/')
            	.then(handleSuccess, handleError('Error while getting user last year sales'));
        }
        
        function getAdmnRangeSales(rangesObj) {
            return $http.post('rest/api/reports/admin/sales/range/', rangesObj)
            	.then(handleSuccess, handleError('Error while getting user range year sales'));
        }
        
        function getAdmnCurrentYearFee() {
            return $http.get('rest/api/reports/admin/fee/current/')
            	.then(handleSuccess, handleError('Error while getting user current year fees paid'));
        }
        
        function getAdmnCustomFees(rangesObj) {
            return $http.post('rest/api/reports/admin/fee/range/', rangesObj)
            	.then(handleSuccess, handleError('Error while getting user range year fees'));
        }
        
        function getAdmnFeePending() {
            return $http.get('rest/api/reports/admin/fee/pending/')
        	.then(handleSuccess, handleError('Error while getting user pending paid fees'));
        }
        
        function getCreatedUsers() {
            return $http.get('rest/api/reports/admin/users/created/')
        	.then(handleSuccess, handleError('Error while getting user created'));
        }
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
