﻿(function() {
	'use strict';

	angular.module('app').factory('ArtService', ArtService);

	ArtService.$inject = [ '$http' ];
	function ArtService($http) {
		var service = {};

		service.getTypes = getTypes;
		service.getCurrency = getCurrency;
		service.getSubTypes = getSubTypes;
		service.getMedia = getMedia;
		service.saveArt = saveArt;
		service.saveMedia = saveMedia;
		service.getArtComments = getArtComments;
		service.addArtComments = addArtComments;
		service.deleteArtComments = deleteArtComments;
		service.getArtRatingByUser = getArtRatingByUser;
		service.rateArtByUser = rateArtByUser;
		service.getArtByUser = getArtByUser;

		return service;

		function getTypes() {
			return $http.post('rest/art/getTypes/').then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getCurrency() {
			return $http.post('rest/art/getCurrency/').then(handleSuccess,
					handleError('Error getting user by id'));
		}

	}

	// private functions

	function handleSuccess(data) {
		return data;
	}

	function handleError(error) {
		return function() {
			return {
				success : false,
				message : error
			};
		};
	}
})();
