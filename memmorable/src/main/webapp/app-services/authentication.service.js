﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService', 'AclService'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService, AclService) {
        var service = {};

        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;

        return service;

        function Login(username, password, callback) {

            $http.post('rest/api/authenticate', {username:username,password:password})
                .success(function (response) {
                    callback(response);
                });

        }

        function SetCredentials(response) {
            var authdata = response.authData;
          

            $rootScope.globals = {
                currentUser: {
                    username: response.user_name,
                    name: response.name,
                    lastname: response.lastname,
                    authdata: authdata
                }
            };

            $http.defaults.headers.common['Authorization'] = authdata;
            $cookieStore.put('globals', $rootScope.globals);
            
           	// Get the user role, and add it to AclService
           	var userRole = JSON.parse(atob(authdata)).roleToken;
           	if (userRole === 'ADMIN') {
           		userRole = userRole.toLowerCase();
            }else{
            	userRole = 'member';
            }
           	AclService.attachRole(userRole);
        }

        function ClearCredentials() {
            $rootScope.globals = {};
            $rootScope.inboxList = null;
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = '';
            AclService.flushRoles();
            AclService.attachRole('guest');
        }
    }


})();