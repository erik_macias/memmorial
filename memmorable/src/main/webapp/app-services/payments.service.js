﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('PaymentsService', PaymentsService);

    PaymentsService.$inject = ['$http'];
    function PaymentsService($http) {
        var service = {};

        service.getPaymentPlans = getPaymentPlans;
        service.getPaymentPlanDetail = getPaymentPlanDetail;
        service.getPaymentPlanId = getPaymentPlanId;
        service.getPaymentCreatePlanTemplate = getPaymentCreatePlanTemplate;
        service.createNewPaypalBillingPlan = createNewPaypalBillingPlan;
        service.createPaypalBillingPlanAgrmnt = createPaypalBillingPlanAgrmnt;
        service.activatePaymentPlan = activatePaymentPlan;
        service.deletePaymentPlan = deletePaymentPlan;
        service.execPaymentPlanAgrmnt = execPaymentPlanAgrmnt;
        service.updateUserMembership = updateUserMembership;
        
        service.validateArtFeePayment = validateArtFeePayment;
        service.execArtFeePayment = execArtFeePayment;
        service.confirmArtFeePayment = confirmArtFeePayment;
        service.validateArtGoodsPayment = validateArtGoodsPayment;
        service.execArtPayment = execArtPayment;
        service.confirmArtPayment = confirmArtPayment;
        service.getArtTypeFeeDetUsr = getArtTypeFeeDetUsr;
        service.activateArtWithNoFee = activateArtWithNoFee;
        service.execArtBankDepositPayment = execArtBankDepositPayment;
        service.execArtBankTransferPayment = execArtBankTransferPayment;
        service.hasUserBoughtToSeller = hasUserBoughtToSeller;
        service.execArtFeeCreditPayment = execArtFeeCreditPayment;
        
        return service;

        function getPaymentPlans() {
            return $http.post('rest/paypal/plans').then(handleSuccess, handleError('Error getting Paypal Billing Plans catalog'));
        }
        
        function getPaymentPlanDetail(plan) {
            return $http.post('rest/paypal/plan/', plan).then(handleSuccess, handleError('Error getting Paypal Billing Plan details'));
        }
        
        function getPaymentCreatePlanTemplate() {
            return $http.post('rest/paypal/plan/create/template').then(handleSuccess, handleError('Error getting Paypal Billing Plan template'));
        }
        
        function createNewPaypalBillingPlan(billingPlan) {
            return $http.post('rest/paypal/plan/create', billingPlan).then(handleSuccess, handleError('Error creating new Paypal Billing Plan'));
        }
        
        function createPaypalBillingPlanAgrmnt(plan) {
            return $http.post('rest/paypal/plan/agreement/create', plan).then(handleSuccess, handleError('Error creating Paypal Billing Plan Agreement'));
        }
        
        function activatePaymentPlan(plan) {
            return $http.post('rest/paypal/plan/activate', plan).then(handleSuccess, handleError('Error activating Paypal Billing Plan'));
        }
        
        function deletePaymentPlan(planId) {
            return $http.delete('rest/paypal/plan/' + planId).then(handleSuccess, handleError('Error deleting Paypal Billing Plan'));
        }
        
        function getPaymentPlanId(role) {
            return $http.post('rest/paypal/plan/id', role).then(handleSuccess, handleError('Error getting Paypal Billing Plan Id'));
        }
        
        function execPaymentPlanAgrmnt(token) {
            return $http.post('rest/paypal/plan/agreement/exec', token).then(handleSuccess, handleError('Error while executing Paypal Billing Plan Agreement'));
        }
        
        function updateUserMembership(membership) {
            return $http.post('rest/paypal/plan/user/activate', membership).then(handleSuccess, handleError('Error while upgrating user membership'));
        }
        
        function validateArtFeePayment(art) {
        	return $http.post('rest/paypal/good/fee/validate', art).then(handleSuccess, handleError('Error while Validating Paypal Goods Fee Payment'));
        }

        function execArtFeePayment(artId) {
        	return $http.post('rest/paypal/good/fee/exec', artId).then(handleSuccess, handleError('Error while executing Paypal Goods Fee Payment'));
        }
        
        function execArtFeeCreditPayment(artId) {
        	return $http.post('rest/paypal/good/fee/credit', artId).then(handleSuccess, handleError('Error while executingGoods Fee Credit'));
        }
        
        function confirmArtFeePayment(art) {
        	return $http.post('rest/paypal/good/fee/conf', art).then(handleSuccess, handleError('Error while Confirming Paypal Goods Fee Payment'));
        }
        
        function validateArtGoodsPayment(art) {
        	return $http.post('rest/paypal/good/validate', art).then(handleSuccess, handleError('Error while Validating Paypal Goods Payment'));
        }
        
        function execArtPayment(artId) {
        	return $http.post('rest/paypal/good/exec', artId).then(handleSuccess, handleError('Error while executing Paypal Goods Payment'));
        }
        
        function confirmArtPayment(art) {
        	return $http.post('rest/paypal/good/conf', art).then(handleSuccess, handleError('Error while Confirming Paypal Goods Payment'));
        }
        
        function getArtTypeFeeDetUsr(artTypeWithUsr) {
            return $http.post('rest/catalog/arttypefeeusr/', artTypeWithUsr).then(handleSuccess, handleError('Error getting art types fee catalog'));
        }
        
        function activateArtWithNoFee(art) {
            return $http.post('rest/paypal/good/nofee/activate', art).then(handleSuccess, handleError('Error while activating art'));
        }
        
        function execArtBankDepositPayment(artId) {
        	return $http.post('rest/paypal/good/exec/bank/deposit', artId).then(handleSuccess, handleError('Error while executing Bank Deposit Goods Payment'));
        }
        
        function execArtBankTransferPayment(artId) {
        	return $http.post('rest/paypal/good/exec/bank/transfer', artId).then(handleSuccess, handleError('Error while executing Bank Transfer Goods Payment'));
        }
        
        function hasUserBoughtToSeller(usersBought) {
        	return $http.post('rest/api/user/bought/', usersBought).then(handleSuccess, handleError('Error while Payment Profiles'));
        }

        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
