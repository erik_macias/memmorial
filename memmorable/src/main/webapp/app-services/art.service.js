﻿
(function() {
	'use strict';

	angular.module('app').factory('ArtService', ArtService);

	ArtService.$inject = [ '$http' ];
	function ArtService($http) {
		var service = {};

		service.getTypes = getTypes;
		service.getCurrency = getCurrency;
		service.getSubTypes = getSubTypes;
		service.getMedia = getMedia;
		service.saveArt = saveArt;
		service.updateArt = updateArt;
		service.saveMedia = saveMedia;
		service.getArtComments = getArtComments;
		service.addArtComments = addArtComments;
		service.deleteArtComments = deleteArtComments;
		service.getArtRatingByUser = getArtRatingByUser;
		service.rateArtByUser = rateArtByUser;
		service.getArtByUser = getArtByUser;
		service.getPurchasedArtByUser = getPurchasedArtByUser;
		service.getMediaRules = getMediaRules;
		service.isUploadAvailable = isUploadAvailable;
		service.getArtRules = getArtRules;
		service.getServices = getServices;
		service.getGenres = getGenres;
		service.getFormats = getFormats;
		service.deleteMedia = deleteMedia;
		service.removePrimaryAll = removePrimaryAll;
		service.setPrimaryMedia = setPrimaryMedia;
		service.getAllArtList = getAllArtList;
		service.activateArt = activateArt;
		service.deleteArt = deleteArt;
		service.isAbleToPublish = isAbleToPublish;
		service.registerArtPayment = registerArtPayment;

		return service;

		function getTypes() {
			return $http.post('rest/art/getTypes/').then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getCurrency() {
			return $http.post('rest/art/getCurrency/').then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getSubTypes(type) {
			return $http.post('rest/art/getSubTypes/' , type).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getServices(type) {
			return $http.post('rest/art/getServices/' , type).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getGenres(type) {
			return $http.post('rest/art/getGenres/' , type).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getFormats(type) {
			return $http.post('rest/art/getFormats/' , type).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getMedia(art) {
			return $http.post('rest/art/getMedia/' , art).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function deleteMedia(id) {
			return $http.post('rest/art/deleteMedia/' , id).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function removePrimaryAll(id) {
			return $http.post('rest/art/removePrimaryAll/' , id).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function setPrimaryMedia(id) {
			return $http.post('rest/art/setPrimaryMedia/' , id).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function saveArt(art) {
			return $http.post('rest/art/saveArt',art).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function updateArt(art) {
			return $http.post('rest/art/updateArt',art).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function saveMedia(media) {
			return $http.post('rest/art/saveMedia',media).then(handleSuccess,
					handleError('Error getting user by id'));
		}
		function getArtComments(artId) {
			return $http.post('rest/art/comments/', artId).then(handleSuccess, handleError('Error getting art comments.'));
		}
		function addArtComments(artComments) {
			return $http.put('rest/art/comments/', artComments).then(handleSuccess, handleError('Error adding art comments.'));
		}
		function deleteArtComments(artCommentId) {
            return $http.delete('rest/art/comments/' + artCommentId).then(handleSuccess, handleError('Error deleting art comment'));
        }
		function getArtRatingByUser(artRateUser) {
			return $http.post('rest/art/rating/', artRateUser).then(handleSuccess, handleError('Error getting art rating.'));
		}
		function rateArtByUser(artRating) {
			return $http.put('rest/art/rating/', artRating).then(handleSuccess, handleError('Error adding art rating.'));
		}
		function getArtByUser(currentUser) {
			return $http.post('rest/art/userart/', currentUser).then(handleSuccess, handleError('Error getting user art.'));
		}
		function getPurchasedArtByUser(currentUser) {
			return $http.post('rest/art/userart/purchased', currentUser).then(handleSuccess, handleError('Error getting user purchased art.'));
		}
		function getArtRules(currentUser) {
			return $http.post('rest/artRules/getArtRules/', artRateUser).then(handleSuccess, handleError('Error getting art rules.'));
		}
		function getMediaRules(mediaRequest) {
			return $http.post('rest/artRules/getMediaRules/', mediaRequest).then(handleSuccess, handleError('Error adding media rules.'));
		}
		function isUploadAvailable(currentUser) {
			return $http.post('rest/artRules/isAbleToUpload/', currentUser).then(handleSuccess, handleError('Error getting boolean.'));
		}
		function getAllArtList() {
			return $http.post('rest/art/list/').then(handleSuccess, handleError('Error getting art list.'));
		}
		function deleteArt(artId) {
            return $http.delete('rest/art/delete/' + artId).then(handleSuccess, handleError('Error deleting art'));
        }
		function activateArt(artRequest) {
			return $http.post('rest/art/activate/', artRequest).then(handleSuccess, handleError('Error activating art.'));
		}
		function registerArtPayment(artRequest) {
			return $http.post('rest/art/payment/register/', artRequest).then(handleSuccess, handleError('Error while registering art payment.'));
		}
		function isAbleToPublish(username) {
			return $http.post('rest/art/publishallowed', username).then(handleSuccess, handleError('Error while trying to verify publish for user'));
		}
	}

	// private functions

	function handleSuccess(data) {
		return data;
	}

	function handleError(error) {
		return function() {
			return {
				success : false,
				message : error
			};
		};
	}
})();
