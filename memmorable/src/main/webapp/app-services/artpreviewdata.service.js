﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('PreviewService', PreviewService);

    PreviewService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService', 'AclService'];
    function PreviewService($http, $cookieStore, $rootScope, $timeout, UserService, AclService) {
    	var vm = this;
    	var preview = {};
        vm.data = ''; 
        preview.setArtData = setArtData;
        preview.getArtData = getArtData;
        return preview;

        function setArtData(Data) {
            vm.data = Data;   
        }
        
        function getArtData() {
            if (vm.data === null){
            	return vm.data; 
            }
        }
        
        function clearArtData(){
        	vm.data=null;
        }
        

    }
})();