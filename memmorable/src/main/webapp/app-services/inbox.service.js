﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('InboxService', InboxService);

    InboxService.$inject = ['$http'];
    function InboxService($http) {
        var service = {};

        service.getAllByStatusAndUser = getAllByStatusAndUser;
        service.getAllUnreadByStatusAndUser = getAllUnreadByStatusAndUser;
        service.sendNewMessageToUser = sendNewMessageToUser;
        service.getByInboxMessageId = getByInboxMessageId;
        service.deleteMsg = deleteMsg;
        
        return service;

        function getAllByStatusAndUser(user) {
            return $http.post('rest/inbox/all', user).then(handleSuccess, handleError('Error getting all inbox messages'));
        }
        
        function getAllUnreadByStatusAndUser(user) {
            return $http.post('rest/inbox/unread', user).then(handleSuccess, handleError('Error getting unread inbox messages'));
        }
        
        function sendNewMessageToUser(inboxMsg) {
        	return $http.post('rest/inbox/new', inboxMsg).then(handleSuccess, handleError('Error sending new inbox messages'));
        }
        
        function getByInboxMessageId(msgId) {
        	return $http.post('rest/inbox/message', msgId).then(handleSuccess, handleError('Error getting unread inbox message'));
        }
        
        function deleteMsg(msgId) {
            return $http.delete('rest/inbox/message/' + msgId).then(handleSuccess, handleError('Error deleting message'));
        }
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
