﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('FlashService', FlashService);

    FlashService.$inject = ['$rootScope', '$anchorScroll'];
    function FlashService($rootScope, $anchorScroll) {
        var service = {};

        service.Success = Success;
        service.Warning = Warning;
        service.Error = Error;

        initService();

        return service;

        function initService() {
            $rootScope.$on('$locationChangeStart', function () {
                clearFlashMessage();
            });

            function clearFlashMessage() {
                var flash = $rootScope.flash;
                if (flash) {
                    if (!flash.keepAfterLocationChange) {
                        delete $rootScope.flash;
                    } else {
                        // only keep for a single location change
                        flash.keepAfterLocationChange = false;
                    }
                }
            }
        }

        function Success(message, keepAfterLocationChange) {
            $rootScope.flash = {
                message: message,
                type: 'success', 
                keepAfterLocationChange: keepAfterLocationChange
            };
            $anchorScroll();
        }
        
        function Warning(message, keepAfterLocationChange) {
            $rootScope.flash = {
                message: message,
                type: 'warning',
                keepAfterLocationChange: keepAfterLocationChange
            };
            $anchorScroll();
        }

        function Error(message, keepAfterLocationChange) {
            $rootScope.flash = {
                message: message,
                type: 'error',
                keepAfterLocationChange: keepAfterLocationChange
            };
            $anchorScroll();
        }
    }

})();