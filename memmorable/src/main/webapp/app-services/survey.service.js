﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('SurveyService', SurveyService);

    SurveyService.$inject = ['$http'];
    function SurveyService($http) {
        var service = {};

        service.getSurveyById = getSurveyById;
        service.getSurveyByArtId = getSurveyByArtId;
        service.getSurveyByFromUser = getSurveyByFromUser;
        service.getSurveyByToUser = getSurveyByToUser;
        service.getSurveyByStatus = getSurveyByStatus;
        service.getSurveyByStatusAndFromUser = getSurveyByStatusAndFromUser;
        service.getSurveyByStatusAndToUser = getSurveyByStatusAndToUser;
        service.sendSurvey = sendSurvey;
        service.isSurveysPending = isSurveysPending;
        
        return service;
        
        function getSurveyById(id) {
            return $http.get('rest/api/survey/find/id/' + id)
            	.then(handleSuccess, handleError('Error while searching for survey'));
        }
        
        function getSurveyByArtId(id) {
            return $http.get('rest/api/survey/find/art/' + id)
            	.then(handleSuccess, handleError('Error while searching for survey by art'));
        }
        
        function getSurveyByFromUser(username) {
            return $http.get('rest/api/survey/find/from/' + username)
            	.then(handleSuccess, handleError('Error while searching for survey from user'));
        }
        
        function getSurveyByToUser(username) {
            return $http.get('rest/api/survey/find/to/' + username)
            	.then(handleSuccess, handleError('Error while searching for survey to user'));
        }
        
        function getSurveyByStatus(status) {
            return $http.get('rest/api/survey/find/status/' + status)
            	.then(handleSuccess, handleError('Error while searching for survey by status'));
        }
        
        function getSurveyByStatusAndFromUser(urlParam) {
            return $http.get('rest/api/survey/find/from/' + urlParam.username + '/status/' +  urlParam.status)
            	.then(handleSuccess, handleError('Error while searching for survey from user by status'));
        }
        
        function getSurveyByStatusAndToUser(urlParam) {
            return $http.get('rest/api/survey/find/to/' + urlParam.username + '/status/' +  urlParam.status)
            	.then(handleSuccess, handleError('Error while searching for survey to user by status'));
        }
        
        function sendSurvey(survey) {
            return $http.put('rest/api/survey/send', survey).then(handleSuccess, handleError('Error while sending survey'));
        }
        
        function isSurveysPending(username) {
            return $http.get('rest/api/survey/expired/from/' + username)
            	.then(handleSuccess, handleError('Error while searching for survey from user'));
        }
        
        // private functions
        function handleSuccess(data) {
        	return data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
