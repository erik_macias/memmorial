package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class GenericEntityListResponse<T> extends GenericResponse {
	
	public List<T> entityList;

}
