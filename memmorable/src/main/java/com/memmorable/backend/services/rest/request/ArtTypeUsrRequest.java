package com.memmorable.backend.services.rest.request;

public class ArtTypeUsrRequest {
	
	private String username;
	private String artType;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getArtType() {
		return artType;
	}
	public void setArtType(String artType) {
		this.artType = artType;
	}
	
}
