package com.memmorable.backend.services.rest.request;

import java.util.List;
import java.util.Map;

import com.memmorable.backend.services.rest.responses.ArtMediaResponse;

public class ArtRequest {
	private int art_id;
	private String art_Name;
	private String art_Description;
	private Map<Integer, String> art_tag;
	private List<ArtMediaResponse> media;
	private String art_type;
	private String art_subtype;

	public int getArt_id() {
		return art_id;
	}

	public void setArt_id(int art_id) {
		this.art_id = art_id;
	}

	public String getArt_Name() {
		return art_Name;
	}

	public void setArt_Name(String art_Name) {
		this.art_Name = art_Name;
	}

	public String getArt_Description() {
		return art_Description;
	}

	public void setArt_Description(String art_Description) {
		this.art_Description = art_Description;
	}

	public Map<Integer, String> getArt_tag() {
		return art_tag;
	}

	public void setArt_tag(Map<Integer, String> art_tag) {
		this.art_tag = art_tag;
	}

	public List<ArtMediaResponse> getMedia() {
		return media;
	}

	public void setMedia(List<ArtMediaResponse> media) {
		this.media = media;
	}

	public String getArt_type() {
		return art_type;
	}

	public void setArt_type(String art_type) {
		this.art_type = art_type;
	}

	public String getArt_subtype() {
		return art_subtype;
	}

	public void setArt_subtype(String art_subtype) {
		this.art_subtype = art_subtype;
	}

}
