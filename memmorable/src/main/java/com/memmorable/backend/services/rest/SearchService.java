package com.memmorable.backend.services.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.ArtAdministrationDTO;
import com.memmorable.backend.orm.dto.ArtSearchResultDTO;
import com.memmorable.backend.orm.dto.ArtSummaryDTO;
import com.memmorable.backend.services.rest.request.ArtDetailsRequest;
import com.memmorable.backend.services.rest.responses.ArtSearchDetailsResultsResponse;
import com.memmorable.backend.services.rest.responses.ArtSearchResultsResponse;
import com.memmorable.backend.services.rest.responses.ArtSummaryResponse;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SearchService {
	ArtAdministrationDTO artDTO = new ArtAdministrationDTO();

	@GET
	@PermitAll
	@Path("/search/{text}/{maxresults}/{firstresult}/{currentUser}")
	public ArtSearchResultsResponse search(@PathParam("text") String searchText, 
			@PathParam("maxresults") int maxResults, 
			@PathParam("firstresult") int firstResult,
			@PathParam("currentUser") String currentUser) {
		ArtSearchResultsResponse response = new ArtSearchResultsResponse();
		SearchResultEntityList<ArtSearchResultDTO> artSearchResultList = artDTO.searchArtByFreeText(searchText, maxResults, firstResult);
		List<ArtSearchResultDTO> artList = artSearchResultList.getEntityList();
		if (!artList.isEmpty()) {
			response.setSuccess(true);
			response.setMessage("Search Result success");
			response.setSearchResultCount(artSearchResultList.getResultCount());
			response.setArtEntityList(artList);
			response.setSurveysPending(artDTO.isUserSurveysPending(currentUser));
		} else {
			response.setSuccess(false);
			response.setMessage("Search Result Empty");
		}
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/searchdetails")
	public ArtSearchDetailsResultsResponse searchDetails(ArtDetailsRequest artReq) {
		ArtSearchDetailsResultsResponse response = new ArtSearchDetailsResultsResponse();
		
		ArtSearchResultDTO artResultDTO = artDTO.getArtDetailsById(artReq);
		if (artResultDTO != null) {
			response.setSuccess(true);
			response.setMessage("Search Details Result success");
			response.setArtDetailsDTO(artResultDTO);
			response.setSurveysPending(artDTO.isUserSurveysPending(artReq.getCurrentUser()));
		} else {
			response.setSuccess(false);
			response.setMessage("Search Details Result Empty");
		}

		return response;
	}
	
	@POST
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/edit/details")
	public ArtSearchDetailsResultsResponse editArtDetails(ArtDetailsRequest art) {
		ArtSearchDetailsResultsResponse response = new ArtSearchDetailsResultsResponse();
		
		ArtSearchResultDTO artResultDTO = artDTO.getArtDetailsToEditById(art);
		if (artResultDTO != null) {
			response.setSuccess(true);
			response.setMessage("Details Result success");
			response.setArtDetailsDTO(artResultDTO);
		} else {
			response.setSuccess(false);
			response.setMessage("You are not allowed to edit the art requested");
		}
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/searchcarusel")
	public ArtSummaryResponse searchcarousel() {
		ArtSummaryResponse response = new ArtSummaryResponse();
		
		List<ArtSummaryDTO> artSummary = artDTO.getTopTenNewArtDetails();
		if (!artSummary.isEmpty()) {
			response.setSuccess(true);
			response.setMessage("Art Symmary result is success");
			response.setArtSummary(artSummary);
		} else {
			response.setSuccess(false);
			response.setMessage("Art Summary result is Empty");
		}
		return response;
	}

}
