package com.memmorable.backend.services.rest.responses;

import java.util.Date;

public class UserCommentsResponse extends GenericResponse {
	private int userCommentId;
	private String createdBy;
	private Date createdDate;
	private String comment;
	
	public int getUserCommentId() {
		return userCommentId;
	}
	public void setUserCommentId(int userCommentId) {
		this.userCommentId = userCommentId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
