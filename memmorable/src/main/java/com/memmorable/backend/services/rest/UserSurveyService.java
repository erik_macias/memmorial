package com.memmorable.backend.services.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.UserSurveyAdministrationDTO;
import com.memmorable.backend.orm.dto.UserSurveyDTO;
import com.memmorable.backend.services.rest.request.SurveyUpdateRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityListResponse;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;

@Path("/api/survey")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserSurveyService {
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/id/{id}")
	public GenericEntityResponse<UserSurveyDTO> getSurveyById(@PathParam("id") int id) {
		GenericEntityResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveyById(id);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/art/{id}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByArtId(@PathParam("id") int id) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysByArtId(id);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/from/{username}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByFromUser(@PathParam("username") String username) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysFromUser(username);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/to/{username}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByToUser(@PathParam("username") String username) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysToUser(username);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/status/{status}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByStatus(@PathParam("status") String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysByStatus(status);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/from/{username}/status/{status}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByStatusAndFromUser(@PathParam("username") String username, @PathParam("status") String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysByStatusAndFromUser(username, status);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/find/to/{username}/status/{status}")
	public GenericEntityListResponse<UserSurveyDTO> getSurveyByStatusAndToUser(@PathParam("username") String username, @PathParam("status") String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new UserSurveyAdministrationDTO().getSurveysByStatusAndToUser(username, status);
		return response;
	}
	
	@PUT
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/send")
	public GenericResponse sendSurvey(SurveyUpdateRequest request) {
		GenericResponse response = new UserSurveyAdministrationDTO().sendSurvey(request);
		return response;
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/expired/from/{username}")
	public GenericEntityResponse<Boolean> getPendingExpiredSurveyFromUser(@PathParam("username") String username) {
		GenericEntityResponse<Boolean> response = new UserSurveyAdministrationDTO().getPendingExpiredSurveyFromUser(username);
		return response;
	}
}