package com.memmorable.backend.services.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.ReportRanges;
import com.memmorable.backend.orm.dto.ReportsAdministrationDTO;
import com.memmorable.backend.orm.dto.UserFeePaidPendingDTO;
import com.memmorable.backend.orm.dto.UserSalesReportDTO;
import com.memmorable.backend.services.rest.responses.ReportListResponse;
import com.memmorable.backend.services.rest.responses.UsersResponse;

@Path("/api/reports")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserReportsService {
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/user/visits/{username}")
	public ReportListResponse<String> getAllVisitsFromUser(@PathParam("username") String username) {
		return new ReportsAdministrationDTO().getAllVisitsReportFromUser(username);
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/user/sales/current/{username}")
	public ReportListResponse<String> getCurrentYearSalesFromUser(@PathParam("username") String username) {
		return new ReportsAdministrationDTO().getCurrentYearSalesReportFromUser(username);
	}
	
	@GET
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/user/sales/last/{username}")
	public ReportListResponse<String> getLastYearSalesFromUser(@PathParam("username") String username) {
		return new ReportsAdministrationDTO().getLastYearSalesReportFromUser(username);
	}
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Path("/admin/sales/current/")
	public ReportListResponse<String> getCurrentYearSales() {
		return new ReportsAdministrationDTO().getCurrentYearSalesReport();
	}
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Path("/admin/sales/last/")
	public ReportListResponse<String> getLastYearSales() {
		return new ReportsAdministrationDTO().getLastYearSalesReportFromUser();
	}
	
	@POST
	@RolesAllowed({"ADMIN"})
	@Path("/admin/sales/range/")
	public ReportListResponse<UserSalesReportDTO> getRangeSales(ReportRanges ranges) {
		return new ReportsAdministrationDTO().getRangeSalesReportFromUser(ranges);
	}
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Path("/admin/fee/current/")
	public ReportListResponse<String> getCurrentYearFeePaid() {
		return new ReportsAdministrationDTO().getCurrentYearFeeReport();
	}
	
	@POST
	@RolesAllowed({"ADMIN"})
	@Path("/admin/fee/range/")
	public ReportListResponse<UserSalesReportDTO> getRangeFeePaid(ReportRanges ranges) {
		return new ReportsAdministrationDTO().getRangeFeeReportFromUser(ranges);
	}
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Path("/admin/fee/pending/")
	public UsersResponse<UserFeePaidPendingDTO> getFeePaidPending() {
		return new ReportsAdministrationDTO().getFeePaidPending();
	}
	
	@GET
	@RolesAllowed({"ADMIN"})
	@Path("/admin/users/created/")
	public ReportListResponse<String> getCreatedUsers() {
		return new ReportsAdministrationDTO().getCreatedUsers();
	}
	
}