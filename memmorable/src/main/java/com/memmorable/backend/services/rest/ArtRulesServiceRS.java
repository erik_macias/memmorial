package com.memmorable.backend.services.rest;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.memmorable.backend.orm.dto.ArtRulesDTO;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.MediaRuleRequest;

@Path("/artRules")
public class ArtRulesServiceRS {
	ArtRulesDTO artRulesDTO = new ArtRulesDTO();


	@POST
	@Path("/getArtRules")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArtRulesByUser(CurrentUser user) {
		return artRulesDTO.getArtRulesByUser(user);
	}
	
	@POST
	@Path("/getMediaRules")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response  getMediaRulesByUserAndType(MediaRuleRequest request) {
		return artRulesDTO.getMediaRulesByUserAndType(request.user, request.artType);
	}
	@POST
	@Path("/isAbleToUpload")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response  isUploadAvailable(CurrentUser user) {
		return artRulesDTO.isUploadAvailable(user);
	}
	
}
