package com.memmorable.backend.services.rest.responses;


public class ArtRatingResponse extends GenericResponse {
	private int rating;
	
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
}
