package com.memmorable.backend.services.rest;


import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.memmorable.app.s3.S3FileUploader;
import com.memmorable.backend.orm.dto.ProfileAdministrationDTO;
import com.memmorable.backend.services.rest.request.GetProfileRequest;
import com.memmorable.backend.services.rest.request.ProfilePictureRequest;
import com.memmorable.backend.services.rest.request.ProfileRequest;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.MediaResponse;
import com.memmorable.backend.services.rest.responses.ProfileReponse;

@Path("/pro")
@Produces(MediaType.APPLICATION_JSON)
public class ProfileService {
	
	@POST
	@Path("/getProfile")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	public ProfileReponse getProfile(GetProfileRequest user) {
		ProfileReponse response = new ProfileAdministrationDTO().getProfileInformation(user);	
	return response;
	}
	
	@POST
	@Path("/updateProfile")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateProfile(ProfileRequest request) {
	GenericResponse response = new ProfileAdministrationDTO().UpdateProfileInformation(request);	
	return response;
	}
	
	@POST
	@Path("/uploadfile")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public MediaResponse uploadFile(MultipartFormDataInput input ){
	MediaResponse response = new S3FileUploader().uploaProfiledFile(input);
	return response;
	}
	
	@POST
	@Path("/updateProfilePicture")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateProfilePicutre(ProfilePictureRequest request) {
	GenericResponse response = new ProfileAdministrationDTO().updateProfilePicture(request.user.username, request.fileName);
	return response;
	}
	
	
}
	