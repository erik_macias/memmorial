package com.memmorable.backend.services.rest.request;

public class PasswordChange {
	
	public String password;
	public String newPassword;
	public CurrentUser currentUser;
	
}
