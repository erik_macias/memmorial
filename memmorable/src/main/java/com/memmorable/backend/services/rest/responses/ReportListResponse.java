package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class ReportListResponse<T> extends GenericResponse {

	private List<T> labelsList;
	private List<T> seriesList;
	private List<Double> dataListA;
	private List<Double> dataListB;
	private List<List<Double>> dataLists;

	public List<T> getLabelsList() {
		return labelsList;
	}

	public void setLabelsList(List<T> labelsList) {
		this.labelsList = labelsList;
	}

	public List<T> getSeriesList() {
		return seriesList;
	}

	public void setSeriesList(List<T> seriesList) {
		this.seriesList = seriesList;
	}

	public List<Double> getDataListA() {
		return dataListA;
	}

	public void setDataListA(List<Double> dataListA) {
		this.dataListA = dataListA;
	}

	public List<Double> getDataListB() {
		return dataListB;
	}

	public void setDataListB(List<Double> dataListB) {
		this.dataListB = dataListB;
	}

	public List<List<Double>> getDataLists() {
		return dataLists;
	}

	public void setDataLists(List<List<Double>> dataLists) {
		this.dataLists = dataLists;
	}
	
}
