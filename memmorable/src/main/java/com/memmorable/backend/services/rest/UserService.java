package com.memmorable.backend.services.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.AddressDTO;
import com.memmorable.backend.orm.dto.ArtAdministrationDTO;
import com.memmorable.backend.orm.dto.ContactFormDTO;
import com.memmorable.backend.orm.dto.ProfileAdministrationDTO;
import com.memmorable.backend.orm.dto.UserAdministrationDTO;
import com.memmorable.backend.orm.dto.UserLoginDTO;
import com.memmorable.backend.orm.dto.UserPersonalDTO;
import com.memmorable.backend.orm.dto.UserRegistryDTO;
import com.memmorable.backend.orm.dto.UsersBought;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.services.rest.request.ActivateUserCertRequest;
import com.memmorable.backend.services.rest.request.Credential;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.EditPersonalInfo;
import com.memmorable.backend.services.rest.request.GetProfileRequest;
import com.memmorable.backend.services.rest.request.PasswordChange;
import com.memmorable.backend.services.rest.request.UserCommentsRequest;
import com.memmorable.backend.services.rest.request.UserRegistry;
import com.memmorable.backend.services.rest.responses.AuthenticationResponse;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.RoleResponse;
import com.memmorable.backend.services.rest.responses.UserAddressResponse;
import com.memmorable.backend.services.rest.responses.UserArtReponse;
import com.memmorable.backend.services.rest.responses.UserBasicInfoResponse;
import com.memmorable.backend.services.rest.responses.UserCommentsResponse;
import com.memmorable.backend.services.rest.responses.UserDetailsResponse;
import com.memmorable.backend.services.rest.responses.UsersResponse;

@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserService {
	
	@POST
	@PermitAll
	@Path("/authenticate")
	public AuthenticationResponse authenticate(Credential user) {
		AuthenticationResponse response = new UserLoginDTO().Authenticate(user);
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/create")
	public GenericResponse create(UserRegistry user) {
		GenericResponse response = new UserRegistryDTO().create(user);
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/personalByName")
	public GenericEntityResponse<PersonalEntity> personalByName(CurrentUser user){
		GenericEntityResponse<PersonalEntity> response= new UserAdministrationDTO().getPersonalInformation(user);
		return response;
		
	}
	
	@POST
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/updatePersonal")
	public GenericEntityResponse<PersonalEntity> updatePersonal(EditPersonalInfo personalinfo){
		GenericEntityResponse<PersonalEntity> response= new UserAdministrationDTO().ChangePersonalInfo(personalinfo);
		return response;
		
	}
	
	@POST
	@PermitAll
	@Path("/changePassword")
	public AuthenticationResponse changePassword(PasswordChange passChange){
		AuthenticationResponse response= new UserAdministrationDTO().changePassword(passChange);
		return response;	
	}
	
	@GET
	@PermitAll
	@Path("/verify/{urlParam}")
	public GenericResponse verify(@PathParam("urlParam") String urlParam) {
		GenericResponse response = new UserRegistryDTO().verify(urlParam);
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/pwdForgot")
	public GenericResponse passwordReset(String email) {
		GenericResponse response = new UserRegistryDTO().generatePasswordResetLink(email);
		return response;
	}
	
	@GET
	@PermitAll
	@Path("/pwdReset/{urlParam}")
	public UserBasicInfoResponse getUserFromUrlEncodedLink(@PathParam("urlParam") String urlParam) {
		UserBasicInfoResponse response = new UserRegistryDTO().passwordReset(urlParam);
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/getuserart")
	public UserArtReponse getArtByUser(GetProfileRequest profileRequest) {
		UserArtReponse response = new UserArtReponse();
		ArtAdministrationDTO artDTO = new ArtAdministrationDTO();
		CurrentUser currentUser = new CurrentUser();
		currentUser.username = profileRequest.getUser_name();
		response = artDTO.getArtByUser(currentUser);
		return response;
	}
	
	@GET
	@RolesAllowed("ADMIN")
	@Path("/users")
	public UsersResponse<UserPersonalDTO> getUsers() {
		UsersResponse<UserPersonalDTO> response = new UserAdministrationDTO().getAllUsers();
		return response;
	}
	
	@DELETE
	@RolesAllowed("ADMIN")
    @Path("/user/{username}")
    public GenericResponse deleteUser(@PathParam("username") String username) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.deleteUser(username);
		response.message = "User has been deleted successfully";
        return response;
    }
	
	@PUT
	@RolesAllowed("ADMIN")
    @Path("/user")
    public GenericResponse updateUser(UserPersonalDTO user) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.updateUser(user);
		response.message = "User has been updated successfully";
        return response;
    }
	
	@POST
	@PermitAll
    @Path("/user")
    public UserDetailsResponse<UserPersonalDTO> getUserDetails(CurrentUser user) {
		UserDetailsResponse<UserPersonalDTO> response = new UserAdministrationDTO().getUserDetails(user);
        return response;
    }
	
	@POST
	@PermitAll
    @Path("/addresses")
    public UserAddressResponse<AddressDTO> getUserAddresses(CurrentUser user) {
		UserAddressResponse<AddressDTO> response = new UserAdministrationDTO().getUserAddresses(user);
        return response;
    }
	
	@POST
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/user/address/")
	public GenericResponse addUserAddress(AddressDTO userAddress) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.addUserAddress(userAddress);
		return response;
	}
	
	@DELETE
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
    @Path("/user/address/{addressId}")
    public GenericResponse deleteUserAddress(@PathParam("addressId") int addressId) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.deleteUserAddress(addressId);
		response.message = "User Address has been deleted successfully";
        return response;
    }
	
	@PUT
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
    @Path("/user/address")
    public GenericResponse updateUserAddress(AddressDTO userAddress) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.updateUserAddress(userAddress);
		response.message = "User Address has been updated successfully";
        return response;
    }
	
	@POST
	@Path("/user/comments")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserCommentsResponse> getUserComments(String username) {
		return new UserAdministrationDTO().getUserComments(username);
	}
	
	@POST
	@Path("/user/form/contact")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse sendContactFormComments(ContactFormDTO contactForm) {
		return new UserAdministrationDTO().sendContactFormComments(contactForm);
	}
	
	@PUT
	@Path("/user/comments")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse saveUserComments(UserCommentsRequest comments) {
		return new UserAdministrationDTO().addArtComments(comments);
	}
	
	@DELETE
	@RolesAllowed("ADMIN")
    @Path("/user/comments/{userCommentId}")
    public GenericResponse deleteUserComments(@PathParam("userCommentId") int userCommentId) {
		return new UserAdministrationDTO().deleteUserComments(userCommentId);
    }
	
	@POST
	@Path("/user/certificate")
	@RolesAllowed("ADMIN")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse certifyUser(String username) {
		return new UserAdministrationDTO().certifyUser(username);
	}
	
	@DELETE
	@RolesAllowed("ADMIN")
	@Path("/user/certificate/{username}")
    public GenericResponse removeCertifyUser(@PathParam("username") String username) {
		return new UserAdministrationDTO().removeCertifyUser(username);
    }
	
	@POST
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Path("/user/bought/")
	public GenericResponse hasUserBoughtToSeller(UsersBought usersBought) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = new GenericResponse();
		response.success = userAdminDTO.hasUserBoughtToSeller(usersBought);
		return response;
	}
	
	@POST
	@PermitAll
	@Path("/user/membership/")
	public RoleResponse getUserRole(GetProfileRequest user) {
		RoleResponse response = new RoleResponse();
		response = new ProfileAdministrationDTO().getMembershipInformation(user);	
		return response;
	}
	
	@POST
	@RolesAllowed("ADMIN")
	@Path("/user/payment/register/")
	public GenericResponse registerCertificatePayment(ActivateUserCertRequest user) {
		UserAdministrationDTO userAdminDTO = new UserAdministrationDTO();
		GenericResponse response = userAdminDTO.registerUserCertificatePayment(user);
		return response;
	}
}