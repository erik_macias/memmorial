package com.memmorable.backend.services.rest.request;

public class ArtTypeFeeRequest {
	public int artTypeFeeId;
	public String artType;
	public String role;
	public Double fee;
	
	public int getArtTypeFeeId() {
		return artTypeFeeId;
	}
	public void setArtTypeFeeId(int artTypeFeeId) {
		this.artTypeFeeId = artTypeFeeId;
	}
	public String getArtType() {
		return artType;
	}
	public void setArtType(String artType) {
		this.artType = artType;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	
}
