package com.memmorable.backend.services.rest.request;

public class GetProfileRequest {
	private String user_name;
	private String currrentUser;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getCurrrentUser() {
		return currrentUser;
	}

	public void setCurrrentUser(String currrentUser) {
		this.currrentUser = currrentUser;
	}

}
