package com.memmorable.backend.services.rest.request;


public class ArtDetailsRequest {

	private int artId;
	private String currentUser;
	
	public int getArtId() {
		return artId;
	}
	public void setArtId(int artId) {
		this.artId = artId;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
}
