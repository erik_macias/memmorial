package com.memmorable.backend.services.rest.request;

public class AgreementRequest {
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
