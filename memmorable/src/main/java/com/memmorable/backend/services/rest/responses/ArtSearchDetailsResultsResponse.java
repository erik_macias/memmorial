package com.memmorable.backend.services.rest.responses;

import com.memmorable.backend.orm.dto.ArtSearchResultDTO;

public class ArtSearchDetailsResultsResponse extends GenericResponse {

	private ArtSearchResultDTO artDetailsDTO;
	private Boolean surveysPending;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArtSearchResultDTO getArtDetailsDTO() {
		return artDetailsDTO;
	}

	public void setArtDetailsDTO(ArtSearchResultDTO artDetailsDTO) {
		this.artDetailsDTO = artDetailsDTO;
	}

	public Boolean getSurveysPending() {
		return surveysPending;
	}

	public void setSurveysPending(Boolean surveysPending) {
		this.surveysPending = surveysPending;
	}

}
