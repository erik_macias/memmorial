package com.memmorable.backend.services.rest.responses;

import java.util.List;

import com.memmorable.backend.orm.dto.ArtSearchResultDTO;

public class ArtSearchResultsResponse extends GenericResponse {

	private int searchResultCount;
	private List<ArtSearchResultDTO> artEntityList;
	private Boolean surveysPending;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getSearchResultCount() {
		return searchResultCount;
	}

	public void setSearchResultCount(int searchResultCount) {
		this.searchResultCount = searchResultCount;
	}

	public List<ArtSearchResultDTO> getArtEntityList() {
		return artEntityList;
	}

	public void setArtEntityList(List<ArtSearchResultDTO> artEntityList) {
		this.artEntityList = artEntityList;
	}

	public Boolean getSurveysPending() {
		return surveysPending;
	}

	public void setSurveysPending(Boolean surveysPending) {
		this.surveysPending = surveysPending;
	}

}
