package com.memmorable.backend.services.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.memmorable.backend.services.rest.responses.MediaResponse;

public class FileUploader {

	public MediaResponse uploadFile(MultipartFormDataInput input ){
		String fileName = "";
		
		String UPLOADED_FILE_PATH =  getClass().getProtectionDomain().getCodeSource().getLocation().toString();
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");
		
		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = Math.random() * 100 + getFileName(header);
				fileName = UPLOADED_FILE_PATH + "/../../mediaFiles/" + fileName;

				InputStream inputFile = inputPart.getBody(InputStream.class, null);
				writeToFile(inputFile, fileName);
				inputFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		MediaResponse response = new MediaResponse();
		response.setSuccess(true);
		response.setMessage("File was Uploaded Correctly");
		response.setFilePath(fileName);
		return response;
	}
	
	public MediaResponse uploadProfilePIcture(MultipartFormDataInput input ){
		String fileName = "";
		String nameFile = Math.random()*100+"profile.jpg";
		String UPLOADED_FILE_PATH =  getClass().getProtectionDomain().getCodeSource().getLocation().toString();
		
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");

		for (InputPart inputPart : inputParts) {
			try {
				fileName = UPLOADED_FILE_PATH + "/../../images/" + nameFile;
				fileName = fileName.replace("vfs:", "");

				InputStream inputFile = inputPart.getBody(InputStream.class, null);
				writeToFile(inputFile, fileName);
				inputFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		MediaResponse response = new MediaResponse();
		response.setSuccess(true);
		response.setMessage("File was Uploaded Correctly");
		response.setFilePath("./images/"+ nameFile);
		return response;
	}
	
	private String getFileName(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
		try {
			int read = 0;
			byte[] bytes = new byte[1024];

			OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
