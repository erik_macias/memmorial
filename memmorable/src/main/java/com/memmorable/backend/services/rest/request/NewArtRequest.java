package com.memmorable.backend.services.rest.request;

import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;

public class NewArtRequest {
	public String username;
	public String art_tag;
	public String art_name;
	public Double art_price;
	public String art_description;
	public ArtTypeServiceEntity art_service;
	public ArtSubTypeEntity art_subType;
	public ArtTypeEntity art_type;
	public ArtTypeGenreEntity art_genre;
	public ArtTypeFormatEntity art_format;
	
	public CurrencyEntity currency;
}

