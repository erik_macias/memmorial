package com.memmorable.backend.services.rest.request;

public class MembershipInfoRequest {
	private String username;
	private String agreementId;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAgreementId() {
		return agreementId;
	}
	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
	
}
