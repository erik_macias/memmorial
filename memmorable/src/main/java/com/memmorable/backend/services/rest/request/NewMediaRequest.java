package com.memmorable.backend.services.rest.request;


public class NewMediaRequest {
	public int art_id;
	public String media_url;
	public String media_type;
	public boolean media_primary;
	public boolean downloadable;
	
}

