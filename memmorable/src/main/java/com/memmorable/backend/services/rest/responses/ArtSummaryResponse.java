package com.memmorable.backend.services.rest.responses;

import java.util.List;

import com.memmorable.backend.orm.dto.ArtSummaryDTO;

public class ArtSummaryResponse extends GenericResponse {
	
	public List<ArtSummaryDTO> artSummary;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ArtSummaryDTO> getArtSummary() {
		return artSummary;
	}
	public void setArtSummary(List<ArtSummaryDTO> artSummary) {
		this.artSummary = artSummary;
	}
	
}
