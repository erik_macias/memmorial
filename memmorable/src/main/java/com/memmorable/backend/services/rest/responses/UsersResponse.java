package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class UsersResponse<T> extends GenericResponse {
	
	public List<T> users;

}
