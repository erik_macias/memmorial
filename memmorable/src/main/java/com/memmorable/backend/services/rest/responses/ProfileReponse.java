package com.memmorable.backend.services.rest.responses;

public class ProfileReponse extends GenericResponse {
	private String personal_name;
	private String personal_l_name;
	private String personal_gender;
	private String user_name;
	private String profile_resume;
	private String profile_art_statement;
	private String profile_picture;
	private String UserName;
	private String location;
	private String phone;
	private String mobile;
	private String email;
	private String membership;
	private boolean isUserCertified;
	private int visitsCounter;

	public String getPersonal_name() {
		return personal_name;
	}

	public void setPersonal_name(String personal_name) {
		this.personal_name = personal_name;
	}

	public String getPersonal_l_name() {
		return personal_l_name;
	}

	public void setPersonal_l_name(String personal_l_name) {
		this.personal_l_name = personal_l_name;
	}

	public String getPersonal_gender() {
		return personal_gender;
	}

	public void setPersonal_gender(String personal_gender) {
		this.personal_gender = personal_gender;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getProfile_resume() {
		return profile_resume;
	}

	public void setProfile_resume(String profile_resume) {
		this.profile_resume = profile_resume;
	}

	public String getProfile_art_statement() {
		return profile_art_statement;
	}

	public void setProfile_art_statement(String profile_art_statement) {
		this.profile_art_statement = profile_art_statement;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public boolean isUserCertified() {
		return isUserCertified;
	}

	public void setUserCertified(boolean isUserCertified) {
		this.isUserCertified = isUserCertified;
	}

	public int getVisitsCounter() {
		return visitsCounter;
	}

	public void setVisitsCounter(int visitsCounter) {
		this.visitsCounter = visitsCounter;
	}

}
