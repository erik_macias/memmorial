package com.memmorable.backend.services.rest.request;

import com.memmorable.backend.orm.entity.RoleEntity;

public class CreatePaypalBillingPlanRequest {
	private String plan;
	private RoleEntity role;
	
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public RoleEntity getRole() {
		return role;
	}
	public void setRole(RoleEntity role) {
		this.role = role;
	}
}
