package com.memmorable.backend.services.rest.responses;

public class ArtMediaResponse extends GenericResponse {

	public String media_name;
	public String media_fileName;
	public String media_description;
	public String Type;
	
}
