package com.memmorable.backend.services.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.PaypalGoodsAdministrationDTO;
import com.memmorable.backend.orm.dto.PaypalPlanAdministrationDTO;
import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.services.rest.request.AgreementRequest;
import com.memmorable.backend.services.rest.request.ArtPaymentConfRequest;
import com.memmorable.backend.services.rest.request.ArtPaymentRequest;
import com.memmorable.backend.services.rest.request.CreatePaypalBillingPlanRequest;
import com.memmorable.backend.services.rest.request.MembershipInfoRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityListResponse;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.PaypalGoodsFeePaymentResponse;
import com.paypal.api.payments.Agreement;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.Plan;
import com.paypal.api.payments.PlanList;
import com.paypal.base.rest.JSONFormatter;

@Path("/paypal")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PaypalPlanService {
	PaypalPlanAdministrationDTO planDTO = new PaypalPlanAdministrationDTO();
	PaypalGoodsAdministrationDTO goodsDTO = new PaypalGoodsAdministrationDTO();

	@POST
	@Path("/plans")
	@RolesAllowed("ADMIN")
	public GenericEntityListResponse<PaypalPlanEntity> getPaypalPlans() {
		GenericEntityListResponse<PaypalPlanEntity> response = new GenericEntityListResponse<PaypalPlanEntity>();
		List<PaypalPlanEntity> paypalPlanEntityList = planDTO.getPaypalPlans();
		if (paypalPlanEntityList != null && !paypalPlanEntityList.isEmpty()) {
			response.success = true;
			response.message = "Paypal Plans retrieved successful";
			response.entityList = paypalPlanEntityList;
		} else {
			response.success = false;
			response.message = "Paypal Plans Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plans/active")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<PlanList> getActivePaypalPlans() {
		GenericEntityResponse<PlanList> response = new GenericEntityResponse<PlanList>();
		Map<String, String> containerMap = new HashMap<String, String>();
		containerMap.put("status", "active");
		PlanList planList = planDTO.getPaypalPlans(containerMap);
		if (planList != null) {
			response.success = true;
			response.message = "Paypal Plans retrieved successful";
			response.entity = planList;
		} else {
			response.success = false;
			response.message = "Paypal Plans Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plans/inactive")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<PlanList> getInactivePaypalPlans() {
		GenericEntityResponse<PlanList> response = new GenericEntityResponse<PlanList>();
		Map<String, String> containerMap = new HashMap<String, String>();
		containerMap.put("status", "inactive");
		PlanList planList = planDTO.getPaypalPlans(containerMap);
		if (planList != null) {
			response.success = true;
			response.message = "Paypal Plans retrieved successful";
			response.entity = planList;
		} else {
			response.success = false;
			response.message = "Paypal Plans Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plans/created")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<PlanList> getCreatedPaypalPlans() {
		GenericEntityResponse<PlanList> response = new GenericEntityResponse<PlanList>();
		Map<String, String> containerMap = new HashMap<String, String>();
		containerMap.put("status", "created");
		PlanList planList = planDTO.getPaypalPlans(containerMap);
		if (planList != null) {
			response.success = true;
			response.message = "Paypal Plans retrieved successful";
			response.entity = planList;
		} else {
			response.success = false;
			response.message = "Paypal Plans Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plan")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<String> getPaypalPlan(Plan plan) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Plan planDet = planDTO.getPaypalPlan(plan.getId());
		if (planDet != null) {
			response.success = true;
			response.message = "Paypal Plan retrieved successful";
			response.entity = planDet.toJSON();
		} else {
			response.success = false;
			response.message = "Paypal Plan Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plan/id")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericResponse getPaypalPlanId(RoleEntity role) {
		GenericResponse response = new GenericResponse();
		Plan planDet = planDTO.getPaypalPlanByRole(role.getRole_name());
		if (planDet != null) {
			response.success = true;
			response.message = planDet.getId();
		} else {
			response.success = false;
			response.message = "Paypal Plan Id Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plan/create")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<Plan> createPaypalPlan(CreatePaypalBillingPlanRequest request) {
		GenericEntityResponse<Plan> response = new GenericEntityResponse<Plan>();
		Plan plan = JSONFormatter.fromJSON(request.getPlan(), Plan.class);
		plan = planDTO.createPaypalPlan(plan, request.getRole());
		if (plan != null) {
			response.success = true;
			response.message = "Paypal Plan created successful";
			response.entity = plan;
		} else {
			response.success = false;
			response.message = "There's no any Paypal Plan ready. Please contact the Administrator";
		}
		return response;
	}
	
	@POST
	@Path("/plan/agreement/create")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<Agreement> createPaypalPlanAgreement(Plan plan) {
		GenericEntityResponse<Agreement> response = new GenericEntityResponse<Agreement>();
		Agreement agreement = planDTO.createBillingAgreement(plan);
		if (agreement != null) {
			response.success = true;
			response.message = "Paypal Plan created successful";
			response.entity = agreement;
		} else {
			response.success = false;
			response.message = "Paypal Plan Result Empty";
		}
		return response;
	}
	
	@POST
	@Path("/plan/user/activate")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> getPaypalPlanAgreement(MembershipInfoRequest membership) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		String newRole = planDTO.updateUserMembershipPlan(membership);
		if (newRole != null) {
			response.success = true;
			response.message = "Membership Plan for " + newRole + " has been activated.";
			response.entity = newRole;
		} else {
			response.success = false;
			response.message = "Error found while upgrading membership. Please contact the Administrator.";
		}
		return response;
	}
	
	@POST
	@Path("/plan/activate")
	@RolesAllowed("ADMIN")
	public GenericResponse activatePaypalPlan(Plan plan) {
		GenericResponse response = new GenericResponse();
		Boolean status = planDTO.activatePaypalPlan(plan);
		if (status) {
			response.success = true;
			response.message = "Paypal Plan activated successful";
		} else {
			response.success = false;
			response.message = "Paypal Plan not activated";
		}
		return response;
	}
	
	@POST
	@Path("/plan/create/template")
	@RolesAllowed("ADMIN")
	public GenericEntityResponse<String> getPaypalPlanTemplate() {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Plan plan = planDTO.getPaypalPlanTemplate();
		if (plan != null) {
			response.success = true;
			response.message = "Paypal Plan Template retrieved successful";
			response.entity = plan.toJSON();
		} else {
			response.success = false;
			response.message = "Paypal Plan Template Result Empty";
		}
		return response;
	}
	
	@DELETE
	@Path("/plan/{planId}")
	@RolesAllowed("ADMIN")
	public GenericResponse deletePaypalPlan(@PathParam("planId") String planId) {
		GenericResponse response = new GenericResponse();
		planDTO.deletePaypalPlan(planId);
		response.success = true;
		response.message = "Paypal Plan deleted successful";
		return response;
	}
	
	@POST
	@Path("/plan/agreement/exec")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> execPaypalPlanAgreement(AgreementRequest agreement) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Agreement agreementConf = planDTO.execPaypalPlanAgreement(agreement.getToken());
		if (agreementConf != null) {
			response.success = true;
			response.message = "Paypal Plan Agreement executed successful";
			response.entity = agreementConf.getId();
		} else {
			response.success = false;
			response.message = "Error while executing Paypal Plan Agreement";
		}
		return response;
	}
	
	@POST
	@Path("/good/fee/validate")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public PaypalGoodsFeePaymentResponse validatePaypalGoodsFeePayment(ArtPaymentRequest artReq) {
		PaypalGoodsFeePaymentResponse response = new PaypalGoodsFeePaymentResponse();
		
		Boolean isPaid = goodsDTO.isPaypalGoodsFeePaid(artReq.art_Id);
		Double userCreditAvailable = goodsDTO.getUserCreditAvailable(artReq.art_Id);
		
		if (isPaid) {
			response.success = isPaid;
			response.message = "Paypal Goods Fee already paid";
		} else {
			response.success = isPaid;
			response.setCreditAvailable(userCreditAvailable);
			response.message = "Paypal Goods Fee has not been paid";
		}
		return response;
	}
	
	@POST
	@Path("/good/fee/exec")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> execPaypalGoodsFeePayment(ArtPaymentRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Payment payment = goodsDTO.execPaypalGoodsFee(artReq.art_Id, artReq.username);
		if (payment != null) {
			response.success = true;
			response.message = "Paypal Goods Fee executed successful";
			Iterator<Links> iter = payment.getLinks().iterator();
			
			while (iter.hasNext()) {
				Links link = iter.next();
				if (link.getRel().equalsIgnoreCase("approval_url")) {
					response.entity = link.getHref();
				}
			}
		} else {
			response.success = false;
			response.message = "Error while executing Paypal Payment";
		}
		return response;
	}
	
	@POST
	@Path("/good/fee/credit")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> execGoodsFeePaymentWithCredit(ArtPaymentRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Boolean paymentCredited = goodsDTO.execGoodsFeePaymentWithCredit(artReq.art_Id, artReq.username);
		if (paymentCredited) {
			response.success = true;
			response.message = "Goods Fee with Credit executed successful";
		} else {
			response.success = false;
			response.message = "Error while executing Paypal Payment";
		}
		return response;
	}
	
	@POST
	@Path("/good/fee/conf")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> confPaypalGoodsFeePayment(ArtPaymentConfRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Payment payment = null;
		
		try {
			payment = goodsDTO.confPaypalGoodsFee(artReq);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (payment != null) {
			response.success = true;
			response.message = "Paypal Goods Fee confirmed successfully";
			response.entity = payment.getId();
		} else {
			response.success = false;
			response.message = "Error while confirming Paypal Payment";
		}
		return response;
	}
	
	@POST
	@Path("/good/nofee/activate")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericResponse activateArtWithNoFee(ArtPaymentRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		
		Boolean isActivated = goodsDTO.activateArtWithNoFee(artReq);
		
		if (isActivated) {
			response.success = true;
			response.message = "Paypal Goods Fee confirmed successfully";
		} else {
			response.success = false;
			response.message = "Error while confirming Paypal Payment";
		}
		return response;
	}
	
	@POST
	@Path("/good/validate")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericResponse validatePaypalGoodsPayment(ArtPaymentRequest artReq) {
		GenericResponse response = new GenericResponse();
		
		Boolean isPaid = goodsDTO.isPaypalGoodsPaid(artReq.art_Id);
		if (isPaid) {
			response.success = isPaid;
			response.message = "Paypal Good already paid";
		} else {
			response.success = isPaid;
			response.message = "Paypal Good has not been paid";
		}
		return response;
	}
	
	@POST
	@Path("/good/exec")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> execPaypalGoodsPayment(ArtPaymentRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Payment payment = goodsDTO.execPaypalGoods(artReq.art_Id);
		if (payment != null) {
			response.success = true;
			response.message = "Paypal Goods executed successful";
			Iterator<Links> iter = payment.getLinks().iterator();
			
			while (iter.hasNext()) {
				Links link = iter.next();
				if (link.getRel().equalsIgnoreCase("approval_url")) {
					response.entity = link.getHref();
				}
			}
		} else {
			response.success = false;
			response.message = "Error while executing Paypal Payment";
		}
		return response;
	}
	
	@POST
	@Path("/good/exec/bank/deposit")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericResponse execBankDepositGoodsPayment(ArtPaymentRequest artReq) {
		return goodsDTO.execBankDepositTransactionGoods(artReq);
	}
	
	@POST
	@Path("/good/exec/bank/transfer")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericResponse execBankTransferGoodsPayment(ArtPaymentRequest artReq) {
		return goodsDTO.execBankTransferTransactionGoods(artReq);
	}
	
	@POST
	@Path("/good/conf")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	public GenericEntityResponse<String> confPaypalGoodsPayment(ArtPaymentConfRequest artReq) {
		GenericEntityResponse<String> response = new GenericEntityResponse<String>();
		Payment payment = null;
		
		try {
			payment = goodsDTO.confPaypalGoods(artReq);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (payment != null) {
			response.success = true;
			response.message = "Paypal Goods confirmed successfully";
			response.entity = payment.getId();
		} else {
			response.success = false;
			response.message = "Error while confirming Paypal Payment";
		}
		return response;
	}
	
}
