package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class MessagesListResponse<T> extends GenericResponse {
	
	public List<T> msgsInbox;
	public List<T> msgsSent;
	
	public List<T> getMsgsInbox() {
		return msgsInbox;
	}
	public void setMsgsInbox(List<T> msgsInbox) {
		this.msgsInbox = msgsInbox;
	}
	public List<T> getMsgsSent() {
		return msgsSent;
	}
	public void setMsgsSent(List<T> msgsSent) {
		this.msgsSent = msgsSent;
	}
	
}
