package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class UserArtReponse extends GenericResponse{

	public String Username;
	public List<ArtResponse> arts;
	
}
