package com.memmorable.backend.services.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.CatalogAdministrationDTO;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.services.rest.request.ArtTypeFeeRequest;
import com.memmorable.backend.services.rest.request.ArtTypeUsrRequest;
import com.memmorable.backend.services.rest.request.LocationRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;

@Path("/catalog")
public class CatalogService {
	CatalogAdministrationDTO catalogDTO = new CatalogAdministrationDTO();

	@POST
	@Path("/countries")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryEntity> getCountries() {
		return catalogDTO.getCountries();
	}
	
	@POST
	@Path("/states")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<CountryStatesEntity> getStates(LocationRequest req) {
		if (req != null && req.getCountryId() >= 0) {
			return catalogDTO.getStates(req.getCountryId());
		} else {
			return null;
		}
	}
	
	@POST
	@Path("/cities")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<CountryStatesCityEntity> getCities(LocationRequest req) {
		if (req != null && req.getStateId() >= 0) {
			return catalogDTO.getCities(req.getStateId());
		} else {
			return null;
		}
	}
	
	@POST
	@Path("/country")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<CountryEntity> getCountryDetails(int countryId) {
		return catalogDTO.getCountryDetails(countryId);
	}
	
	@PUT
	@Path("/countries")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateCountry(CountryEntity country) {
		GenericResponse response = catalogDTO.updateCountry(country);
		return response;
	}
	
	@POST
	@Path("/countries/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createNewCountry(CountryEntity country) {
		GenericResponse response = catalogDTO.createCountry(country);
		return response;
	}
	
	@POST
	@Path("/roles")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<RoleEntity> getRoles() {
		return catalogDTO.getRoles();
	}
	
	@PUT
	@Path("/roles")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateRole(RoleEntity role) {
		GenericResponse response = catalogDTO.updateRole(role);
		return response;
	}

	@POST
	@Path("/role")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public RoleEntity getRole(RoleEntity role) {
		return catalogDTO.getRole(role);
	}
	
	@POST
	@Path("/userstatus")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserStatusEntity> getUserStatus() {
		return catalogDTO.getUserStatus();
	}
	
	@POST
	@Path("/addresstypes")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<AddressTypeEntity> getAddressTypes() {
		return catalogDTO.getAddressTypes();
	}
	
	@POST
	@Path("/addresstype")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<AddressTypeEntity> getAddressTypeDetails(int addressTypeId) {
		return catalogDTO.getAddressTypeDetails(addressTypeId);
	}
	
	@PUT
	@Path("/addresstypes")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateAddressTypes(AddressTypeEntity addressType) {
		GenericResponse response = catalogDTO.updateAddressType(addressType);
		return response;
	}
	
	@POST
	@Path("/addresstypes/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createAddressTypes(AddressTypeEntity addressType) {
		GenericResponse response = catalogDTO.createAddressType(addressType);
		return response;
	}
	
	@POST
	@Path("/arttypes")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeEntity> getArtTypes() {
		return catalogDTO.getArtTypes();
	}
	
	@POST
	@Path("/arttype")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<ArtTypeEntity> getArtTypeDetails(int artTypeId) {
		return catalogDTO.getArtTypeDetails(artTypeId);
	}
	
	@PUT
	@Path("/arttypes")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateArtTypes(ArtTypeEntity artType) {
		GenericResponse response = catalogDTO.updateArtType(artType);
		return response;
	}
	
	@POST
	@Path("/arttypes/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createArtTypes(ArtTypeEntity artType) {
		GenericResponse response = catalogDTO.createArtType(artType);
		return response;
	}
	
	@POST
	@Path("/artsubtypes")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtSubTypeEntity> getArtSubTypes() {
		return catalogDTO.getArtSubTypes();
	}
	
	@POST
	@Path("/artsubtype")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<ArtSubTypeEntity> getArtSubTypeDetails(int artSubTypeId) {
		return catalogDTO.getArtSubTypeDetails(artSubTypeId);
	}
	
	@PUT
	@Path("/artsubtypes")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateArtServices(ArtSubTypeEntity artSubType) {
		GenericResponse response = catalogDTO.updateArtSubType(artSubType);
		return response;
	}
	
	@POST
	@Path("/artsubtypes/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createArtSubTypes(ArtSubTypeEntity artSubType) {
		GenericResponse response = catalogDTO.createArtSubType(artSubType);
		return response;
	}
	
	//----------------------------------
	@POST
	@Path("/artServices")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeServiceEntity> getArtServices() {
		return catalogDTO.getArtServices();
	}
	
	@POST
	@Path("/artServices")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<ArtTypeServiceEntity> getArtServicesDetails(int artServiceId) {
		return catalogDTO.getArtServicesDetails(artServiceId);
	}
	
	@PUT
	@Path("/artServices")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateArtServices(ArtTypeServiceEntity artService) {
		GenericResponse response = catalogDTO.updateArtServices(artService);
		return response;
	}
	
	@POST
	@Path("/artServices/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createArtServices(ArtTypeServiceEntity artService) {
		GenericResponse response = catalogDTO.createArtService(artService);
		return response;
	}
	//----------------------------------------------
	
	
	@POST
	@Path("/currencies")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<CurrencyEntity> getCurrency() {
		return catalogDTO.getCurrencies();
	}
	
	@POST
	@Path("/currency")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<CurrencyEntity> getCurrencyDetails(int currencyId) {
		return catalogDTO.getCurrencyDetails(currencyId);
	}
	
	@PUT
	@Path("/currencies")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateCurrency(CurrencyEntity currency) {
		GenericResponse response = catalogDTO.updateCurrency(currency);
		return response;
	}
	
	@POST
	@Path("/currencies/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createCurrency(CurrencyEntity currency) {
		GenericResponse response = catalogDTO.createCurrency(currency);
		return response;
	}
	
	@POST
	@Path("/arttypefees")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeFeeEntity> getArtTypeFees() {
		return catalogDTO.getArtTypeFees();
	}
	
	@POST
	@Path("/arttypefee")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<ArtTypeFeeEntity> getArtTypeFeeDetails(int artTypeFeeId) {
		return catalogDTO.getArtTypeFeeDetails(artTypeFeeId);
	}

	@POST
	@Path("/arttypefeeusr")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<ArtTypeFeeEntity> getArtTypeFeeDetUsr(ArtTypeUsrRequest artTypeUsr) {
		return catalogDTO.getArtTypeFeeDetUsr(artTypeUsr);
	}
	
	@PUT
	@Path("/arttypefees")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateArtTypeFees(ArtTypeFeeRequest artTypeFee) {
		GenericResponse response = catalogDTO.updateArtTypeFee(artTypeFee);
		return response;
	}
	
	@DELETE
	@Path("/arttypefees")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse deleteArtTypeFee(int artTypeFeeId) {
		return catalogDTO.deleteArtTypeFee(artTypeFeeId);
	}
	
	@POST
	@Path("/arttypefees/create")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createArtTypesFee(ArtTypeFeeRequest artTypeFee) {
		GenericResponse response = catalogDTO.createArtTypeFee(artTypeFee);
		return response;
	}
	
}
