package com.memmorable.backend.services.rest.request;

import com.memmorable.backend.orm.entity.CurrencyEntity;

public class ArtPaymentRequest {
	public String username;
	public int art_Id;
	public String art_name;
	public Double art_price;
	public String art_description;
	public CurrencyEntity currency;
}

