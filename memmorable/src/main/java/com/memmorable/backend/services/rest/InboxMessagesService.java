package com.memmorable.backend.services.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.InboxAdministrationDTO;
import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.InboxMessageRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.MessagesListResponse;

@Path("/inbox")
public class InboxMessagesService {

	@POST
	@Path("/all")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public MessagesListResponse<InboxEntity> getAllInboxMessages(CurrentUser user) {
		MessagesListResponse<InboxEntity> response = new InboxAdministrationDTO().getAllInboxMessages(user);
		return response;
	}
	
	@POST
	@Path("/unread")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public MessagesListResponse<InboxEntity> getUnreadInboxMessages(CurrentUser user) {
		MessagesListResponse<InboxEntity> response = new InboxAdministrationDTO().getAllUnreadInboxMessages(user);
		return response;
	}
	
	@POST
	@Path("/new")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse sendNewInboxMessage(InboxMessageRequest message) {
		GenericResponse response = new InboxAdministrationDTO().sendNewInboxMessage(message);
		return response;
	}
	
	@POST
	@Path("/message")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public GenericEntityResponse<InboxEntity> getByInboxMessage(int msgId) {
		GenericEntityResponse<InboxEntity> response = new InboxAdministrationDTO().getByInboxMessage(msgId);
		return response;
	}
	
	@DELETE
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
    @Path("/message/{msgId}")
    public GenericResponse deleteMessage(@PathParam("msgId") int msgId) {
		GenericResponse response = new InboxAdministrationDTO().deleteMsg(msgId);
		return response;
    }
	
	@PUT
	@Path("/message/read")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse setInboxMessageAsRead(int msgId) {
		GenericResponse response = new InboxAdministrationDTO().setInboxMessageAsRead(msgId);
		return response;
	}
	
	@PUT
	@Path("/message/unread")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse setInboxMessageAsUnread(int msgId) {
		GenericResponse response = new InboxAdministrationDTO().setInboxMessageAsUnread(msgId);
		return response;
	}
	
}
