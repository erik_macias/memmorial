package com.memmorable.backend.services.rest.responses;

public class RoleResponse extends GenericResponse {

	private String membership;

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}
	
}
