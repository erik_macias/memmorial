package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class SearchResultEntityList<T> {
	
	private List<T> entityList;
	private int resultCount;
	
	public List<T> getEntityList() {
		return entityList;
	}
	public void setEntityList(List<T> entityList) {
		this.entityList = entityList;
	}
	public int getResultCount() {
		return resultCount;
	}
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}
}
