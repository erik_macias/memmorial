package com.memmorable.backend.services.rest.request;

public class CurrentUser {
public String username;
public String name;
public String lastname;
public String authdata;

public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getAuthdata() {
	return authdata;
}
public void setAuthdata(String authdata) {
	this.authdata = authdata;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getLastname() {
	return lastname;
}
public void setLastname(String lastname) {
	this.lastname = lastname;
} 

}
