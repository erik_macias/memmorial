package com.memmorable.backend.services.rest.responses;

import java.util.List;
import java.util.Map;

public class ArtResponse extends GenericResponse {
	public String art_Name;
	public String art_Description;
	public Map<Integer, String> art_tag;
	public List<ArtMediaResponse> media;
	public String art_type;
	public String art_subtype;
	
}
