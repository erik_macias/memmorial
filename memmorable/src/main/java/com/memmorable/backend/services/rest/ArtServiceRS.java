package com.memmorable.backend.services.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.memmorable.app.s3.S3FileUploader;
import com.memmorable.backend.orm.dto.ArtAdministrationDTO;
import com.memmorable.backend.orm.dto.ArtSearchResultDTO;
import com.memmorable.backend.orm.entity.ArtMediaEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.services.rest.request.ActivateArtRequest;
import com.memmorable.backend.services.rest.request.ArtCommentsRequest;
import com.memmorable.backend.services.rest.request.ArtRatingRequest;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.MediaRequest;
import com.memmorable.backend.services.rest.request.NewArtRequest;
import com.memmorable.backend.services.rest.request.NewMediaRequest;
import com.memmorable.backend.services.rest.request.TypeRequest;
import com.memmorable.backend.services.rest.request.UpdateArtRequest;
import com.memmorable.backend.services.rest.responses.ArtCommentsResponse;
import com.memmorable.backend.services.rest.responses.ArtRatingResponse;
import com.memmorable.backend.services.rest.responses.ArtSearchResultsResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.MediaResponse;
import com.memmorable.backend.services.rest.responses.SaveGenericResponse;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

@Path("/art")
public class ArtServiceRS {
	ArtAdministrationDTO artDTO = new ArtAdministrationDTO();

	@POST
	@Path("/uploadfile")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public MediaResponse uploadFile(MultipartFormDataInput input) {
		MediaResponse response = new S3FileUploader().uploadFile(input);;
		return response;
	}

	@POST
	@Path("/getTypes")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeEntity> GetTypes() {
		return artDTO.getTypes();
	}
	
	@POST
	@Path("/getCurrency")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public List<CurrencyEntity> GetCurrency() {
		return artDTO.getCurrency();
	}

	@POST
	@Path("/getSubTypes")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtSubTypeEntity> GetSubTypes(TypeRequest type) {
		return artDTO.getSubTypes(type.Type);
	}
	
	@POST
	@Path("/getServices")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeServiceEntity> GetServices(TypeRequest type) {
		return artDTO.getServices(type.Type);
	}
	
	@POST
	@Path("/getGenres")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeGenreEntity> GetGenres(TypeRequest type) {
		return artDTO.getGenres(type.Type);
	}
	@POST
	@Path("/getFormats")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtTypeFormatEntity> GetFormat(TypeRequest type) {
		return artDTO.getFormats(type.Type);
	}

	@POST
	@Path("/getMedia")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtMediaEntity> getMedia(MediaRequest request) {
		return artDTO.getMediaByArt(request.artName);
	}
	
	@POST
	@Path("/deleteMedia")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse deleteMedia(int id) {
		return artDTO.deleteMediaById(id);
	}
	
	
	@POST
	@Path("/removePrimaryAll")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse removePrimaryAll(int id) {
		return artDTO.removePrimaryAll(id);
	}
	
	@POST
	@Path("/setPrimaryMedia")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse setPrimaryMedia(int id) {
		return artDTO.setPrimaryMedia(id);
	}

	@POST
	@Path("/saveArt")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SaveGenericResponse SaveArt(NewArtRequest art) {
		return artDTO.saveArt(art);
		
	}
	
	@POST
	@Path("/updateArt")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SaveGenericResponse updateArt(UpdateArtRequest art) {
		return artDTO.updateArt(art);
	}
	
	@POST
	@Path("/saveMedia")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse saveMedia(NewMediaRequest media) {
		return artDTO.saveMedia(media);
	}
	
	@POST
	@Path("/comments")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ArtCommentsResponse> getArtComments(int artId) {
		return artDTO.getArtComments(artId);
	}
	
	@PUT
	@Path("/comments")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse saveArtComments(ArtCommentsRequest comments) {
		return artDTO.addArtComments(comments);
	}
	
	@DELETE
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
    @Path("/comments/{artCommentId}")
    public GenericResponse deleteArtComments(@PathParam("artCommentId") int artCommentId) {
		return artDTO.deleteArtComments(artCommentId);
    }
	
	@POST
	@Path("/rating")
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArtRatingResponse getArtRatingByUser(ArtRatingRequest request) {
		ArtRatingResponse response = new ArtRatingResponse();
		response.setRating(artDTO.getArtRatingByUser(request));
		response.message = "";
		response.success = true;
		return response;
	}
	
	@PUT
	@Path("/rating")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse rateArtByUser(ArtRatingRequest rating) {
		return artDTO.setArtRating(rating);
	}
	
	@POST
	@PermitAll
	@Path("/userart")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArtSearchResultsResponse getArtByUser(CurrentUser user) {
		ArtSearchResultsResponse response = new ArtSearchResultsResponse();
		
		SearchResultEntityList<ArtSearchResultDTO> artSearchResultList = artDTO.getArtDetailsByUser(user);
		List<ArtSearchResultDTO> artList = artSearchResultList.getEntityList();
		if (!artList.isEmpty()) {
			response.setArtEntityList(artList);
			response.setSearchResultCount(artSearchResultList.getResultCount());
			response.setSuccess(true);
			response.setMessage("Search Result success");
		} else {
			response.setSuccess(false);
			response.setMessage("Search Result Empty");
		}
		return response;
	}

	@POST
	@PermitAll
	@Path("/userart/purchased")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArtSearchResultsResponse getPurchasedArtByUser(CurrentUser user) {
		ArtSearchResultsResponse response = new ArtSearchResultsResponse();
		
		SearchResultEntityList<ArtSearchResultDTO> artSearchResultList = artDTO.getPurchasedArtByUser(user);
		List<ArtSearchResultDTO> artPurchasedList = artSearchResultList.getEntityList();
		if (!artPurchasedList.isEmpty()) {
			response.setArtEntityList(artPurchasedList);
			response.setSearchResultCount(artSearchResultList.getResultCount());
			response.setSuccess(true);
			response.setMessage("Purchased Art search success");
		} else {
			response.setSuccess(false);
			response.setMessage("Purchased Art search result empty");
		}
		return response;
	}
	
	@POST
	@RolesAllowed({"ADMIN"})
	@Path("/list")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArtSearchResultsResponse getAdminArtCatalogList() {
		ArtSearchResultsResponse response = new ArtSearchResultsResponse();
		
		SearchResultEntityList<ArtSearchResultDTO> artSearchResultList = artDTO.getAllArt();
		List<ArtSearchResultDTO> artResultsDTO = artSearchResultList.getEntityList();
		if (!artResultsDTO.isEmpty()) {
			response.setArtEntityList(artResultsDTO);
			response.setSearchResultCount(artSearchResultList.getResultCount());
			response.setSuccess(true);
			response.setMessage("Search Result success");
		} else {
			response.setSuccess(false);
			response.setMessage("Search Result Empty");
		}
		return response;
	}
	
	@DELETE
	@RolesAllowed({"ADMIN"})
    @Path("/delete/{artId}")
    public GenericResponse deleteArt(@PathParam("artId") int artId) {
		return artDTO.deleteArt(artId);
    }
	
	@POST
	@Path("/activate")
	@RolesAllowed({"ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse activateArt(ActivateArtRequest artReq) {
		return artDTO.activateArt(artReq);
	}
	
	@POST
	@Path("/payment/register")
	@RolesAllowed({"ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse registerArtPayment(ActivateArtRequest artReq) {
		return artDTO.registerArtPayment(artReq);
	}
	
	@POST
	@Path("/publishallowed")
	@RolesAllowed({"FREE_FOR_ALL", "MONTHLY_MADNESS", "BICYCLE", "QUATERLY", "ONE_FOURTH", "HALF", "ONE", "CERTIFIED", "ADMIN"})
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public GenericResponse isAllowedToPublish(String username) {
		CurrentUser currentUser = new CurrentUser();
		currentUser.username = username;
		GenericResponse response = new GenericResponse();
		boolean isUserProfilePending = artDTO.isUserProfilePending(currentUser.username);
		
		if (isUserProfilePending) {
			response.success = false;
			response.message = "PROFILE_PENDING";
		} else {
			boolean isUserAllowed = artDTO.isUserAllowedToPublish(currentUser);
			
			if (!isUserAllowed) {
				response.success = false;
				response.message = "Your membership has reached out the number of publications. Please upgrade it.";
			} else {
				response.success = true;
				response.message = "User is able to publish";
			}
		}
		
		return response;
	}

}
