package com.memmorable.backend.services.rest.responses;

public class PaypalGoodsFeePaymentResponse extends GenericResponse {

	private Double creditAvailable;

	public Double getCreditAvailable() {
		return creditAvailable;
	}

	public void setCreditAvailable(Double creditAvailable) {
		this.creditAvailable = creditAvailable;
	}
	
}
