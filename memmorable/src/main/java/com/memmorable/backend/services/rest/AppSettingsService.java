package com.memmorable.backend.services.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.memmorable.backend.orm.dto.AppSettingsAdministrationDTO;
import com.memmorable.backend.orm.entity.AppSettingsEntity;
import com.memmorable.backend.services.rest.responses.GenericResponse;

@Path("/admin/settings")
public class AppSettingsService {
	
	AppSettingsAdministrationDTO appSettingsDTO = new AppSettingsAdministrationDTO();

	@POST
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AppSettingsEntity> getKeyValueList() {
		return appSettingsDTO.getKeyValueList();
	}
	
	@POST
	@Path("/key")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	public AppSettingsEntity getKeyValue(AppSettingsEntity appSetting) {
		return appSettingsDTO.getKeyValue(appSetting.getApp_setting_name());
	}
	
	@PUT
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse updateKeyValue(AppSettingsEntity appSetting) {
		GenericResponse response = appSettingsDTO.updateKeyValue(appSetting);
		return response;
	}

	@POST
	@Path("/new")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse createKeyValue(AppSettingsEntity appSetting) {
		GenericResponse response = appSettingsDTO.createKeyValue(appSetting);
		return response;
	}
	
	@DELETE
	@RolesAllowed("ADMIN")
	@Path("{key}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse deleteKeyValue(@PathParam("key") String key) {
		GenericResponse response = appSettingsDTO.deleteKeyValue(key);
		return response;
	}
	
	@POST
	@Path("/recaptcha/client/key")
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public AppSettingsEntity getRecaptchaClientKey() {
		return appSettingsDTO.getRecaptchaClientKey();
	}
	
	@POST
	@Path("/index/rebuild")
	@RolesAllowed("ADMIN")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenericResponse reBuildLuceneIndex() {
		GenericResponse response = appSettingsDTO.reBuildLuceneIndex();
		return response;
	}
	
}
