package com.memmorable.backend.services.rest.request;

public class LocationRequest {
	private int countryId;
	private int countryName;
	private int stateId;
	private int stateName;
	private int cityId;
	private int cityName;

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public int getCountryName() {
		return countryName;
	}

	public void setCountryName(int countryName) {
		this.countryName = countryName;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public int getStateName() {
		return stateName;
	}

	public void setStateName(int stateName) {
		this.stateName = stateName;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getCityName() {
		return cityName;
	}

	public void setCityName(int cityName) {
		this.cityName = cityName;
	}

}
