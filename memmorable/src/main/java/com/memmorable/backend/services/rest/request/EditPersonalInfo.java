package com.memmorable.backend.services.rest.request;

import com.memmorable.backend.orm.entity.PersonalEntity;


public class EditPersonalInfo {

	public CurrentUser user;
	public PersonalEntity personal;
}
