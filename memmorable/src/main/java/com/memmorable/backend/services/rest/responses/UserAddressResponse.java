package com.memmorable.backend.services.rest.responses;

import java.util.List;

public class UserAddressResponse<T> extends GenericResponse {
	
	public List<T> addresses;

}
