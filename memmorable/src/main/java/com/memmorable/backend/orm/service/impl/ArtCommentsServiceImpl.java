package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtCommentsDAO;
import com.memmorable.backend.orm.entity.ArtCommentsEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ArtCommentsService;

@Service("artCommentsService")
@Transactional
public class ArtCommentsServiceImpl implements ArtCommentsService {
	
	@Autowired
	private ArtCommentsDAO dao;

	@Override
	public ArtCommentsEntity findbyId(ArtCommentsEntity id) {
		return dao.find(id);
	}

	@Override
	public void update(ArtCommentsEntity artComments) {
		dao.update(artComments);
	}

	@Override
	public void save(ArtCommentsEntity artComments) {
		dao.save(artComments);
	}

	@Override
	public List<ArtCommentsEntity> findByArt(ArtEntity art) {
		return dao.findByArt(art);
	}

	@Override
	public List<ArtCommentsEntity> findByUser(UserEntity user) {
		return dao.findByUser(user);
	}

	@Override
	public ArtCommentsEntity findbyId(int id) {
		return dao.findById(id);
	}

}
