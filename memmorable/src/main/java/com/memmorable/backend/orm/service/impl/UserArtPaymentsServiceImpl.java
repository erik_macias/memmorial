package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.dao.UserArtPaymentsDAO;
import com.memmorable.backend.orm.dto.ReportRanges;
import com.memmorable.backend.orm.dto.UserCreatedReportDTO;
import com.memmorable.backend.orm.dto.UserSalesReportDTO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.UserArtPaymentsService;

@Service("userArtPaymentsService")
@Transactional
public class UserArtPaymentsServiceImpl implements UserArtPaymentsService {
	
	@Autowired
	private UserArtPaymentsDAO dao;
	
	@Override
	public void save(UserArtPaymentsEntity userArtPayments) {
		dao.save(userArtPayments);
	}

	@Override
	public void update(UserArtPaymentsEntity userArtPayments) {
		dao.update(userArtPayments);
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user) {
		return dao.findAllUserArtPayments(user);
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user, String paymentType) {
		return dao.findAllUserArtPayments(user, paymentType);
	}
	
	@Override
	public UserArtPaymentsEntity findByPaymentId(String paymentId) {
		return dao.findByPaymentId(paymentId);
	}

	@Override
	public UserArtPaymentsEntity findByArt(ArtEntity art, String paymentType) {
		return dao.findByArt(art, paymentType);
	}
	
	@Override
	public UserArtPaymentsEntity findByArtAndBuyer(ArtEntity art, UserEntity user) {
		return dao.findByArtAndBuyer(art, user);
	}

	@Override
	public List<UserSalesReportDTO> getUserArtPaymentsReport(UserEntity user, int year) {
		return dao.findAllUserArtPaymentsReport(user, MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT, year);
	}
	
	@Override
	public List<UserSalesReportDTO> getUserArtFeePaymentsReport(UserEntity user, int year) {
		return dao.findAllUserArtPaymentsReport(user, MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT, year);
	}

	@Override
	public List<UserSalesReportDTO> getUsersArtPaymentsReport(int year) {
		return dao.findAllUserArtPaymentsReport(MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT, year);
	}
	
	@Override
	public List<UserSalesReportDTO> getUsersArtPaymentsReport(ReportRanges ranges) {
		return dao.findAllUserArtPaymentsReport(MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT, ranges);
	}
	
	@Override
	public List<UserSalesReportDTO> getUsersArtFeePaymentsReport(int year) {
		return dao.findAllUserArtPaymentsReport(MemmorableConstants.ART_PAYMENT_TYPE_FEE, year);
	}

	@Override
	public List<UserArtPaymentsEntity> getFeePaidPending() {
		return dao.findUserWithFeePending();
	}

	@Override
	public List<UserCreatedReportDTO> getUsersCreatedReport(int year) {
		return dao.findUsersCreated(year);
	}

	@Override
	public UserArtPaymentsEntity getArtPaymentsAmountPending(ArtEntity art) {
		return dao.getArtPaymentsAmountPending(art);
	}
	
	@Override
	public UserArtPaymentsEntity getArtPaymentsFeePending(ArtEntity art) {
		return dao.getArtPaymentsFeePending(art);
	}

	@Override
	public List<ArtEntity> getArtsWithFeePending(int intervalDays) {
		return dao.getArtsWithFeePending(intervalDays);
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserPaymentsToSeller(UserEntity buyerUser, UserEntity sellerUser) {
		return dao.findAllUserPaymentsToSeller(buyerUser, sellerUser);
	}

	@Override
	public List<UserSalesReportDTO> getUsersArtFeePaymentsReport(ReportRanges ranges) {
		return dao.findAllUserArtPaymentsReport(MemmorableConstants.ART_PAYMENT_TYPE_FEE, ranges);
	}

}
