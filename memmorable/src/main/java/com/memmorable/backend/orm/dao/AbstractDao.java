package com.memmorable.backend.orm.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractDao <T> implements InterfaceDAO<T>{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Class<T> type ;
	@SuppressWarnings("unchecked")
	private T obj = (T) new Object();
	 
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void persist(Object entity) {
		getSession().persist(entity);
	}

	public void delete(Object entity) {
		getSession().delete(entity);
	}
	
	public void save(T entity){
		getSession().save(entity);
	}

    @SuppressWarnings("unchecked")
	public  T find(final Serializable id) {
        return (T) getSession().get(type,id);
    }

    @SuppressWarnings("unchecked")
	public T update(final T t) {
        return (T) getSession().merge(t);    
    }
    
    @SuppressWarnings("unchecked")
	public List<T> findAll(Class<T> classType){
    	Criteria criteria = getSession().createCriteria(classType);
		return (List<T>) criteria.list();
    	
    }
	
}
