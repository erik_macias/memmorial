package com.memmorable.backend.orm.dto;

public class ArtMediaDTO {
	
	private int artMediaId;
	private String artMediaType;
	private String artMediaFilename;
	boolean primary;
	
	public int getArtMediaId() {
		return artMediaId;
	}
	
	public void setArtMediaId(int artMediaId) {
		this.artMediaId = artMediaId;
	}
	
	public String getArtMediaType() {
		return artMediaType;
	}
	
	public void setArtMediaType(String artMediaType) {
		this.artMediaType = artMediaType;
	}
	
	public String getArtMediaFilename() {
		return artMediaFilename;
	}
	
	public void setArtMediaFilename(String artMediaFilename) {
		this.artMediaFilename = artMediaFilename;
	}
	
	public boolean isPrimary() {
		return primary;
	}
	
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

}
