package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.AddressTypeDAO;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.service.AddressTypeService;

@Service("addressTypeService")
@Transactional
public class AddressTypeServiceImpl implements AddressTypeService {

	@Autowired
	private AddressTypeDAO dao;
	
	@Override
	public void save(AddressTypeEntity addressType) {
		dao.save(addressType);
	}

	@Override
	public void update(AddressTypeEntity addressType) {
		dao.update(addressType);
	}

	@Override
	public AddressTypeEntity findById(AddressTypeEntity id) {
		AddressTypeEntity addressType = (AddressTypeEntity) dao.find(id);
		return addressType;
	}
	
	@Override
	public AddressTypeEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public AddressTypeEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<AddressTypeEntity> findAll() {
		return dao.findAll(AddressTypeEntity.class);
	}

}
