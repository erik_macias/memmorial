package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTypeGenreDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;
import com.memmorable.backend.orm.service.ArtTypeGenreService;


@Service("artTypeGenreService")
@Transactional
public class ArtTypeGenreServiceImpl implements ArtTypeGenreService {
	
	@Autowired
	private ArtTypeGenreDAO dao;

	@Override
	public void save(ArtTypeGenreEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtTypeGenreEntity findGenre(ArtTypeGenreEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtTypeGenreEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<ArtTypeGenreEntity> findByType(ArtTypeEntity type) {
		return dao.findByType(type);
	}
	
	@Override
	public ArtTypeGenreEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void update(ArtTypeGenreEntity artTag) {
		dao.update(artTag);
		
	}

	@Override
	public ArtTypeGenreEntity findByNameType(String name, ArtTypeEntity type) {
	return dao.findByNameType(name, type);
		
	}

	@Override
	public List<ArtTypeGenreEntity> findAll() {
		return dao.findAll(ArtTypeGenreEntity.class);
	}

}
