package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface ProfileService {
	void save(ProfileEntity personal);
	ProfileEntity findProfile(ProfileEntity id);
	ProfileEntity findByUser(UserEntity user);
	void update(ProfileEntity personal);
	int getProfileVisits(UserEntity user);
}
