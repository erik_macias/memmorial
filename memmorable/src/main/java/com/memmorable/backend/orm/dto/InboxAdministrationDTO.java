package com.memmorable.backend.orm.dto;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.InboxService;
import com.memmorable.backend.orm.service.InboxStatusService;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.InboxMessageRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.MessagesListResponse;
import com.nisoph.tools.mailer.engine.EmailTemplateSender;

public class InboxAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();

	UserService userService = (UserService) context.getBean("userService");
	InboxService inboxService = (InboxService) context.getBean("inboxService");
	InboxStatusService inboxStatusService = (InboxStatusService) context.getBean("inboxStatusService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");

	public MessagesListResponse<InboxEntity> getAllInboxMessages(CurrentUser user){
		String username = user.username;
		
		UserEntity userEnt = userService.findByName(username);
		InboxStatusEntity status = inboxStatusService.findByName(MemmorableConstants.INBOX_STATUS_ENABLED);
		List<InboxEntity> msgsInbox = inboxService.findByStatusAndUser(userEnt, status);
		List<InboxEntity> msgsSent = inboxService.findByStatusAndFromUser(userEnt, status);
		
		MessagesListResponse<InboxEntity> response = new MessagesListResponse<InboxEntity>();
		response.setMsgsInbox(msgsInbox);
		response.setMsgsSent(msgsSent);
		
		if (msgsInbox != null && msgsSent != null && !msgsInbox.isEmpty() && !msgsSent.isEmpty()) {
			response.message = "Successfully retrieved all user inbox/sent messages";
		} else {
			response.message = "No messages";
		}
		response.success = true;
		
		return response;
	}

	public MessagesListResponse<InboxEntity> getAllUnreadInboxMessages(CurrentUser user) {
		String username = user.username;
		
		UserEntity userEnt = userService.findByName(username);
		InboxStatusEntity statusEnabled = inboxStatusService.findByName(MemmorableConstants.INBOX_STATUS_ENABLED);
		List<InboxEntity> msgsInbox = inboxService.findAllUnreadByStatusAndUser(userEnt, statusEnabled);
		
		MessagesListResponse<InboxEntity> response = new MessagesListResponse<InboxEntity>();
		response.setMsgsInbox(msgsInbox);
		if (msgsInbox != null && !msgsInbox.isEmpty()) {
			response.message = "Successfully retrieved all unread user inbox messages";
		} else {
			response.message = "No messages";
		}
		response.success = true;
		
		return response;
	}

	public GenericResponse sendNewInboxMessage(InboxMessageRequest message) {
		UserEntity fromUser = userService.findByName(message.getFrom());
		UserEntity toUser = userService.findByName(message.getTo());
		InboxStatusEntity statusEnabled = inboxStatusService.findByName(MemmorableConstants.INBOX_STATUS_ENABLED);
		
		//Add Inbox
		InboxEntity newInbox = new InboxEntity();
		newInbox.setFromUser(fromUser);
		newInbox.setToUser(toUser);
		newInbox.setInboxStatus(statusEnabled);
		newInbox.setSubject(message.getSubject());
		newInbox.setMessage(message.getMessage());
		newInbox.setCreated_date(new Date());
		newInbox.setRead(Boolean.FALSE);
		inboxService.save(newInbox);
		
		sendNewInboxMessageEmail(newInbox);
		
		GenericResponse response = new GenericResponse();
		response.message = "Successfully added new user inbox message";
		response.success = true;
		return response;
	}
	
	public void sendNewInboxMessageEmail(InboxEntity newInbox) {
		try {
			PersonalEntity personalTo = personalService.findByUser(newInbox.getToUser());
			PersonalEntity personalFrom = personalService.findByUser(newInbox.getFromUser());
			
			if (personalTo != null && personalTo.getPersonal_email() != null & !personalTo.getPersonal_email().equalsIgnoreCase("")) {
				
				ResourceBundle properties = ResourceBundle.getBundle("config");
				
				URL domain = new URL(properties.getString("domain.app"));
				URL url = new URL(domain + "/#/inbox/");
				
				EmailTemplateSender.sendUserArtMessagesEmail(
					personalTo.getPersonal_email(), 
					personalFrom.getPersonal_name() + " " + personalFrom.getPersonal_l_name(), 
					"Private Message: " + newInbox.getSubject(),
					url.toString(),
					newInbox.getMessage());
			}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public GenericEntityResponse<InboxEntity> getByInboxMessage(int msgId) {
		InboxEntity message = inboxService.findbyId(msgId);
		
		if (!message.isRead()) {
			setInboxMessageAsRead(msgId);
		}
		
		GenericEntityResponse<InboxEntity> response = new GenericEntityResponse<InboxEntity>();
		response.entity = message;
		response.message = "Successfully loaded inbox message";
		response.success = true;
		return response;
	}

	public GenericResponse setInboxMessageAsRead(int msgId) {
		InboxEntity message = inboxService.findbyId(msgId);
		message.setRead(Boolean.TRUE);
		inboxService.update(message);
		
		GenericResponse response = new GenericResponse();
		response.message = "Successfully updated message as read";
		response.success = true;
		return response;
	}

	public GenericResponse setInboxMessageAsUnread(int msgId) {
		InboxEntity message = inboxService.findbyId(msgId);
		message.setRead(Boolean.FALSE);
		inboxService.update(message);
		
		GenericResponse response = new GenericResponse();
		response.message = "Successfully updated message as unread";
		response.success = true;
		return response;
	}

	public GenericResponse deleteMsg(int msgId) {
		InboxEntity message = inboxService.findbyId(msgId);
		InboxStatusEntity statusDeleted = inboxStatusService.findByName(MemmorableConstants.INBOX_STATUS_DELETED);
		message.setInboxStatus(statusDeleted);
		inboxService.update(message);
		
		GenericResponse response = new GenericResponse();
		response.message = "Message deleted successfully";
		response.success = true;
		return response;
	}

}
