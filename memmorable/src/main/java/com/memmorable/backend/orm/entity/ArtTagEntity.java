package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_art_tag")
public class ArtTagEntity implements Serializable {
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_tag_id", nullable = false, unique = true)
	private int art_tag_id;

	@Column(name = "art_tag_name", nullable = false, length = 100)
	private String art_tag_name;


	public int getArt_tag_id() {
		return art_tag_id;
	}

	public void setArt_tag_id(int art_tag_id) {
		this.art_tag_id = art_tag_id;
	}

	public String getArt_tag_name() {
		return art_tag_name;
	}

	public void setArt_tag_name(String art_tag_name) {
		this.art_tag_name = art_tag_name;
	}

}
