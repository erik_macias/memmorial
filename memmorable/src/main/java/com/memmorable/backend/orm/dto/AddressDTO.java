package com.memmorable.backend.orm.dto;

public class AddressDTO {

	private int addressId;
	private String addressName;
	private String addressInitial;
	private String addressLName;
	private String addressPhone;
	private String addressMobile;
	private String addressLine1;
	private String addressLine2;
	private String addressCity;
	private int addressCityId;
	private String addressState;
	private int addressStateId;
	private String addressZip;
	private String addressType;
	private String country;
	private int countryId;
	private String user;
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public String getAddressName() {
		return addressName;
	}
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}
	public String getAddressInitial() {
		return addressInitial;
	}
	public void setAddressInitial(String addressInitial) {
		this.addressInitial = addressInitial;
	}
	public String getAddressLName() {
		return addressLName;
	}
	public void setAddressLName(String addressLName) {
		this.addressLName = addressLName;
	}
	public String getAddressPhone() {
		return addressPhone;
	}
	public void setAddressPhone(String addressPhone) {
		this.addressPhone = addressPhone;
	}
	public String getAddressMobile() {
		return addressMobile;
	}
	public void setAddressMobile(String addressMobile) {
		this.addressMobile = addressMobile;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressCity() {
		return addressCity;
	}
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}
	public int getAddressCityId() {
		return addressCityId;
	}
	public void setAddressCityId(int addressCityId) {
		this.addressCityId = addressCityId;
	}
	public String getAddressState() {
		return addressState;
	}
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}
	public int getAddressStateId() {
		return addressStateId;
	}
	public void setAddressStateId(int addressStateId) {
		this.addressStateId = addressStateId;
	}
	public String getAddressZip() {
		return addressZip;
	}
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

}
