package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;

public interface UserPaymentsDAO extends InterfaceDAO<UserPaymentsEntity>{
	public UserPaymentsEntity findByAgreementId(String agreementId);
	public List<UserPaymentsEntity> findAllUserPayments(UserEntity user);
	public UserPaymentsEntity findByRole(RoleEntity role);
	public UserPaymentsEntity findByUserAndRole(UserEntity user, RoleEntity role);
}
