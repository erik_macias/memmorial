package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.UserPaymentsDAO;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;
import com.memmorable.backend.orm.service.UserPaymentsService;

@Service("userPaymentsService")
@Transactional
public class UserPaymentsServiceImpl implements UserPaymentsService {
	
	@Autowired
	private UserPaymentsDAO dao;
	
	@Override
	public void save(UserPaymentsEntity userPayments) {
		dao.save(userPayments);
	}

	@Override
	public void update(UserPaymentsEntity userPayments) {
		dao.update(userPayments);
	}

	@Override
	public List<UserPaymentsEntity> findAllUserPayments(UserEntity user) {
		return dao.findAllUserPayments(user);
	}

	@Override
	public UserPaymentsEntity findByAgreementId(String agreementId) {
		return dao.findByAgreementId(agreementId);
	}

	@Override
	public UserPaymentsEntity findByRole(RoleEntity role) {
		return dao.findByRole(role);
	}

	@Override
	public UserPaymentsEntity findByUserAndRole(UserEntity user, RoleEntity role) {
		return dao.findByUserAndRole(user, role);
	}

}
