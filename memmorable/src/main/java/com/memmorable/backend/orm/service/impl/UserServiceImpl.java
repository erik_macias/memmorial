package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.UserDAO;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.UserService;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO dao;
	
	@Override
	public void save(UserEntity user) {
		dao.save(user);
	}

	@Override
	public List<UserEntity> findAllUsers() {
		List<UserEntity> list = dao.findAll(UserEntity.class);
		return list;
	}

	@Override
	public void deleteByName(String name) {
		dao.deleteeByName(name);
		
	}

	@Override
	public UserEntity findByName(String name) {
		UserEntity user = dao.findByName(name);
		return user;
	}
	
	@Override
	public void update(UserEntity user) {
		dao.update(user);
		
	}

}
