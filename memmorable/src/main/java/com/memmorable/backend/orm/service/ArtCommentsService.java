package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtCommentsEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface ArtCommentsService {
	void save(ArtCommentsEntity artComment);
	ArtCommentsEntity findbyId(ArtCommentsEntity id);
	ArtCommentsEntity findbyId(int id);
	List<ArtCommentsEntity> findByArt(ArtEntity art);
	List<ArtCommentsEntity> findByUser(UserEntity user);
	void update(ArtCommentsEntity artComment);
}
