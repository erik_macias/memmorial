package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserPaymentsDAO;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;

@Repository("userPaymentsDAO")
public class UserPaymentsDAOImpl extends AbstractDao<UserPaymentsEntity> implements UserPaymentsDAO {

	@Override
	public UserPaymentsEntity findByAgreementId(String agreementId) {
		Criteria crit = getSession().createCriteria(UserPaymentsEntity.class);
		crit.add(Restrictions.eq("agreementId", agreementId));
		return (UserPaymentsEntity) crit.uniqueResult();
	}

	@Override
	public List<UserPaymentsEntity> findAllUserPayments(UserEntity user) {
		Criteria crit = getSession().createCriteria(UserPaymentsEntity.class);
		crit.add(Restrictions.eq("user", user));
		return crit.list();
	}

	@Override
	public UserPaymentsEntity findByRole(RoleEntity role) {
		DetachedCriteria maxId = DetachedCriteria.forClass(UserPaymentsEntity.class)
			    .setProjection(Projections.max("user_payment_id") );
		Criteria crit = getSession().createCriteria(UserPaymentsEntity.class);
		crit.add(Property.forName("user_payment_id").eq(maxId));
		crit.add(Restrictions.eq("role", role));
		return (UserPaymentsEntity) crit.uniqueResult();
	}

	@Override
	public UserPaymentsEntity findByUserAndRole(UserEntity user, RoleEntity role) {
		Criteria crit = getSession().createCriteria(UserPaymentsEntity.class);
		crit.add(Restrictions.eq("user", user));
		crit.add(Restrictions.eq("role", role));
		return (UserPaymentsEntity) crit.uniqueResult();
	}
	
}
