package com.memmorable.backend.orm.dto;

import java.util.Date;

public class UserSurveyDTO {

	private int surveyId;
	private int artId;
	private String artName;
	private String fromUser;
	private String toUser;
	private int rating;
	private String comments;
	private String transaction;
	private String status;
	private Date createdDate;
	private Date lastUpdatedDate;
	
	public int getSurveyId() {
		return surveyId;
	}
	
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}
	
	public int getArtId() {
		return artId;
	}
	
	public void setArtId(int artId) {
		this.artId = artId;
	}
	
	public String getArtName() {
		return artName;
	}
	
	public void setArtName(String artName) {
		this.artName = artName;
	}
	
	public String getFromUser() {
		return fromUser;
	}
	
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	
	public String getToUser() {
		return toUser;
	}
	
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
}
