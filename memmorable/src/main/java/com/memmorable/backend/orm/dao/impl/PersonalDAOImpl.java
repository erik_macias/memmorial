package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.PersonalDAO;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("PersonalDAO")
public class PersonalDAOImpl extends AbstractDao<PersonalEntity> implements PersonalDAO {
	
	public PersonalEntity findbyUser(UserEntity user) {	
		Criteria crit = getSession().createCriteria(PersonalEntity.class);
		crit.add(Restrictions.eq("user", user));
		return (PersonalEntity) crit.uniqueResult();
	}
	
	public PersonalEntity findbyEmail(String email) {	
		Criteria crit = getSession().createCriteria(PersonalEntity.class);
		crit.add(Restrictions.eq("personal_email", email));
		return (PersonalEntity) crit.uniqueResult();
	}

	@Override
	public List<PersonalEntity> findAll() {
		Criteria crit = getSession().createCriteria(PersonalEntity.class);
		return crit.list();
	}

}
