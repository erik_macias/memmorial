package com.memmorable.backend.orm.dao;

import java.util.List;

import javax.transaction.Transactional;

import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Transactional
public interface PersonalDAO extends InterfaceDAO<PersonalEntity> {
	public List<PersonalEntity> findAll();
	public PersonalEntity findbyUser(UserEntity user);
	public PersonalEntity findbyEmail(String email);
}
