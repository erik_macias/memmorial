package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtRatingEntity;
import com.memmorable.backend.orm.entity.UserEntity;


public interface ArtRatingDAO extends InterfaceDAO<ArtRatingEntity>{
	public ArtRatingEntity findById(int  id);
	public List<ArtRatingEntity> findByArt(ArtEntity art);
	public List<ArtRatingEntity> findByUser(UserEntity user);
	public int findRateByArt(ArtEntity art);
	public ArtRatingEntity findRateByArtAndUser(ArtEntity art, UserEntity user);
}
