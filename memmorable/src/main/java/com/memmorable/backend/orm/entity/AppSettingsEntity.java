package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Table_app_settings")
public class AppSettingsEntity implements Serializable {
	
	private static final long serialVersionUID = -7791914792716629305L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "app_setting_id", nullable = false, unique = true)
	private int app_setting_id;

	@Column(name = "app_setting_name", nullable = false, length = 100)
	private String app_setting_name;

	@Column(name = "app_setting_value", nullable = false, length = 100)
	private String app_setting_value;

	public int getApp_setting_id() {
		return app_setting_id;
	}

	public void setApp_setting_id(int app_setting_id) {
		this.app_setting_id = app_setting_id;
	}

	public String getApp_setting_name() {
		return app_setting_name;
	}

	public void setApp_setting_name(String app_setting_name) {
		this.app_setting_name = app_setting_name;
	}

	public String getApp_setting_value() {
		return app_setting_value;
	}

	public void setApp_setting_value(String app_setting_value) {
		this.app_setting_value = app_setting_value;
	}

}
