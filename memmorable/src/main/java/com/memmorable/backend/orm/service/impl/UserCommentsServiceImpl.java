package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.UserCommentsDAO;
import com.memmorable.backend.orm.entity.UserCommentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.UserCommentsService;

@Service("userCommentsService")
@Transactional
public class UserCommentsServiceImpl implements UserCommentsService {
	
	@Autowired
	private UserCommentsDAO dao;

	@Override
	public UserCommentsEntity findbyId(UserCommentsEntity id) {
		return dao.find(id);
	}

	@Override
	public void update(UserCommentsEntity userComments) {
		dao.update(userComments);
	}

	@Override
	public void save(UserCommentsEntity userComments) {
		dao.save(userComments);
	}

	@Override
	public List<UserCommentsEntity> findByUser(UserEntity user) {
		return dao.findByUser(user);
	}

	@Override
	public UserCommentsEntity findbyId(int id) {
		return dao.findById(id);
	}

}
