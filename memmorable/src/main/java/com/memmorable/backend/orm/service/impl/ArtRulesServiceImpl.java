package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtRulesDAO;
import com.memmorable.backend.orm.entity.ArtRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.service.ArtRulesService;

@Service("artRulesService")
@Transactional
public class ArtRulesServiceImpl implements ArtRulesService {
	
	@Autowired
	private ArtRulesDAO dao;

	
	@Override
	public ArtRulesEntity findById(int id) {
		return dao.findByID(id);
	}

	@Override
	public ArtRulesEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(ArtRulesEntity rule) {
		dao.update(rule);
	}

	@Override
	public void save(ArtRulesEntity rule) {
		dao.save(rule);
	}

	@Override
	public List<ArtRulesEntity> findAll() {
		return dao.findAll(ArtRulesEntity.class);
	}

	@Override
	public ArtRulesEntity findByRole(RoleEntity role) {
		return dao.findByRole(role);
	}

	



}
