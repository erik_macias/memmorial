package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtRatingDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtRatingEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ArtRatingService;

@Service("artRatingService")
@Transactional
public class ArtRatingServiceImpl implements ArtRatingService {
	
	@Autowired
	private ArtRatingDAO dao;

	@Override
	public ArtRatingEntity findbyId(ArtRatingEntity id) {
		return dao.find(id);
	}
	
	@Override
	public void update(ArtRatingEntity artRating) {
		dao.update(artRating);
	}

	@Override
	public void save(ArtRatingEntity artRating) {
		dao.save(artRating);
	}

	@Override
	public List<ArtRatingEntity> findByArt(ArtEntity art) {
		return dao.findByArt(art);
	}

	@Override
	public List<ArtRatingEntity> findByUser(UserEntity user) {
		return dao.findByUser(user);
	}

	@Override
	public int findRateByArt(ArtEntity art) {
		return dao.findRateByArt(art);
	}

	@Override
	public ArtRatingEntity findRateByArtAndUser(ArtEntity art, UserEntity user) {
		return dao.findRateByArtAndUser(art, user);
	}

}
