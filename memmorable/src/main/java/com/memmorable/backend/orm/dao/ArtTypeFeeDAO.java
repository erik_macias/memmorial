package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

public interface ArtTypeFeeDAO extends InterfaceDAO<ArtTypeFeeEntity> {
	public List<ArtTypeFeeEntity> findByRole(RoleEntity role);
	public List<ArtTypeFeeEntity> findByArtType(ArtTypeEntity artType);
	public ArtTypeFeeEntity findByArtTypeAndRole(ArtTypeEntity artType, RoleEntity role);
	public ArtTypeFeeEntity findById(int id);
}
