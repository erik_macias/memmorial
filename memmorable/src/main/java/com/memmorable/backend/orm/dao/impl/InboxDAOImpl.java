package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.InboxDAO;
import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.entity.UserEntity;
@Repository("inboxDAO")
public class InboxDAOImpl extends AbstractDao<InboxEntity> implements InboxDAO {
	
	@Override
	public List<InboxEntity> findById(InboxEntity inbox) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("inbox_id", inbox));
		return crit.list();
	}
	
	@Override
	public InboxEntity findById(int id) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("inbox_id", id));
		return (InboxEntity) crit.uniqueResult();
	}

	@Override
	public List<InboxEntity> findFromUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("fromUser", user));
		return crit.list();
	}

	@Override
	public List<InboxEntity> findToUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("toUser", user));
		return crit.list();
	}

	@Override
	public List<InboxEntity> findByStatusAndUser(UserEntity user,
			InboxStatusEntity status) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("toUser", user));
		crit.add(Restrictions.eq("inboxStatus", status));
		crit.addOrder(Order.desc("created_date"));
		return crit.list();
	}

	@Override
	public List<InboxEntity> findAllUnreadByStatusAndUser(UserEntity user,
			InboxStatusEntity status) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("toUser", user));
		crit.add(Restrictions.eq("inboxStatus", status));
		crit.add(Restrictions.eq("read", Boolean.FALSE));
		crit.addOrder(Order.desc("created_date"));
		return crit.list();
	}

	@Override
	public List<InboxEntity> findByStatusAndFromUser(UserEntity user,
			InboxStatusEntity status) {
		Criteria crit = getSession().createCriteria(InboxEntity.class);
		crit.add(Restrictions.eq("fromUser", user));
		crit.add(Restrictions.eq("inboxStatus", status));
		crit.addOrder(Order.desc("created_date"));
		return crit.list();
	}
	
}
