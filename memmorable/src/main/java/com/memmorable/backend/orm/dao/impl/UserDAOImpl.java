package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserDAO;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("userDAO")
public class UserDAOImpl extends AbstractDao<UserEntity> implements UserDAO {
	
	public UserEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(UserEntity.class);
		crit.add(Restrictions.eq("user_name", name));
		return (UserEntity) crit.uniqueResult();
	}
	public void FailedLoginAttemp(String name) {
		UserEntity user = findByName(name);
		int attemps = user.getLogin_attemps();
		user.setLogin_attemps(attemps++);
		
	}
	public void resetLoginAttemps(String name) {
		UserEntity user = findByName(name);
		user.setLogin_attemps(0);
		
	}
	@Override
	public void deleteeByName(String name) {
		UserEntity user = findByName(name);
		getSession().delete(user);
		
	}

}
