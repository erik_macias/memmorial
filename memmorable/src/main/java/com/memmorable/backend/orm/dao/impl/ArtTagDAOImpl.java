package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTagDAO;
import com.memmorable.backend.orm.entity.ArtTagEntity;
@Repository("artTagDAO")
public class ArtTagDAOImpl extends AbstractDao<ArtTagEntity> implements ArtTagDAO {

		
	@Override
	public ArtTagEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtTagEntity.class);
		crit.add(Restrictions.eq("art_tag_name", name));
		return (ArtTagEntity) crit.uniqueResult();
	}

}
