package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtMediaEntity;

public interface ArtMediaDAO extends InterfaceDAO<ArtMediaEntity>{
	public ArtMediaEntity findByName(String name);
	List<ArtMediaEntity> findByArt(ArtEntity art);
	ArtMediaEntity getDefaultMediadByArt(ArtEntity art);
	ArtMediaEntity getDownloadableMediadByArt(ArtEntity art);
	public void deleteMedia(int id);
	public ArtMediaEntity findById(int id);
}
