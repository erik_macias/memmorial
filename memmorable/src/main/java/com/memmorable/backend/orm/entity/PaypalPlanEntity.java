package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_paypal_plans")
public class PaypalPlanEntity implements Serializable {
	
	private static final long serialVersionUID = -2000423868206682083L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "paypal_plan_id", nullable = false, unique = true)
	private int paypal_plan_id;

	@Column(name = "plan_id", nullable = false, length = 100)
	private String plan_id;

	@ManyToOne(targetEntity = RoleEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private RoleEntity role;

	@Column(name = "plan_status", nullable = false, length = 45)
	private String planStatus;

	public int getPaypal_plan_id() {
		return paypal_plan_id;
	}

	public void setPaypal_plan_id(int paypal_plan_id) {
		this.paypal_plan_id = paypal_plan_id;
	}

	public String getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(String plan_id) {
		this.plan_id = plan_id;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

}
