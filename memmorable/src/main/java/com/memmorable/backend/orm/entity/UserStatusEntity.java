package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_User_Status")
public class UserStatusEntity implements Serializable {
	private static final long serialVersionUID = 6951381181326022466L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_status_id", nullable = false, unique = true)
	private int user_status_id;

	@Column(name = "user_status_type", nullable = false, length = 100)
	private String user_status_type;

	public int getUser_status_id() {
		return user_status_id;
	}

	public void setUser_status_id(int user_status_id) {
		this.user_status_id = user_status_id;
	}

	public String getUser_status_type() {
		return user_status_type;
	}

	public void setUser_status_type(String user_status_type) {
		this.user_status_type = user_status_type;
	}

}
