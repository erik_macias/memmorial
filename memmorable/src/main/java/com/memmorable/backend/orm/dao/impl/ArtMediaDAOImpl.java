package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtMediaDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtMediaEntity;
@Repository("artMediaDAO")
public class ArtMediaDAOImpl extends AbstractDao<ArtMediaEntity> implements ArtMediaDAO {

		
	@Override
	public ArtMediaEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("media_name", name));
		return (ArtMediaEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtMediaEntity> findByArt(ArtEntity art) {
		Session session = getSession();
		session.enableFilter("activeMedias");
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.add(Restrictions.eq("downloadable", false));
		return crit.list();
	}

	@Override
	public ArtMediaEntity getDefaultMediadByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.add(Restrictions.eq("media_primary", true));
		crit.setMaxResults(1);
		return (ArtMediaEntity)crit.uniqueResult();
	}
	
	@Override
	public ArtMediaEntity getDownloadableMediadByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.add(Restrictions.eq("downloadable", true));
		return (ArtMediaEntity)crit.uniqueResult();
	}

	@Override
	public void deleteMedia(int id) {
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("media_id", id));
		ArtMediaEntity media2delete = (ArtMediaEntity) crit.uniqueResult();
		if (media2delete.isMedia_primary()){
			media2delete.setMedia_primary(false);
		}
		media2delete.setDeleted(true);
		update(media2delete);	
	}

	@Override
	public ArtMediaEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtMediaEntity.class);
		crit.add(Restrictions.eq("media_id", id));
		ArtMediaEntity media = (ArtMediaEntity) crit.uniqueResult();
		return media;
	}

}
