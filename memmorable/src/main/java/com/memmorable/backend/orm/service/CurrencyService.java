package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.CurrencyEntity;

public interface CurrencyService {
	void save(CurrencyEntity currency);
	CurrencyEntity findById(CurrencyEntity id);
	CurrencyEntity findById(int id);
	List<CurrencyEntity> findAll();
	CurrencyEntity findByName(String name);
	void update(CurrencyEntity currency);
}
