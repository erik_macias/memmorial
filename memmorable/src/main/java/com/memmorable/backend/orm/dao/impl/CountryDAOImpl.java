package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.CountryDAO;
import com.memmorable.backend.orm.entity.CountryEntity;

@Repository("countryDAO")
public class CountryDAOImpl extends AbstractDao<CountryEntity> implements CountryDAO {

	@Override
	public CountryEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(CountryEntity.class);
		crit.add(Restrictions.eq("country_id", id));
		return (CountryEntity) crit.uniqueResult();
	}

	@Override
	public CountryEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(CountryEntity.class);
		crit.add(Restrictions.eq("country_name", name));
		return (CountryEntity) crit.uniqueResult();
	}

	@Override
	public CountryEntity findByCountryCode(String code) {
		Criteria crit = getSession().createCriteria(CountryEntity.class);
		crit.add(Restrictions.eq("country_code", code));
		return (CountryEntity) crit.uniqueResult();
	}

}
