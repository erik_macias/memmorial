package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.UserSurveyDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;
import com.memmorable.backend.orm.service.UserSurveyService;

@Service("userSurveyService")
@Transactional
public class UserSurveyServiceImpl implements UserSurveyService {

	@Autowired
	private UserSurveyDAO dao;
	
	@Override
	public void save(UserSurveyEntity userSurvey) {
		dao.save(userSurvey);
	}

	@Override
	public void update(UserSurveyEntity userSurvey) {
		dao.update(userSurvey);
	}

	@Override
	public UserSurveyEntity findById(int id) {
		UserSurveyEntity userSurvey = dao.findById(id);
		return userSurvey;
	}

	@Override
	public List<UserSurveyEntity> findByArt(ArtEntity art) {
		return dao.findByArt(art);
	}

	@Override
	public List<UserSurveyEntity> findAllFromUser(UserEntity user) {
		return dao.findByFromUser(user);
	}

	@Override
	public List<UserSurveyEntity> findAllToUser(UserEntity user) {
		return dao.findByToUser(user);
	}

	@Override
	public List<UserSurveyEntity> findByStatus(String status) {
		return dao.findByStatus(status);
	}

	@Override
	public List<UserSurveyEntity> findByStatusAndFromUser(UserEntity user,
			String status) {
		return dao.findByStatusAndFromUser(user, status);
	}

	@Override
	public List<UserSurveyEntity> findByStatusAndToUser(UserEntity user,
			String status) {
		return dao.findByStatusAndToUser(user, status);
	}

	@Override
	public List<UserSurveyEntity> findExpiredSurveysFromUser(UserEntity user) {
		return dao.findExpiredSurveysFromUser(user);
	}

}
