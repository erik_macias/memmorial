package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.AppSettingsDAO;
import com.memmorable.backend.orm.entity.AppSettingsEntity;

@Repository("appSettingsDAO")
public class AppSettingsDAOImpl extends AbstractDao<AppSettingsEntity> implements AppSettingsDAO {

	@Override
	public AppSettingsEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(AppSettingsEntity.class);
		crit.add(Restrictions.eq("app_setting_id", id));
		return (AppSettingsEntity) crit.uniqueResult();
	}

	@Override
	public AppSettingsEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(AppSettingsEntity.class);
		crit.add(Restrictions.eq("app_setting_name", name));
		return (AppSettingsEntity) crit.uniqueResult();
	}

	@Override
	public Boolean reBuildLuceneIndex() {
		FullTextSession fullTextSession = Search.getFullTextSession(getSession());
		
		try {
			fullTextSession.createIndexer().startAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
