package com.memmorable.backend.orm.dao;

import javax.transaction.Transactional;

import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Transactional
public interface ProfileDAO extends InterfaceDAO<ProfileEntity> {
	public ProfileEntity findbyUser(UserEntity user);
	public int getProfileVisits(UserEntity user);
}
