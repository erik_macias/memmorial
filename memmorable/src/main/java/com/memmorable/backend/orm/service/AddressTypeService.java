package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.AddressTypeEntity;

public interface AddressTypeService {
	void save(AddressTypeEntity addressType);
	void update(AddressTypeEntity addressType);
	AddressTypeEntity findById(AddressTypeEntity id);
	AddressTypeEntity findById(int id);
	AddressTypeEntity findByName(String name);
	List<AddressTypeEntity> findAll();
}
