package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_Role")
public class RoleEntity implements Serializable {
	private static final long serialVersionUID = -7464470923820166087L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id", unique = true, nullable = false)
	private int role_id;

	@Column(name = "role_name", nullable = false, length = 100)
	private String role_name;

	@Column(name = "role_token", nullable = false, length = 100)
	private String role_token;
	
	@Column(name = "role_text_name", nullable = false, length = 100)
	private String role_text_name;
	
	@Column(name = "price", nullable = false)
	private Double price;
	
	@Column(name = "cycle_time", nullable = false)
	private int cycle_time;
	
	@Column(name = "fee_own_agreement", nullable = false)
	private Double feeOwnAgreement;

	@Column(name = "fee_shipping", nullable = false)
	private Double feeShipping;

	@Column(name = "publications_allowed", nullable = false)
	private int publicationsAllowed;

	@Column(name = "expiration_days", nullable = false)
	private int expirationDays;

	@Column(name = "paypal_pay_eligible", nullable = false)
	private boolean paypalPayEligible;

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getRole_token() {
		return role_token;
	}

	public void setRole_token(String role_token) {
		this.role_token = role_token;
	}

	public String getRole_text_name() {
		return role_text_name;
	}

	public void setRole_text_name(String role_text_name) {
		this.role_text_name = role_text_name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getCycle_time() {
		return cycle_time;
	}

	public void setCycle_time(int cycle_time) {
		this.cycle_time = cycle_time;
	}

	public Double getFeeOwnAgreement() {
		return feeOwnAgreement;
	}

	public void setFeeOwnAgreement(Double feeOwnAgreement) {
		this.feeOwnAgreement = feeOwnAgreement;
	}

	public Double getFeeShipping() {
		return feeShipping;
	}

	public void setFeeShipping(Double feeShipping) {
		this.feeShipping = feeShipping;
	}

	public int getPublicationsAllowed() {
		return publicationsAllowed;
	}

	public void setPublicationsAllowed(int publicationsAllowed) {
		this.publicationsAllowed = publicationsAllowed;
	}

	public int getExpirationDays() {
		return expirationDays;
	}

	public void setExpirationDays(int expirationDays) {
		this.expirationDays = expirationDays;
	}

	public boolean isPaypalPayEligible() {
		return paypalPayEligible;
	}

	public void setPaypalPayEligible(boolean paypalPayEligible) {
		this.paypalPayEligible = paypalPayEligible;
	}

}
