package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.UserCommentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface UserCommentsDAO extends InterfaceDAO<UserCommentsEntity>{
	public UserCommentsEntity findById(int id);
	public List<UserCommentsEntity> findByUser(UserEntity user);
}
