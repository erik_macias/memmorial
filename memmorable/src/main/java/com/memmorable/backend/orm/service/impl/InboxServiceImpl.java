package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.InboxDAO;
import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.InboxService;

@Service("inboxService")
@Transactional
public class InboxServiceImpl implements InboxService {
	
	@Autowired
	private InboxDAO dao;

	@Override
	public void save(InboxEntity inbox) {
		dao.save(inbox);
	}

	@Override
	public void update(InboxEntity inbox) {
		dao.update(inbox);
	}

	@Override
	public InboxEntity findbyId(InboxEntity id) {
		return dao.find(id);
	}
	
	@Override
	public InboxEntity findbyId(int id) {
		return dao.findById(id);
	}

	@Override
	public List<InboxEntity> findFromUser(UserEntity user) {
		return dao.findFromUser(user);
	}

	@Override
	public List<InboxEntity> findToUser(UserEntity user) {
		return dao.findToUser(user);
	}

	@Override
	public List<InboxEntity> findByStatusAndUser(UserEntity user,
			InboxStatusEntity status) {
		return dao.findByStatusAndUser(user, status);
	}

	@Override
	public List<InboxEntity> findAllUnreadByStatusAndUser(UserEntity user,
			InboxStatusEntity status) {
		return dao.findAllUnreadByStatusAndUser(user, status);
	}

	@Override
	public List<InboxEntity> findByStatusAndFromUser(UserEntity user,
			InboxStatusEntity status) {
		return dao.findByStatusAndFromUser(user, status);
	}

}
