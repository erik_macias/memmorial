package com.memmorable.backend.orm.dto;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserCommentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.AddressService;
import com.memmorable.backend.orm.service.AddressTypeService;
import com.memmorable.backend.orm.service.LocationService;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.ProfileService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserArtPaymentsService;
import com.memmorable.backend.orm.service.UserCommentsService;
import com.memmorable.backend.orm.service.UserPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.orm.util.Decoder;
import com.memmorable.backend.services.rest.request.ActivateUserCertRequest;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.EditPersonalInfo;
import com.memmorable.backend.services.rest.request.PasswordChange;
import com.memmorable.backend.services.rest.request.UserCommentsRequest;
import com.memmorable.backend.services.rest.responses.AuthenticationResponse;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.UserAddressResponse;
import com.memmorable.backend.services.rest.responses.UserBasicInfoResponse;
import com.memmorable.backend.services.rest.responses.UserCommentsResponse;
import com.memmorable.backend.services.rest.responses.UserDetailsResponse;
import com.memmorable.backend.services.rest.responses.UsersResponse;
import com.nisoph.tools.mailer.engine.EmailTemplateSender;
import com.nisoph.utils.EncrypterUtils;

public class UserAdministrationDTO {

	AbstractApplicationContext context = AppContext.getInstance();
	UserService userService = (UserService) context.getBean("userService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	RoleService roleService =  (RoleService) context.getBean("roleService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	AddressService addressService = (AddressService) context.getBean("addressService");
	AddressTypeService addressTypeService = (AddressTypeService) context.getBean("addressTypeService");
	LocationService locationService = (LocationService) context.getBean("locationService");
	UserCommentsService userCommentsService = (UserCommentsService) context.getBean("userCommentsService");
	UserPaymentsService userPaymentsService = (UserPaymentsService) context.getBean("userPaymentsService");
	ProfileService profileService = (ProfileService) context.getBean("profileService");
	UserArtPaymentsService userArtPaymentsService = (UserArtPaymentsService) context.getBean("userArtPaymentsService");
	
	public GenericEntityResponse<PersonalEntity> getPersonalInformation(CurrentUser user){
		String username = user.username;
		
		UserEntity userEnt= userService.findByName(username);
		PersonalEntity personal = personalService.findByUser(userEnt);
		GenericEntityResponse<PersonalEntity> response = new GenericEntityResponse<PersonalEntity>();
		response.success=true;
		response.message="succesfully retrieve the personal information from the user";
		response.entity = personal;
		return response;
	}
	
	
	public AuthenticationResponse changePassword(PasswordChange pasChange){
		CurrentUser user = pasChange.currentUser;
		String username = user.username;
		AuthenticationResponse response = new AuthenticationResponse();
		UserEntity userEnt = userService.findByName(username);
		
		userEnt.setUser_password(EncrypterUtils.encryptValue(pasChange.newPassword));
		userEnt.setLogin_attemps(0);
		UserStatusEntity status = userStatusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_ENABLED);
		userEnt.setUserStatus(status);
		userService.update(userEnt);
		response.setSuccess(true);
		response.setMessage("Login success");
		response.setUser_name(userEnt.getUser_name());
		UserBasicInfoResponse cu = new UserBasicInfoResponse();
		cu.password= EncrypterUtils.decryptValue(userEnt.getUser_password());
		cu.username= userEnt.getUser_name();
		cu.roleToken =userEnt.getRole().getRole_token();
		String authData2 = Decoder.encode(cu);
		response.setAuthData(authData2);
		
		return response;
		
	}
	
	public GenericEntityResponse<PersonalEntity> ChangePersonalInfo(EditPersonalInfo personalinfo){
		CurrentUser user = personalinfo.user;
		String username = user.username;
		PersonalEntity per = personalinfo.personal; 
		UserEntity userEnt= userService.findByName(username);
		PersonalEntity personal = personalService.findByUser(userEnt);
		personal.setPersonal_gender(per.getPersonal_gender());
		personal.setPersonal_l_name(per.getPersonal_l_name());
		personal.setPersonal_name(per.getPersonal_name());
		personal.setPersonal_email(per.getPersonal_email());
		
		personalService.update(personal);
		
		GenericEntityResponse<PersonalEntity> response = new GenericEntityResponse<PersonalEntity>();
		response.success=true;
		response.message="succesfully edited the personal information from the user";
		response.entity = personal;
		return response;
	}
	
	public UsersResponse<UserPersonalDTO> getAllUsers() {
		List<PersonalEntity> personals = personalService.findAll();
		List<UserPersonalDTO> userPersonals = new ArrayList<UserPersonalDTO>();
		
		Iterator<PersonalEntity> iter = personals.iterator();
		while(iter.hasNext()) {
			PersonalEntity personal = iter.next();
			UserPersonalDTO userPersonal = mapPersonalEntityToDTO(personal);
			
			ProfileEntity profile = profileService.findByUser(personal.getUser());
			if (profile != null) {
				userPersonal.setVisitsCounter(profile.getVisitsCounter());
			}
			
			userPersonals.add(userPersonal);
		}

		UsersResponse<UserPersonalDTO> response = new UsersResponse<UserPersonalDTO>();
		response.success = true;
		response.message = "succesfully retrieved all users";
		response.users = userPersonals;
		
		return response;
	}
	
	public UserDetailsResponse<UserPersonalDTO> getUserDetails(CurrentUser user) {
		String username = user.username;
		
		UserEntity userEntity = userService.findByName(username);
		PersonalEntity personal = personalService.findByUser(userEntity);
		UserPersonalDTO userPersonal = mapPersonalEntityToDTO(personal);
		
		UserDetailsResponse<UserPersonalDTO> response = new UserDetailsResponse<UserPersonalDTO>();
		response.success = true;
		response.message = "succesfully retrieved all users";
		response.user = userPersonal;
		
		return response;
	}
	
	public boolean deleteUser(String username) {
		UserStatusEntity userStatusDeleted = userStatusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_DISABLED);
		UserEntity user = userService.findByName(username);
		user.setUserStatus(userStatusDeleted);
		userService.update(user);
		
		return true;
	}

	public boolean updateUser(UserPersonalDTO userPersonal) {
		UserEntity userEntity = userService.findByName(userPersonal.getUsername());
		userEntity.setRole(roleService.findByRoleName(userPersonal.getRole()));
		
		if (userEntity.getUserStatus().getUser_status_type().equals(MemmorableConstants.USER_STATUS_BLOCKED) && 
				!userPersonal.getStatus().equals(MemmorableConstants.USER_STATUS_BLOCKED)) {
			userEntity.setLogin_attemps(0);
		}
		userEntity.setUserStatus(userStatusService.findByfindbyStatusType(userPersonal.getStatus()));

		PersonalEntity personal = personalService.findByUser(userEntity);
		personal.setPersonal_name(userPersonal.getName());
		personal.setPersonal_l_name(userPersonal.getLastname());
		personal.setPersonal_email(userPersonal.getEmail());
		personal.setPersonal_gender(userPersonal.getGender());
		
		userService.update(userEntity);
		personalService.update(personal);
		return true;
	}
	
	private UserPersonalDTO mapPersonalEntityToDTO(PersonalEntity personal) {
		UserPersonalDTO userPersonal = new UserPersonalDTO();
		
		userPersonal.setUsername(personal.getUser().getUser_name());
		userPersonal.setName(personal.getPersonal_name());
		userPersonal.setLastname(personal.getPersonal_l_name());
		userPersonal.setEmail(personal.getPersonal_email());
		userPersonal.setGender(personal.getPersonal_gender());
		userPersonal.setCertified(personal.getUser().isCertified());
		
		RoleEntity role = personal.getUser().getRole();
		userPersonal.setRole(role.getRole_name());
		userPersonal.setStatus(personal.getUser().getUserStatus().getUser_status_type());
		
		if (!role.getRole_name().equalsIgnoreCase(
				MemmorableConstants.USER_ROLE_FREE)
				&& !role.getRole_name().equalsIgnoreCase(
						MemmorableConstants.USER_ROLE_ADMIN)) {
			UserPaymentsEntity userPayment = userPaymentsService.findByUserAndRole(personal.getUser(), role);
			if (userPayment != null) {
				Calendar c = Calendar.getInstance(); 
				c.setTime(userPayment.getCreated_date()); 
				c.add(Calendar.DATE, role.getExpirationDays());
				userPersonal.setCreatedDate(userPayment.getCreated_date());
				userPersonal.setExpirationDate(c.getTime());
			}
		}
		
		return userPersonal;
	}

	public UserAddressResponse<AddressDTO> getUserAddresses(CurrentUser user) {
		UserEntity userEntity = userService.findByName(user.getUsername());
		List<AddressEntity> addresses = addressService.findByUser(userEntity);
		
		List<AddressDTO> addressDTOList = new ArrayList<AddressDTO>();
		Iterator<AddressEntity> iter = addresses.iterator();
		while(iter.hasNext()) {
			AddressEntity address = iter.next();
			AddressDTO addressDTO = mapAddressEntityToDTO(address);
			addressDTOList.add(addressDTO);
		}
		
		UserAddressResponse<AddressDTO> response = new UserAddressResponse<AddressDTO>();
		response.success = true;
		response.message = "succesfully retrieved all users addresses";
		response.addresses = addressDTOList;
		
		return response;
	}
	
	public boolean addUserAddress(AddressDTO userAddress) {
		AddressEntity address = new AddressEntity();
		address = mapAddressDTOtoEntity(userAddress, address);
		UserEntity user = userService.findByName(userAddress.getUser());
		address.setUser(user);
		addressService.save(address);
		return true;
	}

	public boolean updateUserAddress(AddressDTO userAddress) {
		AddressEntity address = addressService.findById(userAddress.getAddressId());
		AddressEntity addressUpdated = mapAddressDTOtoEntity(userAddress, address);
		addressService.update(addressUpdated);
		return true;
	}
	
	public boolean deleteUserAddress(int addressId) {
		AddressEntity address = addressService.findById(addressId);
		addressService.delete(address);
		return true;
	}

	private AddressDTO mapAddressEntityToDTO(AddressEntity address) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setAddressId(address.getAddress_id());
		addressDTO.setAddressInitial(address.getAddress_m_initial());
		addressDTO.setAddressLine1(address.getAddress_line1());
		addressDTO.setAddressLine2(address.getAddress_line2());
		addressDTO.setAddressLName(address.getAddress_l_name());
		addressDTO.setAddressMobile(address.getAddress_mobile());
		addressDTO.setAddressName(address.getAddress_name());
		addressDTO.setAddressPhone(address.getAddress_phone());
		addressDTO.setAddressType(address.getAddressType().getAddress_type_name());
		addressDTO.setAddressZip(address.getAddress_zip());
		addressDTO.setCountry(address.getCity().getCountryState().getCountry().getCountry_name());
		addressDTO.setCountryId(address.getCity().getCountryState().getCountry().getCountry_id());
		addressDTO.setAddressCity(address.getCity().getCityName());
		addressDTO.setAddressCityId(address.getCity().getCityId());
		addressDTO.setAddressState(address.getCity().getCountryState().getStateName());
		addressDTO.setAddressStateId(address.getCity().getCountryState().getStateId());
		addressDTO.setUser(address.getUser().getUser_name());
		
		return addressDTO;
	}
	
	private AddressEntity mapAddressDTOtoEntity(AddressDTO addressDTO, AddressEntity address) {
		address.setAddress_id(addressDTO.getAddressId());
		address.setAddress_line1(addressDTO.getAddressLine1());
		address.setAddress_line2(addressDTO.getAddressLine2());
		address.setAddress_mobile(addressDTO.getAddressMobile());
		address.setAddress_phone(addressDTO.getAddressPhone());
		address.setAddress_zip(addressDTO.getAddressZip());
		address.setAddress_m_initial(addressDTO.getAddressInitial());
		
		if (addressDTO.getAddressName() != null && !addressDTO.getAddressName().equals("")) {
			address.setAddress_name(addressDTO.getAddressName());
		} else {
			address.setAddress_name("Tmp_Name");
		}
		
		if (addressDTO.getAddressLName() != null && !addressDTO.getAddressLName().equals("")) {
			address.setAddress_l_name(addressDTO.getAddressLName());
		} else {
			address.setAddress_l_name("Tmp_L_Name");
		}
		
		AddressTypeEntity addressTypeEntity = new AddressTypeEntity();
		if (addressDTO.getAddressType() != null && !addressDTO.getAddressType().equals("")) {
			addressTypeEntity = addressTypeService.findByName(addressDTO.getAddressType());
		} else {
			addressTypeEntity = addressTypeService.findByName(MemmorableConstants.USER_ADDRESS_DEFAULT);
		}
		
		address.setAddressType(addressTypeEntity);

		CountryStatesCityEntity city = locationService.findCityByName(
				addressDTO.getCountry(), addressDTO.getAddressState(),
				addressDTO.getAddressCity());		
		address.setCity(city);
		address.setAddress_city(city.getCityName());
		address.setAddress_state(city.getCountryState().getStateName());
		
		return address;
	}

	public List<UserCommentsResponse> getUserComments(String username) {
		UserEntity user = userService.findByName(username);
		List<UserCommentsEntity> userComments = userCommentsService.findByUser(user);
		List<UserCommentsResponse> userCommentsResponse = mapArtCommentsEntityToResponse(userComments);
		return userCommentsResponse;
	}

	private List<UserCommentsResponse> mapArtCommentsEntityToResponse(
			List<UserCommentsEntity> userComments) {
		List<UserCommentsResponse> userCommentsResponseList = new ArrayList<UserCommentsResponse>();
		
		Iterator<UserCommentsEntity> iter = userComments.iterator();
		while (iter.hasNext()) {
			UserCommentsEntity userCommentsEntity = iter.next();
			UserCommentsResponse userCommentsResponse = new UserCommentsResponse();
			userCommentsResponse.setUserCommentId(userCommentsEntity.getUser_comment_id());
			userCommentsResponse.setComment(userCommentsEntity.getComment());
			userCommentsResponse.setCreatedBy(userCommentsEntity.getFromUser().getUser_name());
			userCommentsResponse.setCreatedDate(userCommentsEntity.getCreate_date());
			userCommentsResponseList.add(userCommentsResponse);
		}
		
		return userCommentsResponseList;
	}

	public GenericResponse addArtComments(UserCommentsRequest comments) {
		UserCommentsEntity userComment = new UserCommentsEntity();
		userComment.setToUser(userService.findByName(comments.getToUsername()));
		userComment.setFromUser(userService.findByName(comments.getCreatedBy()));
		userComment.setComment(comments.getComment());
		userComment.setCreate_date(new Date());
		userComment.setStatus(true);
		userCommentsService.save(userComment);
		
		sendProfileCommentsEmail(userComment);
		
		GenericResponse response = new GenericResponse();
		response.message = "New user comment has been added successfully.";
		response.success = true;
		return response;
	}
	
	public void sendProfileCommentsEmail(UserCommentsEntity profileComment) {
		try {
			PersonalEntity personalTo = personalService.findByUser(profileComment.getToUser());
			PersonalEntity personalFrom = personalService.findByUser(profileComment.getFromUser());
			
			if (personalTo != null && personalTo.getPersonal_email() != null & !personalTo.getPersonal_email().equalsIgnoreCase("")) {
				
				ResourceBundle properties = ResourceBundle.getBundle("config");
				
				URL domain = new URL(properties.getString("domain.app"));
				URL url = new URL(domain + "/#/profile/" + profileComment.getToUser().getUser_name());
				
				EmailTemplateSender.sendUserArtMessagesEmail(
					personalTo.getPersonal_email(), 
					personalFrom.getPersonal_name() + " " + personalFrom.getPersonal_l_name(), 
					"Profile: " + profileComment.getToUser().getUser_name(),
					url.toString(),
					profileComment.getComment());
			}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public GenericResponse deleteUserComments(int userCommentId) {
		UserCommentsEntity userCommentsEntity = userCommentsService.findbyId(userCommentId);
		userCommentsEntity.setStatus(false);
		userCommentsService.update(userCommentsEntity);
		
		GenericResponse response = new GenericResponse();
		response.success = true;
		response.message = "User comment has been deleted successfully";
        return response;
	}


	public GenericResponse sendContactFormComments(
			ContactFormDTO contactForm) {
		GenericResponse response = new GenericResponse();
		try {
			EmailTemplateSender.sendContactFormEmail(
					contactForm.getInputEmail(), contactForm.getInputName(),
					contactForm.getInputSubject(),
					contactForm.getInputMessage());
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		response.success = true;
		return response;
	}


	public GenericResponse certifyUser(String username) {
		GenericResponse response = new GenericResponse();
		
		UserEntity user = userService.findByName(username);
		user.setCertified(true);
		userService.update(user);
		
		response.success = true;
		response.message = "User comment has been deleted successfully";
		return response;
	}


	public GenericResponse removeCertifyUser(String username) {
		GenericResponse response = new GenericResponse();
		
		UserEntity user = userService.findByName(username);
		user.setCertified(false);
		userService.update(user);
		
		response.success = true;
		response.message = "User comment has been deleted successfully";
		return response;
	}

	public Boolean hasUserBoughtToSeller(UsersBought usersBought) {
		Boolean hasUserBought = Boolean.FALSE;
		
		UserEntity buyerUser = userService.findByName(usersBought.getBuyerUser());
		UserEntity sellerUser = userService.findByName(usersBought.getSellerUser());
		
		if (buyerUser != null && sellerUser != null) {
			List<UserArtPaymentsEntity> payments = userArtPaymentsService.findAllUserPaymentsToSeller(buyerUser, sellerUser);
			if (payments != null && payments.size() > 0) {
				hasUserBought = Boolean.TRUE;
			} else {
				List<UserArtPaymentsEntity> paymentsVice = userArtPaymentsService.findAllUserPaymentsToSeller(sellerUser, buyerUser);
				if (paymentsVice != null && paymentsVice.size() > 0) {
					hasUserBought = Boolean.TRUE;
				}
			}
		} else {
			hasUserBought = Boolean.FALSE;
		}
		
		return hasUserBought;
	}

	public GenericResponse registerUserCertificatePayment(ActivateUserCertRequest userReq) {
		GenericResponse response = new GenericResponse();
		UserEntity user = userService.findByName(userReq.getUsername());
		if (user != null) {
			RoleEntity roleCert = roleService.findByRoleName(MemmorableConstants.USER_ROLE_CERTIFIED);
			if (roleCert != null) {
				UserPaymentsEntity userPayments = new UserPaymentsEntity();
				userPayments.setUser(user);
				userPayments.setRole(roleCert);
				userPayments.setCredit(roleCert.getPrice());
				userPayments.setAgreementId(userReq.getPaymentId());
				userPayments.setCreated_date(new Date());
				userPaymentsService.save(userPayments);
				
				user.setRole(roleCert);
				userService.update(user);
				
				response.success = true;
				response.message = "Payment registered successfully. User is now Certified";
			} else {
				response.success = false;
				response.message = "Error while looking for Certificate role";
			}
		} else {
			response.success = false;
			response.message = "Error while searching for user: " + userReq.getUsername();
		}
		
		return response;
	}

}
