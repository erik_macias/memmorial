package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_inbox_status")
public class InboxStatusEntity implements Serializable {
	private static final long serialVersionUID = 8521438501203956851L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inbox_status_id", nullable = false, unique = true)
	private int inbox_status_id;

	@Column(name = "inbox_status_name", nullable = false, length = 100)
	private String inbox_status_name;

	public int getInbox_status_id() {
		return inbox_status_id;
	}

	public void setInbox_status_id(int inbox_status_id) {
		this.inbox_status_id = inbox_status_id;
	}

	public String getInbox_status_name() {
		return inbox_status_name;
	}

	public void setInbox_status_name(String inbox_status_name) {
		this.inbox_status_name = inbox_status_name;
	}

}
