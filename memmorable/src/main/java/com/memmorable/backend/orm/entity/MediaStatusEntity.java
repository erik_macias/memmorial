package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_media_status")
public class MediaStatusEntity implements Serializable {
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "media_status_id", nullable = false, unique = true)
	private int media_status_id;

	@Column(name = "media_status_type", nullable = false, length = 100)
	private String media_status_type;

	public int getMedia_status_id() {
		return media_status_id;
	}

	public void setMedia_status_id(int media_status_id) {
		this.media_status_id = media_status_id;
	}

	public String getMedia_status_type() {
		return media_status_type;
	}

	public void setMedia_status_type(String media_status_type) {
		this.media_status_type = media_status_type;
	}

}
