package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.IndexedEmbedded;

@Entity
@Table(name = "Catalog_art_subtype")
public class ArtSubTypeEntity implements Serializable {
	
	private static final long serialVersionUID = -4867496259922479686L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_subtype_id", nullable = false, unique = true)
	private int art_subtype_id;
	
	@Field
	@Column(name = "art_subtype_name", nullable = false, length = 100)
	private String art_subtype_name;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtTypeEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_type_id")
	private ArtTypeEntity art_type;	
	
	public ArtTypeEntity getArt_type() {
		return art_type;
	}

	public void setArt_type(ArtTypeEntity art_type) {
		this.art_type = art_type;
	}

	public int getArt_subtype_id() {
		return art_subtype_id;
	}

	public void setArt_subtype_id(int art_subtype_id) {
		this.art_subtype_id = art_subtype_id;
	}

	public String getArt_subtype_name() {
		return art_subtype_name;
	}

	public void setArt_subtype_name(String art_subtype_name) {
		this.art_subtype_name = art_subtype_name;
	}

	
}
