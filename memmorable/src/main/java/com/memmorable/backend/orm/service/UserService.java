package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.UserEntity;



public interface UserService {

		void save(UserEntity user);
		List<UserEntity> findAllUsers();
		void deleteByName(String name);
		UserEntity findByName(String name);
		void update(UserEntity user);
}
