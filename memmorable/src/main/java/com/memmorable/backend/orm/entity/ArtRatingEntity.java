package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_rating")
public class ArtRatingEntity implements Serializable {
	private static final long serialVersionUID = -9157501625391961819L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rating_id", nullable = false, unique = true)
	private int rating_id;
	
	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@ManyToOne(targetEntity = ArtEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_id")
	private ArtEntity art;
	
	@Column(name = "rate", nullable = false)
	private int rating_rate;

	public int getRating_id() {
		return rating_id;
	}

	public void setRating_id(int rating_id) {
		this.rating_id = rating_id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public ArtEntity getArt() {
		return art;
	}

	public void setArt(ArtEntity art) {
		this.art = art;
	}

	public int getRating_rate() {
		return rating_rate;
	}

	public void setRating_rate(int rating_rate) {
		this.rating_rate = rating_rate;
	}
	
}
