package com.memmorable.backend.orm.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserSurveyService;
import com.memmorable.backend.services.rest.request.SurveyUpdateRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityListResponse;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;

public class UserSurveyAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();
	ArtService  artService = (ArtService) context.getBean("artService");
	UserService userService = (UserService) context.getBean("userService");
	UserSurveyService userSurveyService = (UserSurveyService) context.getBean("userSurveyService");
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysFromUser(String fromUsername) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		if (fromUsername != null && !fromUsername.equals("")) {
			UserEntity fromUser = userService.findByName(fromUsername);
			
			if (fromUser != null) {
				List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findAllFromUser(fromUser));
				
				if (userSurvey != null && !userSurvey.isEmpty()) {
					response.entityList = userSurvey;
					response.success = true;
					response.message = "Survey list successfully retrieved.";
				} else {
					response.success = false;
					response.message = "Survey list is empty.";
				}
			} else {
				response.success = false;
				response.message = "From User is null or empty.";
			}
		}
		
		return response;
	}
	
	public GenericEntityResponse<UserSurveyDTO> getSurveyById(int surveyId) {
		GenericEntityResponse<UserSurveyDTO> response = new GenericEntityResponse<UserSurveyDTO>();
		
		UserSurveyDTO userSurvey = mapSurveyEntityToDTO(userSurveyService.findById(surveyId));
		
		if (userSurvey != null) {
			response.entity = userSurvey;
			response.success = true;
			response.message = "Survey successfully retrieved.";
		} else {
			response.success = false;
			response.message = "Survey is empty.";
		}
		
		return response;
	}
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysToUser(String toUsername) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		if (toUsername != null && !toUsername.equals("")) {
			UserEntity toUser = userService.findByName(toUsername);
			
			if (toUser != null) {
				List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findAllToUser(toUser));
				
				if (userSurvey != null && !userSurvey.isEmpty()) {
					response.entityList = userSurvey;
					response.success = true;
					response.message = "Survey list successfully retrieved.";
				} else {
					response.success = false;
					response.message = "Survey list is empty.";
				}
			} else {
				response.success = false;
				response.message = "To User is null or empty.";
			}
		}
		
		return response;
	}
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysByStatusAndFromUser(String fromUsername, String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		if (fromUsername != null && !fromUsername.equals("")) {
			UserEntity fromUser = userService.findByName(fromUsername);
			
			if (fromUser != null) {
				List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findByStatusAndFromUser(fromUser, status));
				
				if (userSurvey != null && !userSurvey.isEmpty()) {
					response.entityList = userSurvey;
					response.success = true;
					response.message = "Survey list successfully retrieved.";
				} else {
					response.success = false;
					response.message = "Survey list is empty.";
				}
			} else {
				response.success = false;
				response.message = "From User is null or empty.";
			}
		}
		
		return response;
	}
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysByStatusAndToUser(String toUsername, String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		if (toUsername != null && !toUsername.equals("")) {
			UserEntity toUser = userService.findByName(toUsername);
			
			if (toUser != null) {
				List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findByStatusAndToUser(toUser, status));
				
				if (userSurvey != null && !userSurvey.isEmpty()) {
					response.entityList = userSurvey;
					response.success = true;
					response.message = "Survey list successfully retrieved.";
				} else {
					response.success = false;
					response.message = "Survey list is empty.";
				}
			} else {
				response.success = false;
				response.message = "To User is null or empty.";
			}
		}
		
		return response;
	}
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysByArtId(int artId) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		ArtEntity art = artService.findbyID(artId);
		
		if (art != null) {
			List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findByArt(art));
			
			if (userSurvey != null && !userSurvey.isEmpty()) {
				response.entityList = userSurvey;
				response.success = true;
				response.message = "Survey list successfully retrieved.";
			} else {
				response.success = false;
				response.message = "Survey list is empty.";
			}
		} else {
			response.success = false;
			response.message = "To User is null or empty.";
		}
		
		return response;
	}
	
	public GenericEntityListResponse<UserSurveyDTO> getSurveysByStatus(String status) {
		GenericEntityListResponse<UserSurveyDTO> response = new GenericEntityListResponse<UserSurveyDTO>();
		
		if (status != null && !status.equals("")) {
			List<UserSurveyDTO> userSurvey = mapSurveyEntityListToDTO(userSurveyService.findByStatus(status));
			
			if (userSurvey != null && !userSurvey.isEmpty()) {
				response.entityList = userSurvey;
				response.success = true;
				response.message = "Survey list successfully retrieved.";
			} else {
				response.success = false;
				response.message = "Survey list is empty.";
			}
		}
		
		return response;
	}
	
	public GenericResponse sendSurvey(SurveyUpdateRequest request) {
		GenericResponse response = new GenericResponse();
		
		UserSurveyEntity userSurvey = userSurveyService.findById(request.getSurveyId());
		if (userSurvey != null) {
			
			if (userSurvey.getTransaction().equals(MemmorableConstants.USER_SURVEY_TRANSACTION_PURCHASE)) {
				if (request.isStatus()) {
					userSurvey.setStatus(MemmorableConstants.USER_SURVEY_STATUS_RECEIVED);
				} else {
					userSurvey.setStatus(MemmorableConstants.USER_SURVEY_STATUS_NOT_RECEIVED);
				}
			} else if (userSurvey.getTransaction().equals(MemmorableConstants.USER_SURVEY_TRANSACTION_SELL)) {
				if (request.isStatus()) {
					userSurvey.setStatus(MemmorableConstants.USER_SURVEY_STATUS_DELIVERED);
				} else {
					userSurvey.setStatus(MemmorableConstants.USER_SURVEY_STATUS_NOT_DELIVERED);
				}
			} else {
				userSurvey.setStatus(MemmorableConstants.USER_SURVEY_STATUS_IN_PROGRESS);
			}
			userSurvey.setRanking(request.getRate());
			userSurvey.setComments(request.getComments());
			userSurvey.setUpdatedDate(new Date());
			userSurveyService.update(userSurvey);
			
			response.success = true;
			response.message = "Survey send successfully.";
		} else {
			response.success = false;
			response.message = "Could not find survey.";
		}
			
		return response;
	}
	
	private List<UserSurveyDTO> mapSurveyEntityListToDTO(List<UserSurveyEntity> userSurveyList) {
		List<UserSurveyDTO> userSurveyDTOList = new ArrayList<UserSurveyDTO>();
		
		if (userSurveyList != null && !userSurveyList.isEmpty()) {
			Iterator<UserSurveyEntity> iter = userSurveyList.iterator();
			
			while (iter.hasNext()) {
				UserSurveyEntity userSurveyEntity = iter.next();
				UserSurveyDTO userSurveyDTO = mapSurveyEntityToDTO(userSurveyEntity);
				userSurveyDTOList.add(userSurveyDTO);
			}
		}
		
		return userSurveyDTOList;
	}
	
	private UserSurveyDTO mapSurveyEntityToDTO(UserSurveyEntity userSurvey) {
		UserSurveyDTO userSurveyDTO = null;
		
		if (userSurvey != null) {
			userSurveyDTO = new UserSurveyDTO();
			userSurveyDTO.setSurveyId(userSurvey.getSurveyId());
			userSurveyDTO.setArtId(userSurvey.getArt().getArt_id());
			userSurveyDTO.setArtName(userSurvey.getArt().getArt_name());
			userSurveyDTO.setFromUser(userSurvey.getFromUser().getUser_name());
			userSurveyDTO.setToUser(userSurvey.getToUser().getUser_name());
			userSurveyDTO.setRating(userSurvey.getRanking());
			userSurveyDTO.setComments(userSurvey.getComments());
			userSurveyDTO.setTransaction(userSurvey.getTransaction());
			userSurveyDTO.setStatus(userSurvey.getStatus());
			userSurveyDTO.setCreatedDate(userSurvey.getCreatedDate());
			userSurveyDTO.setLastUpdatedDate(userSurvey.getUpdatedDate());
		}
		
		return userSurveyDTO;
	}

	public GenericEntityResponse<Boolean> getPendingExpiredSurveyFromUser(String username) {
		GenericEntityResponse<Boolean> response = new GenericEntityResponse<Boolean>();
		
		response.entity = isUserSurveysPending(username);
		response.success = true;
		response.message = "Surveys Pending and Expired retrieved successfully";
		
		return response;
	}
	
	public Boolean isUserSurveysPending(String currentUser) {
		UserEntity user = userService.findByName(currentUser);
		Boolean isSurveyPending = Boolean.FALSE;
		
		if (user != null) {
			List<UserSurveyEntity> list = userSurveyService.findExpiredSurveysFromUser(user);
			if (list.size() > 0) {
				isSurveyPending =  Boolean.TRUE;
			} else {
				isSurveyPending =  Boolean.FALSE;
			}
		} else {
			isSurveyPending =  Boolean.FALSE;
		}
		
		return isSurveyPending;
	}

}
