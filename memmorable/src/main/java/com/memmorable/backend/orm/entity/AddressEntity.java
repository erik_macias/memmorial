package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_Address")
public class AddressEntity implements Serializable {
	private static final long serialVersionUID = -8953864580219172989L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "address_id", nullable = false, unique = true)
	private int address_id;
	
	@Column(name = "address_name", nullable = false, unique = false, length = 200)
	private String address_name;
	
	@Column(name = "address_m_initial", nullable = true, unique = false, length = 10)
	private String address_m_initial;
	
	@Column(name = "address_l_name", nullable = false, unique = false, length = 200)
	private String address_l_name;
	
	@Column(name = "address_phone", nullable = false, unique = false, length = 100)
	private String address_phone;
	
	@Column(name = "address_mobile", nullable = true, unique = false, length = 45)
	private String address_mobile;
	
	@Column(name = "address_line1", nullable = false, unique = false, length = 200)
	private String address_line1;
	
	@Column(name = "address_line2", nullable = true, unique = false, length = 200)
	private String address_line2;
	
	@Column(name = "address_city", nullable = false, unique = false, length = 200)
	private String address_city;
	
	@Column(name = "address_state", nullable = false, unique = false, length = 200)
	private String address_state;
	
	@Column(name = "address_zip", nullable = false, unique = false, length = 200)
	private String address_zip;
	
	@OneToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity user;

	@ManyToOne(targetEntity = AddressTypeEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "address_type_id", nullable = false)
	private AddressTypeEntity addressType;
	
	@ManyToOne(targetEntity = CountryStatesCityEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id", nullable = false)
	private CountryStatesCityEntity city;
	
	public int getAddress_id() {
		return address_id;
	}
	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}
	public String getAddress_name() {
		return address_name;
	}
	public void setAddress_name(String address_name) {
		this.address_name = address_name;
	}
	public String getAddress_m_initial() {
		return address_m_initial;
	}
	public void setAddress_m_initial(String address_m_initial) {
		this.address_m_initial = address_m_initial;
	}
	public String getAddress_l_name() {
		return address_l_name;
	}
	public void setAddress_l_name(String address_l_name) {
		this.address_l_name = address_l_name;
	}
	public String getAddress_phone() {
		return address_phone;
	}
	public void setAddress_phone(String address_phone) {
		this.address_phone = address_phone;
	}
	public String getAddress_mobile() {
		return address_mobile;
	}
	public void setAddress_mobile(String address_mobile) {
		this.address_mobile = address_mobile;
	}
	public String getAddress_line1() {
		return address_line1;
	}
	public void setAddress_line1(String address_line1) {
		this.address_line1 = address_line1;
	}
	public String getAddress_line2() {
		return address_line2;
	}
	public void setAddress_line2(String address_line2) {
		this.address_line2 = address_line2;
	}
	public String getAddress_city() {
		return address_city;
	}
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	public String getAddress_state() {
		return address_state;
	}
	public void setAddress_state(String address_state) {
		this.address_state = address_state;
	}
	public String getAddress_zip() {
		return address_zip;
	}
	public void setAddress_zip(String address_zip) {
		this.address_zip = address_zip;
	}
	public AddressTypeEntity getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressTypeEntity addressType) {
		this.addressType = addressType;
	}
	public UserEntity getUser() {
		return user;
	}
	public void setUser(UserEntity user) {
		this.user = user;
	}
	public CountryStatesCityEntity getCity() {
		return city;
	}
	public void setCity(CountryStatesCityEntity city) {
		this.city = city;
	}
}
