package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.PaypalPlanDAO;
import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.service.PaypalPlanService;

@Service("paypalPlanService")
@Transactional
public class PaypalPlanServiceImpl implements PaypalPlanService {
	
	@Autowired
	private PaypalPlanDAO dao;

	@Override
	public void save(PaypalPlanEntity plan) {
		dao.save(plan);
	}

	@Override
	public void update(PaypalPlanEntity plan) {
		dao.update(plan);
	}

	@Override
	public void delete(PaypalPlanEntity plan) {
		dao.delete(plan);
	}

	@Override
	public PaypalPlanEntity findByPlanId(PaypalPlanEntity id) {
		return dao.find(id);
	}

	@Override
	public PaypalPlanEntity findByPlanId(String planId) {
		return dao.findByPlanID(planId);
	}

	@Override
	public List<PaypalPlanEntity> findAll() {
		return dao.findAll(PaypalPlanEntity.class);
	}

	@Override
	public PaypalPlanEntity findPlanIdByRole(RoleEntity role) {
		return dao.findPlanIdByRole(role);
	}
	
}
