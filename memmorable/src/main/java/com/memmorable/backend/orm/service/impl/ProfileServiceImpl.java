package com.memmorable.backend.orm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ProfileDAO;
import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ProfileService;

@Service("profileService")
@Transactional
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private ProfileDAO dao;

	@Override
	public void save(ProfileEntity personal) {
		dao.save(personal);
	}

	@Override
	public ProfileEntity findProfile(ProfileEntity id) {
		ProfileEntity personal = (ProfileEntity) dao.find(id);
		return personal;
	}

	@Override
	public ProfileEntity findByUser(UserEntity user) {
		ProfileEntity personal = dao.findbyUser(user);
		return personal;
	}

	@Override
	public void update(ProfileEntity personal) {
		dao.update(personal);
	}

	@Override
	public int getProfileVisits(UserEntity user) {
		return dao.getProfileVisits(user);
	}

}
