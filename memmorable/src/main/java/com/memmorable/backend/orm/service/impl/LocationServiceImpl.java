package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.CountryDAO;
import com.memmorable.backend.orm.dao.LocationDAO;
import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.orm.service.LocationService;

@Service("locationService")
@Transactional
public class LocationServiceImpl implements LocationService {
	
	@Autowired
	private CountryDAO countryDao;
	
	@Autowired
	private LocationDAO locationDao;

	@Override
	public CountryEntity findCountryById(CurrencyEntity id) {
		return countryDao.find(id);
	}
	
	@Override
	public CountryEntity findCountryById(int id) {
		return countryDao.findByID(id);
	}

	@Override
	public CountryEntity findCountryByName(String name) {
		return countryDao.findByName(name);
	}

	@Override
	public void updateCountry(CountryEntity country) {
		countryDao.update(country);
	}

	@Override
	public void save(CountryEntity country) {
		countryDao.save(country);
	}

	@Override
	public List<CountryEntity> findAllCountries() {
		return countryDao.findAll(CountryEntity.class);
	}

	@Override
	public CountryEntity findCountryByCode(String code) {
		return countryDao.findByCountryCode(code);
	}

	@Override
	public CountryStatesCityEntity findCityByID(int id) {
		return locationDao.findByID(id);
	}

	@Override
	public CountryStatesCityEntity findCityByName(String countryName,
			String stateName, String cityName) {
		return locationDao.findByCityName(countryName, stateName, cityName);
	}

	@Override
	public List<CountryStatesEntity> findStatesByCountry(CountryEntity country) {
		return locationDao.findStatesByCountry(country);
	}

	@Override
	public List<CountryStatesCityEntity> findCitiesByState(CountryStatesEntity state) {
		return locationDao.findCitiesByState(state);
	}

	@Override
	public CountryStatesEntity findStateByID(int id) {
		return locationDao.findStateByID(id);
	}

}
