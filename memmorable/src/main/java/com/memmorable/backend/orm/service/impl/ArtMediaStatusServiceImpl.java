package com.memmorable.backend.orm.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.MediaStatusDAO;
import com.memmorable.backend.orm.entity.MediaStatusEntity;
import com.memmorable.backend.orm.service.ArtMediaStatusService;

@Service("artMediaStatusService")
@Transactional
public class ArtMediaStatusServiceImpl implements ArtMediaStatusService {
	
	@Autowired
	private MediaStatusDAO dao;

	@Override
	public MediaStatusEntity findId(MediaStatusEntity id) {
		return dao.find(id);
	}

	@Override
	public MediaStatusEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(MediaStatusEntity ArtMediaStatus) {
		dao.update(ArtMediaStatus);
	}

	@Override
	public void save(MediaStatusEntity ArtMediaStatus) {
		dao.save(ArtMediaStatus);
	}

}
