package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserSurveyDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;

@Repository("userSurveyDAO")
public class UserSurveyDAOImpl extends AbstractDao<UserSurveyEntity> implements UserSurveyDAO {

	@Override
	public UserSurveyEntity findById(int id) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("surveyId", id));
		return (UserSurveyEntity) crit.uniqueResult();
	}

	@Override
	public List<UserSurveyEntity> findByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("art", art));
		return crit.list();
	}

	@Override
	public List<UserSurveyEntity> findByFromUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("fromUser", user));
		crit.add(Restrictions.ne("status", MemmorableConstants.USER_SURVEY_STATUS_DELIVERED));
		crit.add(Restrictions.ne("status", MemmorableConstants.USER_SURVEY_STATUS_RECEIVED));
		return crit.list();
	}
	
	@Override
	public List<UserSurveyEntity> findExpiredSurveysFromUser(UserEntity user) {
		String hqlSelect = "from UserSurveyEntity survey "
				+ "where survey.fromUser = :from "
				+ "AND survey.status = :suveyStatus "
				+ "AND CURRENT_TIMESTAMP > ADDDATE(survey.createdDate, :intervalDays)";
		Query selectQuery = getSession().createQuery(hqlSelect);
		selectQuery.setEntity("from", user);
		selectQuery.setString("suveyStatus", MemmorableConstants.USER_SURVEY_STATUS_OPEN);
		selectQuery.setInteger("intervalDays", 14);
		
		return selectQuery.list();
	}
	
	@Override
	public List<UserSurveyEntity> findByToUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("toUser", user));
		return crit.list();
	}

	@Override
	public List<UserSurveyEntity> findByStatus(String status) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("status", status));
		return crit.list();
	}

	@Override
	public List<UserSurveyEntity> findByStatusAndFromUser(UserEntity user,
			String status) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("status", status));
		crit.add(Restrictions.eq("fromUser", user));
		return crit.list();
	}
	
	@Override
	public List<UserSurveyEntity> findByStatusAndToUser(UserEntity user,
			String status) {
		Criteria crit = getSession().createCriteria(UserSurveyEntity.class);
		crit.add(Restrictions.eq("status", status));
		crit.add(Restrictions.eq("toUser", user));
		return crit.list();
	}

}
