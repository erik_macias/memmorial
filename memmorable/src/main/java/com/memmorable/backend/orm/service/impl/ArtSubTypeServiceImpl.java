package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtSubTypeDAO;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.service.ArtSubTypeService;


@Service("artSubTypeService")
@Transactional
public class ArtSubTypeServiceImpl implements ArtSubTypeService {
	
	@Autowired
	private ArtSubTypeDAO dao;

	@Override
	public void save(ArtSubTypeEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtSubTypeEntity findSubType(ArtSubTypeEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtSubTypeEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<ArtSubTypeEntity> findByType(ArtTypeEntity type) {
		return dao.findByType(type);
	}
	
	@Override
	public ArtSubTypeEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void update(ArtSubTypeEntity artTag) {
		dao.update(artTag);
		
	}

	@Override
	public ArtSubTypeEntity findByNameType(String name, ArtTypeEntity type) {
	return dao.findByNameType(name, type);
		
	}

	@Override
	public List<ArtSubTypeEntity> findAll() {
		return dao.findAll(ArtSubTypeEntity.class);
	}

}
