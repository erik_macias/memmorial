package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

public interface ArtService {
	void save(ArtEntity art);
	void update(ArtEntity art);
	ArtEntity findArt(ArtEntity id);
	ArtEntity findByName(String name);
	ArtEntity findbyID(int id);
	SearchResultEntityList<ArtEntity> findByUser(UserEntity user);
	SearchResultEntityList<ArtEntity> findBySubType(ArtSubTypeEntity subtype);
	SearchResultEntityList<ArtEntity> findByType(ArtTypeEntity type);
	SearchResultEntityList<ArtEntity> findByArtTag(ArtTagEntity tag);
	SearchResultEntityList<ArtEntity> findbyFreeText(String searchText, int maxResults, int firstResult);
	SearchResultEntityList<ArtEntity> findbyTopTenNewArt();
	SearchResultEntityList<ArtEntity> getAllArt(int maxResults, int firstResult);
	int getArtVisitsByUser(UserEntity user);
}
