package com.memmorable.backend.orm.util;

import java.io.UnsupportedEncodingException;

import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;

public class Decoder {

public static <T> String encode(T object){
		String resp = new Gson().toJson(object);
	    String s = BaseEncoding.base64().encode(resp.getBytes());
	    return s;
}

public static <T> T decode (String s, Class<T> cl) throws UnsupportedEncodingException{
    String json = new String (BaseEncoding.base64().decode(s),"utf-8");
    T object = new Gson().fromJson(json, cl);
    return object;
}

	
}
