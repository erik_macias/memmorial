package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserStatusDAO;
import com.memmorable.backend.orm.entity.UserStatusEntity;
@Repository("userStatusDAO")
public class UserStatusDAOImpl extends AbstractDao<UserStatusEntity> implements UserStatusDAO{

	 public UserStatusEntity findbyStatusType(String name){
		 Criteria crit = getSession().createCriteria(UserStatusEntity.class);
		crit.add(Restrictions.eq("user_status_type", name));
		return (UserStatusEntity) crit.uniqueResult();
		
	}
	
}
