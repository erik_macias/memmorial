package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;

public interface LocationService {
	void save(CountryEntity country);
	CountryEntity findCountryById(CurrencyEntity id);
	CountryEntity findCountryById(int id);
	CountryEntity findCountryByName(String name);
	CountryEntity findCountryByCode(String code);
	List<CountryEntity> findAllCountries();
	void updateCountry(CountryEntity country);
	
	CountryStatesEntity findStateByID(int id);
	
	CountryStatesCityEntity findCityByID(int id);
	CountryStatesCityEntity findCityByName(String countryName, String stateName, String cityName);
	
	List<CountryStatesEntity> findStatesByCountry(CountryEntity country);
	List<CountryStatesCityEntity> findCitiesByState(CountryStatesEntity state);
}
