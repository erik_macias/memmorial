package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.PersonalDAO;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.PersonalService;


@Service("personalService")
@Transactional
public class PersonalServiceImpl implements PersonalService {
	
	@Autowired
	private PersonalDAO dao;

	@Override
	public void save(PersonalEntity personal) {
		dao.save(personal);
		
	}

	@Override
	public PersonalEntity findPersonal(PersonalEntity id) {
		PersonalEntity personal =(PersonalEntity) dao.find(id);
		return personal;
	}
	
	@Override
	public PersonalEntity findByEmail(String email) {
		PersonalEntity personal = (PersonalEntity) dao.findbyEmail(email);
		return personal;
	}

	@Override
	public PersonalEntity findByUser(UserEntity user) {
		PersonalEntity personal = dao.findbyUser(user);
		return personal;
	}

	@Override
	public void update(PersonalEntity personal) {
		dao.update(personal);
		
	}

	@Override
	public List<PersonalEntity> findAll() {
		return dao.findAll();
	}
	
	
}
