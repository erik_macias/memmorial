package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.AddressDAO;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.AddressService;

@Service("addressService")
@Transactional
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDAO dao;
	
	@Override
	public void save(AddressEntity address) {
		dao.save(address);
	}

	@Override
	public void update(AddressEntity address) {
		dao.update(address);
	}

	@Override
	public AddressEntity findAddress(AddressEntity id) {
		AddressEntity address = (AddressEntity) dao.find(id);
		return address;
	}

	@Override
	public List<AddressEntity> findByUser(UserEntity user) {
		List<AddressEntity> addresses = dao.findbyUser(user);
		return addresses;
	}

	@Override
	public AddressEntity findAddressByTypeAndUser(
			AddressTypeEntity addressType, UserEntity user) {
		return dao.findAddressByTypeAndUser(addressType, user);
	}

	@Override
	public AddressEntity findById(int addressId) {
		return dao.findById(addressId);
	}

	@Override
	public void delete(AddressEntity address) {
		dao.delete(address);
	}
	
}
