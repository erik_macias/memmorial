package com.memmorable.backend.orm.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtDAO;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtStatusEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

@Repository("artDAO")
public class ArtDAOImpl extends AbstractDao<ArtEntity> implements ArtDAO {

	@Override
	public void DeleteArt(int id) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.eq("art_id", id));
		ArtEntity art2delete = (ArtEntity) crit.uniqueResult();
		art2delete.setDeleted(true);
		update(art2delete);		
	}

	@Override
	public ArtEntity findByName(String name) {
		List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
		
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.eq("art_name", name));
		crit.add(Restrictions.in("art_status", artStatusRequired));
		return (ArtEntity) crit.uniqueResult();
	}
	
	@Override
	public ArtEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.eq("art_id", id));
		return (ArtEntity) crit.uniqueResult();
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.eq("user", user));
		return getSearchResultEntityList(crit);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findBySubType(ArtSubTypeEntity subtype) {
		List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
		
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.in("art_status", artStatusRequired));
		crit.add(Restrictions.eq("art_SubType", subtype));
		return getSearchResultEntityList(crit);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByType(ArtTypeEntity type) {
		List<ArtEntity> artList = new ArrayList<ArtEntity>();
		
		Criteria crit = getSession().createCriteria(ArtSubTypeEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		List<ArtSubTypeEntity> subtypes = crit.list();
		
		List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
		
		for (ArtSubTypeEntity subtype : subtypes) {
			crit =	getSession().createCriteria(ArtEntity.class);
			crit.add(Restrictions.in("art_status", artStatusRequired));
			crit.add(Restrictions.eq("art_type", type));
			artList.addAll(crit.list());
		}
		
		SearchResultEntityList<ArtEntity> list = new SearchResultEntityList<ArtEntity>();
		list.setEntityList(artList);
		list.setResultCount(artList.size());
		return list;
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByArtTag(ArtTagEntity tag) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.eq("art_tag", tag));
		return getSearchResultEntityList(crit);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByFreeText(String searchText, int maxResults, int firstResult) {
		if (searchText != null && !searchText.equals("") && !searchText.equals("undefined")) {
			SearchResultEntityList<ArtEntity> artList = getArtFreeTextSearchResults(searchText, maxResults, firstResult);
			
			SearchResultEntityList<ArtEntity> tmpList = getArtByLocation(searchText, maxResults, firstResult);
			if (tmpList != null && tmpList.getEntityList() != null && !tmpList.getEntityList().isEmpty()) {
				List<ArtEntity> artEntityList = artList.getEntityList();
				artEntityList.addAll(tmpList.getEntityList());
				artList.setEntityList(artEntityList);
				artList.setResultCount(artList.getEntityList().size());
			}
			
			if (artList == null || artList.getEntityList().isEmpty()) {
				artList = getArtFreeTextSearchResultsWithWildCard(searchText, maxResults, firstResult);
				
				tmpList = getArtByLocation(searchText, maxResults, firstResult);
				if (tmpList != null && tmpList.getEntityList() != null && !tmpList.getEntityList().isEmpty()) {
					List<ArtEntity> artEntityList = artList.getEntityList();
					artEntityList.addAll(tmpList.getEntityList());
					artList.setEntityList(artEntityList);
					artList.setResultCount(artList.getEntityList().size());
				}
			}
			
			return artList;
		} else {
			return getAllActiveArt(maxResults, firstResult);
		}
	}

	private SearchResultEntityList<ArtEntity> getArtByLocation(String searchText, int maxResults, int firstResult) {
		Criteria critLocation = getSession().createCriteria(AddressEntity.class, "address");
		critLocation.createAlias("address.city", "city");
		critLocation.createAlias("city.countryState", "state");
		critLocation.createAlias("state.country", "country");
		Criterion restCity = Restrictions.like("city.cityName", searchText, MatchMode.START);
		Criterion restState = Restrictions.like("state.stateName", searchText, MatchMode.START);
		Criterion restCountry = Restrictions.like("country.country_name", searchText, MatchMode.START);
		critLocation.add(Restrictions.or(restCity, restState, restCountry));
		List<AddressEntity> addressList = critLocation.list();
		
		if (addressList != null && !addressList.isEmpty()) {
			List<UserEntity> userList = new ArrayList<UserEntity>();
			Iterator<AddressEntity> iter = addressList.iterator();
			while (iter.hasNext()) {
				AddressEntity address = iter.next();
				userList.add(address.getUser());
			}
			
			if (userList != null && !userList.isEmpty()) {
				List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
				artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
				artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
				
				Criteria artCrit = getSession().createCriteria(ArtEntity.class, "art");
				artCrit.add(Restrictions.in("art.user", userList));
				artCrit.add(Restrictions.in("art_status", artStatusRequired));
				return getSearchResultEntityList(artCrit);
			} else {
				return new SearchResultEntityList<ArtEntity>();
			}
		} else {
			return new SearchResultEntityList<ArtEntity>();
		}
	}

	private SearchResultEntityList<ArtEntity> getArtFreeTextSearchResults(String searchText, int maxResults, int firstResult) {
		FullTextSession fullTextSession = Search.getFullTextSession(getSession());
		QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(ArtEntity.class).get();
		org.apache.lucene.search.Query query = qb
				.keyword()
				.onFields("art_name", "art_description",
						"art_SubType.art_subtype_name",
						"art_SubType.art_type.art_type_name",
						"art_status.art_status_type",
						"user.user_name")
				.matching(searchText.toLowerCase())
				.createQuery();
		
		FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(query, ArtEntity.class);
		fullTextQuery.enableFullTextFilter("artStatus");
		
		if (firstResult >= 0) {
			fullTextQuery.setFirstResult(firstResult);
		}
		if (maxResults > 0) {
			fullTextQuery.setMaxResults(maxResults);
		}
		
		return getSearchResultEntityList(fullTextQuery);
	}
	
	private SearchResultEntityList<ArtEntity> getArtFreeTextSearchResultsWithWildCard(String searchText, int maxResults, int firstResult) {
		FullTextSession fullTextSession = Search.getFullTextSession(getSession());
		QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(ArtEntity.class).get();
		org.apache.lucene.search.Query query = qb
				.keyword().wildcard()
				.onFields("art_name", "art_description",
						"art_SubType.art_subtype_name",
						"art_SubType.art_type.art_type_name",
						"art_status.art_status_type",
						"user.user_name")
				.matching("*"+searchText.toLowerCase()+"*")
				.createQuery();
		
		FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(query, ArtEntity.class);
		fullTextQuery.enableFullTextFilter("artStatus");
		
		if (firstResult >= 0) {
			fullTextQuery.setFirstResult(firstResult);
		}
		if (maxResults > 0) {
			fullTextQuery.setMaxResults(maxResults);
		}

		return getSearchResultEntityList(fullTextQuery);
	}
	
	private SearchResultEntityList<ArtEntity> getAllActiveArt(int maxResults, int firstResult) {
		List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
		
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.in("art_status", artStatusRequired));
		crit.addOrder(Order.desc("created_date"));
		
		if (firstResult >= 0) {
			crit.setFirstResult(firstResult);
		}
		if (maxResults > 0) {
			crit.setMaxResults(maxResults);
		}
		
		return getSearchResultEntityList(crit);
	}
	
	@Override
	public SearchResultEntityList<ArtEntity> getAllArt(int maxResults, int firstResult) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.addOrder(Order.desc("created_date"));
		
		if (firstResult >= 0) {
			crit.setFirstResult(firstResult);
		}
		if (maxResults > 0) {
			crit.setMaxResults(maxResults);
		}
		
		return getSearchResultEntityList(crit);
	}
	
	@Override
	public SearchResultEntityList<ArtEntity> findByTopTenNewArt() {
		List<ArtStatusEntity> artStatusRequired = new ArrayList<ArtStatusEntity>();
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_ACTIVE));
		artStatusRequired.add(getStatus(MemmorableConstants.ART_STATUS_IN_PROCESS));
		
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.add(Restrictions.in("art_status", artStatusRequired));
		crit.add(Restrictions.sqlRestriction("1=1 order by rand()"));
		crit.setMaxResults(30);
		
		return getSearchResultFilteredEntityList(crit);
	}
	
	private ArtStatusEntity getStatus(String statusName) {
		Criteria crit = getSession().createCriteria(ArtStatusEntity.class);
		crit.add(Restrictions.eq("art_status_type", statusName));
		return (ArtStatusEntity) crit.uniqueResult();
	}
	
	private SearchResultEntityList<ArtEntity> getSearchResultEntityList(Criteria crit) {
		SearchResultEntityList<ArtEntity> list = new SearchResultEntityList<ArtEntity>();
		List<ArtEntity> artList = crit.list();
		list.setEntityList(artList);
		list.setResultCount(artList.size());
		return list;
	}
	
	private SearchResultEntityList<ArtEntity> getSearchResultFilteredEntityList(Criteria crit) {
		SearchResultEntityList<ArtEntity> list = new SearchResultEntityList<ArtEntity>();
		List<ArtEntity> artEntityList = crit.list();
		List<ArtEntity> artFilteredList = filterExpiredArts(artEntityList);
		
		if (artFilteredList != null && !artFilteredList.isEmpty() && artFilteredList.size() > 10) {
			int counter = artFilteredList.size();
			while (artFilteredList.size() > 10) {
				artFilteredList.remove(counter-1);
				counter = artFilteredList.size();
			}
		}
		
		list.setEntityList(artFilteredList);
		list.setResultCount(artFilteredList.size());
		return list;
	}
	
	private SearchResultEntityList<ArtEntity> getSearchResultEntityList(FullTextQuery fullTextQuery) {
		SearchResultEntityList<ArtEntity> list = new SearchResultEntityList<ArtEntity>();
		List<ArtEntity> artEntityList = fullTextQuery.list();
		List<ArtEntity> artFilteredList = filterExpiredArts(artEntityList);
		list.setEntityList(artFilteredList);
		list.setResultCount(artFilteredList.size());
		
		return list;
	}
	
	private List<ArtEntity> filterExpiredArts(List<ArtEntity> artEntityList) {
		List<ArtEntity> artFilteredList = new ArrayList<ArtEntity>();
		
		if (artEntityList != null && !artEntityList.isEmpty()) {
			Iterator<ArtEntity> iter = artEntityList.iterator();
			while (iter.hasNext()) {
				ArtEntity artEnt = iter.next();
				if (artEnt != null) {
					RoleEntity roleEnt = artEnt.getUser().getRole();
					if (roleEnt != null) {
						// if -1, exp is unlimited
						if (roleEnt.getExpirationDays() >= 0) {
							Calendar cal = Calendar.getInstance();
						    cal.setTime(artEnt.getCreated_date());
						    int expInHours = roleEnt.getExpirationDays()*24;
						    cal.add(Calendar.HOUR, expInHours);
						    Date expDate = cal.getTime();
						    Date currentDate = new Date();
						    
						    if (expDate.after(currentDate)) {
						    	artFilteredList.add(artEnt);
						    }
						} else {
							artFilteredList.add(artEnt);
						}
					}
				}
			}
		}
		
		return artFilteredList;
	}

	@Override
	public int getArtVisitsByUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(ArtEntity.class);
		crit.setProjection(Projections.sum("visitsCounter"));
		crit.add(Restrictions.eq("user", user));
		return ((Long) crit.list().get(0)).intValue();
	}

}
