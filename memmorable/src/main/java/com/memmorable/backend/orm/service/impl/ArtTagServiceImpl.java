package com.memmorable.backend.orm.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTagDAO;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.service.ArtTagService;

@Service("artTagService")
@Transactional
public class ArtTagServiceImpl implements ArtTagService {
	
	@Autowired
	private ArtTagDAO dao;

	@Override
	public void save(ArtTagEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtTagEntity findTag(ArtTagEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtTagEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(ArtTagEntity artTag) {
		dao.update(artTag);
	}

}
