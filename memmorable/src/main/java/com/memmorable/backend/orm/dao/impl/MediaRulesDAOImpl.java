package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.MediaRulesDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.MediaRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
@Repository("mediaRulesDAO")
public class MediaRulesDAOImpl extends AbstractDao<MediaRulesEntity> implements MediaRulesDAO {

		
	@Override
	public MediaRulesEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(MediaRulesEntity.class);
		crit.add(Restrictions.eq("media_rules_name", name));
		return (MediaRulesEntity) crit.uniqueResult();
	}

	@Override
	public MediaRulesEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(MediaRulesEntity.class);
		crit.add(Restrictions.eq("media_rules_id", id));
		return (MediaRulesEntity) crit.uniqueResult();
	}
	@Override 
	public MediaRulesEntity findByRole(RoleEntity role){
		Criteria crit = getSession().createCriteria(MediaRulesEntity.class);
		crit.add(Restrictions.eq("role_id", role));
		return (MediaRulesEntity) crit.uniqueResult();
	}

	@Override
	public MediaRulesEntity findByRoleAndType(RoleEntity role,
			ArtTypeEntity subType) {
		Criteria crit = getSession().createCriteria(MediaRulesEntity.class);
		crit.add(Restrictions.eq("role_id", role));
		crit.add(Restrictions.eq("art_type_id", subType));
		return (MediaRulesEntity) crit.uniqueResult();
		
	}

	@Override
	public MediaRulesEntity findByType(ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(MediaRulesEntity.class);
		crit.add(Restrictions.eq("art_type_id", type));
		return (MediaRulesEntity) crit.uniqueResult();
	}
	
}
