package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.MediaRulesDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.MediaRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.service.MediaRulesService;

@Service("mediaRulesService")
@Transactional
public class MediaRulesServiceImpl implements MediaRulesService {
	
	@Autowired
	private MediaRulesDAO dao;

	
	@Override
	public MediaRulesEntity findById(int id) {
		return dao.findByID(id);
	}

	@Override
	public MediaRulesEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(MediaRulesEntity rule) {
		dao.update(rule);
	}

	@Override
	public void save(MediaRulesEntity rule) {
		dao.save(rule);
	}

	@Override
	public List<MediaRulesEntity> findAll() {
		return dao.findAll(MediaRulesEntity.class);
	}

	@Override
	public MediaRulesEntity findByRole(RoleEntity role) {
		return dao.findByRole(role);
	}

	@Override
	public MediaRulesEntity findByType(ArtTypeEntity type) {
		return dao.findByType(type);
		
	}

	@Override
	public MediaRulesEntity findByRoleAndtype(ArtTypeEntity type,
			RoleEntity role) {
		return dao.findByRoleAndType(role, type);
	}

}
