package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface PersonalService {
	void save(PersonalEntity personal);
	List<PersonalEntity> findAll();
	PersonalEntity findPersonal(PersonalEntity id);
	PersonalEntity findByEmail(String email);
	PersonalEntity findByUser(UserEntity user);
	void update(PersonalEntity personal);
	
}
