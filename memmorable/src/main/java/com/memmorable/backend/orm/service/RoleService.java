package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.RoleEntity;

public interface RoleService {
	void save(RoleEntity role);
	RoleEntity findRole(RoleEntity id);
	RoleEntity findByRoleName(String name);
	void update(RoleEntity Role);
	List<RoleEntity> findAll();
}
