package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

public interface PaypalPlanService {
	void save(PaypalPlanEntity plan);
	void update(PaypalPlanEntity plan);
	void delete(PaypalPlanEntity plan);
	PaypalPlanEntity findByPlanId(PaypalPlanEntity id);
	PaypalPlanEntity findByPlanId(String planId);
	PaypalPlanEntity findPlanIdByRole(RoleEntity role);
	List<PaypalPlanEntity> findAll();
}
