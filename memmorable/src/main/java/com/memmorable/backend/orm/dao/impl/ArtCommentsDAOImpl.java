package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtCommentsDAO;
import com.memmorable.backend.orm.entity.ArtCommentsEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
@Repository("artCommentsDAO")
public class ArtCommentsDAOImpl extends AbstractDao<ArtCommentsEntity> implements ArtCommentsDAO {
		
	@Override
	public ArtCommentsEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtCommentsEntity.class);
		crit.add(Restrictions.eq("art_comment_id", id));
		return (ArtCommentsEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtCommentsEntity> findByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(ArtCommentsEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.add(Restrictions.eq("status", Boolean.TRUE));
		return crit.list();
	}

	@Override
	public List<ArtCommentsEntity> findByUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(ArtCommentsEntity.class);
		crit.add(Restrictions.eq("user", user));
		return crit.list();
	}
	
}
