package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.MediaRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

public interface MediaRulesService {
	void save(MediaRulesEntity country);
	MediaRulesEntity findById(int id);
	MediaRulesEntity findByName(String name);
	MediaRulesEntity findByRole(RoleEntity role);
	MediaRulesEntity findByType(ArtTypeEntity subType);
	MediaRulesEntity findByRoleAndtype(ArtTypeEntity subType, RoleEntity role);
	List<MediaRulesEntity> findAll();
	void update(MediaRulesEntity rule);
}
