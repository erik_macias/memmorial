package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.UserCommentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface UserCommentsService {
	void save(UserCommentsEntity userComment);
	UserCommentsEntity findbyId(UserCommentsEntity id);
	UserCommentsEntity findbyId(int id);
	List<UserCommentsEntity> findByUser(UserEntity user);
	void update(UserCommentsEntity userComment);
}
