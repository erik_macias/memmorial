package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;

@Entity
@Table(name = "Catalog_art_status")
public class ArtStatusEntity implements Serializable {
	private static final long serialVersionUID = -9157501625391961819L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_status_id", nullable = false, unique = true)
	private int art_status_id;

	@Field
	@Column(name = "art_status_type", nullable = false, length = 100)
	private String art_status_type;

	public int getArt_status_id() {
		return art_status_id;
	}

	public void setArt_status_id(int art_status_id) {
		this.art_status_id = art_status_id;
	}

	public String getArt_status_type() {
		return art_status_type;
	}

	public void setArt_status_type(String art_status_type) {
		this.art_status_type = art_status_type;
	}

}
