package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface InboxDAO extends InterfaceDAO<InboxEntity>{
	public List<InboxEntity> findById(InboxEntity inbox);
	public InboxEntity findById(int id);
	public List<InboxEntity> findFromUser(UserEntity user);
	public List<InboxEntity> findToUser(UserEntity user);
	public List<InboxEntity> findByStatusAndUser(UserEntity user, InboxStatusEntity status);
	public List<InboxEntity> findAllUnreadByStatusAndUser(UserEntity user, InboxStatusEntity status);
	public List<InboxEntity> findByStatusAndFromUser(UserEntity user, InboxStatusEntity status);
}
