package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtMediaEntity;

public interface ArtMediaService {
	void save(ArtMediaEntity artMedia);
	ArtMediaEntity findMedia(ArtMediaEntity id);
	ArtMediaEntity findMediaById(int id);
	ArtMediaEntity findByName(String name);
	List<ArtMediaEntity> findByArt(ArtEntity art);
	ArtMediaEntity getDefaultMediadByArt(ArtEntity art);
	ArtMediaEntity getDownloadableMediadByArt(ArtEntity art);
	void update(ArtMediaEntity artMedia);
	void deleteMedia(int id );
	
}
