package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtStatusEntity;


public interface ArtStatusDAO extends InterfaceDAO<ArtStatusEntity>{
	public ArtStatusEntity findByID(int  id);
	public ArtStatusEntity findByName(String name);
}
