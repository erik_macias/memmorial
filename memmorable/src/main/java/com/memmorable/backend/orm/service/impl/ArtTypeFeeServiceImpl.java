package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTypeFeeDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.service.ArtTypeFeeService;

@Service("artTypeFeeService")
@Transactional
public class ArtTypeFeeServiceImpl implements ArtTypeFeeService {

	@Autowired
	private ArtTypeFeeDAO dao;

	@Override
	public void save(ArtTypeFeeEntity artTypeFee) {
		dao.save(artTypeFee);
	}
	
	@Override
	public void update(ArtTypeFeeEntity artTypeFee) {
		dao.update(artTypeFee);
	}
	
	@Override
	public void delete(ArtTypeFeeEntity artTypeFee) {
		dao.delete(artTypeFee);
	}

	@Override
	public List<ArtTypeFeeEntity> findall() {
		return dao.findAll(ArtTypeFeeEntity.class);
	}

	@Override
	public ArtTypeFeeEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public List<ArtTypeFeeEntity> findByArtType(ArtTypeEntity id) {
		return dao.findByArtType(id);
	}

	@Override
	public List<ArtTypeFeeEntity> findByRole(RoleEntity role) {
		return dao.findByRole(role);
	}

	@Override
	public ArtTypeFeeEntity findByArtTypeAndRole(ArtTypeEntity artType,
			RoleEntity role) {
		return dao.findByArtTypeAndRole(artType, role);
	}

}
