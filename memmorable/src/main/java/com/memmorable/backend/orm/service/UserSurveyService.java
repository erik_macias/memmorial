package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;

public interface UserSurveyService {
	void save(UserSurveyEntity userSurvey);
	void update(UserSurveyEntity userSurvey);
	public UserSurveyEntity findById(int id);
	public List<UserSurveyEntity> findByArt(ArtEntity art);
	public List<UserSurveyEntity> findAllFromUser(UserEntity user);
	public List<UserSurveyEntity> findAllToUser(UserEntity user);
	public List<UserSurveyEntity> findByStatus(String status);
	public List<UserSurveyEntity> findByStatusAndFromUser(UserEntity user, String status);
	public List<UserSurveyEntity> findByStatusAndToUser(UserEntity user, String status);
	public List<UserSurveyEntity> findExpiredSurveysFromUser(UserEntity user);
}
