package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;

public interface ArtSubTypeDAO extends InterfaceDAO<ArtSubTypeEntity> {
	public ArtSubTypeEntity findByName(String name);
	public List<ArtSubTypeEntity> findByType(ArtTypeEntity type);
	public ArtSubTypeEntity findByNameType(String name, ArtTypeEntity type);
	public ArtSubTypeEntity findById(int id);
}
