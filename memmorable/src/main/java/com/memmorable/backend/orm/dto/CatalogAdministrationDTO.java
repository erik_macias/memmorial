package com.memmorable.backend.orm.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.AddressTypeService;
import com.memmorable.backend.orm.service.ArtSubTypeService;
import com.memmorable.backend.orm.service.ArtTypeFeeService;
import com.memmorable.backend.orm.service.ArtTypeService;
import com.memmorable.backend.orm.service.ArtTypeServiceService;
import com.memmorable.backend.orm.service.LocationService;
import com.memmorable.backend.orm.service.CurrencyService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.services.rest.request.ArtTypeFeeRequest;
import com.memmorable.backend.services.rest.request.ArtTypeUsrRequest;
import com.memmorable.backend.services.rest.responses.GenericEntityResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;

public class CatalogAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();

	LocationService locationService = (LocationService) context.getBean("locationService");
	RoleService roleService = (RoleService) context.getBean("roleService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	AddressTypeService addressTypeService = (AddressTypeService) context.getBean("addressTypeService");
	ArtTypeService artTypeService  = (ArtTypeService) context.getBean("artTypeService");
	ArtSubTypeService artSubTypeService  = (ArtSubTypeService) context.getBean("artSubTypeService");
	ArtTypeServiceService artServiceService = (ArtTypeServiceService) context.getBean("artTypeServiceService");
	CurrencyService currencyService = (CurrencyService) context.getBean("currencyService");
	ArtTypeFeeService artTypeFeeService  = (ArtTypeFeeService) context.getBean("artTypeFeeService");
	UserService userService = (UserService) context.getBean("userService");

	public List<CountryEntity> getCountries(){
		List<CountryEntity> countries = locationService.findAllCountries();
		return countries;
	}
	
	public GenericEntityResponse<CountryEntity> getCountryDetails(int countryId) {
		CountryEntity country = locationService.findCountryById(countryId);
		
		GenericEntityResponse<CountryEntity> response = new GenericEntityResponse<CountryEntity>();
		response.entity = country;
		response.message = "Country details successfully retrieved";
		response.success = true;
		return response;
	}
	
	public List<CountryStatesEntity> getStates(int countryId) {
		CountryEntity country = locationService.findCountryById(countryId);
		List<CountryStatesEntity> states = locationService.findStatesByCountry(country);
		return states;
	}

	public List<CountryStatesCityEntity> getCities(int stateId) {
		CountryStatesEntity state = locationService.findStateByID(stateId);
		List<CountryStatesCityEntity> cities = locationService.findCitiesByState(state);
		return cities;
	}
	
	public GenericResponse createCountry(CountryEntity country) {
		GenericResponse response = new GenericResponse();

		if (!isCountryExist(country)) {
			CountryEntity newCountry = new CountryEntity();
			newCountry.setCountry_code(country.getCountry_code());
			newCountry.setCountry_name(country.getCountry_name());
			locationService.save(newCountry);
			response.success = true;
			response.message = "Country Successfully created";
		} else {
			response.message = "Country details already exist in the database";
			response.success = false;
		}
		
		return response;
	}
	
	public boolean isCountryExist(CountryEntity country) {
		if (locationService.findCountryByName(country.getCountry_name()) != null || 
				locationService.findCountryByCode(country.getCountry_code()) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public GenericResponse updateCountry(CountryEntity country) {
		CountryEntity countryUpdate = locationService.findCountryById(country.getCountry_id());
		countryUpdate.setCountry_code(country.getCountry_code());
		countryUpdate.setCountry_name(country.getCountry_name());
		locationService.updateCountry(countryUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Country Successfully updated";
		response.success = true;
		return response;
	}

	public List<RoleEntity> getRoles() {
		List<RoleEntity> roles = roleService.findAll();
		return roles;
	}
	
	public RoleEntity getRole(RoleEntity role) {
		return roleService.findByRoleName(role.getRole_name());
	}
	
	public GenericResponse updateRole(RoleEntity role) {
		RoleEntity roleUpd = roleService.findByRoleName(role.getRole_name());
		roleUpd.setExpirationDays(role.getExpirationDays());
		roleUpd.setFeeOwnAgreement(role.getFeeOwnAgreement());
		roleUpd.setFeeShipping(role.getFeeShipping());
		roleUpd.setPaypalPayEligible(role.isPaypalPayEligible());
		roleUpd.setPrice(role.getPrice());
		roleUpd.setPublicationsAllowed(role.getPublicationsAllowed());
		roleUpd.setRole_text_name(role.getRole_text_name());
		roleUpd.setCycle_time(role.getCycle_time());
		roleService.update(roleUpd);
		
		GenericResponse response = new GenericResponse();
		response.message = "Role Successfully updated";
		response.success = true;
		return response;
	}

	public List<UserStatusEntity> getUserStatus() {
		List<UserStatusEntity> userStatus = userStatusService.findAll();
		return userStatus;
	}
	
	public List<AddressTypeEntity> getAddressTypes(){
		List<AddressTypeEntity> addressTypes = addressTypeService.findAll();
		return addressTypes;
	}
	
	public GenericEntityResponse<AddressTypeEntity> getAddressTypeDetails(int addressTypeId) {
		AddressTypeEntity addressType = addressTypeService.findById(addressTypeId);
		
		GenericEntityResponse<AddressTypeEntity> response = new GenericEntityResponse<AddressTypeEntity>();
		response.entity = addressType;
		response.message = "Address Type details successfully retrieved";
		response.success = true;
		return response;
	}

	public GenericResponse updateAddressType(AddressTypeEntity addressType) {
		AddressTypeEntity addressTypeUpdate = addressTypeService.findById(addressType.getAddress_type_id());
		addressTypeUpdate.setAddress_type_name(addressType.getAddress_type_name());
		addressTypeService.update(addressTypeUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Address Type Successfully updated";
		response.success = true;
		return response;
	}

	public GenericResponse createAddressType(AddressTypeEntity addressType) {
		GenericResponse response = new GenericResponse();
		if (!isAddressTypeExist(addressType)) {
			AddressTypeEntity addressTypeNew = new AddressTypeEntity();
			addressTypeNew.setAddress_type_name(addressType.getAddress_type_name());
			addressTypeService.save(addressTypeNew);
			response.success = true;
			response.message = "Address Type Successfully created";
		} else {
			response.success = false;
			response.message = "Address Type details already exist in the database";
		}
		
		return response;
	}
	
	public boolean isAddressTypeExist(AddressTypeEntity addressType) {
		if (addressTypeService.findByName(addressType.getAddress_type_name()) != null) {
			return true;
		} else {
			return false;
		}
	}

	public List<ArtTypeEntity> getArtTypes() {
		List<ArtTypeEntity> artTypes = artTypeService.findall();
		return artTypes;
	}
	
	public GenericEntityResponse<ArtTypeEntity> getArtTypeDetails(int artTypeId) {
		ArtTypeEntity artType = artTypeService.findById(artTypeId);
		
		GenericEntityResponse<ArtTypeEntity> response = new GenericEntityResponse<ArtTypeEntity>();
		response.entity = artType;
		response.message = "Art Type details successfully retrieved";
		response.success = true;
		return response;
	}

	public GenericResponse updateArtType(ArtTypeEntity artType) {
		ArtTypeEntity artTypeUpdate = artTypeService.findById(artType.getArt_type_id());
		artTypeUpdate.setArt_type_name(artType.getArt_type_name());
		artTypeService.update(artTypeUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Art Type Successfully updated";
		response.success = true;
		return response;
	}

	public GenericResponse createArtType(ArtTypeEntity artType) {
		GenericResponse response = new GenericResponse();

		if (!isArtTypeExist(artType)) {
			ArtTypeEntity artTypeNew = new ArtTypeEntity();
			artTypeNew.setArt_type_name(artType.getArt_type_name());
			artTypeService.save(artTypeNew);
			response.success = true;
			response.message = "Art Type Successfully created";
		} else {
			response.success = false;
			response.message = "Art Type details already exist in the database";
		}
		
		return response;
	}
	
	public boolean isArtTypeExist(ArtTypeEntity artType) {
		if (artTypeService.findByName(artType.getArt_type_name()) != null) {
			return true;
		} else {
			return false;
		}
	}

	public List<ArtSubTypeEntity> getArtSubTypes() {
		List<ArtSubTypeEntity> artSubTypes = artSubTypeService.findAll();
		return artSubTypes;
	}
	
	public GenericEntityResponse<ArtSubTypeEntity> getArtSubTypeDetails(int artSubTypeId) {
		ArtSubTypeEntity artSubType = artSubTypeService.findById(artSubTypeId);
		
		GenericEntityResponse<ArtSubTypeEntity> response = new GenericEntityResponse<ArtSubTypeEntity>();
		response.entity = artSubType;
		response.message = "Art SubType details successfully retrieved";
		response.success = true;
		return response;
	}

	public GenericResponse updateArtSubType(ArtSubTypeEntity artSubType) {
		ArtSubTypeEntity artSubTypeUpdate = artSubTypeService.findById(artSubType.getArt_subtype_id());
		artSubTypeUpdate.setArt_subtype_name(artSubType.getArt_subtype_name());
		artSubTypeUpdate.setArt_type(artTypeService.findById(artSubType.getArt_type().getArt_type_id()));
		artSubTypeService.update(artSubTypeUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Art SubType Successfully updated";
		response.success = true;
		return response;
	}

	public GenericResponse createArtSubType(ArtSubTypeEntity artSubType) {
		GenericResponse response = new GenericResponse();

		if (!isArtSubTypeExist(artSubType)) {
			ArtSubTypeEntity artSubTypeNew = new ArtSubTypeEntity();
			artSubTypeNew.setArt_subtype_name(artSubType.getArt_subtype_name());
			artSubTypeNew.setArt_type(artTypeService.findByName(artSubType.getArt_type().getArt_type_name()));
			artSubTypeService.save(artSubTypeNew);
			response.success = true;
			response.message = "Art SubType Successfully created";
		} else {
			response.success = false;
			response.message = "Art SubType details already exist in the database";
		}
		
		return response;
	}
	
	public boolean isArtSubTypeExist(ArtSubTypeEntity artSubType) {
		if (artTypeService.findByName(artSubType.getArt_subtype_name()) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	
	
	public List<ArtTypeServiceEntity> getArtServices() {
		List<ArtTypeServiceEntity> artServicess = artServiceService.findAll();
		return artServicess;
	}
	
	public GenericEntityResponse<ArtTypeServiceEntity> getArtServicesDetails(int artSubTypeId) {
		ArtTypeServiceEntity artServices = artServiceService.findById(artSubTypeId);
		
		GenericEntityResponse<ArtTypeServiceEntity> response = new GenericEntityResponse<ArtTypeServiceEntity>();
		response.entity = artServices;
		response.message = "Art SubType details successfully retrieved";
		response.success = true;
		return response;
	}

	public GenericResponse updateArtServices(ArtTypeServiceEntity artService) {
		ArtTypeServiceEntity  artServiceUpdate = artServiceService.findById(artService.getArt_service_id());
		artServiceUpdate.setArt_service_name(artService.getArt_service_name());
		artServiceUpdate.setArt_type(artTypeService.findById(artService.getArt_type().getArt_type_id()));
		artServiceService.update(artServiceUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Art SubType Successfully updated";
		response.success = true;
		return response;
	}

	public GenericResponse createArtService(ArtTypeServiceEntity artService) {
		GenericResponse response = new GenericResponse();

		if (!isArtServiceExist(artService)) {
			ArtTypeServiceEntity artServiceNew = new ArtTypeServiceEntity();
			artServiceNew.setArt_service_name(artService.getArt_service_name());
			artServiceNew.setArt_type(artTypeService.findByName(artService.getArt_type().getArt_type_name()));
			artServiceService.save(artServiceNew);
			response.success = true;
			response.message = "Art SubType Successfully created";
		} else {
			response.success = false;
			response.message = "Art SubType details already exist in the database";
		}
		
		return response;
	}
	
	public boolean isArtServiceExist(ArtTypeServiceEntity artService) {
		if (artServiceService.findByNameType(artService.getArt_service_name(), artService.getArt_type()) != null) {
			return true;
		} else {
			return false;
		}
	}

	public List<CurrencyEntity> getCurrencies() {
		List<CurrencyEntity> currency = currencyService.findAll();
		return currency;
	}
	
	public GenericEntityResponse<CurrencyEntity> getCurrencyDetails(int currencyId) {
		CurrencyEntity currency = currencyService.findById(currencyId);
		
		GenericEntityResponse<CurrencyEntity> response = new GenericEntityResponse<CurrencyEntity>();
		response.entity = currency;
		response.message = "Currency details successfully retrieved";
		response.success = true;
		return response;
	}

	public GenericResponse updateCurrency(CurrencyEntity currency) {
		CurrencyEntity currencyUpdate = currencyService.findById(currency.getCurrency_id());
		currencyUpdate.setCurrency_type(currency.getCurrency_type());
		currencyService.update(currencyUpdate);
		
		GenericResponse response = new GenericResponse();
		response.message = "Currency Successfully updated";
		response.success = true;
		return response;
	}

	public GenericResponse createCurrency(CurrencyEntity currency) {
		GenericResponse response = new GenericResponse();

		if (!isCurrencyExist(currency)) {
			CurrencyEntity currencyNew = new CurrencyEntity();
			currencyNew.setCurrency_type(currency.getCurrency_type());
			currencyService.save(currencyNew);
			response.success = true;
			response.message = "Currency Successfully created";
		} else {
			response.success = false;
			response.message = "Currency details already exist in the database";
		}
		
		return response;
	}
	
	public boolean isCurrencyExist(CurrencyEntity currency) {
		if (artTypeService.findByName(currency.getCurrency_type()) != null) {
			return true;
		} else {
			return false;
		}
	}

	public List<ArtTypeFeeEntity> getArtTypeFees() {
		return artTypeFeeService.findall();
	}
	
	public GenericEntityResponse<ArtTypeFeeEntity> getArtTypeFeeDetails(
			int artTypeFeeId) {
		ArtTypeFeeEntity artTypeFee = artTypeFeeService.findById(artTypeFeeId);
		
		GenericEntityResponse<ArtTypeFeeEntity> response = new GenericEntityResponse<ArtTypeFeeEntity>();
		response.entity = artTypeFee;
		response.message = "Art Type Fee details successfully retrieved";
		response.success = true;
		return response;
	}
	
	public GenericEntityResponse<ArtTypeFeeEntity> getArtTypeFeeDetUsr(
			ArtTypeUsrRequest artTypeUsr) {
		GenericEntityResponse<ArtTypeFeeEntity> response = new GenericEntityResponse<ArtTypeFeeEntity>();

		UserEntity user = userService.findByName(artTypeUsr.getUsername());
		ArtTypeEntity artType = artTypeService.findByName(artTypeUsr.getArtType());
		
		if (user != null && artType != null) {
			ArtTypeFeeEntity artTypeFee = artTypeFeeService.findByArtTypeAndRole(artType, user.getRole());
			ArtTypeFeeEntity artTypeFeeNew = new ArtTypeFeeEntity();
			if (artTypeFee != null) {
				artTypeFeeNew.setFee_amount(artTypeFee.getFee_amount());
				response.entity = artTypeFeeNew;
				response.message = "Art Type Fee details successfully retrieved";
				response.success = true;
			} else {
				artTypeFeeNew.setFee_amount(new Double(0));
				response.entity = artTypeFeeNew;
				response.message = "No Art Type Fee defined for the role. Please contact Admin for details";
				response.success = false;
			}
		} else {
			response.message = "Error while retrieving Art Type Fee details";
			response.success = false;
		}
		
		return response;
	}

	public GenericResponse updateArtTypeFee(ArtTypeFeeRequest artTypeFee) {
		GenericResponse response = new GenericResponse();
		if (artTypeFee != null && artTypeFee.getArtTypeFeeId() >= 0) {
			ArtTypeFeeEntity artTypeFeeEntity = artTypeFeeService.findById(artTypeFee.getArtTypeFeeId());
			artTypeFeeEntity.setArt_type(artTypeService.findByName(artTypeFee.getArtType()));
			artTypeFeeEntity.setRole(roleService.findByRoleName(artTypeFee.getRole()));
			artTypeFeeEntity.setFee_amount(artTypeFee.getFee());
			artTypeFeeService.update(artTypeFeeEntity);
			response.message = "Art Type Fee Successfully updated";
			response.success = true;
		} else {
			response.message = "Art Type Fee could not be updated";
			response.success = false;
		}
		return response;
	}

	public GenericResponse createArtTypeFee(ArtTypeFeeRequest artTypeFee) {
		GenericResponse response = new GenericResponse();
		if (artTypeFee != null) {
			if (validateArtTypeFeeExist(artTypeFee.getArtType(), artTypeFee.getRole())){
				ArtTypeFeeEntity artTypeFeeEntity = new ArtTypeFeeEntity();
				artTypeFeeEntity.setArt_type(artTypeService.findByName(artTypeFee.getArtType()));
				artTypeFeeEntity.setRole(roleService.findByRoleName(artTypeFee.getRole()));
				artTypeFeeEntity.setFee_amount(artTypeFee.getFee());
				
				artTypeFeeService.save(artTypeFeeEntity);
				response.message = "Art Type Fee Successfully created";
				response.success = true;
			} else {
				response.message = "Art Type Fee already exist in database";
				response.success = false;
			}
		} else {
			response.message = "Art Type Fee already exist in database";
			response.success = false;
		}
		return response;
	}
	
	private boolean validateArtTypeFeeExist(String artType, String role) {
		boolean isValid = true;
		if (role != null && !role.equalsIgnoreCase("")) {
			ArtTypeEntity artTypeEnt = artTypeService.findByName(artType);
			List<ArtTypeFeeEntity> artTypeFees = artTypeFeeService.findByArtType(artTypeEnt);
			Iterator<ArtTypeFeeEntity> iter = artTypeFees.iterator();
			while(iter.hasNext()) {
				ArtTypeFeeEntity artTypeFeeEntity = iter.next();
				if(artTypeFeeEntity != null && artTypeFeeEntity.getRole().getRole_name().equalsIgnoreCase(role)) {
					isValid = false;
				}
			}
		}
		return isValid;
	}

	public GenericResponse deleteArtTypeFee(int artTypeFeeId) {
		ArtTypeFeeEntity artTypeFee = artTypeFeeService.findById(artTypeFeeId);
		artTypeFeeService.delete(artTypeFee);
		GenericResponse response = new GenericResponse();
		response.message = "Art Type Fee Successfully deleted";
		response.success = true;
		return response;
	}

}
