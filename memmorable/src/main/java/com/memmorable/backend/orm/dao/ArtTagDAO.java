package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtTagEntity;


public interface ArtTagDAO extends InterfaceDAO<ArtTagEntity>{
	public ArtTagEntity findByName(String name);		
}
