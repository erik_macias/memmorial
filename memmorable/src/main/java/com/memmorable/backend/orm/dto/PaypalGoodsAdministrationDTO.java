package com.memmorable.backend.orm.dto;

import java.io.IOException;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableAppConstants;
import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.app.common.MemmorableUtils;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtStatusEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.ArtStatusService;
import com.memmorable.backend.orm.service.ArtTypeFeeService;
import com.memmorable.backend.orm.service.ArtTypeService;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserArtPaymentsService;
import com.memmorable.backend.orm.service.UserPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserSurveyService;
import com.memmorable.backend.services.rest.request.ArtPaymentConfRequest;
import com.memmorable.backend.services.rest.request.ArtPaymentRequest;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.paypal.payments.membership.PaypalGoods;
import com.nisoph.tools.mailer.engine.EmailTemplateSender;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

public class PaypalGoodsAdministrationDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(PaypalGoodsAdministrationDTO.class);

	AbstractApplicationContext context = AppContext.getInstance();

	RoleService roleService = (RoleService)context.getBean("roleService");
	UserService userService = (UserService) context.getBean("userService");
	UserArtPaymentsService userArtPaymentsService = (UserArtPaymentsService) context.getBean("userArtPaymentsService");
	ArtService artService = (ArtService) context.getBean("artService");
	ArtStatusService artStatusService = (ArtStatusService) context.getBean("artStatusService");
	ArtTypeService artTypeService  = (ArtTypeService) context.getBean("artTypeService");
	ArtTypeFeeService artTypeFeeService  = (ArtTypeFeeService) context.getBean("artTypeFeeService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	UserSurveyService userSurveyService = (UserSurveyService) context.getBean("userSurveyService");
	UserPaymentsService userPaymentsService = (UserPaymentsService) context.getBean("userPaymentsService");
	
	public Boolean isPaypalGoodsFeePaid(int id) {
		return isGoodAlreadyPaid(id, MemmorableConstants.ART_PAYMENT_TYPE_FEE);
	}
	
	public Boolean isPaypalGoodsPaid(int id) {
		return isGoodAlreadyPaid(id, MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
	}
	
	private Boolean isGoodAlreadyPaid(int artId, String paymentType) {
		ArtEntity art = artService.findbyID(artId);
		UserArtPaymentsEntity payment = null;
		Boolean isPaid = Boolean.FALSE;
		
		if (art != null && !art.getArt_SubType().getArt_type().getArt_type_name().equals(MemmorableConstants.ART_TYPE_LITERATURE)) {
			payment = userArtPaymentsService.findByArt(art, paymentType);
			if (payment != null && !payment.getPaymentId().equals("")) {
				isPaid = Boolean.TRUE;
			}
		}
		
		return isPaid;
	}
	
	public Payment execPaypalGoodsFee(int id, String username) {
		PaypalGoods paypalGoods = new PaypalGoods();
		
		ArtEntity art = artService.findbyID(id);
		UserEntity user = userService.findByName(username);
		Payment payment = null;
		try {
			if (art != null && user != null) {
				Double feePercent = getArtTypeFeeAmount(art.getArt_SubType()
						.getArt_type(), user.getRole());
				Double feeAmount = (art.getPrice() * feePercent) / 100;

				// 20%off
				if (!isUserArtPaymentFeePending(art)) {
					feeAmount = feeAmount * 0.8;
				}

				if (feeAmount > 250.00) {
					feeAmount = 250.00;
				}
				
				//if credit available
				Double userCreditAvailable = getUserCreditAvailable(user, user.getRole());
				if (userCreditAvailable > 0.00) {
					feeAmount = feeAmount - userCreditAvailable;
					if (feeAmount <= 0.00) {
						feeAmount = 0.01;
					}
				}

				payment = paypalGoods.createFeePayment(art, feeAmount);
			}
		} catch (PayPalRESTException e) {
			logger.error("Error on execPaypalGoodsFee()", e);
		}
		return payment;
	}
	
	private Double getUserCreditAvailable(UserEntity user, RoleEntity role) {
		UserPaymentsEntity userPayment = userPaymentsService.findByUserAndRole(user, role);
		if (userPayment != null) {
			return userPayment.getCredit();
		} else {
			return 0.0;
		}
	}
	
	public Double getUserCreditAvailable(int artId) {
		ArtEntity art = artService.findbyID(artId);
		UserEntity user = art.getUser();
		return getUserCreditAvailable(user, user.getRole());
	}

	public Payment confPaypalGoodsFee(ArtPaymentConfRequest artReq) throws AddressException, MessagingException, IOException {
		PaypalGoods paypalGoods = new PaypalGoods();
		
		Payment payment = null;
		try {
			payment = paypalGoods.confirmFeePayment(artReq);
			if (payment != null && payment.getPayer().getStatus().equalsIgnoreCase("VERIFIED")) {
				ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_ACTIVE);
				int artId = Integer.valueOf(payment.getTransactions().get(0).getItemList().getItems().get(0).getSku());
				ArtEntity art = artService.findbyID(artId);
				if (art != null) {
					
					UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
					if (!isUserArtPaymentFeePending(art)) {
						art.setArt_status(artStatus);
						artService.update(art);
						
						//Register Payment
						userArtPayments.setUser(art.getUser());
						userArtPayments.setArt_id(art);
						userArtPayments.setCreatedDate(new Date());
						userArtPayments.setPaymentId(payment.getId());
						userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_FEE);
						userArtPayments.setPaymentMethod(MemmorableConstants.PAYMENT_METHOD_PAYPAL);
						userArtPaymentsService.save(userArtPayments);
					} else {
						//Update Payment
						userArtPayments = userArtPaymentsService.getArtPaymentsFeePending(art);
						userArtPayments.setCreatedDate(new Date());
						userArtPayments.setPaymentId(payment.getId());
						userArtPayments.setPaymentMethod(MemmorableConstants.PAYMENT_METHOD_PAYPAL);
						userArtPaymentsService.update(userArtPayments);
					}
					
					String amount = payment.getTransactions().get(0).getAmount().getTotal() 
							+ " " + payment.getTransactions().get(0).getAmount().getCurrency();
					
					// update remaining credit available
					UserPaymentsEntity userPayment = userPaymentsService.findByUserAndRole(art.getUser(), art.getUser().getRole());
					Double userCreditAvailable = 0.0;
					if (userPayment != null) {
						userCreditAvailable = userPayment.getCredit();
					}
					
					if (userCreditAvailable > 0.00) {
						Double feePercent = getArtTypeFeeAmount(art.getArt_SubType().getArt_type(), art.getUser().getRole());
						Double feeAmount = (art.getPrice() * feePercent) / 100;
						Double remainingCredit = userCreditAvailable - feeAmount;
						if (remainingCredit > 0.00) {
							userPayment.setCredit(remainingCredit);
						} else {
							userPayment.setCredit(0.00);
						}
						
						userPaymentsService.update(userPayment);
					}

					//Notify Seller
					PersonalEntity personal = personalService.findByUser(art.getUser());
					EmailTemplateSender.sendPaymentConfirmationSellerEmail(
							personal.getPersonal_email(),
							personal.getPersonal_name(),
							String.valueOf(art.getArt_id()), art.getArt_name(),
							payment.getId(), amount);
					
					//Notify Admin
					String adminEmail = MemmorableUtils
							.getConfigProperty(MemmorableAppConstants.EMAIL_ADMIN_MEMMORABLE_DEFAULT);
					EmailTemplateSender.sendPaymentConfirmationAdminEmail(
							adminEmail, String.valueOf(art.getArt_id()),
							art.getArt_name(), payment.getId(), amount);
				}
			}
		} catch (PayPalRESTException e) {
			logger.error("Error on confPaypalGoodsFee()", e);
		}
		return payment;
	}
	
	private Boolean isUserArtPaymentFeePending(ArtEntity art) {
		UserArtPaymentsEntity userArtPayments = userArtPaymentsService.getArtPaymentsFeePending(art);
		if (userArtPayments != null && userArtPayments.getPaymentId().equals("")) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public Payment execPaypalGoods(int id) {
		PaypalGoods paypalGoods = new PaypalGoods();
		
		ArtEntity art = artService.findbyID(id);
		Payment payment = null;
		try {
			payment = paypalGoods.createPayment(art);
		} catch (PayPalRESTException e) {
			logger.error("Error on execPaypalGoods()", e);
		}
		return payment;
	}
	
	public GenericResponse execBankDepositTransactionGoods(ArtPaymentRequest artReq) {
		return execBankTransactionGoods(artReq, MemmorableConstants.PAYMENT_METHOD_DEPOSIT);
	}

	public GenericResponse execBankTransferTransactionGoods(ArtPaymentRequest artReq) {
		return execBankTransactionGoods(artReq, MemmorableConstants.PAYMENT_METHOD_TRANSFER);
	}
	
	public GenericResponse execBankTransactionGoods(ArtPaymentRequest artReq, String bankTransaction) {
		GenericResponse response = new GenericResponse();
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_IN_PROCESS);
		if (artReq != null && artReq.art_Id > 0) {
			ArtEntity art = artService.findbyID(artReq.art_Id);
			if (art != null) {
				art.setArt_status(artStatus);
				artService.update(art);
				
				//Register In Process Payment
				UserEntity userBuyer = userService.findByName(artReq.username);
				UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
				userArtPayments.setUser(userBuyer);
				userArtPayments.setArt_id(art);
				userArtPayments.setCreatedDate(new Date());
				userArtPayments.setPaymentId("");
				userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
				userArtPayments.setPaymentMethod(bankTransaction);
				userArtPaymentsService.save(userArtPayments);
				
				response.success = true;
				response.message = "Art In Process to be paid by " + bankTransaction;
			}
		} else {
			response.success = false;
			response.message = "Art request or Art ID was null or empty";
		}
		
		return response;
	}

	public Payment confPaypalGoods(ArtPaymentConfRequest artReq) throws AddressException, MessagingException, IOException {
		PaypalGoods paypalGoods = new PaypalGoods();
		
		Payment payment = null;
		try {
			logger.debug("artReq", artReq);
			logger.debug("Before confirmPayment");
			payment = paypalGoods.confirmPayment(artReq);
			logger.debug("After confirmPayment", payment);
			if (payment != null && payment.getPayer().getStatus().equalsIgnoreCase("VERIFIED")) {
				logger.debug("Payment Verified");
				ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_SOLD);
				int artId = Integer.valueOf(payment.getTransactions().get(0).getItemList().getItems().get(0).getSku());
				ArtEntity art = artService.findbyID(artId);
				logger.debug("Art", art);
				if (art != null) {
					logger.debug("Art Id: ", art.getArt_id());
					art.setArt_status(artStatus);
					artService.update(art);
					logger.debug("Art status updated to Sold", art);
					
					//Register Payment
					logger.debug("Register User Payment");
					UserEntity userBuyer = userService.findByName(artReq.username);
					UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
					userArtPayments.setUser(userBuyer);
					userArtPayments.setArt_id(art);
					userArtPayments.setCreatedDate(new Date());
					userArtPayments.setPaymentId(payment.getId());
					userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
					userArtPayments.setPaymentMethod(MemmorableConstants.PAYMENT_METHOD_PAYPAL);
					userArtPaymentsService.save(userArtPayments);
					
					//Trigger the surveys
					registerSurveys(art, userBuyer);
					
					String amount = payment.getTransactions().get(0).getAmount().getTotal() 
							+ " " + payment.getTransactions().get(0).getAmount().getCurrency();

					//Send email notifications
					logger.debug("Send email notifications");
					if (artReq != null && artReq.username != null && !artReq.username.equals("")) {
						//Notify Buyer
						logger.debug("Send email notifications. Notify Buyer");
						PersonalEntity personal = personalService.findByUser(userBuyer);
						EmailTemplateSender.sendPaymentConfirmationBuyerEmail(
								personal.getPersonal_email(),
								personal.getPersonal_name(),
								String.valueOf(art.getArt_id()), art.getArt_name(),
								payment.getId(), amount);
						
						//Notify Seller
						logger.debug("Send email notifications. Notify Seller");
						personal = personalService.findByUser(art.getUser());
						EmailTemplateSender.sendPaymentConfirmationSellerSoldEmail(
								personal.getPersonal_email(),
								personal.getPersonal_name(),
								String.valueOf(art.getArt_id()), art.getArt_name(),
								payment.getId(), amount);
						
						// Notify Admin
						logger.debug("Send email notifications. Notify Admin");
						String adminEmail = MemmorableUtils
								.getConfigProperty(MemmorableAppConstants.EMAIL_ADMIN_MEMMORABLE_DEFAULT);
						EmailTemplateSender.sendPaymentConfirmationAdminEmail(
								adminEmail, String.valueOf(art.getArt_id()),
								art.getArt_name(), payment.getId(), amount);
					}
				}
			}
		} catch (PayPalRESTException e) {
			logger.error("Error on confPaypalGoods()", e);
		}
		return payment;
	}
	
	private Double getArtTypeFeeAmount(ArtTypeEntity artType, RoleEntity role) {
		if (role != null && artType != null) {
			ArtTypeFeeEntity artTypeFee = artTypeFeeService.findByArtTypeAndRole(artType, role);
			if (artTypeFee != null) {
				return new Double(artTypeFee.getFee_amount());
			} else {
				return new Double(0);
			}
		} else {
			return new Double(0);
		}
	}

	public Boolean activateArtWithNoFee(ArtPaymentRequest artReq) {
		Boolean isActivated = Boolean.FALSE;
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_ACTIVE);
		ArtEntity art = artService.findbyID(artReq.art_Id);
		
		if (art != null) {
			UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
			userArtPayments.setCreatedDate(new Date());
			userArtPayments.setPaymentId("");
			userArtPayments.setUser(art.getUser());
			userArtPayments.setArt_id(art);
			//Default to Deposit
			userArtPayments.setPaymentMethod("");
			userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_FEE);
			userArtPaymentsService.save(userArtPayments);
			
			art.setArt_status(artStatus);
			artService.update(art);
			isActivated = Boolean.TRUE;
		}
		
		return isActivated;
	}
	
	private void registerSurveys(ArtEntity art, UserEntity userBuyer) {
		//Register Surver for Buyer
		UserSurveyEntity userSurveyBuyer = new UserSurveyEntity();
		userSurveyBuyer.setArt(art);
		userSurveyBuyer.setCreatedDate(new Date());
		userSurveyBuyer.setFromUser(userBuyer);
		userSurveyBuyer.setToUser(art.getUser());
		userSurveyBuyer.setTransaction(MemmorableConstants.USER_SURVEY_TRANSACTION_PURCHASE);
		userSurveyBuyer.setStatus(MemmorableConstants.USER_SURVEY_STATUS_OPEN);
		userSurveyService.save(userSurveyBuyer);
		
		//Register Surver for Seller
		UserSurveyEntity userSurveySeller = new UserSurveyEntity();
		userSurveySeller.setArt(art);
		userSurveySeller.setCreatedDate(new Date());
		userSurveySeller.setFromUser(art.getUser());
		userSurveySeller.setToUser(userBuyer);
		userSurveySeller.setTransaction(MemmorableConstants.USER_SURVEY_TRANSACTION_SELL);
		userSurveySeller.setStatus(MemmorableConstants.USER_SURVEY_STATUS_OPEN);
		userSurveyService.save(userSurveySeller);
	}

	public Boolean execGoodsFeePaymentWithCredit(int artId, String username) {
		Boolean status = Boolean.FALSE;
		try {
				ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_ACTIVE);
				ArtEntity art = artService.findbyID(artId);
				if (art != null) {
					String paymentId = "CREDITED-"+ art.getArt_id();

					UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
					if (!isUserArtPaymentFeePending(art)) {
						art.setArt_status(artStatus);
						artService.update(art);
						
						//Register Payment
						userArtPayments.setUser(art.getUser());
						userArtPayments.setArt_id(art);
						userArtPayments.setCreatedDate(new Date());
						userArtPayments.setPaymentId(paymentId);
						userArtPayments.setPaymentMethod(MemmorableConstants.PAYMENT_METHOD_CREDIT);
						userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_FEE);
						userArtPaymentsService.save(userArtPayments);
					} else {
						//Update Payment
						userArtPayments = userArtPaymentsService.getArtPaymentsFeePending(art);
						userArtPayments.setCreatedDate(new Date());
						userArtPayments.setPaymentId(paymentId);
						userArtPayments.setPaymentMethod(MemmorableConstants.PAYMENT_METHOD_CREDIT);
						userArtPaymentsService.update(userArtPayments);
					}
					
					// update remaining credit available
					UserPaymentsEntity userPayment = userPaymentsService.findByUserAndRole(art.getUser(), art.getUser().getRole());
					Double userCreditAvailable = 0.0;
					if (userPayment != null) {
						userCreditAvailable = userPayment.getCredit();
					}
					
					if (userCreditAvailable > 0.00) {
						Double feePercent = getArtTypeFeeAmount(art.getArt_SubType().getArt_type(), art.getUser().getRole());
						Double feeAmount = (art.getPrice() * feePercent) / 100;
						Double remainingCredit = userCreditAvailable - feeAmount;
						if (remainingCredit > 0.00) {
							userPayment.setCredit(remainingCredit);
						} else {
							userPayment.setCredit(0.00);
						}
						
						userPaymentsService.update(userPayment);
					}

					//Notify Seller
					PersonalEntity personal = personalService.findByUser(art.getUser());
					EmailTemplateSender.sendPaymentConfirmationSellerEmail(
							personal.getPersonal_email(),
							personal.getPersonal_name(),
							String.valueOf(art.getArt_id()), art.getArt_name(),
							paymentId, "0.0");
					
					//Notify Admin
					String adminEmail = MemmorableUtils
							.getConfigProperty(MemmorableAppConstants.EMAIL_ADMIN_MEMMORABLE_DEFAULT);
					EmailTemplateSender.sendPaymentConfirmationAdminEmail(
							adminEmail, String.valueOf(art.getArt_id()),
							art.getArt_name(), paymentId, "0.0");
					
					status = Boolean.TRUE;
				}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return status;
	}
	
}
