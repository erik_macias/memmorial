package com.memmorable.backend.orm.entity.interceptor;

import org.hibernate.search.indexes.interceptor.EntityIndexingInterceptor;
import org.hibernate.search.indexes.interceptor.IndexingOverride;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.entity.ArtEntity;

public class IndexWhenPublishedInterceptor implements
		EntityIndexingInterceptor<ArtEntity> {

	@Override
	public IndexingOverride onAdd(ArtEntity entity) {
		if (entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_ACTIVE) ||
				entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_IN_PROCESS) ||
				entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_SOLD)) {
            return IndexingOverride.APPLY_DEFAULT;
        }
        return IndexingOverride.SKIP;
	}

	@Override
	public IndexingOverride onUpdate(ArtEntity entity) {
		if (entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_ACTIVE) ||
				entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_IN_PROCESS) ||
				entity.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_SOLD)) {
            return IndexingOverride.UPDATE;
        }
        return IndexingOverride.REMOVE;
	}

	@Override
	public IndexingOverride onDelete(ArtEntity entity) {
		return IndexingOverride.APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onCollectionUpdate(ArtEntity entity) {
		return onUpdate(entity);
	}

}
