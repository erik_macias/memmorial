package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.AddressTypeEntity;

public interface AddressTypeDAO extends InterfaceDAO<AddressTypeEntity> {

	public AddressTypeEntity findByName(String name);
	public AddressTypeEntity findById(int id);
}
