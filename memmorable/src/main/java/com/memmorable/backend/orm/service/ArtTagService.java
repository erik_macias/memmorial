package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.ArtTagEntity;

public interface ArtTagService {
	void save(ArtTagEntity artTag);
	ArtTagEntity findTag(ArtTagEntity id);
	ArtTagEntity findByName(String name);
	void update(ArtTagEntity artTag);
}
