package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtStatusDAO;
import com.memmorable.backend.orm.entity.ArtStatusEntity;

@Repository("artStatusDAO")
public class ArtStatusDAOImpl extends AbstractDao<ArtStatusEntity> implements ArtStatusDAO {

	@Override
	public ArtStatusEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(ArtStatusEntity.class);
		crit.add(Restrictions.eq("art_status_id", id));
		return (ArtStatusEntity) crit.uniqueResult();
	}

	@Override
	public ArtStatusEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtStatusEntity.class);
		crit.add(Restrictions.eq("art_status_type", name));
		return (ArtStatusEntity) crit.uniqueResult();
	}
	
}
