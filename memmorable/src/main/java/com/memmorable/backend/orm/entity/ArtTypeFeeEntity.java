package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;

@Entity
@Table(name = "Catalog_art_type_fee")
public class ArtTypeFeeEntity implements Serializable {
	private static final long serialVersionUID = 7398083752281161893L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_type_fee_id", nullable = false, unique = true)
	private int art_type_fee_id;

	@ManyToOne(targetEntity = ArtTypeEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_type_id")
	private ArtTypeEntity art_type;
	
	@ManyToOne(targetEntity = RoleEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private RoleEntity role;
	
	@Field
	@Column(name = "fee_amount", nullable = false)
	private Double fee_amount;

	public int getArt_type_fee_id() {
		return art_type_fee_id;
	}

	public void setArt_type_fee_id(int art_type_fee_id) {
		this.art_type_fee_id = art_type_fee_id;
	}

	public ArtTypeEntity getArt_type() {
		return art_type;
	}

	public void setArt_type(ArtTypeEntity art_type) {
		this.art_type = art_type;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public Double getFee_amount() {
		return fee_amount;
	}

	public void setFee_amount(Double fee_amount) {
		this.fee_amount = fee_amount;
	}

}
