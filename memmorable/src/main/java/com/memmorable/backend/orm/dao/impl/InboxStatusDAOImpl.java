package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.InboxStatusDAO;
import com.memmorable.backend.orm.entity.InboxStatusEntity;

@Repository("inboxStatusDAO")
public class InboxStatusDAOImpl extends AbstractDao<InboxStatusEntity> implements InboxStatusDAO {

	@Override
	public InboxStatusEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(InboxStatusEntity.class);
		crit.add(Restrictions.eq("inbox_status_id", id));
		return (InboxStatusEntity) crit.uniqueResult();
	}

	@Override
	public InboxStatusEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(InboxStatusEntity.class);
		crit.add(Restrictions.eq("inbox_status_name", name));
		return (InboxStatusEntity) crit.uniqueResult();
	}

}
