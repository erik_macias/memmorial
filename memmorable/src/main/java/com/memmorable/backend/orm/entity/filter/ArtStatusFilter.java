package com.memmorable.backend.orm.entity.filter;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.TermQuery;

import com.memmorable.app.common.MemmorableConstants;

public class ArtStatusFilter extends QueryWrapperFilter {

	public ArtStatusFilter() {
		super(getBooleanQuery());
	}

	private static BooleanQuery getBooleanQuery() {
		String activeStatus = MemmorableConstants.ART_STATUS_ACTIVE.toLowerCase();
		String inProcessStatus = MemmorableConstants.ART_STATUS_IN_PROCESS.toLowerCase();
		String soldStatus = MemmorableConstants.ART_STATUS_SOLD.toLowerCase();

		TermQuery catQuery1 = new TermQuery(new Term("art_status.art_status_type", activeStatus));
		TermQuery catQuery2 = new TermQuery(new Term("art_status.art_status_type", inProcessStatus));
		TermQuery catQuery3 = new TermQuery(new Term("art_status.art_status_type", soldStatus));

		BooleanQuery artStatusQuery = new BooleanQuery();
		artStatusQuery.add(new BooleanClause(catQuery1, BooleanClause.Occur.SHOULD));
		artStatusQuery.add(new BooleanClause(catQuery2, BooleanClause.Occur.SHOULD));
		artStatusQuery.add(new BooleanClause(catQuery3, BooleanClause.Occur.SHOULD));
		
		return artStatusQuery;
	}
	
}


