package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTypeDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.service.ArtTypeService;


@Service("artTypeService")
@Transactional
public class ArtTypeServiceImpl implements ArtTypeService {
	
	@Autowired
	private ArtTypeDAO dao;

	@Override
	public void save(ArtTypeEntity artTag) {
		dao.save(artTag);
		
	}

	@Override
	public ArtTypeEntity findType(ArtTypeEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtTypeEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(ArtTypeEntity artTag) {
		dao.update(artTag);
		
	}

	@Override
	public List<ArtTypeEntity> findall() {
		return dao.findAll(ArtTypeEntity.class);
	}

	@Override
	public ArtTypeEntity findById(int id) {
		return dao.findById(id);
	}



	
	
}
