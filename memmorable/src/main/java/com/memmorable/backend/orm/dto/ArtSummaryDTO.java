package com.memmorable.backend.orm.dto;

public class ArtSummaryDTO {

	private int artId;
	private String artName;
	private String artDescription;
	private String artMediaType;
	private String artMediaFilename;
	
	public int getArtId() {
		return artId;
	}
	public void setArtId(int artId) {
		this.artId = artId;
	}
	public String getArtName() {
		return artName;
	}
	public void setArtName(String artName) {
		this.artName = artName;
	}
	public String getArtDescription() {
		return artDescription;
	}
	public void setArtDescription(String artDescription) {
		this.artDescription = artDescription;
	}
	public String getArtMediaType() {
		return artMediaType;
	}
	public void setArtMediaType(String artMediaType) {
		this.artMediaType = artMediaType;
	}
	public String getArtMediaFilename() {
		return artMediaFilename;
	}
	public void setArtMediaFilename(String artMediaFilename) {
		this.artMediaFilename = artMediaFilename;
	}

}
