package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;

@Entity
@Table(name = "Catalog_art_type")
public class ArtTypeEntity implements Serializable {
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_type_id", nullable = false, unique = true)
	private int art_type_id;

	@Field
	@Column(name = "art_type_name", nullable = false, length = 100)
	private String art_type_name;

	public int getArt_type_id() {
		return art_type_id;
	}

	public void setArt_type_id(int art_type_id) {
		this.art_type_id = art_type_id;
	}

	public String getArt_type_name() {
		return art_type_name;
	}

	public void setArt_type_name(String art_type_name) {
		this.art_type_name = art_type_name;
	}



}
