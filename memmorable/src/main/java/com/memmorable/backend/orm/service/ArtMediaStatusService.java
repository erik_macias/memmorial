package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.MediaStatusEntity;

public interface ArtMediaStatusService {
	void save(MediaStatusEntity  ArtMediaStatus);
	 MediaStatusEntity findId( MediaStatusEntity id);
	 MediaStatusEntity findByName(String name);
	void update( MediaStatusEntity  ArtMediaStatus);
}
