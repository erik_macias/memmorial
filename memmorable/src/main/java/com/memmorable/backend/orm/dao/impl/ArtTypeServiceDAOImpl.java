package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTypeServiceDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;

@Repository("artTypeServiceDAO")
public class ArtTypeServiceDAOImpl extends AbstractDao<ArtTypeServiceEntity> implements ArtTypeServiceDAO {

	@Override
	public ArtTypeServiceEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtTypeServiceEntity.class);
		crit.add(Restrictions.eq("art_service_name", name));
		return (ArtTypeServiceEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtTypeServiceEntity> findByType(ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeServiceEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		return crit.list();
	}

	@Override
	public ArtTypeServiceEntity findByNameType(String name, ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeServiceEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		crit.add(Restrictions.eq("art_service_name", name ));
		return (ArtTypeServiceEntity) crit.uniqueResult();
	}

	@Override
	public ArtTypeServiceEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtTypeServiceEntity.class);
		crit.add(Restrictions.eq("art_subtype_id", id));
		return (ArtTypeServiceEntity) crit.uniqueResult();
	}

}
