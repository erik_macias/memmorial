package com.memmorable.backend.orm.dto;


import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.ProfileService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.services.rest.request.UserRegistry;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.UserBasicInfoResponse;
import com.nisoph.tools.mailer.engine.EmailTemplateSender;
import com.nisoph.utils.EncrypterUtils;

public class UserRegistryDTO {

	GenericResponse response;

	AbstractApplicationContext context = AppContext.getInstance();
	UserService userService = (UserService) context.getBean("userService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	UserStatusService statusService = (UserStatusService)context.getBean("userStatusService");
	RoleService roleService = (RoleService)context.getBean("roleService");
	ProfileService profileService = (ProfileService)context.getBean("profileService");
	
	public GenericResponse create(UserRegistry user) {
		response = new GenericResponse();
		
		if (isUserNameValid(user.getUserName(), userService)
				&& isUserEmailAlreadyRegistered(user.getEmail(),
						personalService)) {
			
			RoleEntity defaultRole = new RoleEntity();
			if (user.getRole() != null && !user.getRole().isEmpty()) {
				defaultRole = roleService.findByRoleName(user.getRole());
				
				//if empty, putting default
				if (defaultRole.getRole_name().isEmpty()) {
					defaultRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_FREE);
				}
			} else{
				defaultRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_FREE);
			}
			
			UserStatusEntity defaultStatus = new UserStatusEntity();
			if (user.getStatus() != null && !user.getStatus().isEmpty()) {
				defaultStatus = statusService.findByfindbyStatusType(user.getStatus());
				
				//if empty, putting default
				if (defaultStatus.getUser_status_type().isEmpty()) {
					defaultStatus = statusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_PENDIGN_VALIDATION);
				}
			} else {
				defaultStatus = statusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_PENDIGN_VALIDATION);
			}
			
			// UserEntity setters
			UserEntity userEnt = new UserEntity();
			userEnt.setRole(defaultRole);
			userEnt.setUserStatus(defaultStatus);
			userEnt.setUser_name(user.getUserName());
			userEnt.setUser_password(EncrypterUtils.encryptValue(user.getPassword()));
			userEnt.setLogin_attemps(0);
			userEnt.setCreatedDate(new Date());
			
			// User Personal info setters
			PersonalEntity personal = new PersonalEntity();
			personal.setUser(userEnt);
			personal.setPersonal_email(user.getEmail());
			personal.setPersonal_name(user.getFirstName());
			personal.setPersonal_l_name(user.getLastName());
			personal.setPersonal_gender(user.getGender());
			
			ProfileEntity profile = new ProfileEntity();
			profile.setArt_statment("");
			profile.setProfile_picture("./images/user.icon.jpg");
			profile.setResume("");
			profile.setUser(userEnt);
			
			userService.save(userEnt);
			personalService.save(personal);
			profileService.save(profile);
			
			response.success = true;
			response.message = "new user has being created";
			
			if (user.getRole() == null && user.getStatus() == null) {
				sendSignupConfirmationEmail(personal);
			}
			
		} else {
			response.success = false;
			response.message = "Username or Email already exist. Please choose another one";
		}
		//context.close();
		return response;
	}

	private void sendSignupConfirmationEmail(PersonalEntity personal) {
		try {
			ResourceBundle properties = ResourceBundle.getBundle("config");
			
			URL domain = new URL(properties.getString("domain.app"));
			String contextPath = properties.getString("verify.path")
					+ EncrypterUtils.encryptValue(getStringBufferToEncrypt(personal).toString());
			URL url = new URL(domain + contextPath);
			
			EmailTemplateSender.sendSignUpConfirmationEmail(
					personal.getPersonal_email(), personal.getPersonal_name(),
					personal.getUser().getUser_name(), url.toString());
		//TODO: If email fails, save a record into log db table and send it later
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private boolean isUserNameValid(String username, UserService service) {
		if(service.findByName(username)!=null){
			return false;
		}else{
			return true;
		}

	}
	
	private boolean isUserEmailAlreadyRegistered(String email, PersonalService personalService) {
		if (personalService.findByEmail(email) != null) {
			return false;
		} else {
			return true;
		}
	}
	
	public GenericResponse verify(String urlParam) {
		response = new GenericResponse();
		
		String[] decodedParam = getdecryptValue(urlParam);
		String userName = decodedParam[0];
		String userMail = decodedParam[1];
		
		/*
		//TOOO: Add ability to expire the encrypted urlParam
		String timestamp = decodedParam[2];
		*/
		
		if (userName != null && !userName.equals("") && userMail != null
				&& !userMail.equals("")) {
			UserStatusEntity defaultStatus = statusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_ENABLED);
			PersonalEntity personalEnt = personalService.findByEmail(userMail);
			UserEntity userEnt = userService.findByName(userName);

			if (userEnt != null
					&& userEnt
							.getUserStatus()
							.getUser_status_type()
							.equals(MemmorableConstants.USER_STATUS_PENDIGN_VALIDATION)) {
				if (userEnt.getUser_name() != null
						&& !userEnt.getUser_name().equals("")
						&& personalEnt.getPersonal_name() != null
						&& !personalEnt.getPersonal_name().equals("")) {

					userEnt.setUserStatus(defaultStatus);
					userService.update(userEnt);

					response.success = true;
					response.message = "User has been activated.";
				} else {
					response.success = false;
					response.message = "Username or email are null or empty.";
				}
			} else {
				response.success = false;
				response.message = "User is not in pending validation.";
			}
		} else {
			response.success = false;
			response.message = "Error while activating user";
		}
		//context.close();
		return response;
	}
	
	public GenericResponse generatePasswordResetLink(String email) {
		response = new GenericResponse();
		
		PersonalEntity personal = personalService.findByEmail(email);
		
		if (personal != null && personal.getPersonal_email().equals(email)) {
			try {
				ResourceBundle properties = ResourceBundle.getBundle("config");
				
				URL domain = new URL(properties.getString("domain.app"));
				String contextPath = properties.getString("pwdReset.path")
						+ EncrypterUtils.encryptValue(getStringBufferToEncrypt(personal).toString());
				URL url = new URL(domain + contextPath);
				
				EmailTemplateSender.sendPasswordResetEmail(personal
						.getPersonal_email(), personal.getPersonal_name(),
						personal.getUser().getUser_name(), url.toString());
			//TODO: If email fails, save a record into log db table and send it later
			} catch (IOException e) {
				e.printStackTrace();
			} catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			response.success = true;
			response.message = "Email Link for password reset has been generated";
		} else {
			response.success = false;
			response.message = "The provided email does not match with the one registered in our database. Please check and submit it again.";
		}
		
		//context.close();
		return response;
	}
	
	public UserBasicInfoResponse passwordReset(String urlParam) {
		UserBasicInfoResponse response = new UserBasicInfoResponse();
		String[] decodedParam = getdecryptValue(urlParam);
		String userName = decodedParam[0];
		String userMail = decodedParam[1];
		String dateStr = decodedParam[2];
		
		if (verifyExpirationDate(dateStr)) {
			if (userName != null && !userName.equals("") && userMail != null
					&& !userMail.equals("")) {
				UserEntity userEnt = userService.findByName(userName);
				response.username = userEnt.getUser_name();
				response.success = true;
				response.message = "User retrieved successful";
			} else {
				response.success = false;
				response.message = "Error while generating password link";
			}
		} else {
			response.success = false;
			response.message = "Generated Link is not valid. Please reset your password again";
		}
		
		//context.close();
		return response;
	}
	
	private boolean verifyExpirationDate(String dateStr) {
		SimpleDateFormat sdf = new SimpleDateFormat(MemmorableConstants.DATE_FORMAT);
		Date date = new Date();
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		//Add +-12 hrs to validate if link is still valid
		Calendar cal = Calendar.getInstance();
		Date today = new Date();
	    cal.setTime(today);
	    cal.add(Calendar.HOUR, -12);
	    Date dateBefore = cal.getTime();
	    cal.add(Calendar.HOUR, +24);
	    Date dateAfter = cal.getTime();
	    
	    if (dateAfter.after(date) && dateBefore.before(date)) {
	    	return Boolean.TRUE;
	    } else {
	    	return Boolean.FALSE;
	    }
	}
	
	private StringBuffer getStringBufferToEncrypt(PersonalEntity personal) {
		StringBuffer stringToEncryptSB = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(MemmorableConstants.DATE_FORMAT);
		Date date = new Date();
		String todayDateStr = sdf.format(date);
		
		stringToEncryptSB.append(personal.getUser().getUser_name())
				.append("_").append(personal.getPersonal_email())
				.append("_").append(todayDateStr);
		return stringToEncryptSB;
	}
	
	private String[] getdecryptValue(String urlParam) {
		String[] decodedParam = EncrypterUtils.decryptValue(urlParam).split("_");
		return decodedParam;
	}


}
