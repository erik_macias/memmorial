package com.memmorable.backend.orm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

@Service("artService")
@Transactional
public class ArtServiceImpl implements ArtService {

	@Autowired
	private ArtDAO dao;

	@Override
	public void save(ArtEntity art) {
		dao.save(art);
	}

	@Override
	public ArtEntity findArt(ArtEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByUser(UserEntity user) {
		return dao.findByUser(user);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findBySubType(ArtSubTypeEntity subtype) {
		return dao.findBySubType(subtype);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByType(ArtTypeEntity type) {
		return findByType(type);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findByArtTag(ArtTagEntity tag) {
		return dao.findByArtTag(tag);
	}

	@Override
	public void update(ArtEntity art) {
		dao.update(art);
	}

	@Override
	public ArtEntity findbyID(int id) {
		return dao.findByID(id);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findbyFreeText(String searchText, int maxResults, int firstResult) {
		return dao.findByFreeText(searchText, maxResults, firstResult);
	}

	@Override
	public SearchResultEntityList<ArtEntity> findbyTopTenNewArt() {
		return dao.findByTopTenNewArt();
	}

	@Override
	public SearchResultEntityList<ArtEntity> getAllArt(int maxResults, int firstResult) {
		return dao.getAllArt(maxResults, firstResult);
	}

	@Override
	public int getArtVisitsByUser(UserEntity user) {
		return dao.getArtVisitsByUser(user);
	}

}
