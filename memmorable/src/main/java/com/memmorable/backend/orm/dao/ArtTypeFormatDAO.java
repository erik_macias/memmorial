package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;

public interface ArtTypeFormatDAO extends InterfaceDAO<ArtTypeFormatEntity> {
	public ArtTypeFormatEntity findByName(String name);
	public List<ArtTypeFormatEntity> findByType(ArtTypeEntity type);
	public ArtTypeFormatEntity findByNameType(String name, ArtTypeEntity type);
	public ArtTypeFormatEntity findById(int id);
}
