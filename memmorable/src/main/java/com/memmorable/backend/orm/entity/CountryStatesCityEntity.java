package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_Cities")
public class CountryStatesCityEntity implements Serializable {
	private static final long serialVersionUID = -1226281151020369010L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id", nullable = false, unique = true)
	private int cityId;

	@Column(name = "city_name", nullable = false, unique = true, length = 100)
	private String cityName;
	
	@OneToOne(targetEntity = CountryStatesEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "state_id", nullable = false)
	private CountryStatesEntity countryState;

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public CountryStatesEntity getCountryState() {
		return countryState;
	}

	public void setCountryState(CountryStatesEntity countryState) {
		this.countryState = countryState;
	}

}
