package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;

public interface ArtTypeServiceService {
	void save(ArtTypeServiceEntity artService);
	void update(ArtTypeServiceEntity artService);
	ArtTypeServiceEntity findService(ArtTypeServiceEntity id);
	ArtTypeServiceEntity findByName(String name);
	ArtTypeServiceEntity findById(int id);
	List<ArtTypeServiceEntity> findByType(ArtTypeEntity type);
	ArtTypeServiceEntity findByNameType(String name,ArtTypeEntity type);
	List<ArtTypeServiceEntity> findAll();
	
}
