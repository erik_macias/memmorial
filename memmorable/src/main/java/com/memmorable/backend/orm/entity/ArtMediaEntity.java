package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;

@Entity
@Table(name = "Table_media")
@FilterDef(name = "activeMedias")
@Filter(name = "activeMedias", condition = "DELETED = 'false'")
public class ArtMediaEntity implements Serializable {
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "media_id", nullable = false, unique = true)
	private int media_id;

	@Column(name = "media_type", nullable = false, length = 100)
	private String media_type;

	@Column(name = "media_filename", nullable = false, length = 100)
	private String media_filename;

	@Column(name = "media_primary", nullable = false)
	private boolean media_primary;

	@ManyToOne(targetEntity = MediaStatusEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "media_status_id")
	private MediaStatusEntity media_status;

	@ManyToOne(targetEntity = ArtEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_id")
	private ArtEntity art;

	@Column(name = "downloadable", nullable = true)
	private boolean downloadable;

	@Column(name = "DELETED", nullable = true)
	private boolean deleted;

	public int getMedia_id() {
		return media_id;
	}

	public void setMedia_id(int media_id) {
		this.media_id = media_id;
	}

	public String getMedia_type() {
		return media_type;
	}

	public void setMedia_type(String media_type) {
		this.media_type = media_type;
	}

	public String getMedia_filename() {
		return media_filename;
	}

	public void setMedia_filename(String media_filename) {
		this.media_filename = media_filename;
	}

	public boolean isMedia_primary() {
		return media_primary;
	}

	public void setMedia_primary(boolean media_primary) {
		this.media_primary = media_primary;
	}

	public ArtEntity getArt() {
		return art;
	}

	public void setArt(ArtEntity art) {
		this.art = art;
	}

	public MediaStatusEntity getMedia_status() {
		return media_status;
	}

	public void setMedia_status(MediaStatusEntity media_status) {
		this.media_status = media_status;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isDownloadable() {
		return downloadable;
	}

	public void setDownloadable(boolean downloadable) {
		this.downloadable = downloadable;
	}

}
