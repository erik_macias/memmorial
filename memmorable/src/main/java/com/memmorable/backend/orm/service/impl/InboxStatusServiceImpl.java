package com.memmorable.backend.orm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.InboxStatusDAO;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.service.InboxStatusService;

@Service("inboxStatusService")
@Transactional
public class InboxStatusServiceImpl implements InboxStatusService {
	
	@Autowired
	private InboxStatusDAO dao;

	@Override
	public InboxStatusEntity findId(InboxStatusEntity id) {
		return dao.find(id);
	}

	@Override
	public InboxStatusEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(InboxStatusEntity inboxStatus) {
		dao.update(inboxStatus);
	}

	@Override
	public void save(InboxStatusEntity inboxStatus) {
		dao.save(inboxStatus);
	}

}
