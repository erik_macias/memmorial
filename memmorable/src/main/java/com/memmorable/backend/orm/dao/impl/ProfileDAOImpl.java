package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ProfileDAO;
import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("ProfileDAO")
public class ProfileDAOImpl extends AbstractDao<ProfileEntity> implements ProfileDAO {
	
	
	public ProfileEntity findbyUser(UserEntity user) {	
		Criteria crit = getSession().createCriteria(ProfileEntity.class);
		crit.add(Restrictions.eq("user", user));
		return (ProfileEntity) crit.uniqueResult();
	}

	@Override
	public int getProfileVisits(UserEntity user) {
		Criteria crit = getSession().createCriteria(ProfileEntity.class);
		crit.setProjection(Projections.sum("visitsCounter"));
		crit.add(Restrictions.eq("user", user));
		return ((Long) crit.list().get(0)).intValue();
	}
	

}
