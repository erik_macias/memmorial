package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTypeDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
@Repository("artTypeDAO")
public class ArtTypeDAOImpl extends AbstractDao<ArtTypeEntity> implements ArtTypeDAO {

		
	@Override
	public ArtTypeEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtTypeEntity.class);
		crit.add(Restrictions.eq("art_type_name", name));
		return (ArtTypeEntity) crit.uniqueResult();
	}

	@Override
	public ArtTypeEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtTypeEntity.class);
		crit.add(Restrictions.eq("art_type_id", id));
		return (ArtTypeEntity) crit.uniqueResult();
	}

}
