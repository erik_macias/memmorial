package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.dto.ReportRanges;
import com.memmorable.backend.orm.dto.UserCreatedReportDTO;
import com.memmorable.backend.orm.dto.UserSalesReportDTO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface UserArtPaymentsService {
		void save(UserArtPaymentsEntity userPayments);
		void update(UserArtPaymentsEntity userPayments);
		List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user);
		List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user, String paymentType);
		UserArtPaymentsEntity findByPaymentId(String paymentId);
		UserArtPaymentsEntity findByArt(ArtEntity art, String paymentType);
		UserArtPaymentsEntity findByArtAndBuyer(ArtEntity art, UserEntity user);
		List<UserSalesReportDTO> getUserArtPaymentsReport(UserEntity user, int year);
		List<UserSalesReportDTO> getUserArtFeePaymentsReport(UserEntity user, int year);
		List<UserSalesReportDTO> getUsersArtPaymentsReport(int year);
		List<UserSalesReportDTO> getUsersArtFeePaymentsReport(int year);
		List<UserArtPaymentsEntity> getFeePaidPending();
		UserArtPaymentsEntity getArtPaymentsAmountPending(ArtEntity art);
		UserArtPaymentsEntity getArtPaymentsFeePending(ArtEntity art);
		List<UserCreatedReportDTO> getUsersCreatedReport(int year);
		List<ArtEntity> getArtsWithFeePending(int intervalDays);
		List<UserArtPaymentsEntity> findAllUserPaymentsToSeller(UserEntity buyerUser, UserEntity sellerUser);
		List<UserSalesReportDTO> getUsersArtPaymentsReport(ReportRanges ranges);
		List<UserSalesReportDTO> getUsersArtFeePaymentsReport(ReportRanges ranges);
}
