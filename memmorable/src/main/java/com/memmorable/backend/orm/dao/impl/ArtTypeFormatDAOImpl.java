package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTypeFormatDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;

@Repository("artTypeFormatDAO")
public class ArtTypeFormatDAOImpl extends AbstractDao<ArtTypeFormatEntity> implements ArtTypeFormatDAO {

	@Override
	public ArtTypeFormatEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtTypeFormatEntity.class);
		crit.add(Restrictions.eq("art_Format_name", name));
		return (ArtTypeFormatEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtTypeFormatEntity> findByType(ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeFormatEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		return crit.list();
	}

	@Override
	public ArtTypeFormatEntity findByNameType(String name, ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeFormatEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		crit.add(Restrictions.eq("art_Format_name", name ));
		return (ArtTypeFormatEntity) crit.uniqueResult();
	}

	@Override
	public ArtTypeFormatEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtTypeFormatEntity.class);
		crit.add(Restrictions.eq("art_subtype_id", id));
		return (ArtTypeFormatEntity) crit.uniqueResult();
	}

}
