package com.memmorable.backend.orm.dto;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.ArtCommentsEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtMediaEntity;
import com.memmorable.backend.orm.entity.ArtRatingEntity;
import com.memmorable.backend.orm.entity.ArtStatusEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.entity.UserSurveyEntity;
import com.memmorable.backend.orm.service.AddressService;
import com.memmorable.backend.orm.service.AddressTypeService;
import com.memmorable.backend.orm.service.ArtCommentsService;
import com.memmorable.backend.orm.service.ArtMediaService;
import com.memmorable.backend.orm.service.ArtMediaStatusService;
import com.memmorable.backend.orm.service.ArtRatingService;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.ArtStatusService;
import com.memmorable.backend.orm.service.ArtSubTypeService;
import com.memmorable.backend.orm.service.ArtTagService;
import com.memmorable.backend.orm.service.ArtTypeFormatService;
import com.memmorable.backend.orm.service.ArtTypeGenreService;
import com.memmorable.backend.orm.service.ArtTypeService;
import com.memmorable.backend.orm.service.ArtTypeServiceService;
import com.memmorable.backend.orm.service.CurrencyService;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.UserArtPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.orm.service.UserSurveyService;
import com.memmorable.backend.services.rest.request.ActivateArtRequest;
import com.memmorable.backend.services.rest.request.ArtCommentsRequest;
import com.memmorable.backend.services.rest.request.ArtDetailsRequest;
import com.memmorable.backend.services.rest.request.ArtRatingRequest;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.request.NewArtRequest;
import com.memmorable.backend.services.rest.request.NewMediaRequest;
import com.memmorable.backend.services.rest.request.UpdateArtRequest;
import com.memmorable.backend.services.rest.responses.ArtCommentsResponse;
import com.memmorable.backend.services.rest.responses.ArtMediaResponse;
import com.memmorable.backend.services.rest.responses.ArtResponse;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.SaveGenericResponse;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;
import com.memmorable.backend.services.rest.responses.UserArtReponse;
import com.nisoph.tools.mailer.engine.EmailTemplateSender;

public class ArtAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();
	
	UserService userService = (UserService) context.getBean("userService");
	ArtService  artService = (ArtService) context.getBean("artService");
	ArtMediaService artMediaservice = (ArtMediaService) context.getBean("artMediaService");
	ArtTypeService artTypeService  = (ArtTypeService) context.getBean("artTypeService");
	ArtSubTypeService artSubTypeService  = (ArtSubTypeService) context.getBean("artSubTypeService");
	ArtTypeServiceService artTypeServiceService  = (ArtTypeServiceService) context.getBean("artTypeServiceService");
	ArtTypeGenreService artTypeGenreService  = (ArtTypeGenreService) context.getBean("artTypeGenreService");
	ArtTypeFormatService artTypeFormatService  = (ArtTypeFormatService) context.getBean("artTypeFormatService");
	
	ArtTagService artTagService = (ArtTagService) context.getBean("artTagService");
	ArtStatusService artStatusService = (ArtStatusService) context.getBean("artStatusService");
	ArtMediaStatusService artMediaStatusService = (ArtMediaStatusService) context.getBean("artMediaStatusService");
	ArtCommentsService artCommentsService = (ArtCommentsService) context.getBean("artCommentsService");
	ArtRatingService artRatingService = (ArtRatingService) context.getBean("artRatingService");
	UserArtPaymentsService userArtPaymentsService = (UserArtPaymentsService) context.getBean("userArtPaymentsService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	UserSurveyService userSurveyService = (UserSurveyService) context.getBean("userSurveyService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	AddressService addressService = (AddressService) context.getBean("addressService");
	AddressTypeService addressTypeService = (AddressTypeService) context.getBean("addressTypeService");
	
	CurrencyService currencyService = (CurrencyService) context.getBean("currencyService");

	public UserArtReponse getArtByUser(CurrentUser currentUser) {
		UserArtReponse response = new UserArtReponse();
		UserEntity userEnt = userService.findByName(currentUser.username);
		if (userEnt != null) {
			SearchResultEntityList<ArtEntity> artList = artService.findByUser(userEnt);
			List<ArtEntity> listArts = artList.getEntityList();
			List<ArtResponse> artResponse = new ArrayList<ArtResponse>();
			ArtResponse art = new ArtResponse();
			for (ArtEntity artEntity : listArts){
				art.art_Name = artEntity.getArt_name();
				art.art_Description = artEntity.getArt_description();
				art.art_subtype = artEntity.getArt_SubType().getArt_subtype_name();
				art.art_type = artEntity.getArt_SubType().getArt_type().getArt_type_name();
				List <ArtMediaEntity> media = artMediaservice.findByArt(artEntity);
				List <ArtMediaResponse> mediaResponseList = new ArrayList<ArtMediaResponse>();
				ArtMediaResponse  mediaResponse = new ArtMediaResponse(); 
				for(ArtMediaEntity artmedia : media){
					mediaResponse.media_fileName=artmedia.getMedia_filename();
					mediaResponse.Type = artmedia.getMedia_type();
					mediaResponseList.add(mediaResponse);
				}
				art.media= mediaResponseList;
				artResponse.add(art);
			}
			response.success = true;
			response.message = "succesfully retriving profile information";
			response.Username = currentUser.username;
			response.arts = artResponse;
			return response;
		} else {
			response.success = false;
			response.message = "user doesnt exist";
			return response;
		}
	}
	
	public boolean isUserAllowedToPublish(CurrentUser currentUser) {
		UserEntity userEnt = userService.findByName(currentUser.username);
		boolean isUserAllowed = true;
		if (userEnt != null) {
			SearchResultEntityList<ArtEntity> artList = artService.findByUser(userEnt);
			List<ArtEntity> listArts = artList.getEntityList();
			if (listArts != null && userEnt.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_FREE)) {
				if (listArts.size() > 4) {
					isUserAllowed = false;
				} else {
					isUserAllowed = true;
				}
			}
		}
		return isUserAllowed;
	}
	
	public SaveGenericResponse saveArt(NewArtRequest artReq) {
		ArtEntity art = new ArtEntity();
		UserEntity user = userService.findByName(artReq.username);
		//TODO: Add dynamic tag
		ArtTagEntity tag = artTagService.findByName("Painting");
		ArtTypeEntity type = artTypeService.findByName(artReq.art_type.getArt_type_name());
		ArtSubTypeEntity subtype = artSubTypeService.findByNameType(artReq.art_subType.getArt_subtype_name(), type);	
		ArtTypeServiceEntity service = artTypeServiceService.findByNameType(artReq.art_service.getArt_service_name(), type);	
		
		art.setArt_description(artReq.art_description);
		art.setArt_name(artReq.art_name);
		art.setArt_SubType(subtype);
		art.setArt_Service(service);
		art.setArt_Genre(artReq.art_genre);
		art.setArt_Format(artReq.art_format);
		art.setUser(user);
		art.setArt_tag(tag);
		art.setCreated_date(new Date());
		art.setLast_updated_date(new Date());
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_PENDING_VALIDATION);
		CurrencyEntity currency = artReq.currency;
		art.setArt_status(artStatus);
		art.setCurrency(currency);
		art.setPrice(artReq.art_price);
		art.setDeleted(false);
		artService.save(art);
		
		SaveGenericResponse genRespond = new SaveGenericResponse();
		genRespond.message = "Art was created successfully ";
		genRespond.success = true;
		genRespond.Entityid = art.getArt_id();
		
		return genRespond;
	}
	
	public SaveGenericResponse updateArt(UpdateArtRequest artReq) {
		UserEntity user = userService.findByName(artReq.username);
		
		//TODO: Add dynamic tag
		ArtEntity art  = artService.findbyID(artReq.art_id);
		ArtTagEntity tag = artTagService.findByName("Painting");
		
		art.setArt_description(artReq.art_description);
		art.setArt_name(artReq.art_name);
		
		art.setUser(user);
		art.setArt_tag(tag);
		art.setLast_updated_date(new Date());
		art.setDeleted(false);
		artService.update(art);
		
		SaveGenericResponse genRespond = new SaveGenericResponse();
		genRespond.message="Art was created successfully ";
		genRespond.success=true;
		genRespond.Entityid=art.getArt_id();
		
		return genRespond;
	}
	
	public GenericResponse saveMedia(NewMediaRequest mediaReq) {
		ArtMediaEntity artMedia = new ArtMediaEntity();
		ArtEntity art = artService.findbyID(mediaReq.art_id);
		artMedia.setArt(art);  
		artMedia.setMedia_status(artMediaStatusService.findByName(MemmorableConstants.ART_STATUS_ACTIVE));
		artMedia.setMedia_filename(mediaReq.media_url);
		artMedia.setMedia_type(getMediaType(mediaReq.media_type));
		artMedia.setMedia_primary(mediaReq.media_primary);
		artMedia.setDeleted(false);
		artMedia.setDownloadable(mediaReq.downloadable);
		
		artMediaservice.save(artMedia);
		GenericResponse genRespond = new GenericResponse();
		genRespond.message="Art was created successfully ";
		genRespond.success=true;
		
		return genRespond;
	}
	
	public GenericResponse deleteArt(int artId) {
		GenericResponse genRespond = new GenericResponse();
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_REMOVED);

		ArtEntity art = artService.findbyID(artId);
		if (art != null) {
			art.setArt_status(artStatus);
			art.setDeleted(true);
			artService.update(art);
			genRespond.message="Art was activated successfully";
			genRespond.success=true;
		} else {
			genRespond.message="Error while activating Art";
			genRespond.success=false;
		}
		
		return genRespond;
	}
	
	public GenericResponse activateArt(ActivateArtRequest artReq) {
		GenericResponse genRespond = new GenericResponse();
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_ACTIVE);

		ArtEntity art = artService.findbyID(artReq.getArtId());
		if (art != null) {
			
			UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
			userArtPayments.setCreatedDate(new Date());
			userArtPayments.setPaymentId(artReq.getPaymentId());
			String paymentMethod = "";
			
			if (artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_PAYPAL) ||
					artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_DEPOSIT) ||
					artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_TRANSFER)) {
				paymentMethod = artReq.getPaymentMethod().toUpperCase();
			} else {
				//Default to Deposit
				paymentMethod = MemmorableConstants.PAYMENT_METHOD_DEPOSIT;
			}
			
			if (!isUserArtPaymentFeePending(art)) {
				userArtPayments.setUser(art.getUser());
				userArtPayments.setArt_id(art);
				userArtPayments.setPaymentMethod(paymentMethod);
				userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_FEE);
				userArtPaymentsService.save(userArtPayments);
			} else {
				userArtPayments = userArtPaymentsService.getArtPaymentsFeePending(art);
				userArtPayments.setPaymentMethod(paymentMethod);
				userArtPayments.setCreatedDate(new Date());
				userArtPayments.setPaymentId(artReq.getPaymentId());
				userArtPaymentsService.update(userArtPayments);
			}
			
			//Update User status
			UserStatusEntity userStatusEnabled = userStatusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_ENABLED);
			UserEntity user = art.getUser();
			user.setUserStatus(userStatusEnabled);
			userService.update(user);
			
			art.setArt_status(artStatus);
			art.setDeleted(false);
			artService.update(art);
			
			genRespond.message = "Art was activated successfully";
			genRespond.success = true;
		} else {
			genRespond.message = "Error while activating Art";
			genRespond.success = false;
		}
		
		return genRespond;
	}
	
	private String getMediaType(String contentType) {
		String mediaType = "";
		
		if (contentType.contains(MemmorableConstants.MEDIA_TYPE_IMAGE)) {
			mediaType = MemmorableConstants.MEDIA_TYPE_IMAGE;
		} else if (contentType.contains(MemmorableConstants.MEDIA_TYPE_VIDEO)) {
			mediaType = MemmorableConstants.MEDIA_TYPE_VIDEO;
		} else if (contentType.contains(MemmorableConstants.MEDIA_TYPE_AUDIO)) {
			mediaType = MemmorableConstants.MEDIA_TYPE_AUDIO;
		} else if (contentType.contains(MemmorableConstants.MEDIA_TYPE_PDF)) {
			mediaType = MemmorableConstants.MEDIA_TYPE_PDF;
		}
		
		return mediaType;
	}
	
	public List<ArtTypeEntity> getTypes(){
		List<ArtTypeEntity> types = artTypeService.findall();
		return types;
	}
	
	public List<CurrencyEntity> getCurrency(){
		List<CurrencyEntity> currency = currencyService.findAll();
		return currency;
	}
	
	public List<ArtSubTypeEntity> getSubTypes(String typename){
		ArtTypeEntity type = artTypeService.findByName(typename);
		List<ArtSubTypeEntity> subtypes = artSubTypeService.findByType(type);
		return subtypes;
	}
	public List<ArtTypeServiceEntity> getServices(String typename){
		ArtTypeEntity type = artTypeService.findByName(typename);
		List<ArtTypeServiceEntity> services = artTypeServiceService.findByType(type);
		return services;
	}
	public List<ArtTypeGenreEntity> getGenres(String typename){
		ArtTypeEntity type = artTypeService.findByName(typename);
		List<ArtTypeGenreEntity> services = artTypeGenreService.findByType(type);
		return services;
	}
	public List<ArtTypeFormatEntity> getFormats(String typename){
		ArtTypeEntity type = artTypeService.findByName(typename);
		List<ArtTypeFormatEntity> formats= artTypeFormatService.findByType(type);
		return formats;
	}
	
	public List<ArtMediaEntity> getMediaByArt(String artName){
		ArtEntity art = artService.findByName(artName);
		List<ArtMediaEntity> response = artMediaservice.findByArt(art);
		return response;
	}
	
	public List<ArtMediaEntity> getMediaByArtId(int artId){
		ArtEntity art = artService.findbyID(artId);
		List<ArtMediaEntity> response = artMediaservice.findByArt(art);
		return response;
	}
	
	public GenericResponse deleteMediaById(int artId){
		 artMediaservice.deleteMedia(artId);;
		
		SaveGenericResponse genRespond = new SaveGenericResponse();
		genRespond.message="Art was created successfully ";
		genRespond.success=true;

		return genRespond;
	}
	
	public GenericResponse removePrimaryAll(int artId){
		ArtEntity art = artService.findbyID(artId);
		List<ArtMediaEntity> medias = artMediaservice.findByArt(art);
		for(ArtMediaEntity media : medias ){
			media.setMedia_primary(false);
			artMediaservice.update(media);
		}
		SaveGenericResponse genRespond = new SaveGenericResponse();
		genRespond.message="Art primary was removed from all";
		genRespond.success=true;
		return genRespond;
	}
	
	
	public GenericResponse setPrimaryMedia(int artId){
		 ArtMediaEntity media = artMediaservice.findMediaById(artId);
		 media.setMedia_primary(true);
		artMediaservice.update(media);
		SaveGenericResponse genRespond = new SaveGenericResponse();
		genRespond.message="media was made primary successfully ";
		genRespond.success=true;
		
		return genRespond;
	}
	
	public ArtMediaEntity getDefaultMediaByArtId(int artId){
		ArtEntity art = artService.findbyID(artId);
		ArtMediaEntity response = artMediaservice.getDefaultMediadByArt(art);
		return response;
	}
	
	public SearchResultEntityList<ArtSearchResultDTO> searchArtByFreeText(String searchText, int maxResults, int firstResult) {
		SearchResultEntityList<ArtEntity> artList = artService.findbyFreeText(searchText, maxResults, firstResult);
		List<ArtSearchResultDTO> artListDTO = mapToArtResultsDTO(artList.getEntityList(), Boolean.TRUE, Boolean.FALSE);
		SearchResultEntityList<ArtSearchResultDTO> artSearchResult = getSearchResultEntityList(artList, artListDTO);
		return artSearchResult;
	}
	
	public SearchResultEntityList<ArtSearchResultDTO> getAllArt() {
		SearchResultEntityList<ArtEntity> artList = artService.getAllArt(-1, -1);
		List<ArtSearchResultDTO> artListDTO = mapToArtResultsDTO(artList.getEntityList(), Boolean.TRUE, Boolean.TRUE);
		SearchResultEntityList<ArtSearchResultDTO> artSearchResult = getSearchResultEntityList(artList, artListDTO);
		return artSearchResult;
	}
	
	public ArtSearchResultDTO getArtDetailsToEditById(ArtDetailsRequest art) {
		ArtEntity artEntity = artService.findbyID(art.getArtId());
		UserEntity userEntity = userService.findByName(art.getCurrentUser());
		
		if (artEntity != null) {
			if ((artEntity.getUser().getUser_name().equals(art.getCurrentUser()) && !isArtExpired(artEntity))
					|| (userEntity != null && userEntity.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_ADMIN))) {
				return getArtDetailsById(art.getArtId());
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public ArtSearchResultDTO getArtDetailsById(ArtDetailsRequest artReq) {
		ArtSearchResultDTO artSearchResultDTO = null;
		UserEntity userEntity = null;
		ArtEntity artEnt = artService.findbyID(artReq.getArtId());
		
		if (artReq.getCurrentUser() != null && !artReq.getCurrentUser().equals("")) {
			userEntity = userService.findByName(artReq.getCurrentUser());
			
			if (userEntity.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_ADMIN)) {
				if (artEnt != null) {
					artSearchResultDTO = getArtDetailsById(artReq.getArtId());
				}
			} else {
				if (artEnt != null && !isArtExpired(artEnt)) {
					artSearchResultDTO = getArtDetailsById(artReq.getArtId());
				}
			}
			
			//Add Download link if Literature
			if (artEnt.getArt_SubType().getArt_type().getArt_type_name().equals(MemmorableConstants.ART_TYPE_LITERATURE) &&
					(hasUserBoughtArtToSeller(userEntity, artReq.getArtId()) || 
					userEntity.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_ADMIN) || 
					userEntity.getUser_name().equals(artEnt.getUser().getUser_name()))) {
				artSearchResultDTO.setDownloadableMediaLink(getDownloadableMediaLink(artReq.getArtId()));
			}
			
		} else {
			if (artEnt != null && !isArtExpired(artEnt)) {
				artSearchResultDTO = getArtDetailsById(artReq.getArtId());
			}
		}
		
		return artSearchResultDTO;
	}
	
	private String getDownloadableMediaLink(int artId) {
		String downloadLink = "";

		ArtEntity art = artService.findbyID(artId);
		if (art != null) {
			ArtMediaEntity mediaDownload = artMediaservice.getDownloadableMediadByArt(art);
			if (mediaDownload != null) {
				downloadLink = mediaDownload.getMedia_filename();
			}
		}
		
		return downloadLink;
	}
	
	public Boolean hasUserBoughtArtToSeller(UserEntity buyerUser, int artId) {
		Boolean hasUserBought = Boolean.FALSE;
		
		ArtEntity art = artService.findbyID(artId);
		if (buyerUser != null && art != null) {
			UserArtPaymentsEntity payment = userArtPaymentsService.findByArtAndBuyer(art, buyerUser);
			if (payment != null && payment.getArt_id().getArt_id() == art.getArt_id()) {
				hasUserBought = Boolean.TRUE;
			}
		} else {
			hasUserBought = Boolean.FALSE;
		}
		
		return hasUserBought;
	}
	
	private ArtSearchResultDTO getArtDetailsById(int artId) {
		//update counter
		updateVisitCounter(artId);
		
		ArtSearchResultDTO artSearchResultDTO = mapArtEntityToDTO(artService.findbyID(artId), Boolean.FALSE, Boolean.TRUE);
		
		if (artSearchResultDTO != null) {
			//Map Media
			List<ArtMediaEntity> artMediaEntityList = getMediaByArtId(artId);
			artSearchResultDTO.setMedia(mapArtMediaEntityListToDTO(artMediaEntityList));
		}
		
		return artSearchResultDTO;
	}
	
	private Boolean isArtExpired(ArtEntity artEnt) {
		Boolean isExpired = Boolean.FALSE;

		if (artEnt != null) {
			RoleEntity roleEnt = artEnt.getUser().getRole();
			if (roleEnt != null) {
				// if -1, exp is unlimited
				if (roleEnt.getExpirationDays() >= 0) {
					Calendar cal = Calendar.getInstance();
				    cal.setTime(artEnt.getCreated_date());
				    int expInHours = roleEnt.getExpirationDays()*24;
				    cal.add(Calendar.HOUR, expInHours);
				    Date expDate = cal.getTime();
				    
				    Date currentDate = new Date();
				    if (expDate.after(currentDate)) {
				    	isExpired = Boolean.FALSE;
				    } else {
				    	isExpired = Boolean.TRUE;
				    }
				}
			}
		}
		
		return isExpired;
	}

	public List<ArtCommentsResponse> getArtComments(int artId) {
		ArtEntity art = artService.findbyID(artId);
		List<ArtCommentsEntity> artComments = artCommentsService.findByArt(art);
		List<ArtCommentsResponse> artCommentsResponse = mapArtCommentsEntityToResponse(artComments);
		return artCommentsResponse;
	}
	
	private List<ArtCommentsResponse> mapArtCommentsEntityToResponse(
			List<ArtCommentsEntity> artComments) {
		List<ArtCommentsResponse> artCommentsResponseList = new ArrayList<ArtCommentsResponse>();
		
		if (artComments != null && !artComments.isEmpty()) {
			Iterator<ArtCommentsEntity> iter = artComments.iterator();
			while (iter.hasNext()) {
				ArtCommentsEntity artCommentsEntity = iter.next();
				ArtCommentsResponse artCommentsResponse = new ArtCommentsResponse();
				artCommentsResponse.setArtCommentId(artCommentsEntity.getArt_comment_id());
				artCommentsResponse.setComment(artCommentsEntity.getComment());
				artCommentsResponse.setCreatedBy(artCommentsEntity.getUser().getUser_name());
				artCommentsResponse.setCreatedDate(artCommentsEntity.getCreate_date());
				artCommentsResponseList.add(artCommentsResponse);
			}
		}
		
		return artCommentsResponseList;
	}
	
	public GenericResponse addArtComments(ArtCommentsRequest comments) {
		ArtCommentsEntity artComment = new ArtCommentsEntity();
		artComment.setArt(artService.findbyID(comments.getArtId()));
		artComment.setComment(comments.getComment());
		artComment.setCreate_date(new Date());
		artComment.setUser(userService.findByName(comments.getCreatedBy()));
		artComment.setStatus(true);
		artCommentsService.save(artComment);
		
		sendArtCommentsEmail(artComment);
		
		GenericResponse response = new GenericResponse();
		response.message = "Art new comment has been added successfully.";
		response.success = true;
		return response;
	}
	
	public void sendArtCommentsEmail(ArtCommentsEntity artComment) {
		try {
			PersonalEntity personalTo = personalService.findByUser(artComment.getArt().getUser());
			PersonalEntity personalFrom = personalService.findByUser(artComment.getUser());
			
			if (personalTo != null && personalTo.getPersonal_email() != null & !personalTo.getPersonal_email().equalsIgnoreCase("")) {
				
				ResourceBundle properties = ResourceBundle.getBundle("config");
				
				URL domain = new URL(properties.getString("domain.app"));
				URL url = new URL(domain + "/#/artDetails/" + artComment.getArt().getArt_id());
				
				EmailTemplateSender.sendUserArtMessagesEmail(
					personalTo.getPersonal_email(), 
					personalFrom.getPersonal_name() + " " + personalFrom.getPersonal_l_name(),
					"Art ID: " + artComment.getArt().getArt_id(),
					url.toString(),
					artComment.getComment());
			}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<ArtSearchResultDTO> mapToArtResultsDTO(List<ArtEntity> artEntityList, boolean shortDescr, boolean isAdminReq) {
		List<ArtSearchResultDTO> artResultDTOList = new ArrayList<ArtSearchResultDTO>();
		
		if (artEntityList != null && !artEntityList.isEmpty()) {
			Iterator<ArtEntity> artEntityIter = artEntityList.iterator();
			while (artEntityIter.hasNext()) {
				ArtEntity artEntity = artEntityIter.next();
				ArtSearchResultDTO artSearchResultDTO = mapArtEntityToDTO(artEntity, shortDescr, isAdminReq);
				ArtMediaEntity artMediaEntity = getDefaultMediaByArtId(artEntity.getArt_id());
				ArtMediaDTO artMediaDTO = mapArtMediaEntityToDTO(artMediaEntity);
				List<ArtMediaDTO> artMediaDTOList = new ArrayList<ArtMediaDTO>();
				artMediaDTOList.add(artMediaDTO);
				artSearchResultDTO.setMedia(artMediaDTOList);
				artSearchResultDTO.setPrimaryImage(artMediaDTO.getArtMediaFilename());
				artSearchResultDTO.setLocation(getArtLocationDetails(artEntity.getUser()));
				artResultDTOList.add(artSearchResultDTO);
			}
		}
		return artResultDTOList;
	}
	
	private List<LocationDTO> getArtLocationDetails(UserEntity user) {
		List<LocationDTO> location = new ArrayList<LocationDTO>();
		
		AddressTypeEntity addressType = addressTypeService.findByName(MemmorableConstants.USER_ADDRESS_DEFAULT);
		AddressEntity address = addressService.findAddressByTypeAndUser(addressType, user);
		
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setCountry(address.getCity().getCountryState().getCountry().getCountry_name());
		locationDTO.setState(address.getCity().getCountryState().getStateName());
		locationDTO.setCity(address.getCity().getCityName());
		
		location.add(locationDTO);
		
		return location;
	}

	private List<ArtSummaryDTO> mapToArtSummaryDTO(List<ArtEntity> artEntityList, boolean shortDescr) {
		List<ArtSummaryDTO> artSummaryDTOList = new ArrayList<ArtSummaryDTO>();
		
		if (artEntityList != null && !artEntityList.isEmpty()) {
			Iterator<ArtEntity> artEntityIter = artEntityList.iterator();
			while (artEntityIter.hasNext()) {
				ArtEntity artEntity = artEntityIter.next();
				ArtSummaryDTO artSummaryDTO = mapArtEntityToSummaryDTO(artEntity, shortDescr);
				ArtMediaEntity artMediaEntity = getDefaultMediaByArtId(artEntity.getArt_id());
				if (artMediaEntity != null) {
					artSummaryDTO.setArtMediaFilename(artMediaEntity.getMedia_filename());
					artSummaryDTO.setArtMediaType(artMediaEntity.getMedia_type());
					artSummaryDTOList.add(artSummaryDTO);
				}
			}
		}
		return artSummaryDTOList;
	}
	
	private ArtSearchResultDTO mapArtEntityToDTO(ArtEntity artEntity, boolean shortDescr, boolean isAdminReq) {
		ArtSearchResultDTO artSearchResultDTO = null;
		
		if (artEntity != null) {
			artSearchResultDTO = new ArtSearchResultDTO();
			
			artSearchResultDTO.setArtId(artEntity.getArt_id());
			artSearchResultDTO.setArtType(artEntity.getArt_SubType().getArt_type().getArt_type_name());
			artSearchResultDTO.setArtSubtype(artEntity.getArt_SubType().getArt_subtype_name());
			artSearchResultDTO.setArtService(artEntity.getArt_Service().getArt_service_name());
			artSearchResultDTO.setCreatedBy(artEntity.getUser().getUser_name());
			artSearchResultDTO.setCreatedDate(artEntity.getCreated_date());
			artSearchResultDTO.setLastUpdatedDate(artEntity.getLast_updated_date());
			artSearchResultDTO.setCurrencyType(artEntity.getCurrency().getCurrency_type());
			artSearchResultDTO.setPrice(artEntity.getPrice());
			artSearchResultDTO.setRating(getArtRatingsCount(artEntity));
			artSearchResultDTO.setReviews(getArtReviewsCount(artEntity));
			artSearchResultDTO.setStatus(artEntity.getArt_status().getArt_status_type());
			String artNameStr = artEntity.getArt_name();
			String artDescStr = artEntity.getArt_description();
			//Truncate the Name to show 25 chars max only in the results page
			if (shortDescr && artNameStr.length() >= 20) {
				artNameStr = artNameStr.substring(0, Math.min(artNameStr.length(), 18)).concat("...");
			}
			//Truncate the Description to show 80 chars max only in the results page
			if (shortDescr && artDescStr.length() >= 80) {
				artDescStr = artDescStr.substring(0, Math.min(artDescStr.length(), 80)).concat("...");
			}
			
			//Set the Genre if not null
			if (artEntity.getArt_Genre() != null) {
				artSearchResultDTO.setArtGenre(artEntity.getArt_Genre().getArt_genre_name());
			}
			
			//Set the Format if not null
			if (artEntity.getArt_Format() != null) {
				artSearchResultDTO.setArtFormat(artEntity.getArt_Format().getArt_format_name());
			}
			
			artSearchResultDTO.setArtName(artNameStr);
			artSearchResultDTO.setArtDescription(artDescStr);
			artSearchResultDTO.setVisitsCounter(artEntity.getVisitsCounter());
			
			if (isAdminReq) {
				artSearchResultDTO.setFeePaymentPending(isUserArtPaymentFeePending(artEntity));
			}
		}
		
		return artSearchResultDTO;
	}
	
	private ArtSummaryDTO mapArtEntityToSummaryDTO(ArtEntity artEntity, boolean shortDescr) {
		ArtSummaryDTO artSummaryDTO = null;
		
		if (artEntity != null) {
			artSummaryDTO = new ArtSummaryDTO();
			
			artSummaryDTO.setArtId(artEntity.getArt_id());
			String artNameStr = artEntity.getArt_name();
			String artDescStr = artEntity.getArt_description();
			//Truncate the Name to show 25 chars max only in the results page
			if (shortDescr && artNameStr.length() >= 20) {
				artNameStr = artNameStr.substring(0, Math.min(artNameStr.length(), 18)).concat("...");
			}
			//Truncate the Description to show 80 chars max only in the results page
			if (shortDescr && artDescStr.length() >= 80) {
				artDescStr = artDescStr.substring(0, Math.min(artDescStr.length(), 80)).concat("...");
			}
			
			artSummaryDTO.setArtName(artNameStr);
			artSummaryDTO.setArtDescription(artDescStr);
		}
		
		return artSummaryDTO;
	}
	
	private void updateVisitCounter(int artId) {
		ArtEntity art = artService.findbyID(artId);
		if (art != null) {
			int currentVisitCounter = art.getVisitsCounter();
			currentVisitCounter += 1;
			art.setVisitsCounter(currentVisitCounter);
			artService.update(art);
		}
	}

	private int getArtRatingsCount(ArtEntity art) {
		return artRatingService.findRateByArt(art);
	}
	
	private int getArtReviewsCount(ArtEntity art) {
		return artCommentsService.findByArt(art).size();
	}
	
	private List<ArtMediaDTO> mapArtMediaEntityListToDTO(List<ArtMediaEntity> artMediaEntityList) {
		List<ArtMediaDTO> artMediaDTOList = new ArrayList<ArtMediaDTO>();
		
		if (artMediaEntityList != null && !artMediaEntityList.isEmpty()) {
			Iterator<ArtMediaEntity> artMediaEntityIter = artMediaEntityList.iterator();
			
			while (artMediaEntityIter.hasNext()) {
				ArtMediaEntity artMediaEntity = artMediaEntityIter.next();
				ArtMediaDTO artMediaDTO = mapArtMediaEntityToDTO(artMediaEntity);
				artMediaDTOList.add(artMediaDTO);
			}
		}
		
		return artMediaDTOList;
	}

	private ArtMediaDTO mapArtMediaEntityToDTO(ArtMediaEntity artMediaEntity) {
		ArtMediaDTO artMediaDTO = new ArtMediaDTO();
		if (artMediaEntity != null) {
			artMediaDTO.setArtMediaId(artMediaEntity.getMedia_id());
			artMediaDTO.setArtMediaType(artMediaEntity.getMedia_type());
			artMediaDTO.setArtMediaFilename(artMediaEntity.getMedia_filename());
			artMediaDTO.setPrimary(artMediaEntity.isMedia_primary());
		} else {
			artMediaDTO.setArtMediaFilename("http://placehold.it/320x150");
		}
		
		return artMediaDTO;
	}

	public int getArtRatingByUser(ArtRatingRequest request) {
		ArtEntity art = artService.findbyID(request.getArtId());
		UserEntity user = userService.findByName(request.getCreatedBy());
		ArtRatingEntity artRatingEntity = artRatingService.findRateByArtAndUser(art, user);
		if (artRatingEntity != null && artRatingEntity.getRating_rate() > 0) {
			return artRatingEntity.getRating_rate();
		} else {
			return 0;
		}
	}

	public GenericResponse setArtRating(ArtRatingRequest rating) {
		ArtEntity art = artService.findbyID(rating.getArtId());
		UserEntity user = userService.findByName(rating.getCreatedBy());
		ArtRatingEntity artRatingEntity = artRatingService.findRateByArtAndUser(art, user);
		GenericResponse response = new GenericResponse();
		
		if (artRatingEntity != null) {
			artRatingEntity.setRating_rate(rating.getRating());
			artRatingService.update(artRatingEntity);
			response.message = "Rating updated successfully";
		} else {
			artRatingEntity = new ArtRatingEntity();
			artRatingEntity.setArt(art);
			artRatingEntity.setUser(user);
			artRatingEntity.setRating_rate(rating.getRating());
			artRatingService.save(artRatingEntity);
			response.message = "Rating saved successfully";
		}
		
		response.success = true;
		return response;
	}

	public GenericResponse deleteArtComments(int artCommentId) {
		ArtCommentsEntity artCommentsEntity = artCommentsService.findbyId(artCommentId);
		artCommentsEntity.setStatus(false);
		artCommentsService.update(artCommentsEntity);
		
		GenericResponse response = new GenericResponse();
		response.success = true;
		response.message = "Comment has been deleted successfully";
        return response;
	}

	public List<ArtSummaryDTO> getTopTenNewArtDetails() {
		SearchResultEntityList<ArtEntity> artList = artService.findbyTopTenNewArt();
		List<ArtSummaryDTO> artSummaryListDTO = mapToArtSummaryDTO(artList.getEntityList(), Boolean.FALSE);
		return artSummaryListDTO;
	}

	public SearchResultEntityList<ArtSearchResultDTO> getArtDetailsByUser(CurrentUser user) {
		UserEntity userEnt = userService.findByName(user.username);
		SearchResultEntityList<ArtEntity> artList = artService.findByUser(userEnt);
		List<ArtSearchResultDTO> artListDTO = mapToArtResultsDTO(artList.getEntityList(), Boolean.TRUE, Boolean.TRUE);
		SearchResultEntityList<ArtSearchResultDTO> artSearchResult = getSearchResultEntityList(artList, artListDTO);
		return artSearchResult;
	}
	
	public SearchResultEntityList<ArtSearchResultDTO> getPurchasedArtByUser(CurrentUser user) {
		UserEntity userEnt = userService.findByName(user.username);
		
		List<UserArtPaymentsEntity> artPaymentList = userArtPaymentsService.findAllUserArtPayments(userEnt, MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
		List<ArtEntity> artPurchasedList = new ArrayList<ArtEntity>();
		
		Iterator<UserArtPaymentsEntity> iter = artPaymentList.iterator();
		while (iter.hasNext()) {
			UserArtPaymentsEntity userArtPayments = iter.next();
			if (userArtPayments != null) {
				ArtEntity art = userArtPayments.getArt_id();
				artPurchasedList.add(art);
			}
			
		}
		
		List<ArtSearchResultDTO> artListDTO = new ArrayList<ArtSearchResultDTO>();
		if (!artPurchasedList.isEmpty()) {
			artListDTO = mapToArtResultsDTO(artPurchasedList, Boolean.TRUE, Boolean.TRUE);
		}
		
		SearchResultEntityList<ArtSearchResultDTO> artSearchResult = new SearchResultEntityList<ArtSearchResultDTO>();
		artSearchResult.setEntityList(artListDTO);
		artSearchResult.setResultCount(artListDTO.size());
		return artSearchResult;
	}
	
	private SearchResultEntityList<ArtSearchResultDTO> getSearchResultEntityList(
			SearchResultEntityList<ArtEntity> artList,
			List<ArtSearchResultDTO> artListDTO) {
		SearchResultEntityList<ArtSearchResultDTO> list = new SearchResultEntityList<ArtSearchResultDTO>();
		list.setEntityList(artListDTO);
		list.setResultCount(artList.getResultCount());
		return list;
	}

	public GenericResponse registerArtPayment(ActivateArtRequest artReq) {
		GenericResponse genRespond = new GenericResponse();
		ArtStatusEntity artStatus = artStatusService.findByName(MemmorableConstants.ART_STATUS_SOLD);

		ArtEntity art = artService.findbyID(artReq.getArtId());
		if (art != null) {
			//Register Payment
			UserArtPaymentsEntity userArtPayments = new UserArtPaymentsEntity();
			userArtPayments.setCreatedDate(new Date());
			userArtPayments.setPaymentId(artReq.getPaymentId());
			String paymentMethod = "";
			
			if (artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_PAYPAL) ||
					artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_DEPOSIT) ||
					artReq.getPaymentMethod().equalsIgnoreCase(MemmorableConstants.PAYMENT_METHOD_TRANSFER)) {
				paymentMethod = artReq.getPaymentMethod().toUpperCase();
			} else {
				//Default to Deposit
				paymentMethod = MemmorableConstants.PAYMENT_METHOD_DEPOSIT;
			}

			if (!isUserArtPaymentAmountPending(art)) {
				UserEntity user = userService.findByName(artReq.getUsername());
				if (user != null) {
					userArtPayments.setUser(user);
					userArtPayments.setArt_id(art);
					userArtPayments.setPaymentMethod(paymentMethod);
					userArtPayments.setPaymentType(MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
					userArtPaymentsService.save(userArtPayments);
				} else {
					genRespond.message = "Error while activating Art";
					genRespond.success = false;
					return genRespond;
				}
			} else {
				userArtPayments = userArtPaymentsService.getArtPaymentsAmountPending(art);
				userArtPayments.setCreatedDate(new Date());
				userArtPayments.setPaymentId(artReq.getPaymentId());
				userArtPayments.setPaymentMethod(paymentMethod);
				userArtPaymentsService.update(userArtPayments);
			}
			
			art.setArt_status(artStatus);
			art.setDeleted(false);
			artService.update(art);
			
			registerSurveys(art, userArtPayments.getUser());
			
			genRespond.message = "Art was activated successfully";
			genRespond.success = true;
		} else {
			genRespond.message = "Error while activating Art";
			genRespond.success = false;
		}
		
		return genRespond;
	}
	
	public Boolean isUserSurveysPending(String currentUser) {
		UserSurveyAdministrationDTO surveyDTO = new UserSurveyAdministrationDTO();
		return surveyDTO.isUserSurveysPending(currentUser);
	}
	
	public Boolean isUserProfilePending(String currentUser) {
		ProfileAdministrationDTO profileDTO = new ProfileAdministrationDTO();
		return profileDTO.isUserProfilePending(currentUser);
	}
	
	private Boolean isUserArtPaymentAmountPending(ArtEntity art) {
		UserArtPaymentsEntity userArtPayments = userArtPaymentsService.getArtPaymentsAmountPending(art);
		if (userArtPayments != null && userArtPayments.getPaymentId().equals("")) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	private Boolean isUserArtPaymentFeePending(ArtEntity art) {
		UserArtPaymentsEntity userArtPayments = userArtPaymentsService.getArtPaymentsFeePending(art);
		if (userArtPayments != null && userArtPayments.getPaymentId().equals("")) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	private void registerSurveys(ArtEntity art, UserEntity userBuyer) {
		//Register Surver for Buyer
		UserSurveyEntity userSurveyBuyer = new UserSurveyEntity();
		userSurveyBuyer.setArt(art);
		userSurveyBuyer.setCreatedDate(new Date());
		userSurveyBuyer.setFromUser(userBuyer);
		userSurveyBuyer.setToUser(art.getUser());
		userSurveyBuyer.setTransaction(MemmorableConstants.USER_SURVEY_TRANSACTION_PURCHASE);
		userSurveyBuyer.setStatus(MemmorableConstants.USER_SURVEY_STATUS_OPEN);
		userSurveyService.save(userSurveyBuyer);
		
		//Register Surver for Seller
		UserSurveyEntity userSurveySeller = new UserSurveyEntity();
		userSurveySeller.setArt(art);
		userSurveySeller.setCreatedDate(new Date());
		userSurveySeller.setFromUser(art.getUser());
		userSurveySeller.setToUser(userBuyer);
		userSurveySeller.setTransaction(MemmorableConstants.USER_SURVEY_TRANSACTION_SELL);
		userSurveySeller.setStatus(MemmorableConstants.USER_SURVEY_STATUS_OPEN);
		userSurveyService.save(userSurveySeller);
	}
	
}
