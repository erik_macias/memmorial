package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserCommentsDAO;
import com.memmorable.backend.orm.entity.UserCommentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("userCommentsDAO")
public class UserCommentsDAOImpl extends AbstractDao<UserCommentsEntity> implements UserCommentsDAO {
		
	@Override
	public UserCommentsEntity findById(int id) {
		Criteria crit = getSession().createCriteria(UserCommentsEntity.class);
		crit.add(Restrictions.eq("user_comment_id", id));
		return (UserCommentsEntity) crit.uniqueResult();
	}

	@Override
	public List<UserCommentsEntity> findByUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(UserCommentsEntity.class);
		crit.add(Restrictions.eq("toUser", user));
		crit.add(Restrictions.eq("status", Boolean.TRUE));
		return crit.list();
	}
	
}
