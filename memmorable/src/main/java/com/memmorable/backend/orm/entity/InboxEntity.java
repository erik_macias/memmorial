package com.memmorable.backend.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Table_inbox")
public class InboxEntity implements Serializable {
	private static final long serialVersionUID = 7052097721508186814L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "inbox_id", nullable = false, unique = true)
	private int inbox_id;

	@Column(name = "created_date", nullable = false, columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_date;

	@Column(name = "subject", nullable = false, length = 200)
	private String subject;
	
	@Column(name = "message", nullable = false, length = 500)
	private String message;
	
	@Column(name = "isRead", nullable = false)
	private boolean read;

	@OneToOne(targetEntity = InboxStatusEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "inbox_status_id")
	private InboxStatusEntity inboxStatus;
	
	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "from_user_id")
	private UserEntity fromUser;
	
	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "to_user_id")
	private UserEntity toUser;

	public int getInbox_id() {
		return inbox_id;
	}

	public void setInbox_id(int inbox_id) {
		this.inbox_id = inbox_id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public InboxStatusEntity getInboxStatus() {
		return inboxStatus;
	}

	public void setInboxStatus(InboxStatusEntity inboxStatus) {
		this.inboxStatus = inboxStatus;
	}

	public UserEntity getFromUser() {
		return fromUser;
	}

	public void setFromUser(UserEntity fromUser) {
		this.fromUser = fromUser;
	}

	public UserEntity getToUser() {
		return toUser;
	}

	public void setToUser(UserEntity toUser) {
		this.toUser = toUser;
	}
	
}