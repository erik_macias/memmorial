package com.memmorable.backend.orm.dto;

public class UsersBought {
	private String buyerUser;
	private String sellerUser;

	public String getBuyerUser() {
		return buyerUser;
	}

	public void setBuyerUser(String buyerUser) {
		this.buyerUser = buyerUser;
	}

	public String getSellerUser() {
		return sellerUser;
	}

	public void setSellerUser(String sellerUser) {
		this.sellerUser = sellerUser;
	}

}
