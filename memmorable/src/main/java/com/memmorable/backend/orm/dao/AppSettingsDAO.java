package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.AppSettingsEntity;


public interface AppSettingsDAO extends InterfaceDAO<AppSettingsEntity>{
	public AppSettingsEntity findByID(int  id);
	public AppSettingsEntity findByName(String name);
	public Boolean reBuildLuceneIndex();
}
