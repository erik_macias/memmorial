package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.InboxStatusEntity;

public interface InboxStatusDAO extends InterfaceDAO<InboxStatusEntity>{
	public InboxStatusEntity findByID(int  id);
	public InboxStatusEntity findByName(String name);
}
