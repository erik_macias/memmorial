package com.memmorable.backend.orm.dto;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;
import com.memmorable.backend.orm.service.PaypalPlanService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.services.rest.request.MembershipInfoRequest;
import com.memmorable.paypal.payments.membership.PaypalMembership;
import com.memmorable.paypal.util.PaypalUtils;
import com.paypal.api.payments.Agreement;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.PaymentDefinition;
import com.paypal.api.payments.Plan;
import com.paypal.api.payments.PlanList;
import com.paypal.base.rest.PayPalRESTException;

public class PaypalPlanAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();

	PaypalPlanService planService = (PaypalPlanService) context.getBean("paypalPlanService");
	RoleService roleService = (RoleService)context.getBean("roleService");
	UserService userService = (UserService) context.getBean("userService");
	UserPaymentsService userPaymentsService = (UserPaymentsService) context.getBean("userPaymentsService");
	
	public List<PaypalPlanEntity> getPaypalPlans() {
		return planService.findAll();
	}

	public PlanList getPaypalPlans(Map<String, String> containerMap) {
		PlanList planList = null;
		try {
			PaypalMembership membership = new PaypalMembership();
			planList = membership.getBillingPlans(containerMap);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return planList;
	}

	public Plan getPaypalPlan(String planId) {
		Plan plan = null;
		try {
			PaypalMembership membership = new PaypalMembership();
			plan = membership.getBillingPlanDetails(planId);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return plan;
	}

	public void deletePaypalPlan(String planId) {
		try {
			PaypalMembership membership = new PaypalMembership();
			membership.deleteBillingPlan(planId);
			PaypalPlanEntity entity = planService.findByPlanId(planId);
			if (entity != null) {
				planService.delete(entity);
			}
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
	}

	public Plan getPaypalPlanTemplate() {
		Plan plan = null;
		PaypalMembership membership = new PaypalMembership();
		plan = membership.getCreateBillingPlanTemplate();
		return plan;
	}

	public Plan createPaypalPlan(Plan plan, RoleEntity role) {
		try {
			PaypalMembership membership = new PaypalMembership();
			plan = membership.createNewBillingPlan(plan);
			PaypalPlanEntity entity = new PaypalPlanEntity();
			entity.setPlan_id(plan.getId());
			entity.setPlanStatus(MemmorableConstants.PAYPAL_BILLING_PLAN_CREATED);
			entity.setRole(roleService.findByRoleName(role.getRole_name()));
			planService.save(entity);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return plan;
	}

	public Boolean activatePaypalPlan(Plan plan) {
		PaypalMembership membership = new PaypalMembership();
		Boolean status = Boolean.FALSE;
		try {
			status = membership.activateBillingPlan(plan.getId());
			if (status) {
				PaypalPlanEntity entity = planService.findByPlanId(plan.getId());
				entity.setPlanStatus(MemmorableConstants.PAYPAL_BILLING_PLAN_ACTIVE);
				planService.update(entity);
			}
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return status;
	}

	public Agreement createBillingAgreement(Plan plan) {
		PaypalMembership membership = new PaypalMembership();
		PaypalUtils utils = PaypalUtils.getInstance();
		Plan paypalPlan = getPaypalPlan(plan.getId());
		Plan planAgrmnt = new Plan();
		planAgrmnt.setId(paypalPlan.getId());
		Agreement agreement = new Agreement();
		agreement.setName(paypalPlan.getName() + " Agreement");
		agreement.setDescription(paypalPlan.getDescription() + " Agreement");
		agreement.setStartDate(utils.getBillingAgrmntStartDate());
		agreement.setPlan(planAgrmnt);
		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");
		agreement.setPayer(payer);
		
		Agreement agreementResp = null;
		try {
			agreementResp = membership.createBillingAgreement(agreement);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		
		return agreementResp;
	}

	public Agreement getPaypalPlanAgreement(String id) {
		Agreement agreement = null;
		try {
			PaypalMembership membership = new PaypalMembership();
			agreement = membership.getBillingPlanAgrmntDetails(id);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return agreement;
	}

	public Plan getPaypalPlanByRole(String roleName) {
		Plan plan = null;
		RoleEntity role = roleService.findByRoleName(roleName);
		PaypalPlanEntity paypalEnt = planService.findPlanIdByRole(role);
		if (paypalEnt != null) {
			plan = new Plan();
			plan.setId(paypalEnt.getPlan_id());
		}
		
		return plan;
	}

	public Agreement execPaypalPlanAgreement(String token) {
		Agreement agreement = null;
		try {
			PaypalMembership membership = new PaypalMembership();
			agreement = membership.executeBillingAgreement(token);
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return agreement;
	}

	public String updateUserMembershipPlan(MembershipInfoRequest membership) {
		UserEntity user = userService.findByName(membership.getUsername());
		Agreement agreement = getPaypalPlanAgreement(membership.getAgreementId());
		RoleEntity newRole = null;
		
		if (agreement != null && !agreement.getPlan().getPaymentDefinitions().isEmpty()) {
			Iterator<PaymentDefinition> iter = agreement.getPlan().getPaymentDefinitions().iterator();
			int frequencyInterval = 0;
			String frequency = "";
			
			while(iter.hasNext()) {
				PaymentDefinition paymentDef = iter.next();
				if(paymentDef != null) {
					if (paymentDef.getType().equals(MemmorableConstants.PAYPAL_BILLING_PLAN_TYPE_REGULAR)) {
						frequencyInterval = Integer.parseInt(paymentDef.getFrequencyInterval());
						frequency = paymentDef.getFrequency();
					}
				}
			}
			
			if (frequencyInterval <= 12) {
				if ((frequencyInterval == 12 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency))
						|| (frequencyInterval == 1 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_YEAR.equalsIgnoreCase(frequency))) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_YEAR);
				} else if (frequencyInterval == 6  && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency)) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_HALF_YEAR);
				} else if (frequencyInterval == 4 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency)) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_FOURTHMONTHLY);
				} else if (frequencyInterval == 3 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency)) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_QUARTER);
				} else if (frequencyInterval == 2 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency)) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_BIMONTHLY);
				} else if (frequencyInterval == 1 && MemmorableConstants.PAYPAL_BILLING_PLAN_FREQ_MONTH.equalsIgnoreCase(frequency)) {
					newRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_MONTHLY);
				}
			}
		}
		
		if (newRole != null) {
			user.setRole(newRole);
			userService.update(user);

			UserPaymentsEntity userPayments = new UserPaymentsEntity();
			userPayments.setUser(user);
			userPayments.setRole(newRole);
			userPayments.setCredit(newRole.getPrice());
			userPayments.setAgreementId(agreement.getId());
			userPayments.setCreated_date(new Date());
			userPaymentsService.save(userPayments);
			
			return newRole.getRole_text_name();
		} else {
			return null;
		}
	}

}
