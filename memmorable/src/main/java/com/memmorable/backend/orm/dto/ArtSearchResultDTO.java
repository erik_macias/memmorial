package com.memmorable.backend.orm.dto;

import java.util.Date;
import java.util.List;

public class ArtSearchResultDTO {

	private int artId;
	private String artName;
	private String artDescription;
	private String artType;
	private String artSubtype;
	private String artService;
	private String artGenre;
	private String artFormat;
	private String createdBy;
	private List<String> artTags;
	private List<ArtMediaDTO> media;
	private int rating;
	private String primaryImage;
	private String currencyType;
	private Date createdDate;
	private Date lastUpdatedDate;
	private Double price;
	private int reviews;
	private String status;
	private String purchasedBy;
	private int visitsCounter;
	private boolean isFeePaymentPending;
	private String downloadableMediaLink;
	private List<LocationDTO> location;

	public int getArtId() {
		return artId;
	}

	public void setArtId(int artId) {
		this.artId = artId;
	}

	public String getArtName() {
		return artName;
	}

	public void setArtName(String artName) {
		this.artName = artName;
	}

	public String getArtDescription() {
		return artDescription;
	}

	public void setArtDescription(String artDescription) {
		this.artDescription = artDescription;
	}

	public String getArtType() {
		return artType;
	}

	public void setArtType(String artType) {
		this.artType = artType;
	}

	public String getArtSubtype() {
		return artSubtype;
	}

	public void setArtSubtype(String artSubtype) {
		this.artSubtype = artSubtype;
	}
	
	public String getArtService() {
		return artService;
	}

	public void setArtService(String artService) {
		this.artService = artService;
	}

	public String getArtGenre() {
		return artGenre;
	}

	public void setArtGenre(String artGenre) {
		this.artGenre = artGenre;
	}

	public String getArtFormat() {
		return artFormat;
	}

	public void setArtFormat(String artFormat) {
		this.artFormat = artFormat;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<String> getArtTags() {
		return artTags;
	}

	public void setArtTags(List<String> artTags) {
		this.artTags = artTags;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public List<ArtMediaDTO> getMedia() {
		return media;
	}

	public void setMedia(List<ArtMediaDTO> media) {
		this.media = media;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getPrimaryImage() {
		return primaryImage;
	}

	public void setPrimaryImage(String primaryImage) {
		this.primaryImage = primaryImage;
	}

	public int getReviews() {
		return reviews;
	}

	public void setReviews(int reviews) {
		this.reviews = reviews;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPurchasedBy() {
		return purchasedBy;
	}

	public void setPurchasedBy(String purchasedBy) {
		this.purchasedBy = purchasedBy;
	}

	public int getVisitsCounter() {
		return visitsCounter;
	}

	public void setVisitsCounter(int visitsCounter) {
		this.visitsCounter = visitsCounter;
	}

	public boolean isFeePaymentPending() {
		return isFeePaymentPending;
	}

	public void setFeePaymentPending(boolean isFeePaymentPending) {
		this.isFeePaymentPending = isFeePaymentPending;
	}

	public String getDownloadableMediaLink() {
		return downloadableMediaLink;
	}

	public void setDownloadableMediaLink(String downloadableMediaLink) {
		this.downloadableMediaLink = downloadableMediaLink;
	}

	public List<LocationDTO> getLocation() {
		return location;
	}

	public void setLocation(List<LocationDTO> location) {
		this.location = location;
	}
	
}
