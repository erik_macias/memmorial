package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.CountryEntity;


public interface CountryDAO extends InterfaceDAO<CountryEntity>{
	public CountryEntity findByID(int  id);
	public CountryEntity findByName(String name);
	public CountryEntity findByCountryCode(String code);
}
