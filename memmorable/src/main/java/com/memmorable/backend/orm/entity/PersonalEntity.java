package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_Personal")
public class PersonalEntity implements Serializable {
	private static final long serialVersionUID = -1240647277790277694L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "personal_id", nullable = false, unique = true)
	private int personal_id;
	@Column(name = "personal_name", nullable = false, unique = false, length = 100)
	private String personal_name;
	@Column(name = "personal_m_initial", nullable = true, unique = false, length = 100)
	private String personal_m_initial;
	@Column(name = "personal_l_name", nullable = false, unique = false, length = 100)
	private String personal_l_name;
	@Column(name = "personal_email", nullable = false, unique = true, length = 100)
	private String personal_email;
	@Column(name = "personal_gender", nullable = false, unique = true, length = 100)
	private String personal_gender;
	
	@OneToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity user;
	
	public int getPersonal_id() {
		return personal_id;
	}
	public void setPersonal_id(int personal_id) {
		this.personal_id = personal_id;
	}
	public String getPersonal_name() {
		return personal_name;
	}
	public void setPersonal_name(String personal_name) {
		this.personal_name = personal_name;
	}
	public String getPersonal_m_initial() {
		return personal_m_initial;
	}
	public void setPersonal_m_initial(String personal_m_initial) {
		this.personal_m_initial = personal_m_initial;
	}
	public String getPersonal_l_name() {
		return personal_l_name;
	}
	public void setPersonal_l_name(String personal_l_name) {
		this.personal_l_name = personal_l_name;
	}
	public String getPersonal_email() {
		return personal_email;
	}
	public void setPersonal_email(String personal_email) {
		this.personal_email = personal_email;
	}
	public String getPersonal_gender() {
		return personal_gender;
	}
	public void setPersonal_gender(String personal_gender) {
		this.personal_gender = personal_gender;
	}
	public UserEntity getUser() {
		return user;
	}
	public void setUser(UserEntity user) {
		this.user = user;
	}

}
