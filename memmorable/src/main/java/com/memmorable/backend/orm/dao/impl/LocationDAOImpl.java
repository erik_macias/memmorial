package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.LocationDAO;
import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;

@Repository("locationDAO")
public class LocationDAOImpl extends AbstractDao<CountryStatesCityEntity> implements LocationDAO {

	@Override
	public CountryStatesCityEntity findByID(int cityId) {
		Criteria crit = getSession().createCriteria(CountryStatesCityEntity.class);
		crit.add(Restrictions.eq("cityId", cityId));
		return (CountryStatesCityEntity) crit.uniqueResult();
	}

	@Override
	public CountryStatesCityEntity findByCityName(String countryName, String stateName, String cityName) {
		
		Criteria crit = getSession().createCriteria(CountryEntity.class);
		crit.add(Restrictions.eq("country_name", countryName));
		CountryEntity country = (CountryEntity) crit.uniqueResult();
		
		crit = getSession().createCriteria(CountryStatesEntity.class);
		crit.add(Restrictions.eq("stateName", stateName));
		crit.add(Restrictions.eq("country", country));
		CountryStatesEntity state = (CountryStatesEntity) crit.uniqueResult();
		
		crit = getSession().createCriteria(CountryStatesCityEntity.class);
		crit.add(Restrictions.eq("cityName", cityName));
		crit.add(Restrictions.eq("countryState", state));
		
		return (CountryStatesCityEntity) crit.uniqueResult();
	}

	@Override
	public List<CountryStatesEntity> findStatesByCountry(CountryEntity country) {
		Criteria crit = getSession().createCriteria(CountryStatesEntity.class);
		crit.add(Restrictions.eq("country", country));
		return crit.list();
	}

	@Override
	public List<CountryStatesCityEntity> findCitiesByState(CountryStatesEntity state) {
		Criteria crit = getSession().createCriteria(CountryStatesCityEntity.class);
		crit.add(Restrictions.eq("countryState", state));
		return crit.list();
	}

	@Override
	public CountryStatesEntity findStateByID(int stateId) {
		Criteria crit = getSession().createCriteria(CountryStatesEntity.class);
		crit.add(Restrictions.eq("stateId", stateId));
		return (CountryStatesEntity) crit.uniqueResult();
	}

}
