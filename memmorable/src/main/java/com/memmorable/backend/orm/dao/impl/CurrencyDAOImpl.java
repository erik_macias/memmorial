package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.CurrencyDAO;
import com.memmorable.backend.orm.entity.CurrencyEntity;

@Repository("currencyDAO")
public class CurrencyDAOImpl extends AbstractDao<CurrencyEntity> implements CurrencyDAO {

	@Override
	public CurrencyEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(CurrencyEntity.class);
		crit.add(Restrictions.eq("currency_id", id));
		return (CurrencyEntity) crit.uniqueResult();
	}

	@Override
	public CurrencyEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(CurrencyEntity.class);
		crit.add(Restrictions.eq("currency_type", name));
		return (CurrencyEntity) crit.uniqueResult();
	}

}
