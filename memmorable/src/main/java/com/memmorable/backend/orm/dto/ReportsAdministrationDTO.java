package com.memmorable.backend.orm.dto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.ArtTypeFeeService;
import com.memmorable.backend.orm.service.ProfileService;
import com.memmorable.backend.orm.service.UserArtPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.services.rest.responses.ReportListResponse;
import com.memmorable.backend.services.rest.responses.UsersResponse;

public class ReportsAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();
	ArtService  artService = (ArtService) context.getBean("artService");
	UserService userService = (UserService) context.getBean("userService");
	ProfileService profileService = (ProfileService) context.getBean("profileService");
	UserArtPaymentsService userArtPaymentsService = (UserArtPaymentsService) context.getBean("userArtPaymentsService");
	ArtTypeFeeService artTypeFeeService  = (ArtTypeFeeService) context.getBean("artTypeFeeService");
	
	public ReportListResponse<String> getAllVisitsReportFromUser(String user) {
		ReportListResponse<String> response = new ReportListResponse<String>();
		List<String> labels = new ArrayList<String>();
		List<Double> dataA = new ArrayList<Double>();
		List<Double> dataB = new ArrayList<Double>();
		
		UserEntity userEnt = userService.findByName(user);
		if (userEnt != null) {
			labels.add("Profile Visits");
			Integer profVisits = new Integer(profileService.getProfileVisits(userEnt));
			dataA.add(profVisits.doubleValue());
			
			labels.add("Art Visits");
			Integer artVisits = new Integer(artService.getArtVisitsByUser(userEnt));
			dataB.add(artVisits.doubleValue());
			
			response.setLabelsList(labels);
			response.setDataListA(dataA);
			response.setDataListB(dataB);
			
			response.success = true;
			response.message = "Data Retrieved Successfully";
		} else {
			response.success = false;
			response.message = "No results found";
		}
		
		return response;
	}
	
	public ReportListResponse<String> getCurrentYearSalesReportFromUser(String username) {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		return getSalesReportFromUser(username, currentYear);
	}
	
	public ReportListResponse<String> getLastYearSalesReportFromUser(String username) {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		return getSalesReportFromUser(username, currentYear-1);
	}
	
	public ReportListResponse<String> getCurrentYearSalesReport() {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		return getSalesReportFromUser(currentYear);
	}
	
	public ReportListResponse<String> getLastYearSalesReportFromUser() {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		return getSalesReportFromUser(currentYear-1);
	}
	
	public ReportListResponse<UserSalesReportDTO> getRangeSalesReportFromUser(ReportRanges ranges) {
		return getSalesReportFromUser(ranges);
	}
	
	public ReportListResponse<String> getCurrentYearFeeReport() {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		return getFeeReport(currentYear);
	}
	
	public ReportListResponse<UserSalesReportDTO> getRangeFeeReportFromUser(ReportRanges ranges) {
		return getFeeReport(ranges);
	}
	
	public UsersResponse<UserFeePaidPendingDTO> getFeePaidPending() {
		List<UserArtPaymentsEntity> artFeePendingPaymenstList = userArtPaymentsService.getFeePaidPending();
		UsersResponse<UserFeePaidPendingDTO> response = new UsersResponse<UserFeePaidPendingDTO>();
		
		List<UserFeePaidPendingDTO> userFeePaidPendingDTOList = new ArrayList<UserFeePaidPendingDTO>();
		if (artFeePendingPaymenstList != null) {
			Iterator<UserArtPaymentsEntity> iter = artFeePendingPaymenstList.iterator();
			while (iter.hasNext()) {
				UserArtPaymentsEntity artFeePendingPaymenst = iter.next();
				if (artFeePendingPaymenst != null) {
					UserFeePaidPendingDTO userFeePaidPendingDTO = getFeePaidPending(artFeePendingPaymenst.getArt_id());
					if (userFeePaidPendingDTO != null) {
						userFeePaidPendingDTOList.add(userFeePaidPendingDTO);
					}
				}
			}
		}
		
		response.users = userFeePaidPendingDTOList;
		response.success = true;
		response.message = "Successfully retrieved pending fee";
		
		return response;
	}
	
	public UserFeePaidPendingDTO getFeePaidPending(ArtEntity art) {
		return mapArtEntityToDTO(art);
	}
	
	public ReportListResponse<String> getCreatedUsers() {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);
		
		List<UserCreatedReportDTO> list = userArtPaymentsService.getUsersCreatedReport(currentYear);
		return getUsersCreatedReportResponse(list, currentYear);
	}
	
	private UserFeePaidPendingDTO mapArtEntityToDTO(ArtEntity art) {
		UserFeePaidPendingDTO artFeePendingDTO = null;
		
		ArtTypeFeeEntity artTypeFee = artTypeFeeService.findByArtTypeAndRole(art.getArt_SubType().getArt_type(), art.getUser().getRole());
		if (artTypeFee != null) {
			artFeePendingDTO = new UserFeePaidPendingDTO();
			double feeAmount = (artTypeFee.getFee_amount()*art.getPrice())/100;
			artFeePendingDTO.setAmount(feeAmount);
			artFeePendingDTO.setUsername(art.getUser().getUser_name());
			artFeePendingDTO.setArtId(art.getArt_id());
		}
		
		return artFeePendingDTO;
	}

	private ReportListResponse<String> getSalesReportFromUser(String username, int year) {
		ReportListResponse<String> response = new ReportListResponse<String>();
		
		UserEntity userEnt = userService.findByName(username);
		if (userEnt != null) {
			List<UserSalesReportDTO> list = userArtPaymentsService.getUserArtPaymentsReport(userEnt, year);
			response = getSalesReportResponse(list, year);
		} else {
			response.success = false;
			response.message = "No results found";
		}
		
		return response;
	}
	
	private ReportListResponse<String> getSalesReportFromUser(int year) {
		List<UserSalesReportDTO> list = userArtPaymentsService.getUsersArtPaymentsReport(year);
		return getSalesReportResponse(list, year);
	}
	
	private ReportListResponse<UserSalesReportDTO> getSalesReportFromUser(ReportRanges ranges) {
		List<UserSalesReportDTO> list = userArtPaymentsService.getUsersArtPaymentsReport(ranges);
		ReportListResponse<UserSalesReportDTO> response = new ReportListResponse<UserSalesReportDTO>();
		if (!list.isEmpty()) {
			response.setSeriesList(list);
			response.success = true;
			response.message = "Data Retrieved Successfully";
		} else {
			response.success = false;
			response.message = "No results found";
		}
		return response;
	}
	
	private ReportListResponse<String> getFeeReport(int year) {
		List<UserSalesReportDTO> list = userArtPaymentsService.getUsersArtFeePaymentsReport(year);
		return getSalesReportResponse(list, year);
	}
	
	private ReportListResponse<UserSalesReportDTO> getFeeReport(ReportRanges ranges) {
		List<UserSalesReportDTO> list = userArtPaymentsService.getUsersArtFeePaymentsReport(ranges);
		ReportListResponse<UserSalesReportDTO> response = new ReportListResponse<UserSalesReportDTO>();
		if (!list.isEmpty()) {
			response.setSeriesList(list);
			response.success = true;
			response.message = "Data Retrieved Successfully";
		} else {
			response.success = false;
			response.message = "No results found";
		}
		return response;
	}
	
	private ReportListResponse<String> getSalesReportResponse(List<UserSalesReportDTO> list, int year) {
		ReportListResponse<String> response = new ReportListResponse<String>();
		List<String> labels = new ArrayList<String>();
		List<Double> data = new ArrayList<Double>();
		List<String> series = new ArrayList<String>();
		series.add(String.valueOf(year));
		
		Iterator<UserSalesReportDTO> iter = list.iterator();
		while (iter.hasNext()) {
			UserSalesReportDTO userSales = (UserSalesReportDTO) iter.next();
			data.add(userSales.getAmount());
			labels.add(userSales.getMonth());
		}
		
		if (!data.isEmpty()) {
			response.setLabelsList(labels);
			response.setSeriesList(series);
			response.setDataListA(data);
			
			response.success = true;
			response.message = "Data Retrieved Successfully";
		} else {
			response.success = false;
			response.message = "No results found";
		}
		return response;
	}
	
	private ReportListResponse<String> getUsersCreatedReportResponse(List<UserCreatedReportDTO> list, int year) {
		ReportListResponse<String> response = new ReportListResponse<String>();
		List<String> labels = new ArrayList<String>();
		List<Double> data = new ArrayList<Double>();
		List<String> series = new ArrayList<String>();
		series.add(String.valueOf(year));
		
		Iterator<UserCreatedReportDTO> iter = list.iterator();
		while (iter.hasNext()) {
			UserCreatedReportDTO usersCreated = (UserCreatedReportDTO) iter.next();
			data.add(usersCreated.getCount());
			labels.add(usersCreated.getMonth());
		}
		
		if (!data.isEmpty()) {
			response.setLabelsList(labels);
			response.setSeriesList(series);
			response.setDataListA(data);
			
			response.success = true;
			response.message = "Data Retrieved Successfully";
		} else {
			response.success = false;
			response.message = "No results found";
		}
		return response;
	}

}
