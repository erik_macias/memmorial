package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;

public interface ArtTypeFormatService {
	void save(ArtTypeFormatEntity artFormat );
	void update(ArtTypeFormatEntity artFormat );
	ArtTypeFormatEntity findFormat (ArtTypeFormatEntity id);
	ArtTypeFormatEntity findByName(String name);
	ArtTypeFormatEntity findById(int id);
	List<ArtTypeFormatEntity> findByType(ArtTypeEntity type);
	ArtTypeFormatEntity findByNameType(String name,ArtTypeEntity type);
	List<ArtTypeFormatEntity> findAll();
	
}
