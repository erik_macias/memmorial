package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.AddressDAO;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("addressDAO")
public class AddressDAOImpl extends AbstractDao<AddressEntity> implements AddressDAO {
	
	public List<AddressEntity> findbyUser(UserEntity user) {	
		Criteria crit = getSession().createCriteria(AddressEntity.class);
		crit.add(Restrictions.eq("user", user));
		return crit.list();
	}

	@Override
	public AddressEntity findAddressByTypeAndUser(
			AddressTypeEntity addressType, UserEntity user) {
		Criteria crit = getSession().createCriteria(AddressEntity.class);
		crit.add(Restrictions.eq("user", user));
		crit.add(Restrictions.eq("addressType", addressType));
		return (AddressEntity) crit.uniqueResult();
	}

	@Override
	public AddressEntity findById(int addressId) {
		Criteria crit = getSession().createCriteria(AddressEntity.class);
		crit.add(Restrictions.eq("address_id", addressId));
		return (AddressEntity) crit.uniqueResult();
	}

}
