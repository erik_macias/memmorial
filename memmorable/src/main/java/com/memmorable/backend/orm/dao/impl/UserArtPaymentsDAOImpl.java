package com.memmorable.backend.orm.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.stereotype.Repository;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.UserArtPaymentsDAO;
import com.memmorable.backend.orm.dto.ReportRanges;
import com.memmorable.backend.orm.dto.UserCreatedReportDTO;
import com.memmorable.backend.orm.dto.UserSalesReportDTO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

@Repository("userArtPaymentsDAO")
public class UserArtPaymentsDAOImpl extends AbstractDao<UserArtPaymentsEntity> implements UserArtPaymentsDAO {

	@Override
	public UserArtPaymentsEntity findByPaymentId(String paymentId) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("paymentId", paymentId));
		return (UserArtPaymentsEntity) crit.uniqueResult();
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("user", user));
		return crit.list();
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user, String paymentType) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("user", user));
		crit.add(Restrictions.eq("paymentType", paymentType));
		return crit.list();
	}
	
	@Override
	public UserArtPaymentsEntity findByArt(ArtEntity art, String paymentType) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("art_id", art));
		crit.add(Restrictions.eq("paymentType", paymentType));
		return (UserArtPaymentsEntity) crit.uniqueResult();
	}
	
	@Override
	public UserArtPaymentsEntity findByArtAndBuyer(ArtEntity art, UserEntity user) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("art_id", art));
		crit.add(Restrictions.eq("user", user));
		crit.add(Restrictions.eq("paymentType", MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT));
		return (UserArtPaymentsEntity) crit.uniqueResult();
	}

	@Override
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(String paymentType, int year) {
		return findAllUserArtPaymentsReport(null, paymentType, year);
	}
	
	@Override
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(String paymentType, ReportRanges ranges) {
		return findAllUserArtPaymentsReport(null, paymentType, ranges);
	}
	
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(UserEntity user, String paymentType, ReportRanges ranges) {
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.sum("art.price").as("amount"));
		projList.add(Projections
				.sqlGroupProjection(
						"MONTH(created_date) as monthNo, MONTHNAME(created_date) as month, year(created_date) as year",
						"monthNo, month, year", new String[] { "monthNo", "month", "year" },
						new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER }));

		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class, "payments");
        crit.createAlias("payments.art_id", "art");
        if (user != null && !user.equals("")) {
        	crit.add(Restrictions.eq("payments.user", user));
        }
        
        Date startDate = new Date();
        Date endDate = new Date();
		try {
			startDate =  new SimpleDateFormat("yyyy-MM-dd").parse(ranges.getStartDate());
			endDate = new SimpleDateFormat("yyyy-MM-dd").parse(ranges.getEndDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        crit.add(Restrictions.between("createdDate", startDate, endDate));
		crit.add(Restrictions.eq("payments.paymentType", paymentType));
        crit.setProjection(projList);
        crit.setResultTransformer(Transformers.aliasToBean(UserSalesReportDTO.class));
		
		List<UserSalesReportDTO> results = crit.list();
		
		return results;
	}
	
	@Override
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(UserEntity user, String paymentType, int year) {
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.sum("art.price").as("amount"));
		projList.add(Projections
				.sqlGroupProjection(
						"MONTH(created_date) as monthNo, MONTHNAME(created_date) as month, year(created_date) as year",
						"monthNo, month, year", new String[] { "monthNo", "month", "year" },
						new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER }));

		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class, "payments");
        crit.createAlias("payments.art_id", "art");
        if (user != null && !user.equals("")) {
        	crit.add(Restrictions.eq("payments.user", user));
        }
        crit.add(Restrictions.sqlRestriction("YEAR(created_date)=" + year));
		crit.add(Restrictions.eq("payments.paymentType", paymentType));
        crit.setProjection(projList);
        crit.setResultTransformer(Transformers.aliasToBean(UserSalesReportDTO.class));
		
		List<UserSalesReportDTO> results = crit.list();
		
		return results;
	}

	@Override
	public List<UserArtPaymentsEntity> findUserWithFeePending() {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("paymentId", ""));
		crit.add(Restrictions.eq("paymentType", MemmorableConstants.ART_PAYMENT_TYPE_FEE));
		crit.add(Restrictions.in("paymentMethod", new String[] {
				"",
				MemmorableConstants.PAYMENT_METHOD_TRANSFER,
				MemmorableConstants.PAYMENT_METHOD_DEPOSIT }));
		
		return crit.list();
	}

	@Override
	public List<UserCreatedReportDTO> findUsersCreated(int year) {
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.count("user_id").as("count"));
		projList.add(Projections
				.sqlGroupProjection(
						"MONTH(created_date) as monthNo, MONTHNAME(created_date) as month, year(created_date) as year",
						"monthNo, month, year", new String[] { "monthNo", "month", "year" },
						new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER }));

		Criteria crit = getSession().createCriteria(UserEntity.class, "users");
        crit.add(Restrictions.sqlRestriction("YEAR(created_date)=" + year));
        crit.setProjection(projList);
        crit.setResultTransformer(Transformers.aliasToBean(UserCreatedReportDTO.class));
		
		List<UserCreatedReportDTO> results = crit.list();
		
		return results;
	}

	@Override
	public UserArtPaymentsEntity getArtPaymentsAmountPending(ArtEntity art) {
		return getArtPaymentsPending(art, MemmorableConstants.ART_PAYMENT_TYPE_AMOUNT);
	}
	
	@Override
	public UserArtPaymentsEntity getArtPaymentsFeePending(ArtEntity art) {
		return getArtPaymentsPending(art, MemmorableConstants.ART_PAYMENT_TYPE_FEE);
	}
	
	@Override
	public List<ArtEntity> getArtsWithFeePending(int intervalDays) {
		String hqlSelect = "Select payments.art_id from UserArtPaymentsEntity payments, UserEntity usr "
				+ "where payments.user = usr.user_id AND payments.paymentId = '' "
				+ "AND payments.paymentType = :pymntType AND payments.paymentMethod in (:pymntMethod) "
				+ "AND CURRENT_TIMESTAMP > ADDDATE(payments.createdDate, :days)";
		
		Query selectQuery = getSession().createQuery(hqlSelect);
		selectQuery.setString("pymntType", MemmorableConstants.ART_PAYMENT_TYPE_FEE);
		selectQuery.setParameterList("pymntMethod", new String[] { "",
				MemmorableConstants.PAYMENT_METHOD_TRANSFER,
				MemmorableConstants.PAYMENT_METHOD_DEPOSIT });
		selectQuery.setInteger("days", intervalDays);
		return selectQuery.list();
	}
	
	private UserArtPaymentsEntity getArtPaymentsPending(ArtEntity art, String paymentType) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("art_id", art));
		crit.add(Restrictions.eq("paymentId", ""));
		crit.add(Restrictions.eq("paymentType", paymentType));
		crit.add(Restrictions.in("paymentMethod", new String[] {
				"",
				MemmorableConstants.PAYMENT_METHOD_TRANSFER,
				MemmorableConstants.PAYMENT_METHOD_DEPOSIT }));
		return (UserArtPaymentsEntity) crit.uniqueResult();
	}

	@Override
	public List<UserArtPaymentsEntity> findAllUserPaymentsToSeller(UserEntity buyerUser, UserEntity sellerUser) {
		Criteria crit = getSession().createCriteria(UserArtPaymentsEntity.class);
		crit.add(Restrictions.eq("user", buyerUser));
		crit.createAlias("art_id", "art");
		crit.add(Restrictions.eq("art.user", sellerUser));
		return crit.list();
	}

}
