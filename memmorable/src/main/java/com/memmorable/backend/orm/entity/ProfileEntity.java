package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_Profile")
public class ProfileEntity implements Serializable {
	private static final long serialVersionUID = 3019411557564433428L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "profile_id", nullable = false, unique = true)
	private int profile_int;

	@Column(name = "profile_resume", nullable = true, unique = false)
	private String resume;

	@Column(name = "profile_art_statement", nullable = true, unique = false)
	private String art_statment;

	@Column(name = "profile_picture", nullable = true, unique = false)
	private String profile_picture;
	
	@Column(name="visits_counter", nullable=true)
    private int visitsCounter;

	@OneToOne(targetEntity = UserEntity.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity user;

	public int getProfile_int() {
		return profile_int;
	}

	public void setProfile_int(int profile_int) {
		this.profile_int = profile_int;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getArt_statment() {
		return art_statment;
	}

	public void setArt_statment(String art_statment) {
		this.art_statment = art_statment;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}
	
	public int getVisitsCounter() {
		return visitsCounter;
	}

	public void setVisitsCounter(int visitsCounter) {
		this.visitsCounter = visitsCounter;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}
