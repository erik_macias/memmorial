package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.ArtStatusEntity;

public interface ArtStatusService {
	void save(ArtStatusEntity artStatus);
	ArtStatusEntity findId(ArtStatusEntity id);
	ArtStatusEntity findByName(String name);
	void update(ArtStatusEntity artStatus);
}
