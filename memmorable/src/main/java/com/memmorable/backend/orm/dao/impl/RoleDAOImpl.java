package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.RoleDAO;
import com.memmorable.backend.orm.entity.RoleEntity;
@Repository("roleDAO")
public class RoleDAOImpl extends AbstractDao<RoleEntity> implements RoleDAO {

	public RoleEntity findbyRoleName(String name){	
		Criteria crit = getSession().createCriteria(RoleEntity.class);
		crit.add(Restrictions.eq("role_name", name));
		return (RoleEntity) crit.uniqueResult();
		
	}
}
