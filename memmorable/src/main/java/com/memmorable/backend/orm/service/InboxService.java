package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.InboxEntity;
import com.memmorable.backend.orm.entity.InboxStatusEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface InboxService {
	void save(InboxEntity inbox);
	void update(InboxEntity inbox);
	InboxEntity findbyId(InboxEntity id);
	InboxEntity findbyId(int id);
	List<InboxEntity> findFromUser(UserEntity user);
	List<InboxEntity> findToUser(UserEntity user);
	List<InboxEntity> findByStatusAndUser(UserEntity user, InboxStatusEntity status);
	List<InboxEntity> findAllUnreadByStatusAndUser(UserEntity user, InboxStatusEntity status);
	List<InboxEntity> findByStatusAndFromUser(UserEntity user, InboxStatusEntity status);
}
