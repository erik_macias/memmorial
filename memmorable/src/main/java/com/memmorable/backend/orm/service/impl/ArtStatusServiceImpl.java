package com.memmorable.backend.orm.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtStatusDAO;
import com.memmorable.backend.orm.entity.ArtStatusEntity;
import com.memmorable.backend.orm.service.ArtStatusService;

@Service("artStatusService")
@Transactional
public class ArtStatusServiceImpl implements ArtStatusService {
	
	@Autowired
	private ArtStatusDAO dao;

	@Override
	public ArtStatusEntity findId(ArtStatusEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtStatusEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(ArtStatusEntity artStatus) {
		dao.update(artStatus);
	}

	@Override
	public void save(ArtStatusEntity artStatus) {
		dao.save(artStatus);
	}

}
