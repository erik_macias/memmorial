package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_art_rules")
public class ArtRulesEntity implements Serializable {

	private static final long serialVersionUID = -5168160458848210846L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_rules_id", nullable = false, unique = true)
	private int art_rules_id;

	@Column(name = "art_rules_name", nullable = false, length = 100)
	private String art_rules_name;

	@Column(name = "art_qty", nullable = false)
	private Integer art_qty;
	
	@OneToOne(targetEntity = RoleEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", nullable = false)
	private RoleEntity role_id;

	public int getArt_rules_id() {
		return art_rules_id;
	}

	public void setArt_rules_id(int art_rules_id) {
		this.art_rules_id = art_rules_id;
	}

	public String getArt_rules_name() {
		return art_rules_name;
	}

	public void setArt_rules_name(String art_rules_name) {
		this.art_rules_name = art_rules_name;
	}

	public Integer getArt_qty() {
		return art_qty;
	}

	public void setArt_qty(Integer art_qty) {
		this.art_qty = art_qty;
	}

	public RoleEntity getRole_id() {
		return role_id;
	}

	public void setRole_id(RoleEntity role_id) {
		this.role_id = role_id;
	}


	

}
