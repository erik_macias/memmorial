package com.memmorable.backend.orm.dto;

public class ContactFormDTO {

	private String inputName;
	private String inputEmail;
	private String inputSubject;
	private String inputMessage;
	
	public String getInputName() {
		return inputName;
	}
	public void setInputName(String inputName) {
		this.inputName = inputName;
	}
	public String getInputEmail() {
		return inputEmail;
	}
	public void setInputEmail(String inputEmail) {
		this.inputEmail = inputEmail;
	}
	public String getInputSubject() {
		return inputSubject;
	}
	public void setInputSubject(String inputSubject) {
		this.inputSubject = inputSubject;
	}
	public String getInputMessage() {
		return inputMessage;
	}
	public void setInputMessage(String inputMessage) {
		this.inputMessage = inputMessage;
	}
	
}
