package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.UserEntity;


public interface UserDAO extends InterfaceDAO<UserEntity>{
	public UserEntity findByName(String name);
	public void FailedLoginAttemp(String name);
	public void deleteeByName(String name);
	
		
}
