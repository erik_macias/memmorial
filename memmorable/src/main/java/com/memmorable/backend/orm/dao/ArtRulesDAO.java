package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;


public interface ArtRulesDAO extends InterfaceDAO<ArtRulesEntity>{
	public ArtRulesEntity findByID(int  id);
	public ArtRulesEntity findByName(String name);
	public ArtRulesEntity findByRole(RoleEntity role);
}
