package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtRulesDAO;
import com.memmorable.backend.orm.entity.ArtRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
@Repository("artRulesDAO")
public class ArtRulesDAOImpl extends AbstractDao<ArtRulesEntity> implements ArtRulesDAO {

		
	@Override
	public ArtRulesEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtRulesEntity.class);
		crit.add(Restrictions.eq("art_rules_name", name));
		return (ArtRulesEntity) crit.uniqueResult();
	}

	@Override
	public ArtRulesEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(ArtRulesEntity.class);
		crit.add(Restrictions.eq("art_rules_id", id));
		return (ArtRulesEntity) crit.uniqueResult();
	}

	public ArtRulesEntity findByRole(RoleEntity role){
		Criteria crit = getSession().createCriteria(ArtRulesEntity.class);
		crit.add(Restrictions.eq("role_id", role));
		return (ArtRulesEntity) crit.uniqueResult();
	}
}
