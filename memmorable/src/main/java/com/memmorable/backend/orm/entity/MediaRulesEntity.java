package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Table_media_rules")
public class MediaRulesEntity implements Serializable {
	
	private static final long serialVersionUID = -5710328194239536399L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "media_rules_id", nullable = false, unique = true)
	private int media_rules_id;

	@Column(name = "media_rules_name", nullable = false, length = 100)
	private String media_rules_name;

	@Column(name = "media_pdf_qty", nullable = false)
	private Integer media_pdf_qty;

	@Column(name = "media_image_qty", nullable = false)
	private Integer media_image_qty;

	@Column(name = "media_audio_qty", nullable = false)
	private Integer media_audio_qty;
	
	@Column(name = "media_video_qty", nullable = false)
	private Integer media_video_qty;
	
	
	@ManyToOne(targetEntity = ArtTypeEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_type_id", nullable = false)
	private ArtTypeEntity art_type_id;
	
	@ManyToOne(targetEntity = RoleEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", nullable = false)
	private RoleEntity role_id;

	public int getMedia_rules_id() {
		return media_rules_id;
	}

	public void setMedia_rules_id(int media_rules_id) {
		this.media_rules_id = media_rules_id;
	}

	public String getMedia_rules_name() {
		return media_rules_name;
	}

	public void setMedia_rules_name(String media_rules_name) {
		this.media_rules_name = media_rules_name;
	}

	public Integer getMedia_pdf_qty() {
		return media_pdf_qty;
	}

	public void setMedia_pdf_qty(Integer media_pdf_qty) {
		this.media_pdf_qty = media_pdf_qty;
	}

	public Integer getMedia_image_qty() {
		return media_image_qty;
	}

	public void setMedia_image_qty(Integer media_image_qty) {
		this.media_image_qty = media_image_qty;
	}

	public Integer getMedia_audio_qty() {
		return media_audio_qty;
	}

	public void setMedia_audio_qty(Integer media_audio_qty) {
		this.media_audio_qty = media_audio_qty;
	}

	public Integer getMedia_video_qty() {
		return media_video_qty;
	}

	public void setMedia_video_qty(Integer media_video_qty) {
		this.media_video_qty = media_video_qty;
	}


	public ArtTypeEntity getArt_type_id() {
		return art_type_id;
	}

	public void setArt_type_id(ArtTypeEntity art_type_id) {
		this.art_type_id = art_type_id;
	}

	public RoleEntity getRole_id() {
		return role_id;
	}

	public void setRole_id(RoleEntity role_id) {
		this.role_id = role_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
