package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;

public interface UserPaymentsService {
		void save(UserPaymentsEntity userPayments);
		void update(UserPaymentsEntity userPayments);
		List<UserPaymentsEntity> findAllUserPayments(UserEntity user);
		UserPaymentsEntity findByAgreementId(String agreementId);
		UserPaymentsEntity findByRole(RoleEntity role);
		UserPaymentsEntity findByUserAndRole(UserEntity user, RoleEntity role);
}
