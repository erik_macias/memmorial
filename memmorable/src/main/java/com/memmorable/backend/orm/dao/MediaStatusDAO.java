package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.MediaStatusEntity;


public interface MediaStatusDAO extends InterfaceDAO<MediaStatusEntity>{
	public MediaStatusEntity findByID(int  id);
	public MediaStatusEntity findByName(String name);
}
