package com.memmorable.backend.orm.dto;

import java.util.List;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableAppConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.AppSettingsEntity;
import com.memmorable.backend.orm.service.AppSettingsService;
import com.memmorable.backend.services.rest.responses.GenericResponse;

public class AppSettingsAdministrationDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();
	AppSettingsService appSettingsService = (AppSettingsService) context.getBean("appSettingsService");

	public List<AppSettingsEntity> getKeyValueList() {
		return appSettingsService.findAll();
	}
	
	public AppSettingsEntity getRecaptchaClientKey() {
		return getKeyValue(MemmorableAppConstants.GOOGLE_RECAPTCHA_CLIENT_KEY_NAME);
	}
	
	public GenericResponse updateKeyValue(AppSettingsEntity appSetting) {
		AppSettingsEntity appSettingUpd = getKeyValue(appSetting.getApp_setting_name());
		GenericResponse response = new GenericResponse();
		
		if (appSettingUpd != null) {
			appSettingUpd.setApp_setting_value(appSetting.getApp_setting_value());
			appSettingsService.update(appSettingUpd);
			
			response.success = true;
			response.message = "Key / Value has been updated successfully.";
		} else {
			response.success = false;
			response.message = "Key / Value not found to be updated.";
		}
		
		return response;
	}

	public GenericResponse createKeyValue(AppSettingsEntity appSetting) {
		GenericResponse response = new GenericResponse();

		if (getKeyValue(appSetting.getApp_setting_name()) != null) {
			response.message = "Key / Value already exist in the database.";
			response.success = false;
		} else {
			AppSettingsEntity appSettingsNew = new AppSettingsEntity();
			appSettingsNew.setApp_setting_name(appSetting.getApp_setting_name());
			appSettingsNew.setApp_setting_value(appSetting.getApp_setting_value());
			appSettingsService.save(appSettingsNew);
			response.success = true;
			response.message = "Key / Value  Successfully created.";
		}
		
		return response;
	}
	
	public GenericResponse deleteKeyValue(String key) {
		AppSettingsEntity appSettingsDel = getKeyValue(key);
		GenericResponse response = new GenericResponse();

		if (appSettingsDel != null) {
			appSettingsService.delete(appSettingsDel);
			response.success = true;
			response.message = "Key / Value has been deleted successfully.";
		} else {
			response.success = false;
			response.message = "Key / Value not found to be deleted.";
		}
		
		return response;
	}
	
	public AppSettingsEntity getKeyValue(String key) {
		return appSettingsService.findByAppSettingName(key);
	}

	public GenericResponse reBuildLuceneIndex() {
		return appSettingsService.reBuildLuceneIndex();
	}

}
