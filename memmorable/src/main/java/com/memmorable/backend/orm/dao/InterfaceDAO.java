package com.memmorable.backend.orm.dao;

import java.io.Serializable;
import java.util.List;

public interface InterfaceDAO<T> {
	public void persist(Object entity);

	public void delete(Object entity);

	public void save(T entity);

	public T find(final Serializable id);

	public T update(final T t);

	public List<T> findAll(Class<T> classType);
}
