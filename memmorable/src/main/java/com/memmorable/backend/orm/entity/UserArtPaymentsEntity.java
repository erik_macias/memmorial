package com.memmorable.backend.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Table_User_Art_Payments")
public class UserArtPaymentsEntity implements Serializable {
	
	private static final long serialVersionUID = -3419551238828455990L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_art_payment_id", nullable = false, unique = true)
	private int userArtPaymentId;

	@Column(name = "payment_id", nullable = false, length = 100)
	private String paymentId;
	
	@Column(name = "payment_type", nullable = false, length = 45)
	private String paymentType;
	
	@Column(name = "payment_method", nullable = false, length = 45)
	private String paymentMethod;
	
	@Column(name = "created_date", nullable = false, columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@ManyToOne(targetEntity = ArtEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_id")
	private ArtEntity art_id;

	public int getUserArtPaymentId() {
		return userArtPaymentId;
	}

	public void setUserArtPaymentId(int userArtPaymentId) {
		this.userArtPaymentId = userArtPaymentId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public ArtEntity getArt_id() {
		return art_id;
	}

	public void setArt_id(ArtEntity art_id) {
		this.art_id = art_id;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
}