package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface AddressService {
	void save(AddressEntity address);
	void update(AddressEntity address);
	AddressEntity findAddress(AddressEntity id);
	List<AddressEntity> findByUser(UserEntity user);
	AddressEntity findAddressByTypeAndUser(AddressTypeEntity addressType, UserEntity user);
	AddressEntity findById(int addressId);
	void delete(AddressEntity address);
}
