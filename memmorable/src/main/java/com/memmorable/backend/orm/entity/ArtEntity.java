package com.memmorable.backend.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FullTextFilterDef;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import com.memmorable.backend.orm.entity.filter.ArtStatusFilter;
import com.memmorable.backend.orm.entity.interceptor.IndexWhenPublishedInterceptor;

@Entity
@Indexed(interceptor = IndexWhenPublishedInterceptor.class)
@AnalyzerDef(name = "customanalyzer", 
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), 
	filters = {
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = { 
			@Parameter(name = "language", value = "English") })
		})
@FullTextFilterDef(name = "artStatus", impl = ArtStatusFilter.class)
@Table(name = "Table_art")
@FilterDef(name = "activeArts")
@Filter(name = "activeArts", condition = "deleted = 'false'")
public class ArtEntity implements Serializable {
	
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_id", nullable = false, unique = true)
	private int art_id;

	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition = "customanalyzer")
	@Column(name = "art_name", nullable = false, length = 100)
	private String art_name;

	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition = "customanalyzer")
	@Column(name = "art_description", nullable = true, length = 100)
	private String art_description;

	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtSubTypeEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_subtype_id")
	private ArtSubTypeEntity art_SubType;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtTypeServiceEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_service_id")
	private ArtTypeServiceEntity art_Service;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtTypeGenreEntity.class, optional=true, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_genre_id" )
	private ArtTypeGenreEntity art_Genre;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtTypeFormatEntity.class, optional=true, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_format_id" )
	private ArtTypeFormatEntity art_Format;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@ManyToOne(targetEntity = ArtTagEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_tag_id")
	private ArtTagEntity art_tag;
	
	@IndexedEmbedded
	@ManyToOne(targetEntity = ArtStatusEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_status_id")
	private ArtStatusEntity art_status;
	
	@ManyToOne(targetEntity = CurrencyEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_id")
	private CurrencyEntity currency;
	
	@Column(name = "create_date", nullable = false, length = 100)
	private Date created_date;
	
	@Column(name = "last_updated", nullable = false, length = 100)
	private Date last_updated_date;
	
	@Column(name = "price", nullable = false, length = 100)
	private Double price;

	@Column(name="deleted", nullable=true)
    private boolean deleted;
	
	@Column(name="visits_counter", nullable=true)
    private int visitsCounter;
	
	public ArtTypeServiceEntity getArt_Service() {
		return art_Service;
	}

	public void setArt_Service(ArtTypeServiceEntity art_Service) {
		this.art_Service = art_Service;
	}

	public ArtTypeGenreEntity getArt_Genre() {
		return art_Genre;
	}

	public void setArt_Genre(ArtTypeGenreEntity art_Genre) {
		this.art_Genre = art_Genre;
	}

	public ArtTypeFormatEntity getArt_Format() {
		return art_Format;
	}

	public void setArt_Format(ArtTypeFormatEntity art_Format) {
		this.art_Format = art_Format;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getArt_id() {
		return art_id;
	}

	public void setArt_id(int art_id) {
		this.art_id = art_id;
	}

	public String getArt_name() {
		return art_name;
	}

	public void setArt_name(String art_name) {
		this.art_name = art_name;
	}

	public String getArt_description() {
		return art_description;
	}

	public void setArt_description(String art_description) {
		this.art_description = art_description;
	}

	public ArtSubTypeEntity getArt_SubType() {
		return art_SubType;
	}

	public void setArt_SubType(ArtSubTypeEntity art_SubType) {
		this.art_SubType = art_SubType;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public ArtTagEntity getArt_tag() {
		return art_tag;
	}

	public void setArt_tag(ArtTagEntity art_tag) {
		this.art_tag = art_tag;
	}

	public ArtStatusEntity getArt_status() {
		return art_status;
	}

	public void setArt_status(ArtStatusEntity art_status) {
		this.art_status = art_status;
	}

	public CurrencyEntity getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyEntity currency) {
		this.currency = currency;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getLast_updated_date() {
		return last_updated_date;
	}

	public void setLast_updated_date(Date last_updated_date) {
		this.last_updated_date = last_updated_date;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getVisitsCounter() {
		return visitsCounter;
	}

	public void setVisitsCounter(int visitsCounter) {
		this.visitsCounter = visitsCounter;
	}
	
}