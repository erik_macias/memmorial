package com.memmorable.backend.orm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.memmorable.backend.orm")
public class AppConfig {

}
