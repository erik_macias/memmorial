package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTypeFeeDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

@Repository("artTypeFeeDAO")
public class ArtTypeFeeDAOImpl extends AbstractDao<ArtTypeFeeEntity> implements
		ArtTypeFeeDAO {

	@Override
	public ArtTypeFeeEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtTypeFeeEntity.class);
		crit.add(Restrictions.eq("art_type_fee_id", id));
		return (ArtTypeFeeEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtTypeFeeEntity> findByRole(RoleEntity role) {
		Criteria crit = getSession().createCriteria(ArtTypeFeeEntity.class);
		crit.add(Restrictions.eq("role", role));
		return crit.list();
	}

	@Override
	public List<ArtTypeFeeEntity> findByArtType(ArtTypeEntity artType) {
		Criteria crit = getSession().createCriteria(ArtTypeFeeEntity.class);
		crit.add(Restrictions.eq("art_type", artType));
		return crit.list();
	}

	@Override
	public ArtTypeFeeEntity findByArtTypeAndRole(ArtTypeEntity artType,
			RoleEntity role) {
		Criteria crit = getSession().createCriteria(ArtTypeFeeEntity.class);
		crit.add(Restrictions.eq("art_type", artType));
		crit.add(Restrictions.eq("role", role));
		return (ArtTypeFeeEntity) crit.uniqueResult();
	}

}
