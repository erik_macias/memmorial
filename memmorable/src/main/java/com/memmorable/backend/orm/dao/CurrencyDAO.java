package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.CurrencyEntity;


public interface CurrencyDAO extends InterfaceDAO<CurrencyEntity>{
	public CurrencyEntity findByID(int  id);
	public CurrencyEntity findByName(String name);
}
