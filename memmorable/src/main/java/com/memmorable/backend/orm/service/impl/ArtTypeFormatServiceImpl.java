package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTypeFormatDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFormatEntity;
import com.memmorable.backend.orm.service.ArtTypeFormatService;


@Service("artTypeFormatService")
@Transactional
public class ArtTypeFormatServiceImpl implements ArtTypeFormatService {
	
	@Autowired
	private ArtTypeFormatDAO dao;

	@Override
	public void save(ArtTypeFormatEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtTypeFormatEntity findFormat(ArtTypeFormatEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtTypeFormatEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<ArtTypeFormatEntity> findByType(ArtTypeEntity type) {
		return dao.findByType(type);
	}
	
	@Override
	public ArtTypeFormatEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void update(ArtTypeFormatEntity artTag) {
		dao.update(artTag);
		
	}

	@Override
	public ArtTypeFormatEntity findByNameType(String name, ArtTypeEntity type) {
	return dao.findByNameType(name, type);
		
	}

	@Override
	public List<ArtTypeFormatEntity> findAll() {
		return dao.findAll(ArtTypeFormatEntity.class);
	}

}
