package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtRatingDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtRatingEntity;
import com.memmorable.backend.orm.entity.UserEntity;
@Repository("artRatingDAO")
public class ArtRatingDAOImpl extends AbstractDao<ArtRatingEntity> implements ArtRatingDAO {
		
	@Override
	public ArtRatingEntity findById(int  id) {
		Criteria crit = getSession().createCriteria(ArtRatingEntity.class);
		crit.add(Restrictions.eq("rating_id", id));
		return (ArtRatingEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtRatingEntity> findByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(ArtRatingEntity.class);
		crit.add(Restrictions.eq("art", art));
		return crit.list();
	}

	@Override
	public List<ArtRatingEntity> findByUser(UserEntity user) {
		Criteria crit = getSession().createCriteria(ArtRatingEntity.class);
		crit.add(Restrictions.eq("user", user));
		return crit.list();
	}
	
	@Override
	public int findRateByArt(ArtEntity art) {
		Criteria crit = getSession().createCriteria(ArtRatingEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.setProjection(Projections.avg("rating_rate"));
		
		List results = crit.list();
		Double avgRate = (Double) results.get(0);
		int roundedRate = 0;
		if (avgRate != null) {
			roundedRate = (int) Math.ceil(avgRate);
		}
		return roundedRate;
	}

	@Override
	public ArtRatingEntity findRateByArtAndUser(ArtEntity art, UserEntity user) {
		Criteria crit = getSession().createCriteria(ArtRatingEntity.class);
		crit.add(Restrictions.eq("art", art));
		crit.add(Restrictions.eq("user", user));
		return (ArtRatingEntity) crit.uniqueResult();
	}

}
