package com.memmorable.backend.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Table_art_comments")
public class ArtCommentsEntity implements Serializable {
	private static final long serialVersionUID = -4867496259922479686L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "art_comment_id", nullable = false, unique = true)
	private int art_comment_id;

	@Column(name = "comment", nullable = false, length = 500)
	private String comment;
	
	@Column(name = "status", nullable = false)
	private boolean status;
	
	@Column(name = "create_date", nullable = false, columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date create_date;
	
	@ManyToOne(targetEntity = UserEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private UserEntity user;
	
	@ManyToOne(targetEntity = ArtEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "art_id")
	private ArtEntity art;

	public int getArt_comment_id() {
		return art_comment_id;
	}

	public void setArt_comment_id(int art_comment_id) {
		this.art_comment_id = art_comment_id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public ArtEntity getArt() {
		return art;
	}

	public void setArt(ArtEntity art) {
		this.art = art;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
