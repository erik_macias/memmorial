package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

public interface ArtRulesService {
	void save(ArtRulesEntity country);
	ArtRulesEntity findById(int id);
	ArtRulesEntity findByName(String name);
	ArtRulesEntity findByRole(RoleEntity role);
	List<ArtRulesEntity> findAll();
	void update(ArtRulesEntity rule);
}
