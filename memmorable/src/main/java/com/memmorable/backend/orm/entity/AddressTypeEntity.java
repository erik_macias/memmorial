package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_Address_Type")
public class AddressTypeEntity implements Serializable {
	private static final long serialVersionUID = -1226281151020369010L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "address_type_id", nullable = false, unique = true)
	private int address_type_id;

	@Column(name = "Address_type_name", nullable = false, unique = true, length = 100)
	private String address_type_name;

	public int getAddress_type_id() {
		return address_type_id;
	}

	public void setAddress_type_id(int address_type_id) {
		this.address_type_id = address_type_id;
	}

	public String getAddress_type_name() {
		return address_type_name;
	}

	public void setAddress_type_name(String address_type_name) {
		this.address_type_name = address_type_name;
	}

}
