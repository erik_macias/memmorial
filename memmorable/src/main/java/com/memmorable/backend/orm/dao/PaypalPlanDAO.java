package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;


public interface PaypalPlanDAO extends InterfaceDAO<PaypalPlanEntity>{
	public PaypalPlanEntity findByPlanID(String  planId);
	public PaypalPlanEntity findPlanIdByRole(RoleEntity role);
}
