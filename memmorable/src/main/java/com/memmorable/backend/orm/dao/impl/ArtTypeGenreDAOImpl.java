package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtTypeGenreDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;

@Repository("artTypeGenreDAO")
public class ArtTypeGenreDAOImpl extends AbstractDao<ArtTypeGenreEntity> implements ArtTypeGenreDAO {

	@Override
	public ArtTypeGenreEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtTypeGenreEntity.class);
		crit.add(Restrictions.eq("art_genre_name", name));
		return (ArtTypeGenreEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtTypeGenreEntity> findByType(ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeGenreEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		return crit.list();
	}

	@Override
	public ArtTypeGenreEntity findByNameType(String name, ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtTypeGenreEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		crit.add(Restrictions.eq("art_genre_name", name ));
		return (ArtTypeGenreEntity) crit.uniqueResult();
	}

	@Override
	public ArtTypeGenreEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtTypeGenreEntity.class);
		crit.add(Restrictions.eq("art_subtype_id", id));
		return (ArtTypeGenreEntity) crit.uniqueResult();
	}

}
