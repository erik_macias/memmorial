package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.CurrencyDAO;
import com.memmorable.backend.orm.entity.CurrencyEntity;
import com.memmorable.backend.orm.service.CurrencyService;

@Service("currencyService")
@Transactional
public class CurrencyServiceImpl implements CurrencyService {
	
	@Autowired
	private CurrencyDAO dao;

	@Override
	public CurrencyEntity findById(CurrencyEntity id) {
		return dao.find(id);
	}
	
	@Override
	public CurrencyEntity findById(int id) {
		return dao.findByID(id);
	}

	@Override
	public CurrencyEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void update(CurrencyEntity currency) {
		dao.update(currency);
	}

	@Override
	public void save(CurrencyEntity currency) {
		dao.save(currency);
	}

	@Override
	public List<CurrencyEntity> findAll() {
		return dao.findAll(CurrencyEntity.class);
	}

}
