package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtCommentsEntity;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface ArtCommentsDAO extends InterfaceDAO<ArtCommentsEntity>{
	public ArtCommentsEntity findById(int  id);
	public List<ArtCommentsEntity> findByArt(ArtEntity art);
	public List<ArtCommentsEntity> findByUser(UserEntity user);
}
