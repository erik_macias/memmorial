package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_States")
public class CountryStatesEntity implements Serializable {
	private static final long serialVersionUID = -1226281151020369010L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "state_id", nullable = false, unique = true)
	private int stateId;

	@Column(name = "state_name", nullable = false, unique = true, length = 100)
	private String stateName;
	
	@OneToOne(targetEntity = CountryEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id", nullable = false)
	private CountryEntity country;

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public CountryEntity getCountry() {
		return country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}
	
}
