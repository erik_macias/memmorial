package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.UserStatusDAO;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.UserStatusService;


@Service("userStatusService")
@Transactional
public class UserStatusServiceImpl implements UserStatusService {
	
	@Autowired
	private UserStatusDAO dao;

	@Override
	public void save(UserStatusEntity status) {
		dao.save(status);
	}

	@Override
	public UserStatusEntity findUserStatus(UserStatusEntity status) {
		UserStatusEntity stat = dao.find(status);
		return stat;
	}

	@Override
	public UserStatusEntity findByfindbyStatusType(String name) {
		UserStatusEntity userStatus = dao.findbyStatusType(name);
		return userStatus;
	}

	@Override
	public void update(UserStatusEntity status) {
		dao.update(status);
		
	}

	@Override
	public List<UserStatusEntity> findAll() {
		return dao.findAll(UserStatusEntity.class);
	}
	


}
