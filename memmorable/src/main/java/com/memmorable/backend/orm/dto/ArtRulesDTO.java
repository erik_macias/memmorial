package com.memmorable.backend.orm.dto;

import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtRulesEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.MediaRulesEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.ArtRulesService;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.ArtTypeService;
import com.memmorable.backend.orm.service.MediaRulesService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.services.rest.request.CurrentUser;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;

public class ArtRulesDTO {
	
	AbstractApplicationContext context = AppContext.getInstance();

	MediaRulesService mediaRulesService = (MediaRulesService) context.getBean("mediaRulesService");
	ArtRulesService artRulesService = (ArtRulesService) context.getBean("artRulesService");
	
	ArtTypeService artTypeService  = (ArtTypeService) context.getBean("artTypeService");
	UserService userService  = (UserService) context.getBean("userService");
	ArtService artService = (ArtService) context.getBean("artService");

	public Response getArtRulesByUser(CurrentUser currentUser) {
		UserEntity userEnt = userService.findByName(currentUser.username);
		if (userEnt != null) {
			ArtRulesEntity rule = artRulesService.findByRole(userEnt.getRole());
					return Response.ok(rule).build();
		} else {
				return Response.noContent().build();
		}
	}
	
	public Response getMediaRulesByUserAndType(CurrentUser currentUser, String artType) {
		UserEntity userEnt = userService.findByName(currentUser.username);
		ArtTypeEntity typeEnt = artTypeService.findByName(artType);
		
		if (userEnt != null && typeEnt != null) {
			MediaRulesEntity rule = mediaRulesService.findByRoleAndtype(typeEnt, userEnt.getRole());
					return Response.ok(rule).build();
		} else {
				return Response.noContent().build();
		}
	}
	
	public Response isUploadAvailable(CurrentUser currentUser) {
		UserEntity userEnt = userService.findByName(currentUser.username);
		if (userEnt != null) {
			SearchResultEntityList<ArtEntity> artList = artService.findByUser(userEnt);
			List<ArtEntity> arts = artList.getEntityList();

			if (arts != null && arts.size() == 0) {
				ArtRulesEntity rule = artRulesService.findByRole(userEnt.getRole());
				if (rule.getArt_qty() > arts.size()) {
					return Response.ok(Boolean.TRUE).build();
				} else {
					return Response.ok(Boolean.FALSE).build();
				}
			} else {
				return Response.ok(Boolean.TRUE).build();
			}
		} else {
			return Response.noContent().build();
		}

	}
	
}
