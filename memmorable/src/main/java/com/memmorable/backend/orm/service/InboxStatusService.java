package com.memmorable.backend.orm.service;

import com.memmorable.backend.orm.entity.InboxStatusEntity;

public interface InboxStatusService {
	void save(InboxStatusEntity inboxStatus);
	InboxStatusEntity findId(InboxStatusEntity id);
	InboxStatusEntity findByName(String name);
	void update(InboxStatusEntity inboxStatus);
}
