package com.memmorable.backend.orm.configuration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class AppContext {
	
	public static AbstractApplicationContext context = null;
	
	protected AppContext() {
	}

	public static AbstractApplicationContext getInstance() {
		if (context == null) {
			context = new AnnotationConfigApplicationContext( AppConfig.class);;
		}
		return context;
	}
}
