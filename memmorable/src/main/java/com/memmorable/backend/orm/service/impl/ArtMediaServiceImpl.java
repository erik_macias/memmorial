package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtMediaDAO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtMediaEntity;
import com.memmorable.backend.orm.service.ArtMediaService;

@Service("artMediaService")
@Transactional
public class ArtMediaServiceImpl implements ArtMediaService {

	@Autowired
	private ArtMediaDAO dao;

	@Override
	public void save(ArtMediaEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtMediaEntity findMedia(ArtMediaEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtMediaEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<ArtMediaEntity> findByArt(ArtEntity art) {
		return dao.findByArt(art);
	}

	@Override
	public void update(ArtMediaEntity artMedia) {
		dao.update(artMedia);
	}

	@Override
	public ArtMediaEntity getDefaultMediadByArt(ArtEntity art) {
		return dao.getDefaultMediadByArt(art);
	}
	
	@Override
	public ArtMediaEntity getDownloadableMediadByArt(ArtEntity art) {
		return dao.getDownloadableMediadByArt(art);
	}

	@Override
	public void deleteMedia(int id) {
		dao.deleteMedia(id);
		
	}

	@Override
	public ArtMediaEntity findMediaById(int id) {
		return dao.findById(id);
	}

}
