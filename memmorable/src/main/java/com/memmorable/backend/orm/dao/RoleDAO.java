package com.memmorable.backend.orm.dao;

import javax.transaction.Transactional;

import com.memmorable.backend.orm.entity.RoleEntity;

@Transactional
public interface RoleDAO extends InterfaceDAO<RoleEntity>{
	public RoleEntity findbyRoleName(String name);

}
