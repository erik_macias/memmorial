package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;

public interface ArtSubTypeService {
	void save(ArtSubTypeEntity artSubType);
	void update(ArtSubTypeEntity artSubType);
	ArtSubTypeEntity findSubType(ArtSubTypeEntity id);
	ArtSubTypeEntity findByName(String name);
	ArtSubTypeEntity findById(int id);
	List<ArtSubTypeEntity> findByType(ArtTypeEntity type);
	ArtSubTypeEntity findByNameType(String name,ArtTypeEntity type);
	List<ArtSubTypeEntity> findAll();
	
}
