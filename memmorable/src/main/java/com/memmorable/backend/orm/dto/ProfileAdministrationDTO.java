package com.memmorable.backend.orm.dto;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.ProfileEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.service.AddressService;
import com.memmorable.backend.orm.service.AddressTypeService;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.ProfileService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.services.rest.request.GetProfileRequest;
import com.memmorable.backend.services.rest.request.ProfileRequest;
import com.memmorable.backend.services.rest.responses.GenericResponse;
import com.memmorable.backend.services.rest.responses.ProfileReponse;
import com.memmorable.backend.services.rest.responses.RoleResponse;

public class ProfileAdministrationDTO {
	AbstractApplicationContext context = AppContext.getInstance();

	UserService userService = (UserService) context.getBean("userService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	RoleService roleService = (RoleService) context.getBean("roleService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	ProfileService profileService = (ProfileService) context.getBean("profileService");
	AddressService addressService = (AddressService) context.getBean("addressService");
	AddressTypeService addressTypeService = (AddressTypeService) context.getBean("addressTypeService");

	public ProfileReponse getProfileInformation(GetProfileRequest request) {
		ProfileReponse response = new ProfileReponse();
		UserEntity userEnt = userService.findByName(request.getUser_name());
		
		if (userEnt != null) {
			if (request.getCurrrentUser() != null && !request.getUser_name().equalsIgnoreCase(request.getCurrrentUser())) {
				updateVisitCounter(userEnt);
			}
			
			ProfileEntity profile = profileService.findByUser(userEnt);
			PersonalEntity personal = personalService.findByUser(userEnt);
			AddressTypeEntity addressType = addressTypeService.findByName(MemmorableConstants.USER_ADDRESS_DEFAULT);
			AddressEntity address = addressService.findAddressByTypeAndUser(addressType, userEnt);
			
			response.setPersonal_l_name(personal.getPersonal_l_name());
			response.setPersonal_name(personal.getPersonal_name());
			response.setPersonal_gender(personal.getPersonal_gender());
			response.setProfile_art_statement(profile.getArt_statment());
			response.setProfile_picture(profile.getProfile_picture());
			response.setProfile_resume(profile.getResume());
			
			if (userEnt.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_FREE)) {
				response.setMembership(userEnt.getRole().getRole_text_name());
			} else if (userEnt.getRole().getRole_name().equals(MemmorableConstants.USER_ROLE_CERTIFIED)) {
				response.setMembership("Certified (" + userEnt.getRole().getRole_text_name() + ")");
			} else {
				response.setMembership("Preferred (" + userEnt.getRole().getRole_text_name() + ")");
			}
			
			response.setUserCertified(userEnt.isCertified());
			response.setVisitsCounter(profile.getVisitsCounter());

			if (address != null) {
				response.setLocation(getUserDefaultLocation(address));
				response.setPhone(address.getAddress_phone());
				response.setMobile(address.getAddress_mobile());
				response.setEmail(personal.getPersonal_email());
			}
			
			response.success = true;
			response.message = "Succesfully retriving profile information";
			return response;
		} else {
			response.success = false;
			response.message = "User does not exist";
			return response;
		}
	}
	
	public RoleResponse getMembershipInformation(GetProfileRequest request) {
		RoleResponse response = new RoleResponse();
		UserEntity userEnt = userService.findByName(request.getUser_name());
		
		if (userEnt != null) {
			response.setMembership(userEnt.getRole().getRole_text_name());
			response.success = true;
			response.message = "Succesfully retriving membership information";
			return response;
		} else {
			response.success = false;
			response.message = "User does not exist";
			return response;
		}
	}
	
	private String getUserDefaultLocation(AddressEntity address) {
		
		StringBuffer sb = new StringBuffer();
		sb.append(address.getCity().getCityName()).append(", ")
				.append(address.getCity().getCountryState().getStateName()).append(", ")
				.append(address.getCity().getCountryState().getCountry().getCountry_name())
				.append(" (").append(address.getCity().getCountryState().getCountry().getCountry_code())
				.append(")");
		
		return sb.toString();
	}

	public GenericResponse UpdateProfileInformation(ProfileRequest request) {
		GenericResponse response = new GenericResponse();
		UserEntity userEnt = userService.findByName(request.user.username);
		ProfileEntity profile = profileService.findByUser(userEnt);
		profile.setArt_statment(request.profile_art_statement);
		profile.setResume(request.profile_resume);
		profileService.update(profile);
		response.success = true;
		response.message = "successfully updated profile";
		return response;
	}
	
	public GenericResponse updateProfilePicture(String userName, String filePAth) {
		GenericResponse response = new GenericResponse();
		UserEntity userEnt = userService.findByName(userName);
		ProfileEntity profile = profileService.findByUser(userEnt);
		profile.setProfile_picture(filePAth);
		profileService.update(profile);
		response.success = true;
		response.message = "successfully updated profile";
		return response;
	}
	
	private void updateVisitCounter(UserEntity user) {
		ProfileEntity profile = profileService.findByUser(user);
		int currentVisitCounter = profile.getVisitsCounter();
		currentVisitCounter += 1;
		profile.setVisitsCounter(currentVisitCounter);
		profileService.update(profile);
	}

	public Boolean isUserProfilePending(String currentUser) {
		UserEntity user = userService.findByName(currentUser);
		Boolean isprofilePending = Boolean.FALSE;
		
		if (user != null) {
			//PersonalEntity personal = personalService.findByUser(user);
			AddressTypeEntity addressType = addressTypeService.findByName(MemmorableConstants.USER_ADDRESS_DEFAULT);
			AddressEntity address = addressService.findAddressByTypeAndUser(addressType, user);
			
			/*if (personal.getPersonal_l_name() != null && !personal.getPersonal_l_name().equals("") ||
					personal.getPersonal_name() != null && !personal.getPersonal_name().equals("")) {
				isprofilePending =  Boolean.FALSE;
			} else {
				isprofilePending =  Boolean.TRUE;
			}*/
			if (address != null) {
				/*String addressDefault = getUserDefaultLocation(address);
				if (address.getAddress_phone() != null && !address.getAddress_phone().equals("") ||
						addressDefault != null && !addressDefault.equals("") ||
						address.getAddress_mobile() != null && !address.getAddress_mobile().equals("") ||
						address.getAddress_line1() != null && !address.getAddress_line1().equals("")) {
					isprofilePending =  Boolean.FALSE;
				}*/
				if (address.getAddress_phone() != null && !address.getAddress_phone().equals("") ||
						address.getAddress_mobile() != null && !address.getAddress_mobile().equals("")) {
					isprofilePending =  Boolean.FALSE;
				} else {
					isprofilePending =  Boolean.TRUE;
				}
			} else {
				isprofilePending =  Boolean.TRUE;
			}
		} else {
			isprofilePending =  Boolean.TRUE;
		}
		
		return isprofilePending;
	}

}
