package com.memmorable.backend.orm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Catalog_Country")
public class CountryEntity implements Serializable {
	private static final long serialVersionUID = -1226281151020369010L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "country_id", nullable = false, unique = true)
	private int country_id;

	@Column(name = "country_name", nullable = false, unique = true, length = 100)
	private String country_name;
	
	@Column(name = "country_code", nullable = false, length = 45)
	private String country_code;
	
	@Column(name = "phone_code", nullable = false, unique = false)
	private int phone_code;

	public int getCountry_id() {
		return country_id;
	}

	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public int getPhone_code() {
		return phone_code;
	}

	public void setPhone_code(int phone_code) {
		this.phone_code = phone_code;
	}
	
}
