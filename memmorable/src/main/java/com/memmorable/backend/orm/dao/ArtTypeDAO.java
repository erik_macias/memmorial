package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtTypeEntity;

public interface ArtTypeDAO extends InterfaceDAO<ArtTypeEntity> {
	public ArtTypeEntity findByName(String name);
	public ArtTypeEntity findById(int id);
}
