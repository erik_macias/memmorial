package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.AddressTypeDAO;
import com.memmorable.backend.orm.entity.AddressTypeEntity;

@Repository("addressTypeDAO")
public class AddressTypeDAOImpl extends AbstractDao<AddressTypeEntity> implements AddressTypeDAO {

	@Override
	public AddressTypeEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(AddressTypeEntity.class);
		crit.add(Restrictions.eq("address_type_name", name));
		return (AddressTypeEntity) crit.uniqueResult();
	}

	@Override
	public AddressTypeEntity findById(int id) {
		Criteria crit = getSession().createCriteria(AddressTypeEntity.class);
		crit.add(Restrictions.eq("address_type_id", id));
		return (AddressTypeEntity) crit.uniqueResult();
	}

}
