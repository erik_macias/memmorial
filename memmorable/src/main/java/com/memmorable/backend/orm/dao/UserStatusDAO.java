package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.UserStatusEntity;


public interface UserStatusDAO extends InterfaceDAO<UserStatusEntity>{
 public UserStatusEntity findbyStatusType(String name);
}
