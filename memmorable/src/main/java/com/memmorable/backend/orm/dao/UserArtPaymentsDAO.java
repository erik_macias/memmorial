package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.dto.ReportRanges;
import com.memmorable.backend.orm.dto.UserCreatedReportDTO;
import com.memmorable.backend.orm.dto.UserSalesReportDTO;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.UserArtPaymentsEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface UserArtPaymentsDAO extends InterfaceDAO<UserArtPaymentsEntity>{
	public UserArtPaymentsEntity findByPaymentId(String paymentId);
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user);
	public List<UserArtPaymentsEntity> findAllUserPaymentsToSeller(UserEntity buyerUser, UserEntity sellerUser);
	public List<UserArtPaymentsEntity> findAllUserArtPayments(UserEntity user, String paymentType);
	public UserArtPaymentsEntity findByArt(ArtEntity art, String paymentType);
	public UserArtPaymentsEntity findByArtAndBuyer(ArtEntity art, UserEntity user);
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(String paymentType, int year);
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(UserEntity user, String paymentType, int year);
	public List<UserArtPaymentsEntity> findUserWithFeePending();
	public List<UserCreatedReportDTO> findUsersCreated(int year);
	public UserArtPaymentsEntity getArtPaymentsAmountPending(ArtEntity art);
	public UserArtPaymentsEntity getArtPaymentsFeePending(ArtEntity art);
	public List<ArtEntity> getArtsWithFeePending(int intervalDays);
	public List<UserSalesReportDTO> findAllUserArtPaymentsReport(String artPaymentTypeAmount, ReportRanges ranges);
}
