package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.MediaRulesEntity;
import com.memmorable.backend.orm.entity.RoleEntity;


public interface MediaRulesDAO extends InterfaceDAO<MediaRulesEntity>{
	public MediaRulesEntity findByID(int  id);
	public MediaRulesEntity findByName(String name);
	public MediaRulesEntity findByRole(RoleEntity role);
	public MediaRulesEntity findByRoleAndType(RoleEntity role,ArtTypeEntity subType);
	public MediaRulesEntity findByType(ArtTypeEntity subType);
	
}
