package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;

public interface ArtTypeGenreDAO extends InterfaceDAO<ArtTypeGenreEntity> {
	public ArtTypeGenreEntity findByName(String name);
	public List<ArtTypeGenreEntity> findByType(ArtTypeEntity type);
	public ArtTypeGenreEntity findByNameType(String name, ArtTypeEntity type);
	public ArtTypeGenreEntity findById(int id);
}
