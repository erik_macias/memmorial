package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeFeeEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

public interface ArtTypeFeeService {
	void save(ArtTypeFeeEntity artTypeFee);
	void update(ArtTypeFeeEntity artTypeFee);
	void delete(ArtTypeFeeEntity artTypeFee);
	ArtTypeFeeEntity findById(int id);
	List<ArtTypeFeeEntity> findByArtType(ArtTypeEntity id);
	List<ArtTypeFeeEntity> findByRole(RoleEntity role);
	ArtTypeFeeEntity findByArtTypeAndRole(ArtTypeEntity id, RoleEntity role);
	List<ArtTypeFeeEntity> findall();
}
