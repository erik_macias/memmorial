package com.memmorable.backend.orm.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.ArtTypeServiceDAO;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;
import com.memmorable.backend.orm.service.ArtTypeServiceService;


@Service("artTypeServiceService")
@Transactional
public class ArtTypeServiceServiceImpl implements ArtTypeServiceService {
	
	@Autowired
	private ArtTypeServiceDAO dao;

	@Override
	public void save(ArtTypeServiceEntity artTag) {
		dao.save(artTag);
	}

	@Override
	public ArtTypeServiceEntity findService(ArtTypeServiceEntity id) {
		return dao.find(id);
	}

	@Override
	public ArtTypeServiceEntity findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<ArtTypeServiceEntity> findByType(ArtTypeEntity type) {
		return dao.findByType(type);
	}
	
	@Override
	public ArtTypeServiceEntity findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void update(ArtTypeServiceEntity artTag) {
		dao.update(artTag);
		
	}

	@Override
	public ArtTypeServiceEntity findByNameType(String name, ArtTypeEntity type) {
	return dao.findByNameType(name, type);
		
	}

	@Override
	public List<ArtTypeServiceEntity> findAll() {
		return dao.findAll(ArtTypeServiceEntity.class);
	}

}
