package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.MediaStatusDAO;
import com.memmorable.backend.orm.entity.MediaStatusEntity;
@Repository("MediaStatusDAO")
public class ArtMediaStatusDAOImpl extends AbstractDao<MediaStatusEntity> implements MediaStatusDAO {
	final static String DEFAULT_STATUS = "ACTIVE";
		
	@Override
	public MediaStatusEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(MediaStatusEntity.class);
		crit.add(Restrictions.eq("media_status_type", name));
		return (MediaStatusEntity) crit.uniqueResult();
	}

	public MediaStatusEntity getDefaultMediaStatus() {
		return findByName(DEFAULT_STATUS);
	}

	@Override
	public MediaStatusEntity findByID(int id) {
		Criteria crit = getSession().createCriteria(MediaStatusEntity.class);
		crit.add(Restrictions.eq("media_status_id",id));
		return (MediaStatusEntity) crit.uniqueResult();
	}

}
