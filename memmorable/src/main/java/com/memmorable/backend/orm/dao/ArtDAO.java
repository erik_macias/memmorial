package com.memmorable.backend.orm.dao;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTagEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.services.rest.responses.SearchResultEntityList;


public interface ArtDAO extends InterfaceDAO<ArtEntity>{
	public ArtEntity findByName(String name);
	public ArtEntity findByID(int  id) ;
	public SearchResultEntityList<ArtEntity> findByUser(UserEntity user);
	public SearchResultEntityList<ArtEntity> findBySubType(ArtSubTypeEntity subtype);
	public SearchResultEntityList<ArtEntity> findByType(ArtTypeEntity type);
	public SearchResultEntityList<ArtEntity> findByArtTag(ArtTagEntity tag);
	public SearchResultEntityList<ArtEntity> findByFreeText(String searchText, int maxResults, int firstResult);
	public SearchResultEntityList<ArtEntity> findByTopTenNewArt();
	public void DeleteArt(int id);
	public SearchResultEntityList<ArtEntity> getAllArt(int maxResults, int firstResult);
	public int getArtVisitsByUser(UserEntity user);
	
}
