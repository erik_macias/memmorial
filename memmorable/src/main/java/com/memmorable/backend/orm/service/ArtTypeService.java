package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;

public interface ArtTypeService {
	void save(ArtTypeEntity artType);
	ArtTypeEntity findType(ArtTypeEntity id);
	ArtTypeEntity findByName(String name);
	void update(ArtTypeEntity artType);
	List<ArtTypeEntity> findall();
	ArtTypeEntity findById(int id);
}
