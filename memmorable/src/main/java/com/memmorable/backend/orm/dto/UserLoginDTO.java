package com.memmorable.backend.orm.dto;


import java.util.Calendar;
import java.util.Date;

import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.app.common.MemmorableUtils;
import com.memmorable.backend.orm.configuration.AppContext;
import com.memmorable.backend.orm.entity.PersonalEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserPaymentsEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.PersonalService;
import com.memmorable.backend.orm.service.RoleService;
import com.memmorable.backend.orm.service.UserPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.orm.util.Decoder;
import com.memmorable.backend.services.rest.request.Credential;
import com.memmorable.backend.services.rest.responses.AuthenticationResponse;
import com.memmorable.backend.services.rest.responses.UserBasicInfoResponse;
import com.nisoph.utils.EncrypterUtils;
import com.paypal.api.payments.Agreement;

public class UserLoginDTO {
	AuthenticationResponse response;
	AbstractApplicationContext context = AppContext.getInstance();
	UserService userService = (UserService) context.getBean("userService");
	PersonalService personalService = (PersonalService) context.getBean("personalService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	UserPaymentsService userPaymentsService = (UserPaymentsService) context.getBean("userPaymentsService");
	RoleService roleService =  (RoleService) context.getBean("roleService");
	
	public AuthenticationResponse Authenticate(Credential cred) {
		response = new AuthenticationResponse();
		UserEntity user = userService.findByName(cred.username);
		PersonalEntity personal = personalService.findByUser(user);

		if (user != null && user.getLogin_attemps() < 3) {
			if (isCredentialvalid(user, cred)) {
				RoleEntity role = user.getRole();
				UserStatusEntity status = user.getUserStatus();
				if (status.getUser_status_type().equals(MemmorableConstants.USER_STATUS_ENABLED) && isUserActive(user)) {
					response.setSuccess(true);
					response.setMessage("Login success");
					response.setUser_name(user.getUser_name());
					response.setName(personal.getPersonal_name());
					response.setLastname(personal.getPersonal_l_name());
					UserBasicInfoResponse cu = new UserBasicInfoResponse();
					cu.password = EncrypterUtils.decryptValue(user.getUser_password());
					cu.username = user.getUser_name();
					cu.roleToken = role.getRole_token();
					String authData = Decoder.encode(cu);
					response.setAuthData(authData);
					clearAttempts(user);
				} else if (status.getUser_status_type().equals(MemmorableConstants.USER_STATUS_DISABLED)) {
					response.setSuccess(false);
					response.setMessage("Unable to Login: The user has been disabled. Please contact the Administrator.");
				} else {
					response.setSuccess(false);
					response.setMessage("Unable to Login: The user status is Invalid. Please contact the Administrator.");
				}
			} else {
				response.setSuccess(false);
				response.setMessage("Unable to Login: Username or password are not valid.");
				increaseAttempts(user);
			}
		} else {
			response.setSuccess(false);
			if (user != null) {
				if (!user.getUserStatus().getUser_status_type().equals(MemmorableConstants.USER_STATUS_BLOCKED)) {
					blockUser(user);
					response.setMessage("Unable to Login: User has been bloqued due to number of attempts reached. You can click on Forgot Password or contact the Administrator.");
				} else {
					response.setMessage("Unable to Login: User is bloqued due to number of attempts reached. You can click on Forgot Password or contact the Administrator.");
				}
			} else {
				response.setMessage("Unable to Login: Username or password are not valid.");
			}
		}
		//context.close();
		return response;
	}
	
	private void increaseAttempts(UserEntity user) {
		int currentAttempts = user.getLogin_attemps();
		currentAttempts = currentAttempts + 1;
		user.setLogin_attemps(currentAttempts);
		userService.update(user);
	}

	private void clearAttempts(UserEntity user) {
		user.setLogin_attemps(0);
		userService.update(user);
	}
	
	private void blockUser(UserEntity user) {
		UserStatusEntity status = userStatusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_BLOCKED);
		user.setUserStatus(status);
		userService.update(user);
	}

	private boolean isCredentialvalid(UserEntity user, Credential cred) {
		String userPwd = EncrypterUtils.decryptValue(user.getUser_password());
		if(userPwd.equals(cred.password))
			return true;
		else
			return false;
	}
	
	public Boolean isUserActive(UserEntity user) {
		Boolean isUserActive = Boolean.TRUE;
		if (user != null) {
			RoleEntity role = user.getRole();
			if (!role.getRole_name().equalsIgnoreCase(MemmorableConstants.USER_ROLE_FREE)
					&& !role.getRole_name().equalsIgnoreCase(MemmorableConstants.USER_ROLE_CERTIFIED)
					&& !role.getRole_name().equalsIgnoreCase(MemmorableConstants.USER_ROLE_ADMIN)) {
				UserPaymentsEntity userPayment = userPaymentsService.findByUserAndRole(user, role);
				if (userPayment != null) {
					Agreement agreement = new PaypalPlanAdministrationDTO().getPaypalPlanAgreement(userPayment.getAgreementId());
					if (agreement != null) {
						String lastPaymentDateStr = agreement.getAgreementDetails().getLastPaymentDate();
						Date lastPaymentDate = MemmorableUtils.getDateFromStr(lastPaymentDateStr, MemmorableConstants.DATE_FORMAT);

						//String nextBillingDateStr = agreement.getAgreementDetails().getNextBillingDate();
						//Date nextBillingDate = MemmorableUtils.getDateFromStr(nextBillingDateStr, MemmorableConstants.DATE_FORMAT);
						
						Calendar cal = Calendar.getInstance();
					    cal.setTime(lastPaymentDate);
					    cal.add(Calendar.MONTH, role.getCycle_time());
					    Date expDate = cal.getTime();
					    Date today = new Date();

					    if (expDate.compareTo(today) >= 0) {
					    	isUserActive = Boolean.TRUE;
					    } else {
					    	isUserActive = Boolean.FALSE;
					    	updateUserRoleAsFreeReg(user);
					    }
					}
				}
			} else {
				isUserActive = Boolean.TRUE;
			}
		} else {
			isUserActive = Boolean.FALSE;
		}
		
		return isUserActive;
	}
	
	public void updateUserRoleAsFreeReg(UserEntity user) {
		if (user != null) {
			RoleEntity defaultRole = roleService.findByRoleName(MemmorableConstants.USER_ROLE_FREE);
			user.setRole(defaultRole);
			userService.update(user);
		}
	}
	
}
