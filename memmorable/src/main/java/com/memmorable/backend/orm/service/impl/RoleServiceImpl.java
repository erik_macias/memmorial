package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.RoleDAO;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.service.RoleService;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDAO dao;

	@Override
	public void save(RoleEntity role) {
		dao.save(role);

	}

	@Override
	public RoleEntity findRole(RoleEntity id) {
		RoleEntity role = dao.find(id);
		return role;
	}

	@Override
	public RoleEntity findByRoleName(String name) {
		RoleEntity role = dao.findbyRoleName(name);
		return role;
	}

	@Override
	public void update(RoleEntity role) {
		dao.update(role);

	}

	@Override
	public List<RoleEntity> findAll() {
		return dao.findAll(RoleEntity.class);
	}

}
