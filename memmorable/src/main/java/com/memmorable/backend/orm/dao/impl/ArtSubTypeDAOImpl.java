package com.memmorable.backend.orm.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.ArtSubTypeDAO;
import com.memmorable.backend.orm.entity.ArtSubTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeEntity;

@Repository("artSubTypeDAO")
public class ArtSubTypeDAOImpl extends AbstractDao<ArtSubTypeEntity> implements ArtSubTypeDAO {

	@Override
	public ArtSubTypeEntity findByName(String name) {
		Criteria crit = getSession().createCriteria(ArtSubTypeEntity.class);
		crit.add(Restrictions.eq("art_subtype_name", name));
		return (ArtSubTypeEntity) crit.uniqueResult();
	}

	@Override
	public List<ArtSubTypeEntity> findByType(ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtSubTypeEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		return crit.list();
	}

	@Override
	public ArtSubTypeEntity findByNameType(String name, ArtTypeEntity type) {
		Criteria crit = getSession().createCriteria(ArtSubTypeEntity.class);
		crit.add(Restrictions.eq("art_type", type));
		crit.add(Restrictions.eq("art_subtype_name", name ));
		return (ArtSubTypeEntity) crit.uniqueResult();
	}

	@Override
	public ArtSubTypeEntity findById(int id) {
		Criteria crit = getSession().createCriteria(ArtSubTypeEntity.class);
		crit.add(Restrictions.eq("art_subtype_id", id));
		return (ArtSubTypeEntity) crit.uniqueResult();
	}

}
