package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.UserStatusEntity;

public interface UserStatusService {
	void save(UserStatusEntity status);
	UserStatusEntity findUserStatus(UserStatusEntity status);
	UserStatusEntity findByfindbyStatusType(String name);
	void update(UserStatusEntity status);
	List<UserStatusEntity> findAll();
}
