package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.AddressEntity;
import com.memmorable.backend.orm.entity.AddressTypeEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface AddressDAO extends InterfaceDAO<AddressEntity> {
	public List<AddressEntity> findbyUser(UserEntity user);
	public AddressEntity findAddressByTypeAndUser(AddressTypeEntity addressType, UserEntity user);
	public AddressEntity findById(int addressId);
}
