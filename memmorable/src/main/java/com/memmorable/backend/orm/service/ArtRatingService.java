package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtRatingEntity;
import com.memmorable.backend.orm.entity.UserEntity;

public interface ArtRatingService {
	void save(ArtRatingEntity artRating);
	ArtRatingEntity findbyId(ArtRatingEntity id);
	List<ArtRatingEntity> findByArt(ArtEntity art);
	List<ArtRatingEntity> findByUser(UserEntity user);
	int findRateByArt(ArtEntity art);
	ArtRatingEntity findRateByArtAndUser(ArtEntity art, UserEntity user);
	void update(ArtRatingEntity artRating);
}
