package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeServiceEntity;

public interface ArtTypeServiceDAO extends InterfaceDAO<ArtTypeServiceEntity> {
	public ArtTypeServiceEntity findByName(String name);
	public List<ArtTypeServiceEntity> findByType(ArtTypeEntity type);
	public ArtTypeServiceEntity findByNameType(String name, ArtTypeEntity type);
	public ArtTypeServiceEntity findById(int id);
}
