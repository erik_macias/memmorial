package com.memmorable.backend.orm.dao;

import java.util.List;

import com.memmorable.backend.orm.entity.CountryEntity;
import com.memmorable.backend.orm.entity.CountryStatesCityEntity;
import com.memmorable.backend.orm.entity.CountryStatesEntity;

public interface LocationDAO extends InterfaceDAO<CountryStatesCityEntity> {
	public CountryStatesEntity findStateByID(int id);
	
	public CountryStatesCityEntity findByID(int id);
	public CountryStatesCityEntity findByCityName(String countryName, String stateName, String cityName);
	
	List<CountryStatesEntity> findStatesByCountry(CountryEntity country);
	List<CountryStatesCityEntity> findCitiesByState(CountryStatesEntity state);
}
