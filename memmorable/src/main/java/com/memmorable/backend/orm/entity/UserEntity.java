package com.memmorable.backend.orm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.search.annotations.Field;

@Entity
@Table(name = "Table_User")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = -844106183168417970L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", nullable = false, unique = true)
	private int user_id;

	@Field
	@Column(name = "user_name", nullable = false, unique = true, length = 100)
	private String user_name;

	@Column(name = "user_password", nullable = false, unique = false, length = 100)
	private String user_password;

	@Column(name = "missed_logins", nullable = false, unique = false)
	private int Login_attemps;
	
	@Column(name="certified", nullable=true)
    private boolean certified;
	
	@Column(name = "created_date", nullable = false, columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@ManyToOne(targetEntity = RoleEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private RoleEntity role;

	@ManyToOne(targetEntity = UserStatusEntity.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_status_id")
	private UserStatusEntity userStatus;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public int getLogin_attemps() {
		return Login_attemps;
	}

	public void setLogin_attemps(int login_attemps) {
		Login_attemps = login_attemps;
	}

	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public UserStatusEntity getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatusEntity userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isCertified() {
		return certified;
	}

	public void setCertified(boolean certified) {
		this.certified = certified;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
