package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.AppSettingsEntity;
import com.memmorable.backend.services.rest.responses.GenericResponse;

public interface AppSettingsService {
	void save(AppSettingsEntity appSetting);
	void update(AppSettingsEntity appSetting);
	void delete(AppSettingsEntity appSetting);
	AppSettingsEntity findById(AppSettingsEntity id);
	AppSettingsEntity findById(int id);
	AppSettingsEntity findByAppSettingName(String appSettingName);
	List<AppSettingsEntity> findAll();
	GenericResponse reBuildLuceneIndex();
}
