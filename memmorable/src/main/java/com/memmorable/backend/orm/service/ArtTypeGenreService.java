package com.memmorable.backend.orm.service;

import java.util.List;

import com.memmorable.backend.orm.entity.ArtTypeEntity;
import com.memmorable.backend.orm.entity.ArtTypeGenreEntity;

public interface ArtTypeGenreService {
	void save(ArtTypeGenreEntity artGenre );
	void update(ArtTypeGenreEntity artGenre );
	ArtTypeGenreEntity findGenre (ArtTypeGenreEntity id);
	ArtTypeGenreEntity findByName(String name);
	ArtTypeGenreEntity findById(int id);
	List<ArtTypeGenreEntity> findByType(ArtTypeEntity type);
	ArtTypeGenreEntity findByNameType(String name,ArtTypeEntity type);
	List<ArtTypeGenreEntity> findAll();
	
}
