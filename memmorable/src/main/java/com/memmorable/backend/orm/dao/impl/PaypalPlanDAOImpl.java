package com.memmorable.backend.orm.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.dao.AbstractDao;
import com.memmorable.backend.orm.dao.PaypalPlanDAO;
import com.memmorable.backend.orm.entity.PaypalPlanEntity;
import com.memmorable.backend.orm.entity.RoleEntity;

@Repository("paypalPlanDAO")
public class PaypalPlanDAOImpl extends AbstractDao<PaypalPlanEntity> implements PaypalPlanDAO {

	@Override
	public PaypalPlanEntity findByPlanID(String planId) {
		Criteria crit = getSession().createCriteria(PaypalPlanEntity.class);
		crit.add(Restrictions.eq("plan_id", planId));
		return (PaypalPlanEntity) crit.uniqueResult();
	}

	@Override
	public PaypalPlanEntity findPlanIdByRole(RoleEntity role) {
		Criteria crit = getSession().createCriteria(PaypalPlanEntity.class);
		crit.add(Restrictions.eq("role", role));
		crit.add(Restrictions.eq("planStatus", MemmorableConstants.PAYPAL_BILLING_PLAN_ACTIVE));
		return (PaypalPlanEntity) crit.uniqueResult();
	}

}
