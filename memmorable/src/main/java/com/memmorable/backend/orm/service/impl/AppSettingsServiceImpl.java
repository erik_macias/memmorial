package com.memmorable.backend.orm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.memmorable.backend.orm.dao.AppSettingsDAO;
import com.memmorable.backend.orm.entity.AppSettingsEntity;
import com.memmorable.backend.orm.service.AppSettingsService;
import com.memmorable.backend.services.rest.responses.GenericResponse;

@Service("appSettingsService")
@Transactional
public class AppSettingsServiceImpl implements AppSettingsService {
	
	@Autowired
	private AppSettingsDAO dao;

	@Override
	public void save(AppSettingsEntity appSetting) {
		dao.save(appSetting);
	}

	@Override
	public AppSettingsEntity findById(AppSettingsEntity id) {
		return dao.find(id);
	}

	@Override
	public AppSettingsEntity findById(int id) {
		return dao.findByID(id);
	}

	@Override
	public AppSettingsEntity findByAppSettingName(String appSettingName) {
		return dao.findByName(appSettingName);
	}

	@Override
	public List<AppSettingsEntity> findAll() {
		return dao.findAll(AppSettingsEntity.class);
	}

	@Override
	public void update(AppSettingsEntity appSetting) {
		dao.update(appSetting);
	}

	@Override
	public void delete(AppSettingsEntity appSetting) {
		dao.delete(appSetting);
	}

	@Override
	public GenericResponse reBuildLuceneIndex() {
		GenericResponse response = new GenericResponse();
		response.success = dao.reBuildLuceneIndex();
		return response;
	}
	
}
