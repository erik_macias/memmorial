package com.memmorable.backend.security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.core.Headers;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ServerResponse;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.backend.orm.configuration.AppConfig;
import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.orm.entity.ArtStatusEntity;
import com.memmorable.backend.orm.entity.RoleEntity;
import com.memmorable.backend.orm.entity.UserEntity;
import com.memmorable.backend.orm.entity.UserStatusEntity;
import com.memmorable.backend.orm.service.ArtService;
import com.memmorable.backend.orm.service.ArtStatusService;
import com.memmorable.backend.orm.service.UserArtPaymentsService;
import com.memmorable.backend.orm.service.UserService;
import com.memmorable.backend.orm.service.UserStatusService;
import com.memmorable.backend.orm.util.Decoder;
import com.memmorable.backend.services.rest.responses.UserBasicInfoResponse;
import com.nisoph.utils.EncrypterUtils;

@Provider
public class SecurityInterceptor implements ContainerRequestFilter {

	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHENTICATION_SCHEME = "Basic";
	private static final ServerResponse ACCESS_DENIED = new ServerResponse("Access denied for this resource", 401, new Headers<Object>());
	private static final ServerResponse ACCESS_FORBIDDEN = new ServerResponse("Nobody can access this resource", 403, new Headers<Object>());
	//private static final ServerResponse SERVER_ERROR = new ServerResponse("INTERNAL SERVER ERROR", 500, new Headers<Object>());
	
	AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	UserService userService = (UserService) context.getBean("userService");
	UserArtPaymentsService userArtPaymentsService = (UserArtPaymentsService) context.getBean("userArtPaymentsService");
	UserStatusService userStatusService = (UserStatusService) context.getBean("userStatusService");
	ArtStatusService artStatusService = (ArtStatusService) context.getBean("artStatusService");
	ArtService  artService = (ArtService) context.getBean("artService");

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
		Method method = methodInvoker.getMethod();
		// Access allowed for all
		if (!method.isAnnotationPresent(PermitAll.class)) {
			// Access denied for all
			if (method.isAnnotationPresent(DenyAll.class)) {
				requestContext.abortWith(ACCESS_FORBIDDEN);
				return;
			}

			// Get request headers
			final MultivaluedMap<String, String> headers = requestContext.getHeaders();

			// Fetch authorization header
			final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

			// If no authorization information present; block access
			if (authorization == null || authorization.isEmpty()) {
				requestContext.abortWith(ACCESS_DENIED);
				return;
			}

			// Decode Data
			String token = "";
			if (authorization.get(0).contains(AUTHENTICATION_SCHEME)) {
				token = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");
			} else {
				token = authorization.get(0);
			}
			UserBasicInfoResponse dtoken = Decoder.decode(token, UserBasicInfoResponse.class);

			// Verify user access
			if (method.isAnnotationPresent(RolesAllowed.class)) {
				RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
				Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

				// Is user valid?
				if (!isUserAllowed(dtoken, rolesSet)) {
					requestContext.abortWith(ACCESS_DENIED);
					return;
				}
			}
		}

		// Disable Arts and User for Arts with Fee Pending > 1 Month
		disableArtsAndUserWithFeePending();
	}

	private boolean isUserAllowed(final UserBasicInfoResponse userBasicInfo, final Set<String> rolesSet) {
		boolean isAllowed = false;

		UserEntity user = userService.findByName(userBasicInfo.username);
		if (isCredentialvalid(user, userBasicInfo)) {
			RoleEntity role = user.getRole();
			if (role.getRole_token().equals(userBasicInfo.roleToken) && rolesSet.contains(userBasicInfo.roleToken)) {
				isAllowed = true;
			}
		}
		
		return isAllowed;
	}
	
	private boolean isCredentialvalid(UserEntity user, UserBasicInfoResponse userBasicInfo) {
		String userPwd = EncrypterUtils.decryptValue(user.getUser_password());
		if(userPwd.equals(userBasicInfo.password))
			return true;
		else
			return false;
	}
	
	private void disableArtsAndUserWithFeePending() {
		ArtStatusEntity artStatusRemoved = artStatusService.findByName(MemmorableConstants.ART_STATUS_REMOVED);
		UserStatusEntity userStatusDeleted = userStatusService.findByfindbyStatusType(MemmorableConstants.USER_STATUS_DISABLED);
		
		List<ArtEntity> artsToDisable = userArtPaymentsService.getArtsWithFeePending(MemmorableConstants.ART_FEE_EXPIRATION_DAYS);
		if (artsToDisable != null && !artsToDisable.isEmpty()) {
			Iterator<ArtEntity> iter = artsToDisable.iterator();
			while (iter.hasNext()) {
				ArtEntity artToDisable = iter.next();
				if (artToDisable != null) {
					//Disable user
					UserEntity userToDisable = artToDisable.getUser();
					if (!userToDisable.getUserStatus().getUser_status_type().equals(MemmorableConstants.USER_STATUS_DISABLED)) {
						userToDisable.setUserStatus(userStatusDeleted);
						userService.update(userToDisable);
					}
					
					//Disable Art
					if (!artToDisable.getArt_status().getArt_status_type().equals(MemmorableConstants.ART_STATUS_REMOVED) && !artToDisable.isDeleted()) {
						artToDisable.setArt_status(artStatusRemoved);
						artToDisable.setDeleted(Boolean.TRUE);
						artService.update(artToDisable);
					}
				}
			}
		}
	}

}
