package com.memmorable.paypal.base;

import com.paypal.base.rest.OAuthTokenCredential;
import com.paypal.base.rest.PayPalRESTException;
import com.paypal.base.rest.PayPalResource;

public class BaseConfingLoader {

	private static BaseConfingLoader instance = null;
	private String accessToken = null;
	
	private BaseConfingLoader() {
		initConf();
	}
	
	public static BaseConfingLoader getInstance() {
		if(instance == null) {
	         instance = new BaseConfingLoader();
	      }
	      return instance;
	}
	
	private void initConf() {
		// initialize Invoice with credentials. User credentials must be stored in the file
		try {
			OAuthTokenCredential oAuthTokenCredential = PayPalResource
					.initConfig(BaseConfingLoader.class.getClassLoader().getResourceAsStream("sdk_config.properties"));
			// get an access token
			accessToken =  oAuthTokenCredential.getAccessToken();
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
	}
	
	public String getAccessToken() {
		return accessToken;
	}

}
