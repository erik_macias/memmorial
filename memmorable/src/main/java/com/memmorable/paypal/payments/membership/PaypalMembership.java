package com.memmorable.paypal.payments.membership;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.memmorable.app.common.MemmorableConstants;
import com.memmorable.paypal.util.PaypalUtils;
import com.paypal.api.payments.Agreement;
import com.paypal.api.payments.AgreementStateDescriptor;
import com.paypal.api.payments.AgreementTransactions;
import com.paypal.api.payments.Patch;
import com.paypal.api.payments.Plan;
import com.paypal.api.payments.PlanList;
import com.paypal.base.rest.PayPalRESTException;

public class PaypalMembership {

	public Plan createNewBillingPlan(String jsonStr) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Plan plan = utils.loadFromJSONString(jsonStr, Plan.class);
		return plan.create(utils.getAPIContext());
	}
	
	public Plan createNewBillingPlan(Plan plan) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return plan.create(utils.getAPIContext());
	}

	public Boolean activateBillingPlan(String jsonPlanStr, String jsonPatchStr)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Plan plan = utils.loadFromJSONString(jsonPlanStr, Plan.class);
		Patch[] patch = utils.loadFromJSONString(jsonPatchStr, Patch[].class);
		plan.update(utils.getAPIContext(), Arrays.asList(patch));
		return true;
	}

	public Boolean activateBillingPlan(String planId) throws PayPalRESTException {
		List<Patch> patchList = new ArrayList<Patch>();
		Patch patch = new Patch();
		patch.setPath("/");

		Agreement agreement = new Agreement();
		agreement.setState(MemmorableConstants.PAYPAL_BILLING_PLAN_ACTIVE);
		patch.setValue(agreement);

		patch.setOp("replace");
		patchList.add(patch);

		Plan plan = new Plan();
		plan.setId(planId);
		updateBullingPlan(plan, patchList);
		return Boolean.TRUE;
	}
	
	public Boolean deleteBillingPlan(String planId)
			throws PayPalRESTException {
		List<Patch> patchList = new ArrayList<Patch>();
		Patch patch = new Patch();
		patch.setPath("/");

		Agreement agreement = new Agreement();
		agreement.setState(MemmorableConstants.PAYPAL_BILLING_PLAN_DELETED);
		patch.setValue(agreement);

		patch.setOp("replace");
		patchList.add(patch);

		Plan plan = new Plan();
		plan.setId(planId);
		updateBullingPlan(plan, patchList);
		return Boolean.TRUE;
	}

	public Plan getBillingPlanDetails(String planId) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Plan.get(utils.getAPIContext(), planId);
	}

	public PlanList getBillingPlans() throws PayPalRESTException {
		Map<String, String> containerMap = new HashMap<String, String>();
		return getBillingPlans(containerMap);
	}

	public PlanList getBillingPlans(Map<String, String> containerMap)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Plan.list(utils.getAPIContext(), containerMap);
	}

	public void updateBullingPlan(Plan plan, List<Patch> patchList)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		plan.update(utils.getAPIContext(), patchList);
	}

	public Agreement createBillingAgreement(String jsonStr)
			throws MalformedURLException, UnsupportedEncodingException,
			PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Agreement agreement = utils
				.loadFromJSONString(jsonStr, Agreement.class);
		return agreement.create(utils.getAPIContext());
	}
	
	public Agreement createBillingAgreement(Agreement agreement)
			throws MalformedURLException, UnsupportedEncodingException,
			PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return agreement.create(utils.getAPIContext());
	}
	
	public Agreement executeBillingAgreement(String paymentToken)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Agreement.execute(utils.getAPIContext(), paymentToken);
	}

	public Agreement updateBillingAgreement(String agreementId,
			List<Patch> patchList) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Agreement agreement = new Agreement();
		agreement.setId(agreementId);
		return agreement.update(utils.getAPIContext(), patchList);
	}

	public Agreement getBillingAgreementDetails(String agreementId)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Agreement.get(utils.getAPIContext(), agreementId);
	}

	public void suspendBillingAgreement(String agreementId, String note)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Agreement agreement = new Agreement();
		agreement.setId(agreementId);
		AgreementStateDescriptor agreementStateDescriptor = new AgreementStateDescriptor();
		agreementStateDescriptor.setNote(note);
		agreement.suspend(utils.getAPIContext(), agreementStateDescriptor);
	}

	public void reactivateBillingAgreement(String agreementId, String note)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Agreement agreement = new Agreement();
		agreement.setId(agreementId);
		AgreementStateDescriptor agreementStateDescriptor = new AgreementStateDescriptor();
		agreementStateDescriptor.setNote(note);
		agreement.reActivate(utils.getAPIContext(), agreementStateDescriptor);
	}

	public void cancelBillingAgreement(String agreementId, String note)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Agreement agreement = new Agreement();
		agreement.setId(agreementId);
		AgreementStateDescriptor agreementStateDescriptor = new AgreementStateDescriptor();
		agreementStateDescriptor.setNote(note);
		agreement.cancel(utils.getAPIContext(), agreementStateDescriptor);
	}

	public AgreementTransactions getBillingAgreementTransactions(
			String agreementId, Date startDate, Date endDate)
			throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Agreement.transactions(utils.getAPIContext(), agreementId,
				startDate, endDate);
	}

	public Plan getCreateBillingPlanTemplate() {
		PaypalUtils utils = PaypalUtils.getInstance();
		return utils.getCreateBillingPlanTemplate();
	}
	
	public Agreement getCreateBillingPlanAgreementTemplate() {
		PaypalUtils utils = PaypalUtils.getInstance();
		return utils.getCreateBillingAgreementTemplate();
	}

	public Agreement getBillingPlanAgrmntDetails(String id) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		return Agreement.get(utils.getAPIContext(), id);
	}

}
