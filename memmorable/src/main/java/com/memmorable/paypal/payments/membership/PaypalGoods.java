package com.memmorable.paypal.payments.membership;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.memmorable.backend.orm.entity.ArtEntity;
import com.memmorable.backend.services.rest.request.ArtPaymentConfRequest;
import com.memmorable.paypal.util.PaypalUtils;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.PayPalRESTException;

public class PaypalGoods {
	
	private static final Logger logger = LoggerFactory.getLogger(PaypalGoods.class);

	public Payment createFeePayment(String jsonStr) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		Payment payment = utils.loadFromJSONString(jsonStr, Payment.class);
		return payment.create(utils.getAPIContext());
	}
	
	public Payment createFeePayment(ArtEntity art, Double feeAmount) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		
		String feeStr = String.format("%.2f", feeAmount);
		Amount amount = getGoodsAmmount(feeStr, art.getCurrency().getCurrency_type());

		art.setPrice(feeAmount);
		Transaction transaction = getTransaction("Fee Payment for Art ID: ", art);
		transaction.setAmount(amount);

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = getPayment("sale", utils.getPaypalReturnFeeURL(), utils.getPaypalCancelFeeURL());
		payment.setPayer(payer);
		payment.setTransactions(transactions);
		
		return payment.create(utils.getAPIContext());
	}
	
	public Payment confirmFeePayment(ArtPaymentConfRequest artReq) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		
		Payment payment = new Payment();
		payment.setId(artReq.paymentId);
		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(artReq.PayerID);
		return payment.execute(utils.getAPIContext(), paymentExecute);
	}
	
	public Payment createPayment(ArtEntity art) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		
		String priceStr = String.format("%.2f", art.getPrice());
		Amount amount = getGoodsAmmount(priceStr, art.getCurrency().getCurrency_type());

		Transaction transaction = getTransaction("Goods Payment for Art ID: ", art);
		transaction.setAmount(amount);

		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = getPayment("sale", utils.getPaypalReturnGoodsURL(), utils.getPaypalCancelGoodsURL());
		payment.setPayer(payer);
		payment.setTransactions(transactions);
		
		return payment.create(utils.getAPIContext());
	}

	public Payment confirmPayment(ArtPaymentConfRequest artReq) throws PayPalRESTException {
		PaypalUtils utils = PaypalUtils.getInstance();
		
		Payment payment = new Payment();
		payment.setId(artReq.paymentId);
		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(artReq.PayerID);
		return payment.execute(utils.getAPIContext(), paymentExecute);
	}
	
	private Amount getGoodsAmmount(String price, String currency) {
		logger.debug("getGoodsAmmount(), price: " + price + " currency:" + currency);
		Amount amount = new Amount();
		amount.setCurrency(currency);
		amount.setTotal(price);
		logger.debug("Amount: ", amount);
		
		return amount;
	}
	
	private Transaction getTransaction(String description, ArtEntity art) {
		logger.debug("getTransaction(), description: " + description + " art: " + art);
		Transaction transaction = new Transaction();
		transaction.setDescription(description + art.getArt_id());
		transaction.setItemList(getTransactionItemList(art));
		logger.debug("Transaction", transaction);
		
		return transaction;
	}
	
	private ItemList getTransactionItemList(ArtEntity art) {
		logger.debug("getTransactionItemList(), art: " + art);
		ItemList itemList = new ItemList();
		
		List<Item> items = new ArrayList<Item>();
		Item item = new Item();
		item.setSku(String.valueOf(art.getArt_id()));
		item.setName(art.getArt_name());
		item.setDescription(art.getArt_description());
		item.setQuantity("1");
		item.setPrice(String.format("%.2f", art.getPrice()));
		item.setCurrency(art.getCurrency().getCurrency_type());
		items.add(item);
		logger.debug("Item: " + item);
		logger.debug("items" + items);
		
		return itemList.setItems(items);
	}
	
	private Payment getPayment(String intent, String returnUrl, String cancelUrl) {
		logger.debug("getPayment() intent: " + intent + "returnUrl: " + returnUrl + "cancelUrl: " + cancelUrl);
		Payment payment = new Payment();
		payment.setIntent(intent);
		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setReturnUrl(returnUrl);
		redirectUrls.setCancelUrl(cancelUrl);
		payment.setRedirectUrls(redirectUrls);
		logger.debug("Payment: ", payment);
		
		return payment;
	}
	
}
