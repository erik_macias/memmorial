package com.memmorable.paypal.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.util.ISO8601Utils;
import com.memmorable.app.common.MemmorableAppConstants;
import com.memmorable.app.common.MemmorableUtils;
import com.paypal.api.payments.Agreement;
import com.paypal.api.payments.Patch;
import com.paypal.api.payments.Plan;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.JSONFormatter;

public class PaypalUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(PaypalUtils.class);

	private static PaypalUtils instance = null;

	static {
		logger.debug("PaypalUtils instance value: ", instance);
		if (instance == null) {
			instance = new PaypalUtils();
			logger.debug("New PaypalUtils instance: ", instance);
		}
	}
	
	private PaypalUtils() {
		
	}

	public static PaypalUtils getInstance() {
		logger.debug("getInstance()", instance);
		return instance;
	}

	public Plan getCreateBillingPlanTemplate() {
		return loadFromJSONFile(
				MemmorableAppConstants.PAYPAL_JSON_TEMPLATE_BILLING_PLAN_CREATE,
				Plan.class);
	}
	
	public Patch[] getActivateBillingPlanTemplate() {
		return loadFromJSONFile(
				MemmorableAppConstants.PAYPAL_JSON_TEMPLATE_BILLING_PLAN_ACTIVATE,
				Patch[].class);
	}
	
	public Agreement getCreateBillingAgreementTemplate() {
		return loadFromJSONFile(
				MemmorableAppConstants.PAYPAL_JSON_TEMPLATE_BILLING_AGMNT_CREATE,
				Agreement.class);
	}

	public <T> T loadFromJSONFile(String jsonFile, Class<T> clazz) {
		String json = load(jsonFile);
		return (T) loadFromJSONString(json, clazz);
	}
	
	public <T> T loadFromJSONString(String jsonString, Class<T> clazz) {
		return (T) JSONFormatter.fromJSON(jsonString, clazz);
	}

	private String load(String jsonFile) {
		try {
			InputStream inputStream = this
					.getClass()
					.getClassLoader()
					.getResourceAsStream(
							MemmorableAppConstants.PAYPAL_JSON_TRANS_TEMPLATES_PATH
									+ jsonFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					inputStream));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.getProperty("line.separator"));
				line = br.readLine();
			}
			br.close();
			return sb.toString();

		} catch (IOException e) {
			logger.error("Error while loading JSON file", e);
			return null;
		}
	}
	
	public APIContext getAPIContext() {
		Properties props = MemmorableUtils.getPaypalProperties();
		APIContext apiContext = new APIContext(
				props.getProperty(MemmorableAppConstants.PAYPAL_CLIENT_ID),
				props.getProperty(MemmorableAppConstants.PAYPAL_CLIENT_SECRET),
				props.getProperty(MemmorableAppConstants.PAYPAL_SERVICE_MODE));
		
		logger.debug("getAPIContext()", apiContext);
		return apiContext;
	}
	
	public String getBillingAgrmntStartDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 60);
		return ISO8601Utils.format(cal.getTime(), Boolean.TRUE);
	}
	
	public String getPaypalReturnFeeURL() {
		return MemmorableUtils.getPaypalProperty(MemmorableAppConstants.PAYPAL_URL_RETURN_FEE);
	}
	
	public String getPaypalCancelFeeURL() {
		return MemmorableUtils.getPaypalProperty(MemmorableAppConstants.PAYPAL_URL_CANCEL_FEE);
	}
	
	public String getPaypalReturnGoodsURL() {
		return MemmorableUtils.getPaypalProperty(MemmorableAppConstants.PAYPAL_URL_RETURN_GOODS);
	}
	
	public String getPaypalCancelGoodsURL() {
		return MemmorableUtils.getPaypalProperty(MemmorableAppConstants.PAYPAL_URL_CANCEL_GOODS);
	}
	
}
