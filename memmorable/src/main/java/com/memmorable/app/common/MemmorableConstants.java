package com.memmorable.app.common;

public interface MemmorableConstants {
	
	//User Status
	public static final String USER_STATUS_ENABLED 				= "ENABLED";
	public static final String USER_STATUS_DISABLED 			= "DISABLED";
	public static final String USER_STATUS_PENDIGN_VALIDATION 	= "PENDING_VALIDATION";
	public static final String USER_STATUS_BLOCKED 				= "BLOCKED";
	
	//User Roles
	public static final String USER_ROLE_FREE	 				= "FREE";
	public static final String USER_ROLE_MONTHLY 				= "MONTHLY";
	public static final String USER_ROLE_BIMONTHLY	 			= "BIMONTHLY";
	public static final String USER_ROLE_QUARTER	 			= "QUARTER";
	public static final String USER_ROLE_FOURTHMONTHLY	 		= "FOURTHMONTHLY";
	public static final String USER_ROLE_HALF_YEAR	 			= "HALF_YEAR";
	public static final String USER_ROLE_YEAR	 				= "YEAR";
	public static final String USER_ROLE_CERTIFIED	 			= "CERTIFIED";
	public static final String USER_ROLE_ADMIN					= "ADMIN";
	
	public static final String USER_ROLE_TOKEN_FREE	 			= "FREE_FOR_ALL";
	public static final String USER_ROLE_TOKEN_MONTHLY 			= "MONTHLY_MADNESS";
	public static final String USER_ROLE_TOKEN_BIMONTHLY		= "BICYCLE";
	public static final String USER_ROLE_TOKEN_QUARTER	 		= "QUATERLY";
	public static final String USER_ROLE_TOKEN_FOURTHMONTHLY 	= "ONE_FOURTH";
	public static final String USER_ROLE_TOKEN_HALF_YEAR	 	= "HALF";
	public static final String USER_ROLE_TOKEN_YEAR	 			= "ONE";
	public static final String USER_ROLE_TOKEN_CERTIFIED		= "CERTIFIED";
	public static final String USER_ROLE_TOKEN_ADMIN			= "ADMIN";
	
	//Art Status
	public static final String ART_STATUS_ACTIVE 				= "ACTIVE";
	public static final String ART_STATUS_REMOVED 				= "REMOVED";
	public static final String ART_STATUS_BLOCKED 				= "BLOCKED";
	public static final String ART_STATUS_SOLD 					= "SOLD";
	public static final String ART_STATUS_BOUGHT 				= "BOUGHT";
	public static final String ART_STATUS_PENDING_VALIDATION 	= "PENDING VALIDATION";
	public static final String ART_STATUS_IN_PROCESS 			= "IN_PROCESS";
	
	//Art Types
	public static final String ART_TYPE_LITERATURE				= "Literature";
	public static final String ART_TYPE_PAINTING				= "Painting";
	public static final String ART_TYPE_PHOTOGRAPHY				= "Photography";
	public static final String ART_TYPE_ARCHITECTURE			= "Architecture";
	public static final String ART_TYPE_MUSIC					= "Music";
	public static final String ART_TYPE_DANCE					= "Dance";
	public static final String ART_TYPE_THEATER					= "Theater";
	public static final String ART_TYPE_CINEMATOGRAPHY			= "Cinematography";
	public static final String ART_TYPE_EPHEMERAL				= "Ephemeral";
	public static final String ART_TYPE_SCULPTURE				= "Sculpture";
	
	public static final String CURRENCY_MXN		 				= "MXN";
	public static final String CURRENCY_USD		 				= "USD";
	
	public static final String DATE_FORMAT						="yyyy-MM-dd'T'HH:mm:ss";
	
	public static final String USER_ADDRESS_DEFAULT 			= "DEFAULT";
	
	public static final String MEDIA_TYPE_IMAGE					= "image";
	public static final String MEDIA_TYPE_VIDEO					= "video";
	public static final String MEDIA_TYPE_AUDIO					= "audio";
	public static final String MEDIA_TYPE_PDF					= "pdf";
	
	//Inbox Status
	public static final String INBOX_STATUS_ENABLED 			= "ENABLED";
	public static final String INBOX_STATUS_DISABLED 			= "DISABLED";
	public static final String INBOX_STATUS_DELETED 			= "DELETED";
	public static final String INBOX_STATUS_BLOCKED 			= "BLOCKED";
	
	//Paypal Billing Plan
	public static final String PAYPAL_BILLING_PLAN_TYPE_TRIAL	= "TRIAL";
	public static final String PAYPAL_BILLING_PLAN_TYPE_REGULAR	= "REGULAR";
	public static final String PAYPAL_BILLING_PLAN_FREQ_DAY		= "DAY";
	public static final String PAYPAL_BILLING_PLAN_FREQ_WEEK	= "WEEK";
	public static final String PAYPAL_BILLING_PLAN_FREQ_MONTH	= "MONTH";
	public static final String PAYPAL_BILLING_PLAN_FREQ_YEAR	= "YEAR";
	public static final String PAYPAL_BILLING_PLAN_FREQ_YEAR_CERT	= "YEAR_CERT";
	
	public static final String PAYPAL_BILLING_PLAN_CREATED		= "CREATED";
	public static final String PAYPAL_BILLING_PLAN_ACTIVE		= "ACTIVE";
	public static final String PAYPAL_BILLING_PLAN_INACTIVE		= "INACTIVE";
	public static final String PAYPAL_BILLING_PLAN_DELETED		= "DELETED";
	
	public static final String ART_PAYMENT_TYPE_FEE				= "FEE";
	public static final String ART_PAYMENT_TYPE_AMOUNT			= "AMOUNT";
	
	public static final String USER_SURVEY_TRANSACTION_PURCHASE = "PURCHASE";
	public static final String USER_SURVEY_TRANSACTION_SELL 	= "SELL";
	
	public static final String USER_SURVEY_STATUS_OPEN			= "OPEN";
	public static final String USER_SURVEY_STATUS_IN_PROGRESS	= "IN PROGRESS";
	public static final String USER_SURVEY_STATUS_DELIVERED		= "DELIVERED";
	public static final String USER_SURVEY_STATUS_NOT_DELIVERED	= "NOT DELIVERED";
	public static final String USER_SURVEY_STATUS_RECEIVED		= "RECEIVED";
	public static final String USER_SURVEY_STATUS_NOT_RECEIVED	= "NOT RECEIVED";
	
	public static final String PAYMENT_METHOD_PAYPAL			= "PAYPAL";
	public static final String PAYMENT_METHOD_TRANSFER			= "TRANSFER";
	public static final String PAYMENT_METHOD_DEPOSIT			= "DEPOSIT";
	public static final String PAYMENT_METHOD_CREDIT			= "CREDIT";
	
	public static final Integer ART_FEE_EXPIRATION_DAYS			= 30;

}
