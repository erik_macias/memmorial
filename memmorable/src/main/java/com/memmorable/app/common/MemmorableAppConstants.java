package com.memmorable.app.common;

public interface MemmorableAppConstants {
	
	public static final String GOOGLE_RECAPTCHA_CLIENT_KEY_NAME				= "GOOGLE_RECAPTCHA_CLIENT_KEY";
	
	public static final String PAYPAL_JSON_TRANS_TEMPLATES_PATH				= "com/memmorable/paypal/transactions/templates/";
	public static final String PAYPAL_JSON_TEMPLATE_BILLING_PLAN_CREATE		= "billingplan_create.json";
	public static final String PAYPAL_JSON_TEMPLATE_BILLING_PLAN_ACTIVATE	= "billingplan_activate.json";
	public static final String PAYPAL_JSON_TEMPLATE_BILLING_AGMNT_CREATE	= "billingagreement_create.json";
	
	public static final String MEMMORABLE_PROPERTIES_APPLICATION_FILE_NAME	= "application.properties";
	public static final String MEMMORABLE_PROPERTIES_CONFIG_FILE_NAME		= "config.properties";
	public static final String MEMMORABLE_PROPERTIES_PAYPAL_FILE_NAME		= "sdk_config.properties";

	public static final String PAYPAL_CLIENT_ID								= "clientId";
	public static final String PAYPAL_CLIENT_SECRET							= "clientSecret";
	public static final String PAYPAL_SERVICE_MODE							= "service.mode";

	public static final String PAYPAL_URL_RETURN_GOODS						= "return_good_payment_url";
	public static final String PAYPAL_URL_CANCEL_GOODS						= "cancel_good_payment_url";
	public static final String PAYPAL_URL_RETURN_FEE						= "return_fee_good_payment_url";
	public static final String PAYPAL_URL_CANCEL_FEE						= "cancel_fee_good_payment_url";
	
	public static final String EMAIL_ADMIN_MEMMORABLE_DEFAULT				= "email.admin";
}
