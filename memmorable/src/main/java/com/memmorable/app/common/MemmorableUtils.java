package com.memmorable.app.common;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.memmorable.paypal.base.BaseConfingLoader;

public class MemmorableUtils {

	public static String getAppProperty(String propertyName) {
		return getPropertyFromResource(
				propertyName,
				MemmorableAppConstants.MEMMORABLE_PROPERTIES_APPLICATION_FILE_NAME);
	}

	public static String getConfigProperty(String propertyName) {
		return getPropertyFromResource(propertyName,
				MemmorableAppConstants.MEMMORABLE_PROPERTIES_CONFIG_FILE_NAME);
	}

	public static String getPaypalProperty(String propertyName) {
		return getPropertyFromResource(propertyName,
				MemmorableAppConstants.MEMMORABLE_PROPERTIES_PAYPAL_FILE_NAME);
	}
	
	public static Properties getAppProperties() {
		return getPropertiesFromResource(MemmorableAppConstants.MEMMORABLE_PROPERTIES_APPLICATION_FILE_NAME);
	}

	public static Properties getConfigProperties() {
		return getPropertiesFromResource(MemmorableAppConstants.MEMMORABLE_PROPERTIES_CONFIG_FILE_NAME);
	}

	public static Properties getPaypalProperties() {
		return getPropertiesFromResource(MemmorableAppConstants.MEMMORABLE_PROPERTIES_PAYPAL_FILE_NAME);
	}
	
	public static Date getDateFromStr(String dateStr, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date lastPaymentDate = new Date();
		try {
			lastPaymentDate = sdf.parse(dateStr);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return lastPaymentDate;
	}

	private static String getPropertyFromResource(String propertyName,
			String propertyFileName) {
		Properties prop = new Properties();
		prop = getPropertiesFromResource(propertyFileName);
		return prop.getProperty(propertyName);
	}
	
	private static Properties getPropertiesFromResource(String propertyFileName) {
		Properties prop = new Properties();
		try {
			prop.load(BaseConfingLoader.class.getClassLoader()
					.getResourceAsStream(propertyFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
}
