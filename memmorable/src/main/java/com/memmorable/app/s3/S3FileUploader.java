package com.memmorable.app.s3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MultivaluedMap;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.memmorable.backend.services.rest.responses.MediaResponse;

public class S3FileUploader {

	private String bucketName;
	private String S3URL;
	private String accessKey;
	private String secretKey;

	public S3FileUploader() {
		InputStream inputStream = S3FileUploader.class.getClassLoader()
				.getResourceAsStream("application.properties");
		Properties properties = new Properties();

		try {
			properties.load(inputStream);
			inputStream.close();
			accessKey = properties.getProperty("aws.s3.accessKey");
			secretKey = properties.getProperty("aws.s3.secretKey");
			bucketName = properties.getProperty("aws.s3.bucketName");
			S3URL = properties.getProperty("aws.s3.url");
		} catch (IOException e) {
			try {
				inputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public MediaResponse uploadFile(MultipartFormDataInput input) {
		String fileName = "";
		String fileType = "";
		String artBucketName = bucketName + "/arts";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");

		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = Math.random() * 100 + getFileName(header);
				File file = inputPart.getBody(File.class, null);
				fileType = header.getFirst("Content-Type");

				sendToBucket(file, fileName, fileType, artBucketName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		MediaResponse response = new MediaResponse();
		response.setSuccess(true);
		response.setMessage("File was Uploaded Correctly");
		response.setFilePath(S3URL + artBucketName + "/" + fileName);
		response.setFileType(fileType);
		return response;
	}

	public MediaResponse uploaProfiledFile(MultipartFormDataInput input) {
		String nameFile = Math.random() * 100 + "profile.jpg";
		String profileBucketName = bucketName + "/profiles";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");

		for (InputPart inputPart : inputParts) {
			try {
				File file = inputPart.getBody(File.class, null);
				sendToBucket(file, nameFile, "image/jpeg", profileBucketName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		MediaResponse response = new MediaResponse();
		response.setSuccess(true);
		response.setMessage("File was Uploaded Correctly");
		response.setFilePath(S3URL + profileBucketName + "/" + nameFile);
		return response;
	}

	private void sendToBucket(File file, String fileName, String contentType,
			String awsBucketName) {
		AmazonS3 s3 = new AmazonS3Client(
				new s3Credentials(accessKey, secretKey));
		// Region region = Region.getRegion(Regions.US_EAST_1);
		// s3.setRegion(region);

		String key = fileName;

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentType(contentType);
		PutObjectRequest objeto = new PutObjectRequest(awsBucketName, key, file);
		objeto.setMetadata(meta);
		s3.putObject(objeto);
		s3.setObjectAcl(awsBucketName, key, CannedAccessControlList.PublicRead);
	}

	private String getFileName(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition")
				.split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

}
