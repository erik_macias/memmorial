package com.memmorable.app.s3;

import com.amazonaws.auth.AWSCredentials;

public class s3Credentials implements AWSCredentials {
	
	private String accessKey;
	private String secretKey;
	
	public s3Credentials(String accessKey, String secretKey) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
	}
	
	@Override
	public String getAWSAccessKeyId() {
		return accessKey;
	}

	@Override
	public String getAWSSecretKey() {
		return secretKey;
	}

}
