package com.nisoph.tools.mailer.engine;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.nisoph.tools.mailer.constants.EmailTemplateConstants;

/**
 * File: EmailSenderTemplate.java
 * 
 * This class is a helper to send html emails based on templates.
 * 
 * if you want to add images (depend of the method) use the following:
 * <code>Map<String, String> images = new HashMap<>();</code>
 * <code>images.put("memmorable_logo", "template/images/logo_memmorable.jpg");</code>
 * 
 * if you want to add attachments as well (depend on the method) use the following:
 * <code>setAttachments(String... attachments);</code>
 * 
 */

public class EmailTemplateSender {
	
	private static Map<String, String> images;
	private static String[] attachments;
	
	private Properties properties;
	
	// Facade to send signup confirmation email
	public static void sendSignUpConfirmationEmail(String toEmail, String name,
			String username, String urlAction) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendSignUpEmailTemplate(toEmail, name, username, urlAction,
				sender.getSingUpTemplatePropesties());
	}

	// Facade to send Password Reset confirmation email
	public static void sendPasswordResetEmail(String toEmail, String name,
			String username, String urlAction) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendChangePwdTemplate(toEmail, name, username, urlAction,
				sender.getPwdResetTemplatePropesties());
	}
	
	// Facade to send Contact Form email
	public static void sendContactFormEmail(String fromEmail, String name,
			String subject, String message) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendContactFormEmailTemplate(fromEmail, name, subject, message, 
				sender.getContactFormEmailTemplatePropesties());
	}
	
	// Facade to send Buyer Payment Confirmation
	public static void sendPaymentConfirmationBuyerEmail(String toEmail, String name,
			String artId, String artName, String paymentId, String amount) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendPaymentConfirmationBuyerEmailTemplate(toEmail, name, artId, artName, paymentId, amount, 
				sender.getPaymentConformationBuyerTemplatePropesties());
	}
	
	// Facade to send Seller Payment Confirmation
	public static void sendPaymentConfirmationSellerEmail(String toEmail, String name,
			String artId, String artName, String paymentId, String amount) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendPaymentConfirmationSellerEmailTemplate(toEmail, name, artId, artName, paymentId, amount, 
				sender.getPaymentConformationSellerTemplatePropesties());
	}

	// Facade to send Seller Payment Confirmation Sold
	public static void sendPaymentConfirmationSellerSoldEmail(String toEmail, String name,
			String artId, String artName, String paymentId, String amount) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendPaymentConfirmationSellerSoldEmailTemplate(toEmail, name, artId, artName, paymentId, amount, 
				sender.getPaymentConformationSellerSoldTemplatePropesties());
	}
	
	// Facade to send Admin Payment Confirmation
	public static void sendPaymentConfirmationAdminEmail(String toEmail,
			String artId, String artName, String paymentId, String amount) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendPaymentConfirmationAdminEmailTemplate(toEmail, artId, artName, paymentId, amount, 
				sender.getPaymentConformationAdminTemplatePropesties());
	}
	
	// Facade to send User Art Messages email
	public static void sendUserArtMessagesEmail(String toEmail, String name,
			String subject, String locationLink, String message) throws AddressException,
			MessagingException, IOException {
		EmailTemplateSender sender = new EmailTemplateSender();
		sendUserArtMessagesEmailTemplate(toEmail, name, subject, locationLink, message,
				sender.getUserArtMessagesTemplatePropesties());
	}
	
	public Properties getSingUpTemplatePropesties() {
		return getProperties(EmailTemplateConstants.USER_SIGNUP_CONFIRMATION_PROPERTY_FILE);
	}
	
	public Properties getPwdResetTemplatePropesties() {
		return getProperties(EmailTemplateConstants.USER_CHANGE_PWD_PROPERTY_FILE);
	}
	
	public Properties getContactFormEmailTemplatePropesties() {
		return getProperties(EmailTemplateConstants.USER_CONTACT_FORM_PROPERTY_FILE);
	}
	
	public Properties getPaymentConformationBuyerTemplatePropesties() {
		return getProperties(EmailTemplateConstants.PAYMENT_CONFIRMATION_BUYER_PROPERTY_FILE);
	}
	
	public Properties getPaymentConformationSellerTemplatePropesties() {
		return getProperties(EmailTemplateConstants.PAYMENT_CONFIRMATION_SELLER_PROPERTY_FILE);
	}

	public Properties getPaymentConformationSellerSoldTemplatePropesties() {
		return getProperties(EmailTemplateConstants.PAYMENT_CONFIRMATION_SELLER_SOLD_PROPERTY_FILE);
	}
	
	public Properties getPaymentConformationAdminTemplatePropesties() {
		return getProperties(EmailTemplateConstants.PAYMENT_CONFIRMATION_ADMIN_PROPERTY_FILE);
	}
	
	public Properties getUserArtMessagesTemplatePropesties() {
		return getProperties(EmailTemplateConstants.USER_ART_MESSAGES_PROPERTY_FILE);
	}
	
	private Properties getProperties(String propertyFile) {
		properties = new Properties();

		try {
        	InputStream in = this.getClass().getResourceAsStream(propertyFile);
        	properties.load(in);
        	in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
		return properties;
	}
	
	private static void sendSignUpEmailTemplate(String toEmail, String name,
			String username, String urlAction, Properties props)
			throws AddressException, MessagingException, IOException {
        EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.USER_SIGNUP_CONFIRMATION_TEMPLATE);
        emailSender.setContextValue("name", name);
        emailSender.setContextValue("username", username);
        emailSender.setContextValue("email", toEmail);
        emailSender.setContextValue("btnUrl", urlAction);
        emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
        emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
        emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendChangePwdTemplate(String toEmail, String name,
			String username, String urlAction, Properties props)
			throws AddressException, MessagingException, IOException {
        EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.USER_CHANGE_PWD_TEMPLATE);
        emailSender.setContextValue("name", name);
        emailSender.setContextValue("username", username);
        emailSender.setContextValue("btnUrl", urlAction);
        emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
        emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
        emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendContactFormEmailTemplate(String fromEmail, String name, String subject, String message, Properties props) throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.USER_CONTACT_FORM_EMAIL_TEMPLATE);
        emailSender.setContextValue("name", name);
        emailSender.setContextValue("email", fromEmail);
        emailSender.setContextValue("message", message);
        emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY) + " - " + subject);
        emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
        emailSender.setFromEmail(name + "<" + fromEmail + ">");
        emailSender.setToEmail("Memmorable Contact" + "<" + "no-reply@memmorable.com" + ">");
        emailSender.sendMail(fromEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY)  + " - " + subject, getImages(), getAttachments());
	}
	
	private static void sendPaymentConfirmationBuyerEmailTemplate(String toEmail, String name, String artId, String artName, String paymentId, String amount, Properties props) throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.PAYMENT_CONFIRMATION_BUYER_TEMPLATE);
        emailSender.setContextValue("name", name);
        emailSender.setContextValue("email", toEmail);
        emailSender.setContextValue("artId", artId);
        emailSender.setContextValue("artName", artName);
        emailSender.setContextValue("paymentId", paymentId);
        emailSender.setContextValue("amount", amount);
        emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
        emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
        emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendPaymentConfirmationSellerEmailTemplate(String toEmail, String name, String artId, String artName, String paymentId,  String amount, Properties props) throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.PAYMENT_CONFIRMATION_SELLER_TEMPLATE);
		emailSender.setContextValue("name", name);
		emailSender.setContextValue("email", toEmail);
		emailSender.setContextValue("artId", artId);
		emailSender.setContextValue("artName", artName);
		emailSender.setContextValue("paymentId", paymentId);
		emailSender.setContextValue("amount", amount);
		emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
		emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
		emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendPaymentConfirmationSellerSoldEmailTemplate(String toEmail, String name, String artId, String artName, String paymentId,  String amount, Properties props) throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.PAYMENT_CONFIRMATION_SELLER_SOLD_TEMPLATE);
		emailSender.setContextValue("name", name);
		emailSender.setContextValue("email", toEmail);
		emailSender.setContextValue("artId", artId);
		emailSender.setContextValue("artName", artName);
		emailSender.setContextValue("paymentId", paymentId);
		emailSender.setContextValue("amount", amount);
		emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
		emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
		emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendPaymentConfirmationAdminEmailTemplate(String toEmail, String artId, String artName, String paymentId, String amount, Properties props) throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.PAYMENT_CONFIRMATION_ADMIN_TEMPLATE);
		emailSender.setContextValue("email", toEmail);
		emailSender.setContextValue("artId", artId);
		emailSender.setContextValue("artName", artName);
		emailSender.setContextValue("paymentId", paymentId);
		emailSender.setContextValue("amount", amount);
		emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY));
		emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
		emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY), getImages(), getAttachments());
	}
	
	private static void sendUserArtMessagesEmailTemplate(String toEmail, String name, String subject, String locationLink, String message, Properties props)  throws AddressException, MessagingException, IOException {
		EmailSenderEngine emailSender = new EmailSenderEngine(EmailTemplateConstants.USER_ART_MESSAGES_TEMPLATE);
        emailSender.setContextValue("name", name);
        emailSender.setContextValue("message", message);
        emailSender.setContextValue("location", locationLink);
        emailSender.setContextValue("title", props.getProperty(EmailTemplateConstants.TITLE_PROPERTY) + " - " + subject);
        emailSender.setContextValue("imgSrc", props.getProperty(EmailTemplateConstants.LOGO_PATH_PROPERTY));
        emailSender.sendMail(toEmail, props.getProperty(EmailTemplateConstants.SUBJECT_PROPERTY)  + " - " + subject, getImages(), getAttachments());
	}

	private static Map<String, String> getImages() {
		return images;
	}
	
	public static void setImages(Map<String, String> images) {
		EmailTemplateSender.images = images;
	}
	
	private static String[] getAttachments() {
		return attachments;
	}

	public static void setAttachments(String... attachments) {
		EmailTemplateSender.attachments = attachments;
	}
	
}
