package com.nisoph.tools.mailer.test;

import java.io.IOException;

import javax.mail.MessagingException;

import com.nisoph.tools.mailer.engine.EmailTemplateSender;

public class Main {

    public static void main(String[] args) {
    	
    	try {
			EmailTemplateSender.sendSignUpConfirmationEmail("erikmacias.ruezga@gmail.com", "Erik Macias", "emacias", "#");
		} catch (MessagingException | IOException e) {
			e.printStackTrace();
		}
    	
    	try {
			EmailTemplateSender.sendPasswordResetEmail("erikmacias.ruezga@gmail.com", "Erik Macias", "emacias", "#");
		} catch (MessagingException | IOException e) {
			e.printStackTrace();
		}
    	
    	try {
			EmailTemplateSender.sendContactFormEmail("erikmacias.ruezga@gmail.com", "Juan Perez", "Subject Title", "User Message");
		} catch (MessagingException | IOException e) {
			e.printStackTrace();
		}
    	
    	try {
    		EmailTemplateSender.sendPaymentConfirmationBuyerEmail("erikmacias.ruezga@gmail.com", "Juan Perez", "8", "Sample 05", "ID PAY-0F650474KC510400TK536T5I", "35.70 USD");
    	} catch (MessagingException | IOException e) {
    		e.printStackTrace();
    	}
    	
    	try {
    		EmailTemplateSender.sendPaymentConfirmationSellerEmail("erikmacias.ruezga@gmail.com", "Juan Perez", "8", "Sample 05", "ID PAY-0F650474KC510400TK536T5I", "35.70 USD");
    	} catch (MessagingException | IOException e) {
    		e.printStackTrace();
    	}

    	try {
    		EmailTemplateSender.sendPaymentConfirmationSellerSoldEmail("erikmacias.ruezga@gmail.com", "Juan Perez", "8", "Sample 05", "ID PAY-0F650474KC510400TK536T5I", "35.70 USD");
    	} catch (MessagingException | IOException e) {
    		e.printStackTrace();
    	}
    	
    	try {
    		EmailTemplateSender.sendPaymentConfirmationAdminEmail("erikmacias.ruezga@gmail.com", "8", "Sample 05", "ID PAY-0F650474KC510400TK536T5I", "35.70 USD");
    	} catch (MessagingException | IOException e) {
    		e.printStackTrace();
    	}
    	
    	try {
    		EmailTemplateSender.sendUserArtMessagesEmail("erikmacias.ruezga@gmail.com", "Juan Perez", "DM Contact", "http://memmorable.nisoph.com/#/profile/emacias", "This is a sample message");
    	} catch (MessagingException | IOException e) {
    		e.printStackTrace();
    	}
    }
}
