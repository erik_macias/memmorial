package com.nisoph.tools.mailer.constants;

public final class EmailTemplateConstants {
	
	//Signup Template
	public static final String USER_SIGNUP_CONFIRMATION_TEMPLATE 				= 	"/com/nisoph/tools/mailer/templates/userSingUp.html";
	public static final String USER_SIGNUP_CONFIRMATION_PROPERTY_FILE 			= 	"/com/nisoph/tools/mailer/properties/signupConfirm.properties";
	
	//Password Reset Template
	public static final String USER_CHANGE_PWD_TEMPLATE 						= 	"/com/nisoph/tools/mailer/templates/userChangePwd.html";
	public static final String USER_CHANGE_PWD_PROPERTY_FILE 					= 	"/com/nisoph/tools/mailer/properties/passwordReset.properties";
	
	//Contact Form Template
	public static final String USER_CONTACT_FORM_EMAIL_TEMPLATE		 			= 	"/com/nisoph/tools/mailer/templates/userContactFormEmail.html";
	public static final String USER_CONTACT_FORM_PROPERTY_FILE 					= 	"/com/nisoph/tools/mailer/properties/contactFormEmail.properties";
	
	//Buyer Payment Confirmation Template
	public static final String PAYMENT_CONFIRMATION_BUYER_TEMPLATE 				= 	"/com/nisoph/tools/mailer/templates/paymentConfirmBuyer.html";
	public static final String PAYMENT_CONFIRMATION_BUYER_PROPERTY_FILE			= 	"/com/nisoph/tools/mailer/properties/paymentConfirmBuyer.properties";
	
	//Seller Payment Confirmation Template
	public static final String PAYMENT_CONFIRMATION_SELLER_TEMPLATE 			= 	"/com/nisoph/tools/mailer/templates/paymentConfirmSeller.html";
	public static final String PAYMENT_CONFIRMATION_SELLER_PROPERTY_FILE 		= 	"/com/nisoph/tools/mailer/properties/paymentConfirmSeller.properties";
	
	//Seller Payment Confirmation Sold Template
	public static final String PAYMENT_CONFIRMATION_SELLER_SOLD_TEMPLATE 		= 	"/com/nisoph/tools/mailer/templates/paymentConfirmSellerSold.html";
	public static final String PAYMENT_CONFIRMATION_SELLER_SOLD_PROPERTY_FILE 	= 	"/com/nisoph/tools/mailer/properties/paymentConfirmSellerSold.properties";
	
	//Admin Payment Confirmation Template
	public static final String PAYMENT_CONFIRMATION_ADMIN_TEMPLATE 				= 	"/com/nisoph/tools/mailer/templates/paymentConfirmAdmin.html";
	public static final String PAYMENT_CONFIRMATION_ADMIN_PROPERTY_FILE 		= 	"/com/nisoph/tools/mailer/properties/paymentConfirmAdmin.properties";
	
	//User Art Messages Template
	public static final String USER_ART_MESSAGES_TEMPLATE		 				= 	"/com/nisoph/tools/mailer/templates/userArtMessages.html";
	public static final String USER_ART_MESSAGES_PROPERTY_FILE 					= 	"/com/nisoph/tools/mailer/properties/userArtMessages.properties";
	
	//Common Properties IDs
	public static final String SUBJECT_PROPERTY									= 	"emailSubject";
	public static final String TITLE_PROPERTY									= 	"emailTitle";
	public static final String LOGO_PATH_PROPERTY								= 	"emailLogoPath";

}
