-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema memmorable
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `memmorable` ;

-- -----------------------------------------------------
-- Schema memmorable
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `memmorable` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `memmorable` ;

-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_Role` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_Role` (
  `role_id` INT NOT NULL DEFAULT NULL AUTO_INCREMENT COMMENT '',
  `role_name` VARCHAR(100) NOT NULL COMMENT '',
  `role_token` VARCHAR(100) NOT NULL COMMENT '',
  `role_text_name` VARCHAR(100) NOT NULL COMMENT '',
  `price` DOUBLE NOT NULL COMMENT '',
  `cycle_time` INT NOT NULL COMMENT '',
  `fee_own_agreement` DOUBLE NOT NULL COMMENT '',
  `fee_shipping` DOUBLE NOT NULL COMMENT '',
  `publications_allowed` INT NOT NULL COMMENT '',
  `expiration_days` INT NOT NULL COMMENT '',
  `paypal_pay_eligible` TINYINT(1) NOT NULL COMMENT '',
  PRIMARY KEY (`role_id`)  COMMENT '',
  UNIQUE INDEX `role_token_UNIQUE` (`role_token` ASC)  COMMENT '',
  UNIQUE INDEX `role_name_UNIQUE` (`role_name` ASC)  COMMENT '',
  UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_User_Status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_User_Status` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_User_Status` (
  `user_status_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_status_type` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`user_status_id`)  COMMENT '',
  UNIQUE INDEX `user_status_type_UNIQUE` (`user_status_type` ASC)  COMMENT '',
  UNIQUE INDEX `user_status_id_UNIQUE` (`user_status_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_User` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_User` (
  `user_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `role_id` INT NOT NULL DEFAULT 1 COMMENT '',
  `user_status_id` INT NOT NULL DEFAULT 1 COMMENT '',
  `user_name` VARCHAR(100) NOT NULL COMMENT '',
  `user_Password` VARCHAR(100) NOT NULL COMMENT '',
  `missed_logins` INT NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`user_id`)  COMMENT '',
  INDEX `fk_User_Roles_idx` (`role_id` ASC)  COMMENT '',
  INDEX `fk_Table_User_Catalog_User_Status1_idx` (`user_status_id` ASC)  COMMENT '',
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC)  COMMENT '',
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_User_Roles`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_User_Catalog_User_Status1`
    FOREIGN KEY (`user_status_id`)
    REFERENCES `memmorable`.`Catalog_User_Status` (`user_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_Personal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_Personal` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_Personal` (
  `personal_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `personal_name` VARCHAR(100) NOT NULL COMMENT '',
  `personal_m_initial` VARCHAR(10) NULL COMMENT '',
  `personal_l_name` VARCHAR(100) NOT NULL COMMENT '',
  `personal_email` VARCHAR(100) NOT NULL COMMENT '',
  `personal_gender` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`personal_id`)  COMMENT '',
  INDEX `fk_personal_User1_idx` (`user_id` ASC)  COMMENT '',
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC)  COMMENT '',
  UNIQUE INDEX `personal_id_UNIQUE` (`personal_id` ASC)  COMMENT '',
  CONSTRAINT `I`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_Country` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_Country` (
  `country_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `country_name` VARCHAR(100) NOT NULL COMMENT '',
  `country_code` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`country_id`)  COMMENT '',
  UNIQUE INDEX `country_name_UNIQUE` (`country_name` ASC)  COMMENT '',
  UNIQUE INDEX `country_id_UNIQUE` (`country_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_Address_Type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_Address_Type` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_Address_Type` (
  `address_type_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `Address_type_name` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`address_type_id`)  COMMENT '',
  UNIQUE INDEX `Address_type_name_UNIQUE` (`Address_type_name` ASC)  COMMENT '',
  UNIQUE INDEX `address_type_id_UNIQUE` (`address_type_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_Address` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_Address` (
  `address_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `address_type_id` INT NOT NULL COMMENT '',
  `country_id` INT NOT NULL COMMENT '',
  `address_name` VARCHAR(200) NOT NULL COMMENT '',
  `address_m_initial` VARCHAR(10) NULL COMMENT '',
  `address_l_name` VARCHAR(200) NOT NULL COMMENT '',
  `address_phone` VARCHAR(200) NOT NULL COMMENT '',
  `address_mobile` VARCHAR(45) NULL COMMENT '',
  `address_line1` VARCHAR(200) NOT NULL COMMENT '',
  `address_line2` VARCHAR(200) NULL COMMENT '',
  `address_city` VARCHAR(200) NOT NULL COMMENT '',
  `address_state` VARCHAR(200) NOT NULL COMMENT '',
  `address_zip` VARCHAR(200) NOT NULL COMMENT '',
  PRIMARY KEY (`address_id`)  COMMENT '',
  INDEX `fk_Address_User1_idx` (`user_id` ASC)  COMMENT '',
  INDEX `fk_Table_Address_Catalog_Country1_idx` (`country_id` ASC)  COMMENT '',
  INDEX `fk_Table_Address_Catalog_Adrres_Type1_idx` (`address_type_id` ASC)  COMMENT '',
  UNIQUE INDEX `address_id_UNIQUE` (`address_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Address_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_Address_Catalog_Country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `memmorable`.`Catalog_Country` (`country_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_Address_Catalog_Adrres_Type1`
    FOREIGN KEY (`address_type_id`)
    REFERENCES `memmorable`.`Catalog_Address_Type` (`address_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_tag` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_tag` (
  `art_tag_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_tag_name` VARCHAR(20) NOT NULL COMMENT '',
  PRIMARY KEY (`art_tag_id`)  COMMENT '',
  UNIQUE INDEX `art_tag_id_UNIQUE` (`art_tag_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_Profile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_Profile` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_Profile` (
  `profile_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `Catalog_art_tag_art_tag_id` INT NULL COMMENT '',
  `profile_resume` TEXT NULL COMMENT '',
  `profile_art_statement` TINYTEXT NULL COMMENT '',
  `profile_picture` VARCHAR(200) NULL COMMENT '',
  PRIMARY KEY (`profile_id`)  COMMENT '',
  INDEX `fk_Table_Profile_Table_User1_idx` (`user_id` ASC)  COMMENT '',
  UNIQUE INDEX `profile_id_UNIQUE` (`profile_id` ASC)  COMMENT '',
  INDEX `fk_Table_Profile_Catalog_art_tag1_idx` (`Catalog_art_tag_art_tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_Profile_Table_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_Profile_Catalog_art_tag1`
    FOREIGN KEY (`Catalog_art_tag_art_tag_id`)
    REFERENCES `memmorable`.`Catalog_art_tag` (`art_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_type` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_type` (
  `art_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_name` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`art_type_id`)  COMMENT '',
  UNIQUE INDEX `Name_UNIQUE` (`art_type_name` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_subtype`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_subtype` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_subtype` (
  `art_subtype_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_subtype_name` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`art_subtype_id`)  COMMENT '',
  INDEX `fk_Table_art_subtype_Table_art_type1_idx` (`art_type_id` ASC)  COMMENT '',
  UNIQUE INDEX `art_subtype_id_UNIQUE` (`art_subtype_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_art_subtype_Table_art_type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_status` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_status` (
  `art_status_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_status_type` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`art_status_id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_currency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_currency` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_currency` (
  `currency_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `currency_type` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`currency_id`)  COMMENT '',
  UNIQUE INDEX `currency_id_UNIQUE` (`currency_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_service`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_service` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_service` (
  `art_service_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_service_name` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`art_service_id`)  COMMENT '',
  UNIQUE INDEX `art_service_id_UNIQUE` (`art_service_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_service_Catalog_art_type1_idx` (`art_type_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_service_Catalog_art_type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_genre` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_genre` (
  `art_genre_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_genre_name` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`art_genre_id`)  COMMENT '',
  UNIQUE INDEX `art_genre_id_UNIQUE` (`art_genre_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_genre_Catalog_art_type1_idx` (`art_type_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_genre_Catalog_art_type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_format` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_format` (
  `art_format_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_format_name` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`art_format_id`)  COMMENT '',
  UNIQUE INDEX `art_format_id_UNIQUE` (`art_format_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_format_Catalog_art_type1_idx` (`art_type_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_format_Catalog_art_type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_art`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_art` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_art` (
  `art_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_tag_id` INT NOT NULL COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `art_subtype_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_status_id` INT NOT NULL COMMENT '',
  `currency_id` INT NOT NULL COMMENT '',
  `art_service_id` INT UNSIGNED NOT NULL COMMENT '',
  `art_genre_id` INT UNSIGNED NULL COMMENT '',
  `art_format_id` INT UNSIGNED NULL COMMENT '',
  `art_name` VARCHAR(45) NULL COMMENT '',
  `art_description` TINYTEXT NULL COMMENT '',
  `create_date` DATETIME NULL COMMENT '',
  `last_updated` DATETIME NULL COMMENT '',
  `price` DOUBLE NULL COMMENT '',
  `DELETED` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`art_id`)  COMMENT '',
  UNIQUE INDEX `art_id_UNIQUE` (`art_id` ASC)  COMMENT '',
  INDEX `fk_Table_Art_Table_art_subtype1_idx` (`art_subtype_id` ASC)  COMMENT '',
  INDEX `fk_Table_Art_Table_User1_idx` (`user_id` ASC)  COMMENT '',
  INDEX `fk_Table_Art_Catalog_art_tag1_idx` (`art_tag_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Catalog_art_status1_idx` (`art_status_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Catalog_currency1_idx` (`currency_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Catalog_art_service1_idx` (`art_service_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Catalog_art_genre1_idx` (`art_genre_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Catalog_art_format1_idx` (`art_format_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_Art_Table_art_subtype1`
    FOREIGN KEY (`art_subtype_id`)
    REFERENCES `memmorable`.`Catalog_art_subtype` (`art_subtype_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_Art_Table_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_Art_Catalog_art_tag1`
    FOREIGN KEY (`art_tag_id`)
    REFERENCES `memmorable`.`Catalog_art_tag` (`art_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Catalog_art_status1`
    FOREIGN KEY (`art_status_id`)
    REFERENCES `memmorable`.`Catalog_art_status` (`art_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Catalog_currency1`
    FOREIGN KEY (`currency_id`)
    REFERENCES `memmorable`.`Catalog_currency` (`currency_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Catalog_art_service1`
    FOREIGN KEY (`art_service_id`)
    REFERENCES `memmorable`.`Catalog_art_service` (`art_service_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Catalog_art_genre1`
    FOREIGN KEY (`art_genre_id`)
    REFERENCES `memmorable`.`Catalog_art_genre` (`art_genre_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Catalog_art_format1`
    FOREIGN KEY (`art_format_id`)
    REFERENCES `memmorable`.`Catalog_art_format` (`art_format_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_Art_has_Catalog_art_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_Art_has_Catalog_art_tag` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_Art_has_Catalog_art_tag` (
  `art_tag_id` INT NOT NULL COMMENT '',
  `name` VARCHAR(45) NULL COMMENT '',
  PRIMARY KEY (`art_tag_id`)  COMMENT '',
  INDEX `fk_Table_Art_has_Catalog_art_tag_Catalog_art_tag1_idx` (`art_tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_Art_has_Catalog_art_tag_Catalog_art_tag1`
    FOREIGN KEY (`art_tag_id`)
    REFERENCES `memmorable`.`Catalog_art_tag` (`art_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_tag_has_Table_Art`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_tag_has_Table_Art` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_tag_has_Table_Art` (
  `art_tag_id` INT NOT NULL COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`art_tag_id`, `art_id`)  COMMENT '',
  INDEX `fk_Catalog_art_tag_has_Table_Art_Table_Art1_idx` (`art_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_tag_has_Table_Art_Catalog_art_tag1_idx` (`art_tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_tag_has_Table_Art_Catalog_art_tag1`
    FOREIGN KEY (`art_tag_id`)
    REFERENCES `memmorable`.`Catalog_art_tag` (`art_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Catalog_art_tag_has_Table_Art_Table_Art1`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_Art_Art_Tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_Art_Art_Tag` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_Art_Art_Tag` (
  `art_tag_id` INT NOT NULL COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`art_tag_id`, `art_id`)  COMMENT '',
  INDEX `fk_Catalog_art_tag_has_Table_Art1_Table_Art1_idx` (`art_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_tag_has_Table_Art1_Catalog_art_tag1_idx` (`art_tag_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_tag_has_Table_Art1_Catalog_art_tag1`
    FOREIGN KEY (`art_tag_id`)
    REFERENCES `memmorable`.`Catalog_art_tag` (`art_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Catalog_art_tag_has_Table_Art1_Table_Art1`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_media_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_media_status` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_media_status` (
  `media_status_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `media_status_type` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`media_status_id`)  COMMENT '',
  UNIQUE INDEX `media_status_id_UNIQUE` (`media_status_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_media`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_media` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_media` (
  `media_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  `media_status_id` INT NOT NULL COMMENT '',
  `media_type` VARCHAR(45) NOT NULL COMMENT '',
  `media_filename` VARCHAR(200) NOT NULL COMMENT '',
  `media_primary` TINYINT(1) NOT NULL COMMENT '',
  `DELETED` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`media_id`)  COMMENT '',
  INDEX `fk_Media_Table_Art1_idx` (`art_id` ASC)  COMMENT '',
  UNIQUE INDEX `media_id_UNIQUE` (`media_id` ASC)  COMMENT '',
  INDEX `fk_Table_media_Catalog_media_status1_idx` (`media_status_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Media_Table_Art1`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_media_Catalog_media_status1`
    FOREIGN KEY (`media_status_id`)
    REFERENCES `memmorable`.`Catalog_media_status` (`media_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_art_comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_art_comments` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_art_comments` (
  `art_comment_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `create_date` DATETIME NOT NULL COMMENT '',
  `comment` VARCHAR(500) NOT NULL COMMENT '',
  `status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`art_comment_id`)  COMMENT '',
  UNIQUE INDEX `art_comment_id_UNIQUE` (`art_comment_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Comments_Table_art1_idx` (`art_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Comments_Table_User1_idx` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_art_Comments_Table_art1`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_Comments_Table_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_rating` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_rating` (
  `rating_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `rate` INT NOT NULL COMMENT '',
  PRIMARY KEY (`rating_id`)  COMMENT '',
  UNIQUE INDEX `rating_id_UNIQUE` (`rating_id` ASC)  COMMENT '',
  INDEX `fk_Table_rating_Table_art1_idx` (`art_id` ASC)  COMMENT '',
  INDEX `fk_Table_rating_Table_User1_idx` (`user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_rating_Table_art1`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_rating_Table_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_inbox_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_inbox_status` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_inbox_status` (
  `inbox_status_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `inbox_status_name` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`inbox_status_id`)  COMMENT '',
  UNIQUE INDEX `inbox_status_id_UNIQUE` (`inbox_status_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_inbox`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_inbox` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_inbox` (
  `inbox_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `from_user_id` INT NOT NULL COMMENT '',
  `to_user_id` INT NOT NULL COMMENT '',
  `inbox_status_id` INT NOT NULL COMMENT '',
  `subject` VARCHAR(100) NOT NULL COMMENT '',
  `message` TEXT NULL COMMENT '',
  `created_date` DATETIME NOT NULL COMMENT '',
  `isRead` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`inbox_id`)  COMMENT '',
  UNIQUE INDEX `inbox_comment_id_UNIQUE` (`inbox_id` ASC)  COMMENT '',
  INDEX `fk_Table_inbox_Table_User1_idx` (`to_user_id` ASC)  COMMENT '',
  INDEX `fk_Table_inbox_Table_User_idx` (`from_user_id` ASC)  COMMENT '',
  INDEX `fk_Table_inbox_Table_inbox_status_idx` (`inbox_status_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_inbox_Table_User`
    FOREIGN KEY (`from_user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_inbox_Table_User10`
    FOREIGN KEY (`to_user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_inbox_Table_inbox_status`
    FOREIGN KEY (`inbox_status_id`)
    REFERENCES `memmorable`.`Catalog_inbox_status` (`inbox_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_user_comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_user_comments` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_user_comments` (
  `user_comment_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `to_user_id` INT NOT NULL COMMENT '',
  `from_user_id` INT NOT NULL COMMENT '',
  `create_date` DATETIME NOT NULL COMMENT '',
  `comment` VARCHAR(500) NOT NULL COMMENT '',
  `status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '',
  PRIMARY KEY (`user_comment_id`)  COMMENT '',
  UNIQUE INDEX `art_comment_id_UNIQUE` (`user_comment_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Comments_Table_User1_idx` (`from_user_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_Comments_Table_user10_idx` (`to_user_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_art_comments_Table_User10`
    FOREIGN KEY (`to_user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_art_comments_Table_User11`
    FOREIGN KEY (`from_user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_app_settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_app_settings` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_app_settings` (
  `app_setting_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `app_setting_name` VARCHAR(100) NOT NULL COMMENT '',
  `app_setting_value` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`app_setting_id`)  COMMENT '',
  UNIQUE INDEX `app_settings_id_UNIQUE` (`app_setting_id` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_paypal_plans`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_paypal_plans` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_paypal_plans` (
  `paypal_plan_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `plan_id` VARCHAR(100) NOT NULL COMMENT '',
  `role_id` INT NOT NULL COMMENT '',
  `plan_status` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`paypal_plan_id`)  COMMENT '',
  INDEX `fk_paypal_plans_Role1_idx` (`role_id` ASC)  COMMENT '',
  UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC)  COMMENT '',
  UNIQUE INDEX `paypal_plan_id_UNIQUE` (`paypal_plan_id` ASC)  COMMENT '',
  UNIQUE INDEX `plan_id_UNIQUE` (`plan_id` ASC)  COMMENT '',
  CONSTRAINT `fk_paypal_plans_Role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_User_Art_Payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_User_Art_Payments` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_User_Art_Payments` (
  `user_art_payment_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `art_id` INT NOT NULL COMMENT '',
  `payment_id` VARCHAR(100) NOT NULL COMMENT '',
  `payment_type` VARCHAR(45) NOT NULL COMMENT '',
  `created_date` DATETIME NOT NULL COMMENT '',
  PRIMARY KEY (`user_art_payment_id`)  COMMENT '',
  UNIQUE INDEX `user_payment_id_UNIQUE` (`user_art_payment_id` ASC)  COMMENT '',
  INDEX `fk_User_Payments_Art_idx` (`art_id` ASC)  COMMENT '',
  CONSTRAINT `fk_User_Art_Payments_User`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Art_Payments_Art`
    FOREIGN KEY (`art_id`)
    REFERENCES `memmorable`.`Table_art` (`art_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_media_rules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_media_rules` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_media_rules` (
  `media_rules_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `role_id` INT NOT NULL COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `media_rules_name` VARCHAR(45) NOT NULL COMMENT '',
  `media_pdf_qty` INT NOT NULL COMMENT '',
  `media_image_qty` INT NOT NULL COMMENT '',
  `media_audio_qty` INT NOT NULL COMMENT '',
  `media_video_qty` INT NOT NULL COMMENT '',
  PRIMARY KEY (`media_rules_id`)  COMMENT '',
  UNIQUE INDEX `media_rule_id_UNIQUE` (`media_rules_id` ASC)  COMMENT '',
  UNIQUE INDEX `media_rule__UNIQUE` (`media_rules_name` ASC)  COMMENT '',
  INDEX `fk_Table_media_rules_Catalog_Role1_idx` (`role_id` ASC)  COMMENT '',
  INDEX `fk_Table_media_rules_Catalog_art_type1_idx` (`art_type_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Table_media_rules_Catalog_Role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Table_media_rules_Catalog_art_type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_art_rules`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_art_rules` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_art_rules` (
  `art_rules_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `role_id` INT NOT NULL COMMENT '',
  `art_rules_name` VARCHAR(45) NOT NULL COMMENT '',
  `art_qty` INT NOT NULL COMMENT '',
  PRIMARY KEY (`art_rules_id`)  COMMENT '',
  UNIQUE INDEX `art_rules_id_UNIQUE` (`art_rules_id` ASC)  COMMENT '',
  INDEX `fk_Table_art_rules_Catalog_Role1_idx` (`role_id` ASC)  COMMENT '',
  UNIQUE INDEX `art_rules_name_UNIQUE` (`art_rules_name` ASC)  COMMENT '',
  UNIQUE INDEX `Catalog_Role_role_id_UNIQUE` (`role_id` ASC)  COMMENT '',
  CONSTRAINT `fk_art_rules_roles`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Table_User_Payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Table_User_Payments` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Table_User_Payments` (
  `user_payment_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `role_id` INT NOT NULL COMMENT '',
  `agreement_id` VARCHAR(100) NOT NULL COMMENT '',
  `created_date` DATETIME NOT NULL COMMENT '',
  PRIMARY KEY (`user_payment_id`)  COMMENT '',
  UNIQUE INDEX `user_payment_id_UNIQUE` (`user_payment_id` ASC)  COMMENT '',
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC)  COMMENT '',
  UNIQUE INDEX `role_id_UNIQUE` (`role_id` ASC)  COMMENT '',
  CONSTRAINT `fk_User_Payments_User0`
    FOREIGN KEY (`user_id`)
    REFERENCES `memmorable`.`Table_User` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_Payments_Roles0`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `memmorable`.`Catalog_art_type_fee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `memmorable`.`Catalog_art_type_fee` ;

CREATE TABLE IF NOT EXISTS `memmorable`.`Catalog_art_type_fee` (
  `art_type_fee_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `art_type_id` INT UNSIGNED NOT NULL COMMENT '',
  `role_id` INT NOT NULL COMMENT '',
  `fee_amount` DOUBLE NOT NULL COMMENT '',
  PRIMARY KEY (`art_type_fee_id`)  COMMENT '',
  UNIQUE INDEX `art_type_fee_id_UNIQUE` (`art_type_fee_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_type_Catalog_Art_Role1_idx` (`role_id` ASC)  COMMENT '',
  INDEX `fk_Catalog_art_type_Catalog_Art_Type1_idx` (`art_type_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Catalog_art_type_Catalog_Art_Type1`
    FOREIGN KEY (`art_type_id`)
    REFERENCES `memmorable`.`Catalog_art_type` (`art_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Catalog_art_type_Catalog_Art_Role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `memmorable`.`Catalog_Role` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
