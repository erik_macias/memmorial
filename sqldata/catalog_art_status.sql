INSERT INTO memmorable.Catalog_art_status value(null, 'ACTIVE');
INSERT INTO memmorable.Catalog_art_status value(null, 'REMOVED');
INSERT INTO memmorable.Catalog_art_status value(null, 'BLOCKED');
INSERT INTO memmorable.Catalog_art_status value(null, 'SOLD');
INSERT INTO memmorable.Catalog_art_status value(null, 'BOUGHT');
INSERT INTO memmorable.Catalog_art_status value(null, 'PENDING VALIDATION');
INSERT INTO memmorable.Catalog_art_status value(null, 'IN_PROCESS');

SELECT * FROM memmorable.Catalog_art_status;
commit;