insert into memmorable.Catalog_Role value (null, 'FREE', 'FREE_FOR_ALL', 'Free Plan', 0, 0, 15, 0, 5, 60, false);
insert into memmorable.Catalog_Role value (null, 'MONTHLY', 'MONTHLY_MADNESS', 'Monthly Plan', 15, 1, 15, 0, -1, 30, true);
insert into memmorable.Catalog_Role value (null, 'BIMONTHLY', 'BICYCLE', 'Two Months Plan', 28, 2, 10, 0, -1, 60, true);
insert into memmorable.Catalog_Role value (null, 'QUARTER', 'QUATERLY', 'Three Months Plan', 39, 3, 10, 0, -1, 90, true);
insert into memmorable.Catalog_Role value (null, 'FOURTHMONTHLY', 'ONE_FOURTH', 'Four Months Plan', 48, 4, 10, 0, -1, 120, true);
insert into memmorable.Catalog_Role value (null, 'HALF_YEAR', 'HALF', 'Six Months Plan', 66, 6, 10, 0, -1, 180, true);
insert into memmorable.Catalog_Role value (null, 'YEAR', 'ONE', 'Annual Plan', 120, 12, 10, 0, -1, 365, true);
insert into memmorable.Catalog_Role value (null, 'CERTIFIED', 'CERTIFIED', 'Certified Plan', 459, 12, 10, 0, -1, 365, true);
insert into memmorable.Catalog_Role value (null, 'ADMIN', 'ADMIN', 'Admin Role', 0, 0, 0, -1, 0, 0, true);
select * from memmorable.Catalog_role;

commit;