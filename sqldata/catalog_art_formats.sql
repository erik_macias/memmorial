insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Miniature');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Small');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Medium');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Great Format');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Easel');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Painting') ,'Mural');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Miniature');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Small');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Medium');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Great Format');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Easel');
insert into memmorable.Catalog_art_format value(null, (select art_type_id FROM memmorable.Catalog_art_type where art_type_name='Photography') ,'Mural');


commit;

