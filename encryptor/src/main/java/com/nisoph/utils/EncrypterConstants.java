package com.nisoph.utils;

public interface EncrypterConstants {

	public static final String PBE_WITH_MD5_AND_DES = "PBEWithMD5AndDES";
	public static final String PBE_WITH_MD5_AND_TRIPLE_DES = "PBEWithMD5AndTripleDES";
	public static final String PBE_WITH_SHA1_AND_DESEDE = "PBEWithSHA1AndDESede";
	public static final String PBE_WITH_SHA1_AND_RC2_40 = "PBEWithSHA1AndRC2_40";
	
	public final static String PASSWORD_ENCRYPTOR = "MemmorablePwd";
}
