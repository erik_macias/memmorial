package com.nisoph.utils;

import org.jasypt.commons.CommonUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EncrypterUtils {

	private final static StandardPBEStringEncryptor pbeEncryptor = new StandardPBEStringEncryptor();

	static {
		pbeEncryptor.setAlgorithm(EncrypterConstants.PBE_WITH_MD5_AND_DES);
		pbeEncryptor.setPassword(EncrypterConstants.PASSWORD_ENCRYPTOR);
		pbeEncryptor.setStringOutputType(CommonUtils.STRING_OUTPUT_TYPE_HEXADECIMAL);
	}

	public static String encryptValue(String text) {
		return pbeEncryptor.encrypt(text);
	}

	public static String decryptValue(String text) {
		return pbeEncryptor.decrypt(text);
	}

}
