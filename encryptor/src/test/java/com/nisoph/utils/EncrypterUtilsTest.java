package com.nisoph.utils;

import java.util.Scanner;

public class EncrypterUtilsTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter string to encrypt: ");
		String encryptedString = EncrypterUtils.encryptValue(scanner.nextLine());

		scanner.close();
		System.out.println("Encrypted string: " + encryptedString);

		String dencryptedString = EncrypterUtils.decryptValue(encryptedString);
		System.out.println("Dencrypted string using " + encryptedString + ": " + dencryptedString);
	}
}
